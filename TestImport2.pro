# Created by and for Qt Creator. This file was created for editing the project sources only.
# You may attempt to use it for building too, by modifying this file here.

#TARGET = TestImport2
QT += core network gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
TEMPLATE = app

QMAKE_CXXFLAGS += -I /usr/local/include -lavdevice -lavformat -lavfilter -lavcodec -lswscale -lavutil -lswresample
QMAKE_CXXFLAGS += -I /usr/include/libxml2
QMAKE_CXXFLAGS += $(shell pkg-config --cflags gtk+-2.0 gmodule-2.0 sdl)
QMAKE_CXXFLAGS += -D__STDC_CONSTANT_MACROS -c -g -I ./

LIBS += -L /usr/local/lib -lavdevice  -lavformat -lavfilter -lpostproc \
    -lavcodec -ldl -lasound -lx264 -lfdk-aac -lz -lrt -lswscale -lswresample \
    -lavutil -lm  -lpthread -lcurl \
    -llog4c -lconfig -lX11 -lvpx -lv4l2 -lxml2\
    -export-dynamic  $(shell pkg-config --libs gtk+-2.0 gmodule-2.0 sdl)

HEADERS = \
    headers/controller/av_processing.h \
    headers/controller/creating_lecture.h \
    headers/controller/recording_streaming.h \
    headers/controller/uploading.h \
    headers/controller/utils.h \
    headers/model/frame_queue.h \
    headers/model/read_config.h \
    headers/model/xml.h \
    headers/view/course_management_view.h \
    headers/view/input_view.h \
    headers/view/lecture_management_view.h \
    headers/view/main_view.h \
    headers/view/output_view.h \
    headers/view/processing_view.h \
    headers/view/recording_streaming_view.h \
    headers/view/uploading_view.h \
    headers/view/utils_view.h \
    headers/base.h \
    headers/youtube/authentication.h \
    headers/youtube/checkstatusjob.h \
    headers/youtube/youtubeapi.h \
    headers/view/youtube_view.h \
    headers/youtube/broadcastbuilder.h \
    headers/youtube/youtubehandler.h \
    headers/dto/youtubebroadcastdto.h \
    headers/dto/youtubecredentialdto.h \
    headers/dto/youtubestreamdto.h \
    headers/service/youtubebroadcastservice.h \
    headers/service/youtubecredentialservice.h \
    headers/service/youtubestreamservice.h

SOURCES = \
    sources/controller/av_processing.cpp \
    sources/controller/creating_lecture.cpp \
    sources/controller/recording_streaming.cpp \
    sources/controller/uploading.cpp \
    sources/controller/utils.cpp \
    sources/model/frame_queue.cpp \
    sources/model/read_config.cpp \
    sources/model/xml.cpp \
    sources/view/course_management_view.cpp \
    sources/view/input_view.cpp \
    sources/view/lecture_management_view.cpp \
    sources/view/main_view.cpp \
    sources/view/output_view.cpp \
    sources/view/processing_view.cpp \
    sources/view/recording_streaming_view.cpp \
    sources/view/uploading_view.cpp \
    sources/view/utils_view.cpp \
    sources/broad_cast.cpp \
    sources/youtube/authentication.cpp \
    sources/youtube/youtubeapi.cpp \
    sources/base.cpp \
    sources/view/youtube_view.cpp \
    sources/youtube/broadcastbuilder.cpp \
    sources/youtube/youtubehandler.cpp \
    sources/service/youtubebroadcastservice.cpp \
    sources/service/youtubecredentialservice.cpp \
    sources/service/youtubestreamservice.cpp

DEFINES += QT_DEPRECATED_WARNINGS
CONFIG += no_keywords static
#DEFINES =
