/*
 * File: base.h
 *
 * Author: GVLab
 *
 * Created on: Aug 13, 2015
 */

#ifndef HEADER_BASE_H
#define HEADER_BASE_H
extern "C" {
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <stdlib.h>
#include <X11/Xlib.h>

#include "libavformat/avformat.h"
#include "libavdevice/avdevice.h"
#include "libswscale/swscale.h"
#include "libswresample/swresample.h"
#include "libavutil/fifo.h"
#include "libavcodec/avcodec.h"
#include <gtk/gtk.h>
#include <gdk/gdkx.h>
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <libxml2/libxml/parser.h>
#include <libxml2/libxml/tree.h>
#include <libxml2/libxml/xmlreader.h>

#include <SDL/SDL.h>
#include <SDL/SDL_thread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/statvfs.h>
#include "log4c.h"
}
#include "model/frame_queue.h"

#define MAX_OUT 12
#define MAX_OUT_VIDEO 10
#define DEFAULT_SCREEN_WIDTH 640
#define DEFAULT_SCREEN_HEIGHT 400

#define MAX_LECTURE 20
#define MAX_VERSION 20
#define MAX_SUBJECT 100

#define LONG_STRING_LENGTH 300
#define SHORT_STRING_LENGTH 200
#define STRING_LENGTH 100

#define NOT_USE_STREAM 0
#define USE_STREAM 1

//preview mode
#define ONE_BY_ONE 0
#define ALL_IN_ONE 1
#define FULL_SCREEN 2
#define PREVIEW_MODE 3

#define PAUSE 1
#define NORMAL 0
#define STOP 2

#define HDD 0
#define LIVE 1

#define NO_ERROR 0
#define WRITE_PACKET_ERROR 1
#define NETWORK_BREAK 2
#define NETWORK_DOWN 3
#define DECODE_VIDEO_ERROR 4
#define DECODE_AUDIO_ERROR 5
#define PACKET_INDEX_ERROR 6

//enum for in/out state
typedef enum _state {
	STATE_NORMAL, STATE_NEW, STATE_DELETED
} State;

//enum for input kind
typedef enum _input_kind {
	FFMPEG_SUPPORT, V2U, DECKLINK
} InputKind;

//enum for input kind
typedef enum _status_kind {
	IS_DUPLICATE, IS_NOT_DUPLICATE
} StatusKind;

//enum for input kind
typedef enum _error_kind {
	INPUT_ERROR, OUTPUT_ERROR, COURSE_ERROR, LECTURE_ERROR, PROGRAM_ERROR, IN_VIDEO_ERROR, VALID
} ErrorKind;

//status bar
extern GtkWidget *status_bar;
extern char tmp_status[STRING_LENGTH];
extern gint context_id;

//network status bar
extern GtkWidget *network_status_bar;
extern char tmp_network[STRING_LENGTH];
extern gint network_context_id;

extern int network_refresh_time;
extern int network_break_times;
extern int network_down_times;
extern int status_code;

//input information
typedef struct _input_info {
	char name_device[50];
	char driver[50];
	int input_type;
	int a_out_id[MAX_OUT];
	int a_out_num;
	int v_out_id[MAX_OUT];
	int v_out_num;
	int v_state[MAX_OUT];
	int width;
	int height;

	int frame_rate;
	int sample_rate;

	State state;
} InputInfo;

//output information
typedef struct _output_info {
	char url[SHORT_STRING_LENGTH];
	char format[50];
	char name[50];
	int id;
	int disable;
	int test;
	/*video*/
	char in_video[MAX_OUT_VIDEO][50];
	int in_video_type[MAX_OUT_VIDEO];
	int in_video_state[MAX_OUT_VIDEO];
	char in_video_key[MAX_OUT_VIDEO][2];
	int num_video;
	int buf_size;
	int video_bit_rate;
	int video_queue_size;
	int max_buf_rate;
	enum AVCodecID video_codec;
	int width;
	int height;
	enum AVPixelFormat pix_fmt;
	int frame_rate;
	int gop_size;
	int q_min;
	int q_max;
	int video_stream;
	/*audio*/
	char in_audio[50];
	int audio_bit_rate;
	enum AVCodecID audio_codec;
	enum AVSampleFormat sample_fmt;
	int num_channel;
	int sample_rate;
	int audio_queue_size;
	int baseline;
	/*file*/
	int timeRecord;
	int output_type;
	State state;
	int audio_stream;
} OutputInfo;

//stream data
typedef struct _stream {
	AVFormatContext* oformat;
	AVStream* stream;
	AVCodecContext* codec;
	AVFrame* frame;
	/**
	 * Queue to get frame from input
	 */
	FrameQueue input_queue;
	/**
	 * Queue to put frame to test GUI
	 */
	FrameQueue test_queue;
	/**
	 * Tell input where is the output queue
	 * NULL if don't want input to send frame anymore
	 * */
	// FrameQueue** queue_at_input;
	// struct SwsContext* imageConvert;
	
	pthread_t thread;
	int stream_stop;
} Stream;

//output data
typedef struct _output {
	AVFormatContext* oformat;
	Stream* video;
	Stream* audio;
	int id;
	int disable;
	int type;
	pthread_mutex_t mutex;
	float max_buf_rate;
	int buffer_status;
	int pause_buffer;
	int status;
	struct timeval start_time;
} Output;

//input data
typedef struct _input {
	void * format;
	int input_id;
	/**
	 * The video stream of input,  NULL if no video stream
	 */
	AVStream* v_stream;

	/**
	 * The audio stream of input, NULL if no audio stream
	 */
	AVStream* a_stream;

	/**
	 * Read next packet from 'in' input. The packet received
	 * can be audio or video packet, so check it before use
	 */
	int (*read_packet)(void* in, AVPacket *out_packet);

	/**
	 * Decode packet to draw frame
	 */
	int (*decode_video)(void* in, AVPacket *packet, AVFrame *out_frame);
	int (*decode_audio)(void* in, AVPacket *packet, AVFrame *out_frame);

	/**
	 * List of output queue to put decoded frame, queue's in the output side
	 * this's just a pointer to it
	 **/
	FrameQueue* a_out_queue[MAX_OUT];
	FrameQueue* v_out_queue[MAX_OUT];

	/*
	 * test only
	 */
	Output *a_out[MAX_OUT];
	Output *v_out[MAX_OUT];
	/**
	 * Number of output queue in queue list
	 */
	int a_out_num;
	int v_out_num;

	/**
	 * Kind of input
	 */
	InputKind kind;

	int frame_rate;

	/*
	 * set by user
	 * When start getting first packet, assign it by call clock()
	 * */
	clock_t last_get_frame;

	/**
	 * convert context of current input
	 */
	struct SwsContext* imageConvert[MAX_OUT];

	/**
	 * Tmp frame of output which current input point to
	 */
	AVFrame* frame[MAX_OUT];

	/**
	 * Codec context of output which current input point to
	 */
	AVCodecContext* codec[MAX_OUT];

	/**
	 * If switch_with_input is set, program can switch between two video streams
	 * in_video_state contain signal to controll which input is sent to output
	 */
	int in_video_state[MAX_OUT];

	/**
	 * output id this input point to. Map with in_video_state
	 */
	int out_id[MAX_OUT];

	// fifo for audio
	AVFifoBuffer* fifo[MAX_OUT];
	uint8_t *o_samples[MAX_OUT];
	AVStream* stream[MAX_OUT];
	int64_t o_samples_size[MAX_OUT];

} Input;


/**
 * Data for controll in_video of output stream
 */
typedef struct _control_stream {
	/**
	 * If switch_with_input is set, program can switch between two video streams
	 * in_video_state contain signal to controll which input is sent to output
	 */
	int in_video_state[MAX_OUT][MAX_OUT];

	/**
	 * key
	 */
	char switch_key[MAX_OUT*MAX_OUT_VIDEO][2];

	/**
	 * input id
	 */
	int in_id[MAX_OUT*MAX_OUT_VIDEO];

	/**
	 * output id
	 */
	int out_id[MAX_OUT*MAX_OUT_VIDEO];

	/**
	 * number of group
	 */
	int num;
} ControlStream;

/**
 * Lecture data
 */
typedef struct _lecture_info {
	char lecture_name[SHORT_STRING_LENGTH];
} LectureInfo;

/**
 * Version data
 */
typedef struct _version_info {
	char version_name[SHORT_STRING_LENGTH];
	LectureInfo lecture_list[MAX_LECTURE];
	int num_lecture;
} VersionInfo;

/**
 * Subject data
 */
typedef struct _subject_info {
	char subject_name[SHORT_STRING_LENGTH];
	char subject_id[20];
	VersionInfo version_list[MAX_VERSION];
	int num_version;
} SubjectInfo;

/**
 * Data info
 */
typedef struct _data_info {
	SubjectInfo subject_list[MAX_SUBJECT];
	int num_subject;
} DataInfo;


/**
 * Video4linux device information
 */
typedef struct _v4l_devices {
	char name[MAX_OUT][20];
	char display_name[MAX_OUT][50];
	int num_device;
} V4lDevices;

/**
 * Audio device information
 */
typedef struct _alsa_devices {
	char name[MAX_OUT][30];
	char display_name[MAX_OUT][30];
	int num_device;
} AlsaDevices;

/**
 * Data for close tab in notebook
 */
typedef struct _close_page {
	GtkWidget *window;
	int page_index;
} ClosePage;

/**
 * lecture management elements
 */
typedef struct _display_info {
	//normal elements
	GtkWidget *label;
	GtkWidget *check;
	GtkWidget *name;
	GtkWidget *type;
	GtkWidget *audio;
	GtkWidget *video;
	GtkWidget *switch_key;

} DisplayInfo;

/**
 * course management elements
 */
typedef struct _display_course_manage {
	//normal elements
	GtkWidget *home;
	GtkWidget *home_button;
		
	//course
	GtkWidget *course; 
	GtkWidget *course_new;
	GtkWidget *check_new_course;
	GtkWidget *check_select_course;
	GtkWidget *course_id;

	//version
	GtkWidget *version; 
	GtkWidget *version_new;
	GtkWidget *check_new_version;
	GtkWidget *check_select_version;
} DisplayCourseManage;

/**
 * lecture management elements
 */
typedef struct _display_lecture_manage {
	//lecture
	GtkWidget *lecture; 
	GtkWidget *lecture_new;
	GtkWidget *check_new_lecture;
	GtkWidget *check_select_lecture;

	//path prefix
	GtkWidget *path_prefix;

	//description
	GtkWidget *description;
} DisplayLectureManage;

/**
 * program process elements
 */
typedef struct _display_process {
	//lecture
	GtkWidget *course; 
	GtkWidget *version;
	GtkWidget *lecture;
	GtkWidget *merge;

	char curr_course[SHORT_STRING_LENGTH];
	char curr_version[SHORT_STRING_LENGTH];
	char curr_lecture[SHORT_STRING_LENGTH];

	int course_index;
	int version_index;
	int lecture_index;

	GtkWidget *home;
	GtkWidget *home_button;
} DisplayProcess;

/**
 * uploading elements
 */
typedef struct _display_uploading {
	//data
	GtkWidget *course; 
	GtkWidget *version;
	GtkWidget *lecture;
	GtkWidget *button;

	GtkWidget *type;

	GtkWidget *ftp;
	GtkWidget *user_name;
	GtkWidget *password;
	GtkWidget *server_path;
	GtkWidget *show;
	GtkWidget *save_button;
	GtkWidget *status;
	GtkWidget *class_entry;
	GtkWidget *term_entry;
	GtkWidget *course_entry;

	char curr_course[SHORT_STRING_LENGTH];
	char curr_course_id[30];
	char curr_version[SHORT_STRING_LENGTH];
	char curr_lecture[SHORT_STRING_LENGTH];

	char server[30];
	char user[30];
	char path[50];
	char pass[30];
	char class_id[30];
	char term[30];

	int course_index;
	int version_index;
	int lecture_index;
} DisplayUploading;

/**
 * input gtk elements
 */
typedef struct _display_input {
	//normal elements
	GtkWidget *table;
	GtkWidget *label;
	GtkWidget *name;
	GtkWidget *input_type;
	GtkWidget *display_name;
	GtkWidget *width;
	GtkWidget *height;
	GtkWidget *frame_rate;
	GtkWidget *sample_rate;
	GtkWidget *test_button;
} DisplayInput;

/**
 * output gtk elements
 */
typedef struct _display_output {
	//general elements
	GtkWidget *table;
	GtkWidget *label;
	GtkWidget *name;
	GtkWidget *out_url;
	GtkWidget *out_url_lbl;
	GtkWidget *location;
	GtkWidget *file;
	GtkWidget *file_lbl;
	GtkWidget *out_type;
	GtkWidget *format;
	GtkWidget *format_lbl;

	//output audio elements
	GtkWidget *audio_table;
	GtkWidget *audio_name;
	GtkWidget *num_channel;
	GtkWidget *audio_codec;
	GtkWidget *sample_rate;
	GtkWidget *audio_bit_rate;
	GtkWidget *sample_fmt;
	GtkWidget *audio_buffer;
	GtkWidget *audio_check;
	GtkWidget *audio_queue_size;

	//output video elements
	GtkWidget *video_table;
	GtkWidget *video_name[MAX_OUT_VIDEO];
	GtkWidget *video_type[MAX_OUT_VIDEO];
	GtkWidget *video_key[MAX_OUT_VIDEO];
	GtkWidget *ctrl_label[MAX_OUT_VIDEO];
	GtkWidget *video_bit_rate;
	GtkWidget *gop_size;
	GtkWidget *width;
	GtkWidget *height;
	GtkWidget *video_codec;
	GtkWidget *video_queue_size;
	GtkWidget *max_buf_rate;
	GtkWidget *pix_fmt;
	GtkWidget *remove[MAX_OUT_VIDEO];
	GtkWidget *add;
	GtkWidget *video_check;
	GtkWidget *frame_rate;
	GtkWidget *baseline;
} DisplayOutput;

/**
 * Data for upload thread
 */
typedef struct _upload_thread {
	char local[SHORT_STRING_LENGTH];
	char server[SHORT_STRING_LENGTH];

	GtkWidget *window;
} UploadThread;

/**
 * Preview data
 */
typedef struct _preview_info {
	SDL_Overlay *bmp[PREVIEW_MODE];
	AVCodecContext *codec;
	SDL_Rect rect[PREVIEW_MODE];
	struct SwsContext *convert;
} PreviewInfo;

/**
 * Data for display buffer
 */
typedef struct _display_buffer {
	//frame group progress bar
	GtkWidget *frame;

	//table contain progressbar
	GtkWidget *table;

	//label
	GtkWidget *audio_label;
	GtkWidget *video_label;

	//progress bar
	GtkWidget *video_buffer;
	GtkWidget *audio_buffer;

} DisplayBuffer;

/**
 * Data for display buffer
 */
typedef struct _display_input_status {
	//box group progress bar
	GtkWidget *a_box;
	GtkWidget *v_box;

	//table contain progressbar
	GtkWidget *a_table;
	GtkWidget *v_table;

	//label
	GtkWidget *a_label[MAX_OUT];
	GtkWidget *v_in_label[MAX_OUT];
	GtkWidget *v_out_label[MAX_OUT];

	//progress bar
	GtkWidget *video[MAX_OUT][MAX_OUT];
	GtkWidget *audio[MAX_OUT];

} DisplayInputStatus;

inline GtkAttachOptions operator | (GtkAttachOptions lhs, GtkAttachOptions rhs)

{
    //return (GtkAttachOptions) (static_cast<GtkAttachOptions>(lhs) | static_cast<GtkAttachOptions>(rhs));
    return lhs;
}

#endif /*BASE_H*/
