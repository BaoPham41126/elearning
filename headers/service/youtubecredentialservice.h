#ifndef YOUTUBECREDENTIALSERVICE_H
#define YOUTUBECREDENTIALSERVICE_H

#include <QObject>
#include <QString>
#include <QtNetwork/QNetworkReply>
#include <headers/dto/youtubecredentialdto.h>

class YoutubeCredentialService : public QObject
{
    Q_OBJECT
public:
    static YoutubeCredentialDto* getCurrentCredential();
    YoutubeCredentialDto* getCredentialFromFile();
    void grantPermission();
    void getCredentialByToken(QString code);
    void refreshCredential();
private:
    static YoutubeCredentialDto* credential;
    QString clientId = "99890648815-cuqk1qdh6om3tbftnn0jcrnlhnef4dmq.apps.googleusercontent.com";
    QString clientSecret = "zRaeDKMaoXssbtqKjp3UGaNw";
    void saveCredentialToFile(YoutubeCredentialDto* credential);
public Q_SLOTS:
    void onTokenRequestFinished(QNetworkReply * reply);
    void onTokenRefreshFinished(QNetworkReply * reply);
Q_SIGNALS:
    void authenticateFinished(YoutubeCredentialDto *credential);
    void refreshFinished(YoutubeCredentialDto *credential);
};
#endif // YOUTUBECREDENTIALSERVICE_H
