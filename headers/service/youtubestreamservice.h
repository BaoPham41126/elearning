#ifndef YOUTUBESTREAMSERVICE_H
#define YOUTUBESTREAMSERVICE_H
#include <QObject>
#include <headers/dto/youtubestreamdto.h>
#include <headers/dto/youtubecredentialdto.h>
#include <headers/service/youtubecredentialservice.h>
class YoutubeStreamService : public QObject
{
    Q_OBJECT
public:
    YoutubeStreamService();
    void createNewStream(QString title, QString cdnFormat);
    void loadStream(QString streamId);
    void updateStream(YoutubeStreamDto stream);
    void deleteStream(QString streamId);
private:
    YoutubeCredentialDto* credential;
    YoutubeStreamDto* createStreamFromReply(QNetworkReply *reply);
public Q_SLOTS:
    void onCreateStreamComplete(QNetworkReply *reply);
    void onLoadStreamComplete(QNetworkReply *reply);
    void onUpdateStreamComplete(QNetworkReply *reply);
    void onDeleteStreamComplete(QNetworkReply *reply);
Q_SIGNALS:
    void streamCreated(YoutubeStreamDto *stream);
    void streamDeleted(QString id);
    void streamUpdated(YoutubeStreamDto *stream);
    void loadStreamCompleted(YoutubeStreamDto *stream);
};
#endif // YOUTUBESTREAMSERVICE_H
