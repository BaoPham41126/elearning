#ifndef YOUTUBEBROADCASTSERVICE_H
#define YOUTUBEBROADCASTSERVICE_H

#include <QObject>
#include <QtNetwork/QNetworkReply>
#include <headers/dto/youtubebroadcastdto.h>
#include <headers/dto/youtubestreamdto.h>
#include <headers/dto/youtubecredentialdto.h>
#include <headers/service/youtubecredentialservice.h>
#include <headers/service/youtubestreamservice.h>

class YoutubeBroadcastService : public QObject
{
    Q_OBJECT
public:
    YoutubeBroadcastService();
    void createNewBroadcast(QString title, QDateTime startTime, QString privacyStatus);
    void loadBroadcast();
    void updateBroadcast(YoutubeBroadcastDto broadcast);
    void deleteBroadcast(QString broadcastId);
    void bindStreamToBroadcast(QString streamId, QString broadcastId);
    void changeBroadcastStatus(QString broadcastId, QString status);
private:
    YoutubeCredentialDto* credential;
    void getNextBroadcastPage(QString pageToken);
    YoutubeBroadcastDto* createBroadcastFromReply(QNetworkReply *reply);
public Q_SLOTS:
    void onCreateBroadcastComplete(QNetworkReply *reply);
    void onGetBroadcastComplete(QNetworkReply *reply);
    void onUpdateBroadcastComplete(QNetworkReply *reply);
    void onDeleteBroadcastComplete(QNetworkReply *reply);
    void onBindCompleted(QNetworkReply *reply);
    void onTransitionCompleted(QNetworkReply *reply);
    void onLoadBroadcastCompleted(YoutubeBroadcastDto *broadcast);
Q_SIGNALS:
    void broadcastCreated(YoutubeBroadcastDto *broadcast);
    void broadcastDeleted(QString id);
    void broadcastUpdated(YoutubeBroadcastDto *broadcast);
    void bindCompleted(YoutubeBroadcastDto *broadcast);
    void transitionCompleted(YoutubeBroadcastDto *broadcast);
    void loadBroadcastCompleted(YoutubeBroadcastDto *broadcast);
};

class BroadcastBoundStreamLoader : public QObject
{
    Q_OBJECT
private:
    YoutubeBroadcastDto **broadcasts;
    YoutubeCredentialDto *credential;
    int total;
    int less;
public:
    BroadcastBoundStreamLoader(YoutubeBroadcastDto **broadcasts, int total, YoutubeCredentialDto *credential);
    void load();
public Q_SLOTS:
    void onStreamReturn(YoutubeStreamDto* stream);
Q_SIGNALS:
    void loadBroadcastComplete(YoutubeBroadcastDto *broadcast);
};

#endif // YOUTUBEBROADCASTSERVICE_H
