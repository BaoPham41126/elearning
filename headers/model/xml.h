/*
 * File: xml.h
 *
 * Author: GVLab
 *
 * Created on: Aug 13, 2015
 */
#ifndef XML
#define XML
#include "headers/base.h"
#include "headers/controller/utils.h"

/**
 * Node data
 */
typedef struct _node_info {
	int index;
	char att_name[10][20];
	char att_value[10][50];
	char node_name[20];
} NodeInfo;

//get course name from xml file
void get_course_names(xmlNode *root, char *att, DataInfo *dataInfo);

//get version name from xml file
void get_version_names(xmlNode *root, char *att, SubjectInfo *subject);

//get lecture name from xml file
void get_lecture_names(xmlNode *root, char *att, VersionInfo *version);

//get lecture description from xml file
void get_lecture_description(xmlNode *root, char *lecture, char *description);

//update lecture description to xml file
void update_description(xmlNode *root, char *lecture, char *description);

//open xml file
xmlNodePtr open_xml_file(char *file_path);

//read lecture file
void read_lecture_file(VersionInfo *version, char *home_path);

//read version file
void read_version_file(SubjectInfo *subject, char *home_path);

//read course file
void read_course_file(DataInfo** dataInfo, char *home_path);

//add new attribute to NodeInfo
void add_new_attribue(NodeInfo *node, char *name, char *value);

//create new nodeinfo
xmlNodePtr create_new_node(NodeInfo *node);

//create new xml element
void create_new_element(char *path, char *xml_file, NodeInfo node);

//create new lecture file
void create_new_lecture(char *home_path, char *subject_name, char *version_name, 
    char *lecture_name, char *description);

//create new version file
void create_new_version(char *home_path, char *subject_name, char *version_name);

// create new course file
void create_new_course(char *home_path, char *subject_name, char *course_id);

//create output description
void convert_output_data(OutputInfo* outDevs, char *path, char *node_name);

//create video description
void create_video_description_file(char *home_path, int session, OutputInfo** outDevs, 
    int numOut, char *current_session);

//create xml metadata
void create_metadata(char *path, char *course_name);

//create catalog file
void create_catalog(char *path, char *course_name, char *version, char *lecture);

//read description
void read_lecture_description(char *path, char *lecture, char *description);

//update description
void update_lecture_description(char *path, char *lecture, char *description);
#endif
