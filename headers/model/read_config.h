/*
 * File: read_config.h
 *
 * Author: GVLab
 *
 * Created on: Aug 13, 2015
 */
#ifndef READ_CONFIG
#define READ_CONFIG
#include "headers/base.h"
extern "C"{
#include <libconfig.h>
}

//add out id
int add_out_id(OutputInfo *outDev, InputInfo** inDevs, int numIn);

//map output to input base on config file
void add_out2in(InputInfo** inDevs, int numIn, OutputInfo **outDevs, int numOut);

//get data from config file
void configure(InputInfo** InDevs, int* numIn, OutputInfo** OutDevs,
		int* numOut);

//save input devices to config
void save_input_to_config_file(InputInfo** InDevs, int numIn);

//save output streams to config file
void save_output_to_config_file(OutputInfo** OutDevs, int numOut);

//save output's disable value to config file
void save_info_to_config_file(OutputInfo** OutDevs, int numOut);

//save output url to config file
void save_output_url_to_config_file(OutputInfo** OutDevs, int numOut);

//read data from config file
void read_course_data(char *path, char *data_path, char *course, 
			char *version, char *lecture, char *session, int *refresh_time, int *break_time, int *down_time);

//read data from config file
void read_server_config(char *server, char *user, char *path);

//read data from config file
void read_class_config(char *class_id, char *term);

//save data to config file
void save_server_info_to_config(char *server, char *user, char *path);

//save data to config file
void save_class_info_to_config(char *class_id, char *term);

//save data from config file
void save_location_setting(char *path, char *data_path, char *course, 
	char *version, char *lecture, char *session);
#endif
