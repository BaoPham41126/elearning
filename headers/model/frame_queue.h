/*
 * File: frame_queue.h
 *
 * Author: GVLab
 *
 * Created on: Aug 13, 2015
 */
#ifndef FRAME_QUEUE
#define FRAME_QUEUE
#include <pthread.h>
extern "C" {
#include "libavformat/avformat.h"
#include "log4c.h"
}

//Frame queue data structure
typedef struct FrameQueue {
	AVFrame** frameq;
	int rindex, windex;
	int output_id;
	int size;
	int max_size;
	pthread_mutex_t* mutex;
	pthread_cond_t* cond;
    log4c_category_t* catLogQueue;
} FrameQueue;

//init a FrameQueue
void frame_queue_init(FrameQueue * queue, int max_size);

//put a node to queue
int frame_queue_put(FrameQueue * queue, AVFrame * picture, int *quit);

//get a node from queue
int frame_queue_get(FrameQueue* queue, AVFrame **picture, int *quit);

//destroy a FrameQueue
void frame_queue_deinit(FrameQueue * queue);
#endif
