#ifndef YOUTUBEAPI_H
#define YOUTUBEAPI_H

#include <QObject>
#include <QString>
#include <QtNetwork/QNetworkReply>
#include <headers/youtube/authentication.h>

class YoutubeStream{
public:
    YoutubeStream(){}
    QString id;
    QString title;
    QString address;
    QString streamStatus;
    QString healthStatus;
};

class Broadcast{
public:
    Broadcast(){}
    QString id;
    QString title;
    QString privacyStatus;
    QString lifeCycleStatus;
    QString boundStreamId;
    QString boundStreamAddress;
    QString getAddress(){ return "https://www.youtube.com/watch?v=" + id; }
};

class LoadBroadcastInfo : public QObject
{
    Q_OBJECT
private:
    Broadcast **broadcasts;
    Credential *credential;
    int total;
    int less;
public:
    LoadBroadcastInfo(Broadcast **broadcasts, int total, Credential *credential){
        this->broadcasts = broadcasts;
        this->total = total;
        this->less = total;
        this->credential = credential;
    }
    void load();
public Q_SLOTS:
    void onStreamReturn(YoutubeStream* stream);
Q_SIGNALS:
    void allBroadcastLoadComplete(Broadcast **broadcasts, int size);
};

class YoutubeAPI : public QObject
{
    Q_OBJECT

public:
    YoutubeAPI(Credential *credential);
    void createNewStream(QString title, QString cdnFormat);
    void getStream(QString streamId);
    void createNewBroadcast(QString title, QDateTime startTime, QString privacyStatus);
    void getAllBroadcast();
    void getNextBroadcastPage(QString pageToken);
    void bindStreamToBroadcast(QString streamId, QString broadcastId);
    void changeBroadcastStatus(QString broadcastId, QString status);
    void deleteBroadcast(QString broadcastId);
private:
    Credential *credential;
    void _createMultiBroadcastFromReply(QNetworkReply *reply);
public Q_SLOTS:
    void onStreamCreated(QNetworkReply *reply);
    void onStreamReturn(QNetworkReply *reply);
    void onBroadcastCreated(QNetworkReply *reply);
    void onBindCompleted(QNetworkReply *reply);
    void onTransitionCompleted(QNetworkReply *reply);
    void onAllBroadcastReturn(QNetworkReply *reply);
    void loadBroadcastsComplete(Broadcast **broadcast, int size);
    void onBroadcastDeleted(QNetworkReply *reply);
Q_SIGNALS:
    void streamCreated(YoutubeStream *stream);
    void streamReturn(YoutubeStream *stream);
    void broadcastCreated(Broadcast *broadcast);
    void allBroadcastReturn(Broadcast **broadcasts, int size);
    void bindCompleted(Broadcast *broadcast);
    void transitionCompleted(Broadcast *broadcast);
    void broadcastDeleted(QString id);
};

#endif // YOUTUBEAPI_H
