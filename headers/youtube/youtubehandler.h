#ifndef YOUTUBEHANDLER_H
#define YOUTUBEHANDLER_H

#include <QObject>
#include <QInputDialog>
#include <QLineEdit>
#include "headers/youtube/authentication.h"
#include "headers/youtube/youtubeapi.h"
#include "headers/base.h"
#include "headers/view/youtube_view.h"

#include <iostream>

/**
 * youtube gtk elements
 */
typedef struct _display_youtube {
    Credential *credential;

    GtkWidget *account_connect_status;
    GtkWidget *grant_permission_button;
    GtkWidget *tableChannel;
} DisplayYoutube;

class AuthenticationHandler : public QObject
{
    Q_OBJECT
public:
    explicit AuthenticationHandler(DisplayYoutube* display_youtube);
private:
    DisplayYoutube* display_youtube;
Q_SIGNALS:

public Q_SLOTS:
    void onTokenRefreshFinished(Credential *credential);

    void onAuthenticateFinished(Credential *credential);
};

class YoutubeHandler : public QObject
{
    Q_OBJECT
private:
    DisplayYoutube* display_youtube;
public:
    explicit YoutubeHandler(DisplayYoutube* display_youtube);

public Q_SLOTS:
    void onBroadcastReturn(Broadcast **broadcasts, int length);
    void onBroadcastCreated(Broadcast *broadcast);
    void onBroadcastDeleted(QString broadcastId);
};

#endif // YOUTUBEHANDLER_H
