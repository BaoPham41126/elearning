#ifndef YOUTUBECHECKSTATUS_H
#define YOUTUBECHECKSTATUS_H

#include <QObject>
#include <QObject>
#include <QMutex>
#include <QMutexLocker>
#include "youtubeapi.h"

class CheckStatusJob : public QObject
{
    Q_OBJECT

public:
    CheckStatusJob(YoutubeAPI *youtubeAPI, QString streamId):m_stop(false){
        this->youtubeAPI = youtubeAPI;
        this->streamId = streamId;
        QObject::connect(youtubeAPI, &YoutubeAPI::streamReturn, this, &CheckStatusJob::onStreamReturn);
    }

public Q_SLOTS:
    void stop()
    {
        QMutexLocker locker(&m_mutex);
        m_stop=true;
    }
    void onStreamReturn(YoutubeStream *stream)
    {
        Q_EMIT streamReturn(stream);
        {
        QMutexLocker locker(&m_mutex);
        if (!m_stop)
            youtubeAPI->getStream(streamId);
        }
    }

Q_SIGNALS:
    void streamReturn(YoutubeStream *stream);

private:
    YoutubeAPI *youtubeAPI;
    QString streamId;
    QMutex m_mutex;
    bool m_stop;
public:
    void run()
    {
        youtubeAPI->getStream(streamId);
    }
};
#endif
