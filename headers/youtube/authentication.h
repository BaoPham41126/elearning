#ifndef AUTHENTICATION_H
#define AUTHENTICATION_H
#include <QObject>
#include <QString>
#include <QtNetwork/QNetworkReply>

class Credential
{
public:
    Credential(QString accessToken, QString refreshToken);
    Credential(){}
    QString accessToken;
    QString refreshToken;

    bool readFromFile();
    void saveToFile();
};


class Authentication : public QObject
{
    Q_OBJECT

public:
    Authentication(){}
    void grantPermission();
    void getToken(QString code);
    void refreshToken(QString refreshToken);
private:
    QString clientId = "99890648815-cuqk1qdh6om3tbftnn0jcrnlhnef4dmq.apps.googleusercontent.com";
    QString clientSecret = "zRaeDKMaoXssbtqKjp3UGaNw";
    QString _refreshToken;
public Q_SLOTS:
    void onTokenRequestFinished(QNetworkReply * reply);
    void onTokenRefreshFinished(QNetworkReply * reply);
Q_SIGNALS:
    void authenticateFinished(Credential *credential);
    void refreshFinished(Credential *credential);
};

#endif // AUTHENTICATION_H
