#ifndef BROADCASTBUILDER_H
#define BROADCASTBUILDER_H

#include <QObject>
#include <QMutexLocker>
#include "youtubeapi.h"

class BroadcastBuilder : public QObject
{
    Q_OBJECT

public:
    BroadcastBuilder(Credential *credential);
    void createLiveStream(QString title, QDateTime startTime, QString privacyStatus, QString cdnFormat);
    void goLive();
    void checkStreamStatus();
private:
    YoutubeAPI *youtubeAPI;
    Broadcast *broadcast;
    YoutubeStream *stream;
    void _bind();
private Q_SLOTS:
    void onStreamCreated(YoutubeStream *stream);
    void onStreamRefeshComplete(YoutubeStream *stream);
    void onBroadcastCreated(Broadcast *broadcast);
    void onBindCompleted(Broadcast *broadcast);
    void onBroadcastStarted(Broadcast *broadcast);
Q_SIGNALS:
    void created(Broadcast *broadcast, YoutubeStream *stream);
    void started(Broadcast *broadcast);
    void streamStatusChanged(QString status);
};
#endif // BROADCASTBUILDER_H
