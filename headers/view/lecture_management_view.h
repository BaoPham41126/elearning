/*
 * File: lecture_management_view.h
 *
 * Author: GVLab
 *
 * Created on: Aug 13, 2015
 */

#ifndef LECTURE_MANAGEMENT_VIEW
#define LECTURE_MANAGEMENT_VIEW
#include "headers/base.h"
#include "headers/view/main_view.h"
#include "headers/model/xml.h"

//save input button click
void save_lecture_management_button_click(GtkWidget *widget);

//on function lecture change
void lecture_change(GtkComboBox *lecture, gpointer data);

//new version button click
void check_new_lecture_button(GtkWidget *widget, gpointer button);

//read DisplayInfo data from UI and parse to OutputInfo
void read_UI_lecture_management();

//show output info
void show_output_info(GtkWidget *table, DisplayInfo *info, OutputInfo *output, int index, int new_info);

//hide output info
void hide_output_info(DisplayInfo *info);

//update output info data
void update_output_info();

//check output info button click
void check_output_info(GtkWidget *check, int i);

//create info tab contain all output info

GtkWidget* create_lecture_management_tab(GtkWidget* window);
#endif
