/*
 * File: main_view.h
 *
 * Author: GVLab
 *
 * Created on: Aug 13, 2015
 */

#ifndef MAIN_VIEW
#define MAIN_VIEW

#include "headers/base.h"
#include "headers/model/read_config.h"
#include "headers/model/xml.h"
#include "headers/controller/recording_streaming.h"
#include "headers/controller/creating_lecture.h"
#include "headers/view/output_view.h"
#include "headers/view/input_view.h"
#include "headers/view/lecture_management_view.h"
#include "headers/view/course_management_view.h"
#include "headers/view/processing_view.h"
#include "headers/view/uploading_view.h"
#include "headers/view/utils_view.h"
#include "headers/view/recording_streaming_view.h"
#include "headers/view/youtube_view.h"

extern "C" {
#include <stdlib.h>
#include <locale.h>
}

#define MONITOR 0
#define VIDEO 1
#define AUDIO 2

#define COURSE_CHANGE 1
#define VERSION_CHANGE 2

//input video type
#define OLD 0
#define NEW 1
#define MERGE 2

//uploading
#define UPLOAD_LECTURE 0
#define UPLOAD_VERSION 1
#define UPLOAD_COURSE 2

#define COURSE 1
#define VERSION 2
#define LECTURE 3

#define NORMAL 0
#define NEW_VERSION 1
#define NEW_COURSE 2
#define NEW_LECTURE 3

#define CONTAINER_MP4 0
#define CONTAINER_FLV 1
#define CONTAINER_MP3 2
#define CONTAINER_MKV 3

#define RETURN_OK 0
#define RETURN_ERROR 1

extern OutputInfo* outDevs[MAX_OUT];
extern InputInfo* inDevs[MAX_OUT];
extern int numOut, numIn;

//video4linux devices, Asla devices
extern V4lDevices *v4l_list;
extern AlsaDevices *alsa_list;

//gtk element for program
extern GtkWidget *notebook;
extern GtkWidget *tab;
extern GtkWidget *main_box, *info_box, *location_box, *input_box ,*youtube_box;
extern GtkWidget *output_box, *process_box, *upload_box;
extern GtkWidget *lecture_box, *lecture_label;

//display input devices, output streams
extern DisplayInput *display_input[MAX_OUT];
extern DisplayOutput *display_output[MAX_OUT];
extern DisplayInfo *display_info[MAX_OUT];
extern DisplayCourseManage *display_course;
extern DisplayLectureManage *display_lecture;
extern DisplayProcess *display_process;
extern DisplayUploading *display_upload;

//output data location
extern DataInfo* course_data_info;
extern char path[SHORT_STRING_LENGTH];
extern char data_path[SHORT_STRING_LENGTH];

//buf
extern char buf[50];

//error type to display in UI
extern ErrorKind ui_error;
extern ErrorKind in_video_error;

//show output info
extern GtkWidget *output_info_table;

//show folder settings
extern char home_path[SHORT_STRING_LENGTH];
extern char current_course[SHORT_STRING_LENGTH];
extern char current_course_id[20];
extern char current_version[SHORT_STRING_LENGTH];
extern char current_lecture[SHORT_STRING_LENGTH];
extern char current_path_prefix[SHORT_STRING_LENGTH];
extern char current_description[1024];
extern int course_index, version_index, lecture_index;

//preview video
extern GtkWidget *combo_box;

//record
extern int session;
extern int type;
extern char tmp_home_path[LONG_STRING_LENGTH];
extern int starting;

//start button. stop button
extern GtkWidget *start_button, *stop_button;

//show status audio, video buffer
extern GtkWidget *label;

extern char tmp_space[STRING_LENGTH];
extern GtkWidget *right_status_bar;
extern gint right_context_id;

//status table contain all status of program
extern GtkWidget *buffer_status_box;

//main function for view
int create_UI();

//show combobox
void show_current_stream();

// create buffer progress bar
void create_buffer_progressbar();

//hide all input status
void hide_input_status();

//destroy progressbar elements
void hide_buffer_progressbar();

//change preview id base on current text of combobox
void select_preview_stream_changed(GtkWidget* window);

//close window
void close_window_button_click(GtkWidget *window);

//init data when program start
void init_data();

//set default value
void set_default_value();

//update UI when reread data from config file
void update_program_UI();

//interface of start button click
void stop_button_click(GtkWidget *widget);

//interface of stop button click
void start_button_click(GtkWidget *widget);

#endif
