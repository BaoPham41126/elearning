/*
 * File: recording_streaming.h
 *
 * Author: GVLab
 *
 * Created on: Aug 13, 2015
 */

#ifndef RECORDING_STREAMING_VIEW
#define RECORDING_STREAMING_VIEW
#include "headers/base.h"
#include "headers/view/main_view.h"

//create main tab: show main UI
GtkWidget* create_recording_streaming_tab(GtkWidget* window, GtkWidget *sock);
#endif
