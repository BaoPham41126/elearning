/*
 * File: uploading_view.h
 *
 * Author: GVLab
 *
 * Created on: Aug 13, 2015
 */
#ifndef UPLOADING_VIEW
#define UPLOADING_VIEW
#include "headers/base.h"
#include "headers/view/main_view.h"
#include "headers/controller/uploading.h"

//on change function type upload change
void type_upload_change(GtkWidget *select);

//show course, version, lecture base on type
void data_upload_changed(GtkWidget *combo_box, int type);

//upload function for thread
void *upload_data(void *main_window);

//merge video button click
void upload_button_click(GtkWidget *widget, gpointer window);

//save upload data
void connect_button_click(GtkWidget *button);

//save upload data
void save_class_button_click(GtkWidget *button);

//show/hide password
void show_password(GtkWidget *check, GtkWidget *pass);

//create uploading tab: show upload UI
GtkWidget* create_uploading_tab(GtkWidget* window);
#endif
