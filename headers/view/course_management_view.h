/*
 * File: course_management_view.h
 *
 * Author: GVLab
 *
 * Created on: Aug 13, 2015
 */

#ifndef COURSE_MANAGEMENT_VIEW
#define COURSE_MANAGEMENT_VIEW
#include "headers/view/main_view.h"
#include "headers/base.h"
#include "headers/view/utils_view.h"
#include "headers/view/lecture_management_view.h"

//create Course Management tab: config output location
GtkWidget* create_course_management_tab(GtkWidget* window);

//select home path folder
void select_home_path(GtkWidget *widget);

//save folder location
void save_course_management_button_click(GtkWidget *widget);

//new version button click
void check_new_version_button(GtkWidget *widget, gpointer button);

//on_function course change
void on_course_change( GtkComboBox *course);

//on_function version change
void on_version_change( GtkComboBox *version);

//new course button click
void check_new_course_button(GtkWidget *widget, gpointer button);

//save file name
void store_filename(GtkWidget *widget, gpointer user_data);
#endif
