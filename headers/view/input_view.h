/*
 * File: input_view.h
 *
 * Author: GVLab
 *
 * Created on: Aug 13, 2015
 */

#ifndef INPUT_VIEW
#define INPUT_VIEW
#include "headers/base.h"
#include "headers/view/main_view.h"

//in notebook
extern GtkWidget *in_notebook;

//Show input devices
extern GtkWidget *input_type;
extern GtkWidget *input_hbox;

//create input devices tab: config input devices
GtkWidget* create_input_devices_tab(GtkWidget* window);

//click close tab button and close
void close_input_tab_click( GtkButton *button, int page_index);

//confirm remove input
void ok_remove_input(GtkWidget *widget, ClosePage *close_data);

//refresh input button click
void refresh_input_button_click(GtkWidget *button);

//ok_new_input_button_click create new input and show this input to UI
void ok_new_input_button_click(GtkWidget *widget, gpointer window);

//new input button click
void new_input_button_click(GtkWidget *widget, gpointer window);

//save input button click
void save_input_button_click(GtkWidget *widget);

//show input information base on InputInfo and show it to UI
void show_input(GtkWidget *table, DisplayInput* display_input, InputInfo *input, int new_input);

//test input
void preview_input_button_click(GtkWidget *button, gpointer input);

//show input name base on input type
GtkWidget *show_input_name(int type);

//show all input status
void show_input_status();

//read DisplayInput data from UI and parse to InputInfo
void read_UI_input(DisplayInput* display_input, InputInfo *input);

//save input button click
void save_input_button_click(GtkWidget *widget);
#endif
