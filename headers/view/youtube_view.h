/*
 * File: uploading_view.h
 *
 * Author: GVLab
 *
 * Created on: Aug 13, 2015
 */
#ifndef YOUTUBE_VIEW
#define YOUTUBE_VIEW
#include "headers/base.h"
#include "headers/view/main_view.h"
#include "headers/controller/uploading.h"

void get_all_upcoming_broadcast();
GtkWidget* insertTable (GtkWidget*tableChannel , char * date , char * name,char * rtmpLink,char * status, char* broadcastId, char* streamId);
//create uploading tab: show upload UI
GtkWidget* create_youtube_tab(GtkWidget* window);
#endif
