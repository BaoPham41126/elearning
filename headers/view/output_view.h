/*
 * File: output_view.h
 *
 * Author: GVLab
 *
 * Created on: Aug 13, 2015
 */

#ifndef OUTPUT_VIEW
#define OUTPUT_VIEW
#include "headers/base.h"
#include "headers/view/main_view.h"
#include "headers/view/utils_view.h"
#include "headers/view/lecture_management_view.h"

//Show output streams
extern GtkWidget *out_notebook;

//check exist video stream, audio stream of output
void check_audio_video(DisplayOutput* display_output, OutputInfo *output);

//check audio button click
void check_audio_click(GtkWidget *check, int i);

//check video button click
void check_video_click(GtkWidget *check, int i);

//on_change function when output type change
void on_output_type_change(GtkWidget *select, DisplayOutput* display_output);

//show output information base on OutputInfo and show it to UI
void show_output_general(GtkWidget *table, DisplayOutput* display_output, OutputInfo *output, int new_output);

//remove in video
void remove_in_video(GtkWidget *button, int index);

//add new video
void add_in_video(GtkWidget *button);

//show output information base on OutputInfo and show it to UI
void show_output_video(GtkWidget *table, DisplayOutput* display_output, 
    OutputInfo *output, int new_output);

//show output information base on OutputInfo and show it to UI
void show_output_audio(GtkWidget *table, DisplayOutput* display_output, 
    OutputInfo *output, int new_output);

//show output stream base on output_id
GtkWidget* show_output_stream(int i, int new_output);

//read DisplayOutput data from UI and parse to OutputInfo
void read_UI_output(DisplayOutput* display_output, OutputInfo *output);

//save output button click
void save_output_button_click(GtkWidget *widget);

//confirm remove output
void ok_remove_output(GtkWidget *widget, ClosePage *close_data);

//click close tab button and close
void close_output_tab_click( GtkButton *button, int page_index);

//ok_new_input_button_click create new input and show this input to UI
void ok_new_output_button_click(GtkWidget *widget, gpointer window);

//new output button click
void new_output_button_click(GtkWidget *widget, gpointer window);

//create output Streams tab: config output streams
GtkWidget* create_output_streams_tab(GtkWidget* window);


#endif
