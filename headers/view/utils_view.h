/*
 * File: utils_view.h
 *
 * Author: GVLab
 *
 * Created on: Aug 13, 2015
 */
#ifndef UTILS_VIEW
#define UTILS_VIEW
#include "headers/base.h"
#include "headers/view/main_view.h"
#include "headers/model/xml.h"

//change file path
void change_file_path();

//Create element and show output's in_video_type.
GtkWidget *show_output_video_type();

//Create element and show output's video_codec.
GtkWidget *show_video_codec_type();

//Create element and show uploading's upload_type.
GtkWidget *show_upload_type();

//Create element and show output's audio_codec.
GtkWidget *show_audio_codec_type(enum AVCodecID audio_codec);

//Create element and show output's pix_format.
GtkWidget *show_pix_fmt_type();

//Create element and show input's input_type.
GtkWidget *show_input_type();

//Create element and show output's output_type.
GtkWidget *show_output_type();

//Create element and show output's file_extension.
GtkWidget *show_format_type();

//Change display format in GTK output's file_extension base on given format.
void set_format_combo_box(GtkWidget * select, char *format);

//Get data from output's video_codec and return the id of codec.
int get_video_codec(GtkWidget * select);

//Get data from uploading's upload_type and return.
int get_upload_type(GtkWidget * select);

//Get data from output's pix_format and return.
int get_pix_fmt(GtkWidget * select);

//get current text from combobox, if unset, return ""
void get_combo_box_text(GtkComboBox *combobox, char *value);

//Get data from output's audio_codec and return.
int get_audio_codec_type(GtkWidget *combobox);

//Create close button no border with image close from stock.
GtkWidget *create_remove_button();

//Create delete button no border with image delete from stock.
GtkWidget *create_delete_button();

//Create add button no border with image add from stock.
GtkWidget *create_add_button();

//Create directory button no border with image directory from stock.
GtkWidget *create_directory_button();

//Create apply button no border with image apply from stock.
GtkWidget *create_apply_button();

//Map output's type from integer to string.
void get_output_typpe(int type, char *value);

//Create pixbuf from image.
GdkPixbuf *create_pixbuf(const gchar * filename);

//Create alert popup to screen base on type and show message.
GtkWidget * create_quick_message (gchar *message, gpointer window, int type);

//clear combobox data
void clear_combo_box_data(GtkWidget *combobox);

//Show message to gtk_status bar.
void push_item( GtkWidget *widget, gpointer data, char *text);

//Pop message from gtk_status bar.
void pop_item( GtkWidget *widget, gpointer data);

//Clear all message from gtk_status bar.
void clear_status_bar(GtkWidget *widget, gpointer data);

//Create element and show input's video_name base on v4l list.
GtkWidget *show_input_video_name(char *name);

//Create element and show input's audio_name base on alsa list.
GtkWidget *show_input_audio_name(char *name);

//Get real name of input video device base on input's display name.
void get_input_video_name(char *name, char *return_name);

//Get real name of input audio device base on input's display name.
void get_input_audio_name(char *name, char *return_name);

//Change output's url base on output's file_name and output's format.
void change_output_name();

//Get format_id from output's file_extenxion and pass to string.
char *get_format_combo_box(GtkWidget * select);

//show course, version, lecture base on type
void show_course_management(GtkWidget *combo_box, int type);
#endif
