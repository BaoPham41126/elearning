/*
 * File: processing_view.h
 *
 * Author: GVLab
 *
 * Created on: Aug 13, 2015
 */

#ifndef PROCESSING_VIEW
#define PROCESSING_VIEW
#include "headers/base.h"
#include "headers/view/main_view.h"
#include "headers/controller/creating_lecture.h"

//processing thread
extern pthread_t processing_thread;

//show course, version, lecture base on type
void data_process_changed(GtkWidget *combo_box, int type);

//select data path folder
void select_data_path(GtkWidget *widget);

//save data name
void store_data_name(GtkWidget *widget, gpointer user_data);

//upload function for thread
void *process_data(void *main_window);

//merge video button click
void process_button_click(GtkWidget *widget, gpointer window);

//create processing tab: show process UI
GtkWidget* create_processing_tab(GtkWidget* window);
#endif
