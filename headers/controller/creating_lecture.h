/*
 * File: creating_lecture.h
 *
 * Author: GVLab
 *
 * Created on: Aug 13, 2015
 */
#ifndef CREATING_LECTURE
#define CREATING_LECTURE
#include "headers/base.h"
//add new video to txt file for processing video
void new_session_video(char *path, int session, char *txt_file_name, char *format, char *current_session);

// check number of session
int check_session(char *path, int first);

//get current time and return format d-m-y_h-m-s
void get_current_time(char *output);

//check directory when process video
void check_directory(char *home_path, char *course, char *version, 
	char *lecture, char *return_path, char *current_time);

//merge all video and create folder
void process_video(char *path, char *record_home, char *txt_file_name, int first, char *subject, 
								char *version, char *chapter, char *format, char *curr_time);

void save_time(char *time, char *path);

int get_time(char *output, char *path);

void remove_current_session_data(char *path, char* session_name, int session, char *file_name, char *format);
#endif
