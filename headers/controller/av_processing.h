/*
 * File: av_processing.h
 *
 * Author: GVLab
 *
 * Created on: Aug 13, 2015
 */
#ifndef AV_PROCESSING
#define AV_PROCESSING
#include "headers/base.h"
#include "headers/view/utils_view.h"
extern "C" {
#include <libavutil/imgutils.h>
}

//open input device base on ffmpeg library
Input* open_ffmpeg_input(InputInfo* devInfo);

//close input device base on ffmpeg library
void close_ffmpeg_input(Input* input);

//read input packet from input devices
int read_input_packet(void* in, AVPacket *out_packet);

//decode input video frame
int decode_input_video(void* in, AVPacket *packet, AVFrame *out_frame);

//decode input audio frame
int decode_input_audio(void* in, AVPacket *packet, AVFrame *out_frame);

//Add an audio output stream
AVStream *add_audio_stream(AVFormatContext *oc, OutputInfo dev_info);

//add a video output stream
AVStream *add_video_stream(AVFormatContext *oc, OutputInfo stInfo);

//open output
Output* open_output(OutputInfo* devInfo);

//close output stream
void close_output(Output* output);

//open input from devInfo information
Input* open_input(InputInfo* devInfo);

//close input devices
void close_input(Input* input);

/*Create convert between input and output*/
struct SwsContext *create_convert_context(Input* i, Output* o);

//encode and write video packet to output stream
int encode_write_video(AVFormatContext *oFmtCtx, AVStream* oVideoSt,
		AVFrame* oPicture, int64_t pts);

//encode and write audio packet to output stream
int encode_write_audio(AVFormatContext* oFmtCtx, AVStream *oSt,
		AVFrame *oSamples, int64_t pts);

//calculate size of a sample
int64_t cal_sample_size(AVStream* audioSt);

//get format context for input devices
AVFormatContext * open_format_context(InputInfo devInfo);

//Get video stream from format context and set codec for that stream.
AVStream* get_input_videoSt(AVFormatContext* ic);

//Get audio stream from format context and set codec for that stream.
AVStream* get_input_audioSt(AVFormatContext* ic, InputInfo* devInfo);
#endif
