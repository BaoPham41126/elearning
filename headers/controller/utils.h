/*
 * File: utils.h
 *
 * Author: GVLab
 *
 * Created on: Aug 13, 2015
 */
#ifndef UTILS_H
#define UTILS_H
#include "headers/base.h"

//Set default value for input properties
InputInfo* get_default_in();

//Set default value for output properties
OutputInfo* get_default_out();

//read input packet
int read_packet(Input* in, AVPacket *out_packet);

//decode video frame
int decode_video(Input* in, AVPacket *packet, AVFrame **out_frame);

//decode audio frame
int decode_audio(Input* in, AVPacket *packet, AVFrame **out_frame);

//get default screen size
int get_screen_size(int *w, int*h);

//read input file and return the content of this file
char *read_file(char *fileName);

//open /sys/class/video4linux directory and check all video4linux devices
void get_v4l_devices(V4lDevices *v4l_list);

//open /proc/asound directory and check all alsa cards
void get_alsa_devices(AlsaDevices *alsa_list);

//get devices name
void get_devices(V4lDevices *v4l, AlsaDevices *alsa);

//read output url and get file name
void get_filename(char* url, char *tmp_file);

//read output url and get file name
void get_dev_name(char* name, char *tmp_name);

//read output url and get format
void get_format(char* url, char *tmp_format);

//get number of invideo
void get_num_in_video(OutputInfo *out, char *result);

//get stream name when stream is changed
void get_stream_name(char *stream, char *stream_name);

//get capicity
gchar *get_capacity(gchar *dev_path);

//get free space
gchar *get_free_space(gchar *dev_path);

//get space information
char* get_space_info(gchar *dev_path, char *tmp);
#endif
