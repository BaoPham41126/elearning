/*
 * File: recording_streaming.h
 *
 * Author: GVLab
 *
 * Created on: Aug 13, 2015
 */

#ifndef RECORDING_STREAMING
#define RECORDING_STREAMING

extern "C" {
#include <libavutil/imgutils.h>
#include <libavutil/samplefmt.h>
#include <libavutil/opt.h>
#include <SDL/SDL.h>
#include <SDL/SDL_thread.h>

#include <sys/time.h>
#include <time.h>
#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <glib.h>
}

#include "headers/controller/utils.h"
#include "headers/controller/av_processing.h"

//preview screen
#define NUM_WIDTH 3
#define NUM_HEIGHT 2

#define NOT_PREVIEW 0
#define PREVIEW 1

#define NOT_RUNNING 0
#define RUNNING 1

//GTK element for display buffer, input, output
extern DisplayBuffer *display_buffer[MAX_OUT];
extern DisplayInputStatus *input_status;

//socket to put sdl window
extern GtkWidget *sock;
extern GtkWidget *window;

//create interface of function preview_button_click
void preview_button_click(GtkWidget *widget, gpointer label);

//onkey function
gboolean on_key_press (GtkWidget *widget, GdkEventKey *event, gpointer user_data);

//update preview id
void update_preview_id(int id);

//start record
int start_stream(InputInfo** inDevs, int numI, OutputInfo** outDevs, int numO);

//stop record
int stop_stream();

//show video table
void show_video_table();

//hide video table
void hide_video_table();

//keypress function
void key_press( char *key);

//connect input to output
void connect_in_out(InputInfo** inDevs);

//read audio level base on audio frame
void read_sample_level(AVFrame *frame, int input_id);

//show audio level
static gboolean show_level(gpointer audio);

//show program buffer
static gboolean show_buffer(gpointer buff);

//update preview id base on id user select
void update_preview_id(int out_id);

//update preview mode
void update_preview_mode(int mode);

/*Output video*/
void* out_video(void* output_data);

/*Output audio*/
void* out_audio(void* output_data);

/*Output thread*/
void* output_stream(void* output_data);

/*Decode packet audio and send to output*/
void decode_send_audio(Input* in, AVPacket* i_packet);

/*Copy i_frame data to o_frame data (only for picture frame)*/
void copy_frame(AVFrame ** o_frame, AVFrame *i_frame, AVCodecContext* codec);

/*Copy i_frame data to o_frame data (only for picture frame)*/
void copy_frame(AVFrame ** o_frame, AVFrame *i_frame, AVCodecContext* codec);

//scale frame from input context to output context
void scale_to_output(AVFrame ** o_frame, AVFrame *i_frame, 
	AVCodecContext* codec, struct SwsContext* convert);

/*Show video frame to SDL screen*/
void preview_frame(AVFrame *frame, PreviewInfo pre_info);

/*Decode packet video and send to output*/
void decode_send_video(Input* in, AVPacket *i_packet);

//controll frame rate
void frame_rate_control(Input* in);

/*The input thread get frame and send to output through a queue*/
void* input_stream(void* input_data);

//the sdl thread
void *controll_sdl(void *data);

//init control stream data
void init_control_video_stream(InputInfo** inDevs, int numIn, OutputInfo** outDevs, int numOut);

//cannot open input
void not_open_input();

//close sdl thread
void close_sdl();

//start recording
int start_stream(InputInfo** inDevs, int numI, OutputInfo** outDevs, int numO);

//close sdl thread
void close_sdl();

#endif

