/*
 * File: uploading.h
 *
 * Author: GVLab
 *
 * Created on: Aug 13, 2015
 */
#ifndef UPLOADING
#define UPLOADING
extern "C" {
#include <stdio.h>
#include <curl/curl.h>
#include <sys/stat.h>
#include <fcntl.h>
}
#include "headers/base.h"
#include "headers/view/utils_view.h"

#define RETURN_OK 0
#define RETURN_ERROR 1

//upload a file from local path to server_url
int upload_to_ftp_server(char *local_path, char *server_url, char *file_name);

//connect to ftp server with ip, user name, password
int connect_to_server(char *server, char *path, char *user, char *pass);

//check if given name is a file in specific folder
int is_file(char *string);

//upload all file in this directory, if it has subdir, upload_dir that subdir
int upload_dir(char *server, char *local);

//upload file to server
void upload_to_server(char *server, char *server_path, char *user, 
    char *pass, char *local_path, GtkWidget *window);
#endif
