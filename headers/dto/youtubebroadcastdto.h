#ifndef YOUTUBEBROADCASTDTO_H
#define YOUTUBEBROADCASTDTO_H
#include "headers/dto/youtubestreamdto.h"
#include <QString>
class YoutubeBroadcastDto{
public:
    YoutubeBroadcastDto(){}
    YoutubeBroadcastDto(QString id, QString title, QString privacyStatus, QString lifeCycleStatus){
        this->id = id;
        this->title = title;
        this->privacyStatus = privacyStatus;
        this->lifeCycleStatus = lifeCycleStatus;
    }
    QString id;
    QString title;
    QString privacyStatus;
    QString lifeCycleStatus;
    QString boundStreamId;
    YoutubeStreamDto* boundStream;
    QString getAddress(){ return "https://www.youtube.com/watch?v=" + id; }
};
#endif // BROADCASTDTO_H
