#ifndef YOUTUBECREDENTIALDTO_H
#define YOUTUBECREDENTIALDTO_H
class YoutubeCredentialDto
{
public:
    YoutubeCredentialDto(QString accessToken, QString refreshToken){
        this->accessToken = accessToken;
        this->refreshToken = refreshToken;
    }

    YoutubeCredentialDto(){}
    QString accessToken;
    QString refreshToken;
};
#endif // YOUTUBECREDENTIALDTO_H
