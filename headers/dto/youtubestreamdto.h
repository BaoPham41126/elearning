#ifndef YOUTUBESTREAMDTO_H
#define YOUTUBESTREAMDTO_H
#include <QString>
class YoutubeStreamDto{
public:
    YoutubeStreamDto(){}
    QString id;
    QString title;
    QString address;
    QString streamStatus;
    QString healthStatus;
};
#endif // STREAMDTO_H
