#include "headers/youtube/authentication.h"
#include <QUrl>
#include <QUrlQuery>
#include <QDesktopServices>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>

#include <QJsonDocument>
#include <QJsonObject>

#include <QFile>
#include <QTextStream>
#include <iostream>

void Authentication::grantPermission(){
    static QString authURL = "https://accounts.google.com/o/oauth2/v2/auth";
    static QString scope = "https://www.googleapis.com/auth/youtube";
    QUrl url(authURL);
    QUrlQuery query;
    query.addQueryItem("scope", scope);
    query.addQueryItem("redirect_uri", "http://localhost/code");
    query.addQueryItem("response_type", "code");
    query.addQueryItem("client_id", this->clientId);
    url.setQuery(query);
    QDesktopServices::openUrl(url);
}

void Authentication::getToken(QString code){
    static QString tokenURL = "https://www.googleapis.com/oauth2/v4/token";
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    QUrl url(tokenURL);
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/x-www-form-urlencoded");

    QUrlQuery query;
    query.addQueryItem("client_id", this->clientId);
    query.addQueryItem("client_secret", this->clientSecret);
    query.addQueryItem("code", code);
    query.addQueryItem("grant_type", "authorization_code");
    query.addQueryItem("redirect_uri", "http://localhost/code");

    QByteArray data;
    data.append(query.toString());

    QObject::connect(manager, &QNetworkAccessManager::finished, this, &Authentication::onTokenRequestFinished);
    manager->post(request, data);
}

void Authentication::onTokenRequestFinished(QNetworkReply *reply){
    if (reply->error() != QNetworkReply::NoError){
        std::cout << reply->errorString().toStdString() << std::endl;
        return;
    }
    QString data = (QString) reply->readAll();
    QJsonDocument jsonResponse = QJsonDocument::fromJson(data.toUtf8());
    QJsonObject json = jsonResponse.object();
    Credential *credential = new Credential(json["access_token"].toString(), json["refresh_token"].toString());
    credential->saveToFile();
    Q_EMIT authenticateFinished(credential);
}

void Authentication::refreshToken(QString refreshToken){
    this->_refreshToken = refreshToken;
    static QString tokenURL = "https://www.googleapis.com/oauth2/v4/token";
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    QUrl url(tokenURL);
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/x-www-form-urlencoded");

    QUrlQuery query;
    query.addQueryItem("client_id", this->clientId);
    query.addQueryItem("client_secret", this->clientSecret);
    query.addQueryItem("refresh_token", refreshToken);
    query.addQueryItem("grant_type", "refresh_token");
    QByteArray data;
    data.append(query.toString());

    QObject::connect(manager, &QNetworkAccessManager::finished, this, &Authentication::onTokenRefreshFinished);
    manager->post(request, data);
}

void Authentication::onTokenRefreshFinished(QNetworkReply *reply){
    if (reply->error() != QNetworkReply::NoError){
        std::cout << reply->errorString().toStdString() << std::endl;
        return;
    }
    QString data = (QString) reply->readAll();
    QJsonDocument jsonResponse = QJsonDocument::fromJson(data.toUtf8());
    QJsonObject json = jsonResponse.object();
    Credential *credential = new Credential(json["access_token"].toString(), this->_refreshToken);
    credential->saveToFile();
    Q_EMIT refreshFinished(credential);
}

Credential::Credential(QString accessToken, QString refreshToken){
    this->accessToken = accessToken;
    this->refreshToken = refreshToken;
}

bool Credential::readFromFile(){
    QFile file("credential.txt");
    if (!file.exists() || !file.open(QIODevice::ReadOnly | QIODevice::Text))
        return false;

    QTextStream in(&file);
    // You could use readAll() here, too.
    this->accessToken = in.readLine();
    this->refreshToken = in.readLine();

    file.close();
    return true;
}

void Credential::saveToFile(){
    QFile file("credential.txt");
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
        return;

    QTextStream out(&file);
    out << this->accessToken << "\n" << this->refreshToken;
    file.close();
}
