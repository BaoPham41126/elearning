#include "headers/youtube/broadcastbuilder.h"
#include "headers/youtube/checkstatusjob.h"
#include <QUrl>
#include <QUrlQuery>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>

#include <QJsonDocument>
#include <QJsonObject>

#include <iostream>

BroadcastBuilder::BroadcastBuilder(Credential *credential){
    this->youtubeAPI = new YoutubeAPI(credential);
    QObject::connect(youtubeAPI, &YoutubeAPI::broadcastCreated, this, &BroadcastBuilder::onBroadcastCreated);
    QObject::connect(youtubeAPI, &YoutubeAPI::streamCreated, this, &BroadcastBuilder::onStreamCreated);
    QObject::connect(youtubeAPI, &YoutubeAPI::bindCompleted, this, &BroadcastBuilder::onBindCompleted);
    QObject::connect(youtubeAPI, &YoutubeAPI::transitionCompleted, this, &BroadcastBuilder::onBroadcastStarted);
    this->broadcast = NULL;
    this->stream = NULL;
}

void BroadcastBuilder::createLiveStream(QString title, QDateTime startTime, QString privacyStatus, QString cdnFormat){
    youtubeAPI->createNewBroadcast(title, startTime, privacyStatus);
    youtubeAPI->createNewStream(title + "Default Stream", cdnFormat);
}

void BroadcastBuilder::onStreamCreated(YoutubeStream *stream){
    this->stream = stream;
    this->_bind();
}

void BroadcastBuilder::onBroadcastCreated(Broadcast *broadcast){
    this->broadcast = broadcast;
    this->_bind();
}

void BroadcastBuilder::_bind(){
    if (this->broadcast != NULL && this->stream != NULL){
        youtubeAPI->bindStreamToBroadcast(this->stream->id, this->broadcast->id);
    }
}

void BroadcastBuilder::onBindCompleted(Broadcast *broadcast){
    this->broadcast = broadcast;
    this->broadcast->boundStreamAddress = this->stream->address;
    Q_EMIT created(this->broadcast, this->stream);
}

void BroadcastBuilder::goLive(){
    youtubeAPI->changeBroadcastStatus(this->broadcast->id, "live");
}

void BroadcastBuilder::onBroadcastStarted(Broadcast *broadcast){
    this->broadcast = broadcast;
    Q_EMIT started(this->broadcast);
}

void BroadcastBuilder::checkStreamStatus(){
    CheckStatusJob *thread = new CheckStatusJob(this->youtubeAPI, this->stream->id);
    QObject::connect(thread, &CheckStatusJob::streamReturn, this, &BroadcastBuilder::onStreamRefeshComplete);
    thread->run();
}

void BroadcastBuilder::onStreamRefeshComplete(YoutubeStream *stream){
    std::cout << ("Stream status: " + stream->streamStatus).toStdString() << std::endl;
    if (stream->streamStatus != this->stream->streamStatus){
        this->stream = stream;
        Q_EMIT streamStatusChanged(stream->streamStatus);
    }
}
