#include "headers/youtube/youtubehandler.h"

YoutubeHandler::YoutubeHandler(DisplayYoutube* display_youtube){
    this->display_youtube = display_youtube;
}
void YoutubeHandler::onBroadcastReturn(Broadcast **broadcasts, int length){
    if (broadcasts == NULL){
        std::cout << "No active broadcast" << std::endl;
    }
    for(int i = 0; i < length; i++){
        Broadcast *broadcast = broadcasts[i];
        insertTable(display_youtube->tableChannel, broadcast->getAddress().toLatin1().data(), broadcast->title.toLatin1().data(), broadcast->boundStreamAddress.toLatin1().data(), broadcast->lifeCycleStatus.toLatin1().data(), broadcast->id.toLatin1().data(), broadcast->boundStreamId.toLatin1().data());
        std::cout << broadcast->title.toStdString() << " " << broadcast->boundStreamId.toStdString() << " " << broadcast->getAddress().toStdString() << std::endl;
    }
}

void YoutubeHandler::onBroadcastCreated(Broadcast *broadcast){
    insertTable(display_youtube->tableChannel, broadcast->getAddress().toLatin1().data(), broadcast->title.toLatin1().data(), broadcast->boundStreamAddress.toLatin1().data(), broadcast->lifeCycleStatus.toLatin1().data(), broadcast->id.toLatin1().data(), broadcast->boundStreamId.toLatin1().data());
    //std::cout << broadcast->title.toStdString() << " " << broadcast->boundStreamId.toStdString() << " " << broadcast->getAddress().toStdString() << std::endl;
}

void YoutubeHandler::onBroadcastDeleted(QString broadcastId){
    //insertTable(display_youtube->tableChannel, broadcast->getAddress().toLatin1().data(), broadcast->title.toLatin1().data(), broadcast->boundStreamAddress.toLatin1().data(), broadcast->lifeCycleStatus.toLatin1().data(), broadcast->id.toLatin1().data());
    std::cout << "Delete complete" << std::endl;
}

AuthenticationHandler::AuthenticationHandler(DisplayYoutube *display_youtube)
{
    this->display_youtube = display_youtube;
}

void AuthenticationHandler::onAuthenticateFinished(Credential *credential){
    display_youtube->credential = credential;
    gtk_widget_set_sensitive (display_youtube->grant_permission_button, FALSE);
    gtk_label_set_text ((GtkLabel*) display_youtube->account_connect_status, "Connected");
    get_all_upcoming_broadcast();
}

void AuthenticationHandler::onTokenRefreshFinished(Credential *credential){
    display_youtube->credential = credential;
    std::cout << "Refresh token complete: " << credential->accessToken.toStdString() << std::endl;
    get_all_upcoming_broadcast();
}
