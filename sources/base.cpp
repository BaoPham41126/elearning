#include "headers/base.h"

//status bar
GtkWidget *status_bar;
char tmp_status[STRING_LENGTH];
gint context_id;

//network status bar
GtkWidget *network_status_bar;
char tmp_network[STRING_LENGTH];
gint network_context_id;

int network_refresh_time;
int network_break_times;
int network_down_times;
int status_code;
