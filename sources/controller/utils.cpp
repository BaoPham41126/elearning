/*
 * File: utils.c
 *
 * Author: GVLab
 *
 * Created on: Aug 13, 2015
 */

#include "headers/controller/utils.h"

int frame_count = 0;
pthread_t thread;

/*
 * Function:  get_default_out 
 * --------------------
 * Set default output information.
 * --------------------
 *  Return: Output information.
 */
OutputInfo* get_default_out() {
	int i;
	OutputInfo* out;
	out = (OutputInfo*) malloc(sizeof(OutputInfo));
	strcpy(out->url, "");
	strcpy(out->format, "flv");
	out->disable = 0;
	out->test = 0;

	strcpy(out->in_audio, "");
	out->audio_bit_rate = 64000;
	out->sample_fmt = AV_SAMPLE_FMT_S32P;
	out->sample_rate = 44100;
	out->num_channel = 2;
	out->num_video = 0;
	for(i = 0; i< MAX_OUT_VIDEO; i++){
		strcpy(out->in_video[i], "");
		strcpy(out->in_video_key[i], "");
		out->in_video_type[i] = 0;
		out->in_video_state[i] = 0;
	}
	out->frame_rate = 25;
	out->pix_fmt = AV_PIX_FMT_YUV420P;
	out->video_bit_rate = 400000;
	out->width = 480;
	out->height = 360;
	out->gop_size = 10;
	out->q_max = 50;
	out->q_min = 10;
	out->video_codec = AV_CODEC_ID_H264;
	out->video_queue_size = 300;
	out->max_buf_rate = 10;
	out->audio_queue_size = 100000;
	out->output_type = 0;
	out->state = STATE_NORMAL;
	out->video_stream = NOT_USE_STREAM;
	out->audio_stream = NOT_USE_STREAM;
	out->audio_codec = AV_CODEC_ID_AAC;
	out->baseline = 0;
}

/*
 * Function:  get_default_in 
 * --------------------
 * Set default input information.
 * --------------------
 *  Return: Input information.
 */
InputInfo* get_default_in() {
	InputInfo* in;
	in = (InputInfo*) malloc(sizeof(InputInfo));
	in->a_out_num = 0;
	in->v_out_num = 0;
	in->sample_rate = 44100;
	in->frame_rate = 25;
	strcpy(in->driver, "");
	strcpy(in->name_device, "");
	in->width = 640;
	in->height = 480;
	in->state = STATE_NORMAL;
}

/*
 * Function:  read_packet 
 * --------------------
 * Call read packet function to read packet.
 * --------------------
 *  Return: Read packet status.
 */
int read_packet(Input* in, AVPacket *out_packet) {
	return in->read_packet(in, out_packet);
}

/*
 * Function:  get_screen_size 
 * --------------------
 * Get default screen size.
 *
 *	width: Width get from display.
 *
 * 	height: Height get from display.
 * --------------------
 *  Return: 0 is valid.
 *			-1, -2 is error.
 */
int get_screen_size(int *width, int*height){
	Display* pdsp = NULL;
	Screen* pscr = NULL;

	//open screen display
	pdsp = XOpenDisplay( NULL );
	if ( !pdsp ) {
		fprintf(stderr, "Failed to open default display.\n");
		return -1;
	}

	//get screen information
	pscr = DefaultScreenOfDisplay( pdsp );
	if ( !pscr ) {
		fprintf(stderr, "Failed to obtain the default screen of given display.\n");
		return -2;
	}
	*width = pscr->width;
	*height = pscr->height;

	//close display
	XCloseDisplay( pdsp );
	return 0;
}

/*
 * Function:  read_file 
 * --------------------
 * Read input file and return the content of this file.
 *
 *	filename: File need to be opened.
 * --------------------
 *  Return: Content of file.
 */
char *read_file(char *fileName){
    FILE *file = fopen(fileName, "r");
    char *code;
    size_t n = 0;
    int c;
    char tmp_end = '\n';
    if (file == NULL)
        return NULL; //could not open file

    code = (char*) malloc(50);

    //get characters
    while ((c = fgetc(file)) != EOF)
    {
        if(tmp_end == (char) c){
        	code[n] = '\0'; 
        } else {
        	code[n++] = (char) c;	
        }
    }

    return code;
}

/*
 * Function:  get_v4l_devices 
 * --------------------
 * Open /sys/class/video4linux directory and check all video4linux devices.
 *
 *	v4l_list: List contain all video4linux devices in system.
 */
void get_v4l_devices(V4lDevices *v4l_list){
	char buf[50];
    char tmp[] = "video";
    v4l_list->num_device = 0;
    DIR *d;
    struct dirent *dir;
    FILE *file;
    char *tmp_name;

    //open 
    d = opendir("/sys/class/video4linux");
    if (d){
        while ((dir = readdir(d)) != NULL){
            if(dir->d_name[0] == tmp[0] && dir->d_name[1] == tmp[1]){
                //get input
                sprintf(v4l_list->name[v4l_list->num_device], "/dev/%s", dir->d_name);

                //get input display name
                sprintf(buf, "/sys/class/video4linux/%s/name", dir->d_name);
                tmp_name = read_file(buf);
                sprintf(v4l_list->display_name[v4l_list->num_device], "%s (%s)", tmp_name, dir->d_name);

                //update number of devices
                v4l_list->num_device++;
            }
        }

        //close dir
        closedir(d);
    }
}

/*
 * Function:  get_alsa_devices 
 * --------------------
 * Open /proc/asound directory and check all alsa cards.
 *
 *	alsa_list: List contain all alsa devices in system.
 */
void get_alsa_devices(AlsaDevices *alsa_list){
	char buf[50];
    char tmp[] = "cards";
    alsa_list->num_device = 0;
    DIR *d;
    struct dirent *dir;
    FILE *file;
    char *tmp_name;

    //open 
    d = opendir("/proc/asound");
    if (d){
        while ((dir = readdir(d)) != NULL){
            if(dir->d_name[0] == tmp[0] && dir->d_name[4] != tmp[4]){                
                //set file name
                sprintf(buf, "/proc/asound/%s/id", dir->d_name);
                
                //read file
                tmp_name = read_file(buf);
                
                //get input display name
                sprintf(alsa_list->display_name[alsa_list->num_device], "plughw:CARD=%s", tmp_name);
                
                //get input
                sprintf(alsa_list->name[alsa_list->num_device], "plughw:CARD=%s", tmp_name);
                
                //update number of devices
                alsa_list->num_device++;
            }
        }
        closedir(d);
    }
}

/*
 * Function:  get_devices 
 * --------------------
 * Get all video4linux devices and alsa devices in system.
 *
 *	v4l_list: List contain all video4linux devices in system.
 *
 *	alsa_list: List contain all alsa devices in system.
 */
void get_devices(V4lDevices *v4l, AlsaDevices *alsa){
	//get alsa devices
	get_alsa_devices(alsa);

	//get video4linux devices
    get_v4l_devices(v4l);
}

/*
 * Function:  get_filename 
 * --------------------
 * Read output url and get file name.
 *
 *	url: Path to file.
 *
 *	tmp_file: Result file name.
 */
void get_filename(char* url, char *tmp_file){
	char tmp[LONG_STRING_LENGTH];
	strcpy(tmp, url);
	const char s[2] = "/";
	const char c[2] = ".";
	char *token;

	// get the first token
	token = strtok(tmp, s);
	// walk through other tokens
	while( token != NULL ) 
	{	
		strcpy(tmp_file, token);

		token = strtok(NULL, s);
	}

	//set file name
	token = strtok(tmp_file, c);
	strcpy(tmp_file, token);
}

/*
 * Function:  get_dev_name 
 * --------------------
 * Get device name from name given.
 *
 *	name: Full name of device.
 *
 *	tmp_name: Result device's name.
 */
void get_dev_name(char* name, char *tmp_name){
	char tmp[50];
	strcpy(tmp, name);
	const char s[2] = "/";
	char *token;

	// get the first token
	token = strtok(tmp, s);
	//walk through other tokens
	while( token != NULL ) 
	{	
		//copy device name
		strcpy(tmp_name, token);

		token = strtok(NULL, s);
	}
}

/*
 * Function:  get_format 
 * --------------------
 * Get output format from url given.
 *
 *	url: Path to file.
 *
 *	tmp_format: Result file format.
 */
void get_format(char* url, char *tmp_format){
	char tmp[LONG_STRING_LENGTH];
	strcpy(tmp, url);
	const char s[2] = ".";
	char *token;

	// get the first token
	token = strtok(tmp, s);

	// walk through other tokens
	while( token != NULL ) 
	{	
		//copy format
		strcpy(tmp_format, token);

		token = strtok(NULL, s);
	}
}

/*
 * Function:  get_num_in_video 
 * --------------------
 * Get number of input video map to given output.
 *
 *	out: Output stream information.
 *
 *	result: Number of in_video in a video stream.
 *			Format: "* in 1". * is number of input video.
 */
void get_num_in_video(OutputInfo *out, char *result){
	int i;
	int num = 0;

	//get number of input video in given stream
	for(i = 0; i < out->num_video; i++){
		if(out->in_video_state[i] == 0){
			num++;
		}
	}

	//print result
	sprintf(result, "%d in 1", num);
}

/*
 * Function:  get_stream_name 
 * --------------------
 * Get stream name when stream is changed.
 *
 *	stream: Output stream name.
 *
 *	stream_name: Result stream name.
 */
void get_stream_name(char *stream, char *stream_name){
	char tmp_string[STRING_LENGTH];

	const char c[2] = " ";
	char *token;
	strcpy(tmp_string, stream);

	//get stream name
	token = strtok(tmp_string, c);
	strcpy(stream_name, token);
}

/*
 * Function:  get_capacity 
 * --------------------
 * Get capicity of given path.
 *
 *	dev_path: Path to check capicity.
 * --------------------
 *  Return: Total amount of capicity in given folder.
 */
gchar *get_capacity(gchar *dev_path){
	unsigned long long result = 0;
	int n;
	gchar s_cap[50];
	gchar * ss_cap = "N/A";
	struct statvfs sfs;
	if ( statvfs ( dev_path, &sfs) != -1 ){
		result = (unsigned long long)sfs.f_bsize * sfs.f_blocks;
	}
	if (result > 0){
		double f_cap = (double)result/(1024*1024*1024);
		n = sprintf(s_cap, "%.2f GB", f_cap);
		ss_cap = g_strdup(s_cap);
	}
	return ss_cap;
}

/*
 * Function:  get_free_space 
 * --------------------
 * Get free space of given path.
 *
 *	dev_path: Path to check free space.
 * --------------------
 *  Return: Total amount of free space in given folder.
 */
gchar *get_free_space(gchar *dev_path){
	unsigned long long result = 0;
	int n;
	gchar s_cap[50];
	gchar * ss_cap = "N/A";
	struct statvfs sfs;
	if ( statvfs ( dev_path, &sfs) != -1 ){
		result = (unsigned long long)sfs.f_bsize * sfs.f_bfree;
	}
	if (result > 0){
		double f_cap = (double)result/(1024*1024*1024);
		n = sprintf(s_cap, "%.2f GB", f_cap);
		ss_cap = g_strdup(s_cap);
	}
	return ss_cap;
}

/*
 * Function:  get_space_info 
 * --------------------
 * Get free space and capicity in given path.
 *
 *	dev_path: Path to check free space.
 *
 *	tmp: String result.
 * --------------------
 *  Return: Free space in capicity in current folder.
 */
char* get_space_info(gchar *dev_path, char *tmp){
    gchar *space;

    //get free space
    space = get_free_space(dev_path);
    strcpy(tmp, space);

    //get capacity
    space = get_capacity(dev_path);
    sprintf(tmp, "%s free/%s", tmp, space);
    return tmp;
}
