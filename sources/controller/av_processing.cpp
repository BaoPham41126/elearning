/*
 * File: av_processing.c
 *
 * Author: GVLab
 *
 * Created on: Aug 13, 2015
 */

#include "headers/controller/av_processing.h"
#include "headers/model/frame_queue.h"

#include "iostream"
#include "fdk-aac/aacenc_lib.h"
/*
 * Function:  open_format_context 
 * --------------------
 * Open device and get AVFormatContext of input device base on its information.
 *
 *  devInfo: Information of input device.
 * --------------------
 *  Return: AVFormatContext of input device.
 */
AVFormatContext * open_format_context(InputInfo devInfo){
	AVFormatContext *ic = avformat_alloc_context();
	AVInputFormat* iformat = NULL;
	int ret;
	char a_char[10];
	AVDictionary *options = NULL;

	snprintf(a_char, 10, "%d", devInfo.sample_rate);
	av_dict_set(&options, "sample_rate", a_char, 0);
	snprintf(a_char, 10, "%dx%d", devInfo.width, devInfo.height);
	av_dict_set(&options, "video_size", a_char, 0);


	iformat = av_find_input_format(devInfo.driver);

	ret = avformat_open_input(&ic, devInfo.name_device, iformat, &options);
	if (ret < 0) {
		printf("can't open device: %s\n", devInfo.name_device);
        sprintf(tmp_status, "can't open device: %s", devInfo.name_device);

        push_item(status_bar, GINT_TO_POINTER(context_id), tmp_status);
		return NULL;
	}
	/* If not enough info to get the stream parameters, we decode the
	 first frames to get it. (used in mpeg case for example) */

	ret = avformat_find_stream_info(ic, NULL);
	av_dump_format(ic, 0, ic->filename, 0);
	if (ret < 0) {
		printf("%s: could not find codec parameters\n", devInfo.name_device);
		avformat_close_input(&ic);
		sprintf(tmp_status, "can't find codec for %s", devInfo.name_device);
		
        push_item(status_bar, GINT_TO_POINTER(context_id), tmp_status);

		return NULL;
	}
	return ic;
}

/*
 * Function:  get_input_video_stream 
 * --------------------
 * Find decoder and open input device to get video stream.
 *
 *  ic: Format context of input devices.
 * --------------------
 *  Return: Video stream of input device.
 */
AVStream* get_input_video_stream(AVFormatContext* ic) {
	int i;
	AVStream* ivideo_st = NULL;
	AVCodec* icodec;
	AVDictionary *options = NULL;

	// get video stream id
	for (i = 0; i < ic->nb_streams; i++) {
		if (ic->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO) {
			ivideo_st = ic->streams[i];
		}
	}

	//check if input device has video stream
	if (ivideo_st == NULL) {
		printf("no input video stream\n");
		return NULL;
	}

	//find decoder for video stream
    AVRational avg_frame_rate;
    avg_frame_rate.num = 1;
    avg_frame_rate.den = 10;
    ivideo_st->avg_frame_rate = avg_frame_rate;
    //ivideo_st->avg_frame_rate = (AVRational){ 1, 10 };
	icodec = avcodec_find_decoder(ivideo_st->codec->codec_id);
	if (!icodec) {
		printf("can't find input decoder");
		sprintf(tmp_status, "can't find input decoder");
        push_item(status_bar, GINT_TO_POINTER(context_id), tmp_status);
		return NULL;
	}

	//open device
    if (avcodec_open2(ivideo_st->codec, icodec, &options) < 0) {
		printf("can not open input decoder\n");
		sprintf(tmp_status, "can't open input decoder");
        push_item(status_bar, GINT_TO_POINTER(context_id), tmp_status);
		return NULL;
	}
	
	//set video stream codec value and return
	ivideo_st->codec->codec = icodec;
	return ivideo_st;
}

/*
 * Function:  get_input_audio_stream 
 * --------------------
 * Find decoder and open input device to get audio stream.
 *
 *  ic: Format context of input devices.
 *
 *  devInfo: Information of input devices
 * --------------------
 *  Return: Audio stream of input device.
 */
AVStream* get_input_audio_stream(AVFormatContext* ic, InputInfo* devInfo) {
	int i;
	AVStream* iAudioSt = NULL;
	AVCodec* icodec;
	AVDictionary *options = NULL;

	//get audio stream id
	for (i = 0; i < ic->nb_streams; i++) {
		if (ic->streams[i]->codec->codec_type == AVMEDIA_TYPE_AUDIO) {
			iAudioSt = ic->streams[i];
		}
	}

	//check if input has audio stream
	if (iAudioSt == NULL) {
		printf("no input audio stream\n");
		return NULL;
	}

	//set audio stream value
	iAudioSt->codec->bit_rate = 128000;
	iAudioSt->codec->sample_fmt = AV_SAMPLE_FMT_S32P;
	iAudioSt->codec->sample_rate = devInfo->sample_rate;
	iAudioSt->time_base.den = devInfo->sample_rate;

	//find audio decoder
	icodec = avcodec_find_decoder(iAudioSt->codec->codec_id);
	if (!icodec) {
		printf("can't find input decoder");
		sprintf(tmp_status, "can't find input decoder");
		
        push_item(status_bar, GINT_TO_POINTER(context_id), tmp_status);
		return NULL;
	}

    //open input to get audio stream
    if (avcodec_open2(iAudioSt->codec, icodec, &options) < 0) {
		printf("can not open input decoder\n");
		sprintf(tmp_status, "can't open input decoder");
		
        push_item(status_bar, GINT_TO_POINTER(context_id), tmp_status);
		return NULL;
	}

	//set audio codec and return audio stream
	iAudioSt->codec->codec = icodec;
	return iAudioSt;
}

/*
 * Function:  open_ffmpeg_input 
 * --------------------
 * Get audio stream and video stream of input.
 *
 *  devInfo: Information of input devices
 * --------------------
 *  Return: Input with audio stream or video stream.
 */
Input* open_ffmpeg_input(InputInfo* devInfo) {
	Input* input;
	AVFormatContext * fmtCtx;

	//init data
	input = (Input*) malloc(sizeof(Input));
	fmtCtx = open_format_context(*devInfo);
	if(!fmtCtx)
		return NULL;

	// set value for input
	input->format = fmtCtx;
	input->kind = FFMPEG_SUPPORT;
	input->read_packet = &read_input_packet;
	input->frame_rate = devInfo->frame_rate;

	//get video stream of input
	input->v_stream = get_input_video_stream(fmtCtx);
	if (input->v_stream) {
		input->decode_video = &decode_input_video;
	}

	//get audio stream of input
	input->a_stream = get_input_audio_stream(fmtCtx, devInfo);
    if (input->a_stream) {
		input->decode_audio = &decode_input_audio;
	}
	return input;
}

/*
 * Function:  close_ffmpeg_input 
 * --------------------
 * Close input device.
 *
 *  input: Opened input device .
 */
void close_ffmpeg_input(Input* input) {
	//get format context
	AVFormatContext * fmtCtx;
	fmtCtx = (AVFormatContext*) input->format;

	//close input
	avformat_close_input(&fmtCtx);
	if(input){
		free(input);
		input = NULL;
	}
}

/*
 * Function:  read_input_packet 
 * --------------------
 * Read packet from input.
 *
 *  in: Opened input device.
 *
 *  out_packet: Packet get from input.
 * --------------------
 *  Return: Status of read packet function.
 * 			 Return 0 if OK, < 0 on error or end of file.
 */
int read_input_packet(void* _in, AVPacket *out_packet) {
	//get format context
	AVFormatContext *fmt_ctx;
    Input* in = (Input*) _in;
	fmt_ctx = (AVFormatContext *) in->format;

	//read packet and return
	return av_read_frame(fmt_ctx, out_packet);
}

/*
 * Function:  decode_input_video 
 * --------------------
 * Decode and get video frame from packet.
 *
 *  in: Opened input device.
 *
 *  packet: Packet from input.
 *
 *  out_frame: Video frame get from packet
 * --------------------
 *  Return: A negative error code is returned if an error occurred during decoding.
 *  		 Otherwise the number of bytes consumed from the input.
 */
int decode_input_video(void* _in, AVPacket *packet, AVFrame *out_frame) {
    Input* in = (Input*) _in;
	AVCodecContext *codec;
	int len, got_frame;

	//get codec
	codec = in->v_stream->codec;

	//decode and get video frame
    len = avcodec_decode_video2(codec, out_frame, &got_frame, packet);

	// return value
	return got_frame;
}

/*
 * Function:  decode_input_audio 
 * --------------------
 * Decode and get audio frame from packet.
 *
 *  in: Opened input device.
 *
 *  packet: Packet from input.
 *
 *  out_frame: Audio frame get from packet
 * --------------------
 *  Return: A negative error code is returned if an error occurred during decoding.
 *  		 Otherwise the number of bytes consumed from the input.
 */
int decode_input_audio(void* _in, AVPacket *packet, AVFrame *out_frame) {
    Input* in = (Input*) _in;
	AVCodecContext *codec;
	int len, got_frame;

	//get codec
	codec = in->a_stream->codec;

    //decode audio
    len = avcodec_decode_audio4(codec, out_frame, &got_frame, packet);
	return got_frame;
}

/*
 * Function:  add_video_stream 
 * --------------------
 * Create new video stream for output base on its information 
 *  and open this stream to write data.
 *
 *  oc: Format context of output stream.
 *
 *  stInfo: Output information.
 * --------------------
 *  Return: Opened video stream of output.
 */
AVStream *add_video_stream(AVFormatContext *oc, OutputInfo stInfo) {
	AVCodecContext *c;
	AVStream *st;
	AVCodec *codec;
	AVDictionary * options = NULL;
	char bit_rate[20];

	// init new stream for output
	st = avformat_new_stream(oc, NULL);
	if (!st) {
		fprintf(stderr, "Could not alloc stream\n");
		exit(1);
	}
	// find the video encoder
	codec = avcodec_find_encoder(stInfo.video_codec);

	if (!codec) {
		fprintf(stderr, "codec not found\n");
		exit(1);
	}
	c = avcodec_alloc_context3(codec);

	//set video stream value
	c->thread_count = 1;
	c->codec_id = stInfo.video_codec;
	c->time_base.den = stInfo.frame_rate;
	c->time_base.num = 1;
	c->gop_size = stInfo.gop_size; // emit one intra frame every twelve frames at most
	c->pix_fmt = stInfo.pix_fmt; //PIX_FMT_YUV420P;
	c->height = stInfo.height;
	c->width = stInfo.width;
	c->bit_rate = stInfo.video_bit_rate;

	//set dictionary value for output stream
	snprintf(bit_rate, 20, "%d", stInfo.video_bit_rate);
	av_dict_set(&options, "b:v", bit_rate, 0);
	if (stInfo.video_codec == AV_CODEC_ID_H264)
		av_opt_set(c->priv_data, "tune", "zerolatency", 0);
	if(stInfo.baseline == 1){
		av_opt_set(c->priv_data, "profile", "baseline", AV_OPT_SEARCH_CHILDREN);
	}

	// some formats want stream headers to be separate
	if (oc->oformat->flags & AVFMT_GLOBALHEADER)
		c->flags |= CODEC_FLAG_GLOBAL_HEADER;
	if (codec->capabilities & CODEC_CAP_TRUNCATED)
		c->flags |= CODEC_FLAG_TRUNCATED; // we do not send complete frames

	//open output stream to write data
	if (avcodec_open2(c, codec, &options) < 0) {
		printf("can not open output encoder\n");
		exit(1);
	}

	// set codec
	c->codec = codec;
	st->codec = c;

	//return stream
	return st;
}

/*
 * Function:  add_audio_stream 
 * --------------------
 * Create new audio stream for output base on its information 
 *  and open this stream to write data.
 *
 *  oc: Format context of output stream.
 *
 *  stInfo: Output information.
 * --------------------
 *  Return: Opened audio stream of output.
 */
AVStream *add_audio_stream(AVFormatContext *oc, OutputInfo dev_info) {
	AVCodecContext *c;
	AVStream *st;
	AVCodec *codec;

	AVDictionary * options = NULL;
	int i;

	//create new stream
	st = avformat_new_stream(oc, NULL);
	if (!st) {
		fprintf(stderr, "Could not alloc stream\n");
		exit(1);
	}

	//set codec value to open stream
	c = st->codec;
	c->codec_id = dev_info.audio_codec;
	if(c->codec_id == AV_CODEC_ID_AAC)
		c->profile = FF_PROFILE_AAC_MAIN;

	c->codec_type = AVMEDIA_TYPE_AUDIO;
	c->sample_fmt = dev_info.sample_fmt;

	c->sample_rate = dev_info.sample_rate;
	c->channels = dev_info.num_channel;

	c->bit_rate = dev_info.audio_bit_rate;
	c->channel_layout = AV_CH_LAYOUT_STEREO;
    
	// some formats want stream headers to be separate
	if (oc->oformat->flags & AVFMT_GLOBALHEADER)
		c->flags |= CODEC_FLAG_GLOBAL_HEADER;
	
	// find the audio encoder
	codec = avcodec_find_encoder(c->codec_id);
	if (!codec) {
		fprintf(stderr, "codec not found\n");
		exit(1);
	}

	// set channel layout
	if(c->codec_id == AV_CODEC_ID_AAC){
		c->channel_layout = AV_CH_LAYOUT_STEREO;
	} else {
		c->channel_layout = AV_CH_LAYOUT_MONO;
	}

	//get channel
    c->channels  = av_get_channel_layout_nb_channels(c->channel_layout);
	c->sample_fmt  = (codec)->sample_fmts ?
            (codec)->sample_fmts[0]: AV_SAMPLE_FMT_FLTP;
    AVRational time_base;
    time_base.num = 1;
    time_base.den = c->sample_rate;
    st->time_base = time_base;
    //st->time_base = (AVRational){ 1, c->sample_rate };
    //open output
	if (avcodec_open2(c, codec, &options) < 0) {
		fprintf(stderr, "could not open codec\n");
		exit(1);
	}
	
	//set codec and return audio stream
	c->codec = codec;
	return st;
}

/*
 * Function:  open_output 
 * --------------------
 * Init output data and add video stream, audio stream to output.
 * Write output header.
 *
 *  devInfo: Output information.
 * --------------------
 *  Return: Opened output.
 */
Output* open_output(OutputInfo* devInfo) {
	Output* out;
	AVOutputFormat *oformat;
	Stream* audio, *video;
	AVCodecContext * aCodec, *vCodec;

	//init data
	out = (Output*) malloc(sizeof(Output));
	out->audio = NULL;
	out->video = NULL;
	out->disable = devInfo->disable;
	out->buffer_status = NORMAL;
	out->max_buf_rate = (float) devInfo->max_buf_rate/100;

	//create format context for output
	avformat_alloc_output_context2(&out->oformat, NULL, devInfo->format,
			devInfo->url);


	//set format context data
	oformat = out->oformat->oformat;
	if(!out->disable){
		//set audio data, add audio stream and create audio buffer
        if (strcmp(devInfo->in_audio, "") != 0) {
			out->audio = (Stream*) malloc(sizeof(Stream));
			audio = out->audio;

			audio->oformat = out->oformat;
			
			//add audio stream
			audio->stream = add_audio_stream(out->oformat, *devInfo);
			audio->codec = audio->stream->codec;
			audio->stream_stop = 0;

			//init buffer
			frame_queue_init(&audio->input_queue, devInfo->audio_queue_size);

		}

		//set video data, add video stream and create video buffer
		if (strcmp(devInfo->in_video[0], "") != 0) {
			out->video = (Stream*) malloc(sizeof(Stream));
			video = out->video;
			video->oformat = out->oformat;

			//add video stream
			video->stream = add_video_stream(out->oformat, *devInfo);

			//create video buffer
			frame_queue_init(&video->input_queue,
					devInfo->video_queue_size * devInfo->frame_rate);

			//set video stream data
			video->codec = video->stream->codec;
			video->stream_stop = 0;
			vCodec = video->codec;

            video->frame = av_frame_alloc();

            av_image_alloc(video->frame->data, video->frame->linesize, vCodec->width, vCodec->height, vCodec->pix_fmt, 1);

			video->frame->width = vCodec->width;
			video->frame->height = vCodec->height;

		}

		if (!(oformat->flags & AVFMT_NOFILE)) {
			if (avio_open(&out->oformat->pb, devInfo->url, AVIO_FLAG_WRITE) < 0) {
				fprintf(stderr, "Could not open '%s'\n", devInfo->url);
				
                sprintf(tmp_status, "Could not open '%s'", devInfo->name);

                //push_item(status_bar, GINT_TO_POINTER(context_id), tmp_status);

				return NULL;
			}
		}

		//write header for output
		avformat_write_header(out->oformat, NULL);
	}
	return out;
}

/*
 * Function:  close_output 
 * --------------------
 * Close output and free memory of output.
 *
 *  output: Output data.
 */
void close_output(Output* output) {
	AVFormatContext* format_ctx = output->oformat;

	//write trailer for output
	av_write_trailer(format_ctx);

	//close audio stream of output
	if (output->audio) {
		avcodec_close(output->audio->codec);
		frame_queue_deinit(&output->audio->input_queue);
		if(output->audio){
			free(output->audio);
			output->audio = 0;
		}
		output->audio = NULL;
	}

	//close video stream of output
	if (output->video) {
		if(output->video->frame){
			av_free(output->video->frame);
			output->video->frame = NULL;
		}
		
		avcodec_close(output->video->codec);
		frame_queue_deinit(&output->video->input_queue);
		if(output->video){
			free(output->video);
			output->video = NULL;
		}
	}
	if (!(format_ctx->oformat->flags & AVFMT_NOFILE) && format_ctx->pb) {
		if (avio_close(format_ctx->pb) < 0) {
			fprintf(stderr, "Could not close '%s'\n", format_ctx->filename);
		}
	}

	//free memory of output
	avformat_free_context(format_ctx);
	if(output){
		free(output);
		output = NULL;
	}
}

/*
 * Function:  create_convert_context 
 * --------------------
 * Create convert context for converting video frame from input to output.
 *
 *  i: Input device.
 *
 *  o: Output.
 * --------------------
 *  Return: Convert context for converting video frame from input to output.
 */
struct SwsContext *create_convert_context(Input* i, Output* o) {
	struct SwsContext* result = NULL;
	AVCodecContext* iCodec;
	AVCodecContext* oCodec;
	AVFormatContext* f;

	//get codec of input and output
	iCodec = i->v_stream->codec;
	oCodec = o->video->stream->codec;

	//check if need to create convert context
	if (iCodec->pix_fmt != oCodec->pix_fmt || iCodec->width != oCodec->width
			|| iCodec->height != oCodec->height || 1) {
		//create convert context
		result = sws_getContext(iCodec->width/*360*/, iCodec->height/*640*/,
				iCodec->pix_fmt, oCodec->width, oCodec->height, oCodec->pix_fmt,
				SWS_FAST_BILINEAR, NULL, NULL, NULL);
	}

	//return data
	return result;
}

/*
 * Function:  encode_write_video 
 * --------------------
 * Encode video frame and write it to output.
 *
 *  oFmtCtx: Format context of output.
 *
 *  oVideoSt: Output video stream.
 *
 *  oPicture: Video frame need to be wriren to output.
 *
 *  pts: Time pts of oPicture in output stream.
 * --------------------
 *  Return: Size of packet write to output.
 */
int encode_write_video(AVFormatContext *oFmtCtx, AVStream* oVideoSt,
		AVFrame* oPicture, int64_t pts) 
{
	int out_size;
	int got_frame;
	AVCodecContext* oVCodecCtx;
	AVPacket oVideoPkt;
	uint8_t outVideoBuf[200000];
    log4c_category_t* catLog = NULL;
    catLog = log4c_category_get("log.app.stream");
	int ret = -1;

	//get codec
	oVCodecCtx = oVideoSt->codec;

	//init packe to write to
	av_init_packet(&oVideoPkt);

	//encode video frame
	avcodec_encode_video2(oVCodecCtx, &oVideoPkt, oPicture, &got_frame);

	//set packet data
	oVideoPkt.stream_index = oVideoSt->index;
	oVideoPkt.pts = pts;
	out_size = oVideoPkt.size;

	// write the compressed frame to a output
	if (got_frame) {
		ret = av_interleaved_write_frame(oFmtCtx, &oVideoPkt);
		if(ret != 0){
			fprintf(stderr, "Error while writing video frame\n");
			printf("Error while writing video frame\n");
			
			//show to status bar
			status_code = WRITE_PACKET_ERROR;

			//show error instatus bar if write frame error
            log4c_category_log(catLog, LOG4C_PRIORITY_INFO, "Error while writing video frame");
		}
	}

	//free packet
	av_free_packet(&oVideoPkt);

	//return
	return out_size;
}

/*
 * Function:  open_input 
 * --------------------
 * Call open_ffmpeg_input function to open input device.
 *
 *  devInfo: Input information.
 * --------------------
 *  Return: Opened Input.
 */
Input* open_input(InputInfo* devInfo) {
	Input* input;

	input = open_ffmpeg_input(devInfo);

	return input;
}

/*
 * Function:  close_input 
 * --------------------
 * Call close_ffmpeg_input function to close input device.
 *
 *  devInfo: Input information.
 */
void close_input(Input* input) {
	close_ffmpeg_input(input);
}

/*
 * Function:  cal_sample_size 
 * --------------------
 * Calculate audio sample size base on audio stream information.
 *
 *  audioSt: Audio information.
 * --------------------
 *  Return: Size of audio sample.
 */
int64_t cal_sample_size(AVStream* audioSt) {
	AVCodecContext* codec = audioSt->codec;

	//get byte per an audio sample
	int osize = av_get_bytes_per_sample(codec->sample_fmt);

	//calculate sample size
	int64_t sample_size = codec->frame_size * osize * codec->channels;

	//return size
	return sample_size;
}

/*
 * Function:  encode_write_audio 
 * --------------------
 * Encode audio frame and write it to output.
 *
 *  oFmtCtx: Format context of output.
 *
 *  oSt: Output audio stream.
 *
 *  oFrame: Audio frame need to be wriren to output.
 *
 *  pts: Time pts of oFrame.
 * --------------------
 *  Return: Size of packet write to output.
 */
int encode_write_audio(AVFormatContext* oFmtCtx, AVStream *oSt, AVFrame *oFrame,
		int64_t pts) 
{
	AVCodecContext *encCtx;
    AVPacket oPkt;
	int out_size;
	int got_packet;
	uint8_t outAudioBuf[FF_MIN_BUFFER_SIZE];
	int ret = -1;

    log4c_category_t* catLog = NULL;
    catLog = log4c_category_get("log.app.stream");

	//get audio codec
	encCtx = oSt->codec;

    //init audio packet
	av_init_packet(&oPkt);

    //encode audio frame to packet
    oFrame->pts = pts;
    oPkt.pts = pts;

    ret = avcodec_encode_audio2(encCtx, &oPkt, oFrame, &got_packet);
    if (ret < 0) {
        std::cout << "Error encoding audio frame: " <<  ret << std::endl;
    }
	//set packet time and index
	oPkt.stream_index = oSt->index;
	oPkt.pts = pts;
	out_size = oPkt.size;

	//write packet to output
    if (got_packet) {
		ret = av_interleaved_write_frame(oFmtCtx, &oPkt);
		if ( ret != 0) {

			fprintf(stderr, "Error while writing audio frame\n");
			printf("Error while writing audio frame\n");

			//show to status bar
			status_code = WRITE_PACKET_ERROR;
			
			//show error to status bar when write error
            log4c_category_log(catLog, LOG4C_PRIORITY_INFO, "Error while writing audio frame");
		}

	} 

	//free packet and return
	av_free_packet(&oPkt);
	return out_size;
}

