/*
 * File: uploading.c
 *
 * Author: GVLab
 *
 * Created on: Aug 13, 2015
 */

#include "headers/controller/uploading.h"

//synced file name
char sync_file[] = ".synced";

/*
 * Function:  upload_to_ftp_server 
 * --------------------
 * Upload a file from local path to ftp server.
 *
 *  local_path: Local path to processed data.
 *
 *  server_url: Server path to upload data (include user name, password).
 *              Format: ftp://name:pass@server_ip/path.
 *
 *  file_name: File need to be uploaded.
 * --------------------
 * Return: Return upload status. 
 *         RETURN_OK is upload ok. RETURN_ERROR is upload error.
 */
int upload_to_ftp_server(char *local_path, char *server_url, char *file_name){
	CURL *curl;
    CURLcode res;
    struct stat file_info;
    double speed_upload, total_time;
    FILE *fd;
    char tmp_string[200];

    //get file location
    sprintf(tmp_string, "%s/%s", local_path, file_name);
    fd = fopen(tmp_string, "rb"); /* open file to upload */ 
    if(!fd) {
        return RETURN_ERROR; /* can't continue */ 
    }

    /* to get the file size */ 
    if(fstat(fileno(fd), &file_info) != 0) {

        return RETURN_ERROR; /* can't continue */ 
    }

    curl = curl_easy_init();
    if(curl) {
        /* upload to this place */ 
        sprintf(tmp_string, "%s/%s", server_url, file_name);
        curl_easy_setopt(curl, CURLOPT_URL, tmp_string);

        /* tell it to "upload" to the URL */ 
        curl_easy_setopt(curl, CURLOPT_UPLOAD, 1L);

        /* set where to read from (on Windows you need to use READFUNCTION too) */ 
        curl_easy_setopt(curl, CURLOPT_READDATA, fd);

        /* and give the size of the upload (optional) */ 
        curl_easy_setopt(curl, CURLOPT_INFILESIZE_LARGE,
                     (curl_off_t)file_info.st_size);

        /* enable verbose for easier tracing */ 
        curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);

        curl_easy_setopt(curl, CURLOPT_FTP_CREATE_MISSING_DIRS, 1L);
        // curl_easy_setopt(curl, CURLOPT_QUOTE, headers);


        res = curl_easy_perform(curl);
        /* Check for errors */ 
        if(res != CURLE_OK) {
            fprintf(stderr, "curl_easy_perform() failed: %s\n",
                curl_easy_strerror(res));

            curl_easy_cleanup(curl);
            return RETURN_ERROR;
        }
        else {
            /* now extract transfer info */ 
            curl_easy_getinfo(curl, CURLINFO_SPEED_UPLOAD, &speed_upload);
            curl_easy_getinfo(curl, CURLINFO_TOTAL_TIME, &total_time);

            fprintf(stderr, "Speed: %.3f bytes/sec during %.3f seconds\n",
                speed_upload, total_time);

            curl_easy_cleanup(curl);

            return RETURN_OK;
        }
        /* always cleanup */ 
    }
    return RETURN_ERROR;
}

/*
 * Function:  connect_to_server 
 * --------------------
 * Connect to ftp server with ip, user name, password.
 *
 *  server: FTP server ip.
 *
 *  user: User name to login to ftp server.
 *
 *  file_name: File need to be uploaded.
 * --------------------
 * Return: Return connect status. 
 *         RETURN_OK is connect ok. RETURN_ERROR is connect error.
 */
int connect_to_server(char *server, char *path, char *user, char *pass){
	char tmp_url[200];

	//get full url
	sprintf(tmp_url, "ftp://%s:%s@%s", user, pass, server);
	CURL *curl;
    CURLcode res;

    curl = curl_easy_init();
    if(curl) {
        curl_easy_setopt(curl, CURLOPT_URL, tmp_url);

        res = curl_easy_perform(curl);
        /* Check for errors */ 
        if(res != CURLE_OK) {

            curl_easy_cleanup(curl);
            return RETURN_ERROR;
        }
        else {
            curl_easy_cleanup(curl);

            return RETURN_OK;
        }
    }
}

/*
 * Function:  is_file 
 * --------------------
 * Check a path given is a file or not.
 *
 *  string: Path to file or folder.
 * --------------------
 * Return: Path is file or not. 
 *         Return 1 is file. Return 0 is not a file.
 */
int is_file(char *string){
    char tmp[STRING_LENGTH];
    const char c[2] = ".";
    char *token;

    //copy
    strcpy(tmp, string);

    //check
    token = strtok(tmp, c);
    token = strtok(NULL, c);
    if(token){
        return 1;
    } else {
        return 0;
    }
}

/*
 * Function:  upload_dir 
 * --------------------
 * Upload all file in given path to server.
 * If it has subdir, upload_dir that subdir.
 *
 *  server: Server path to upload data (include user name, password).
 *          Format: ftp://name:pass@server_ip/path.
 *
 *  local: Local folder contain data need to be uploaded.
 * --------------------
 * Return: Upload status. 
 *         Return RETURN_ERROR is uploading error. Return RETURN_OK is uploading ok.
 */
int upload_dir(char *server, char *local){
    DIR *d;
    struct dirent *dir;
    char tmp[] = ".";
    char server_full[SHORT_STRING_LENGTH];
    char local_full[SHORT_STRING_LENGTH];
    
    int ret = RETURN_OK;

    //sync
    FILE *open_file;
    char file[SHORT_STRING_LENGTH];

    //open directory
    d = opendir(local);

    //read all file in directory
    if (d){ 
        while ((dir = readdir(d)) != NULL){
            if(dir->d_name[0] != tmp[0]){
                //get directory
                
                if(is_file(dir->d_name)){
                    //upload file
                    if(upload_to_ftp_server(local, server, dir->d_name) == RETURN_ERROR)
                        ret = RETURN_ERROR;
                } else {
                    //upload folder
                    sprintf(server_full, "%s/%s", server, dir->d_name);
                    sprintf(local_full, "%s/%s", local, dir->d_name);

                    //set .sync file location
                    sprintf(file, "%s/%s/%s", local, dir->d_name, sync_file);
                    
                    // check file exist
                    open_file = fopen(file,"r");

                    if(!open_file){
                        //upload subdir
                        if(upload_dir(server_full, local_full) == RETURN_ERROR){
                            //upload error
                            ret = RETURN_ERROR;
                        } else {
                            //create file
                            open_file = fopen(file,"w");
                        }
                    }
                }
            }
        }

        closedir(d);
    }
    return ret;
}

/*
 * Function:  upload_to_server 
 * --------------------
 * Upload file in given path to server.
 * If it has subdir, upload_dir that subdir.
 *
 *  server: Server ip.
 *
 *  server_path: Path in server to upload to.
 *
 *  user: User name to login to server.
 *
 *  pass: Password of user to login to server.
 *
 *  window: Main window of program.
 *
 *  local_path: Local folder contain data need to be uploaded.
 * --------------------
 * Return: Upload status. 
 *         Return RETURN_ERROR is uploading error. Return RETURN_OK is uploading ok.
 */
void upload_to_server(char *server, char *server_path, char *user, 
    char *pass, char *local_path, GtkWidget *window){
    char server_full[SHORT_STRING_LENGTH];
    int return_status;

    FILE *open_file;
    char file[SHORT_STRING_LENGTH];

    //get ftp server url
    sprintf(server_full, "ftp://%s:%s@%s/%s", user, pass, server, server_path); 

    //set .sync file location
    sprintf(file, "%s/%s", local_path, sync_file);

    // check file exist
    open_file = fopen(file,"r");

    if(!open_file){
        //upload all file in directory
        return_status = upload_dir(server_full, local_path);
        
        //create .synced file
        if(return_status == 0)
            open_file = fopen(file,"w");
    } else {
        return_status = 0;
    }

    //show status and return
    if(return_status == 0){
        push_item(status_bar, GINT_TO_POINTER(context_id), "Upload finished");
        create_quick_message("Upload finish!", window, 1);
    } else {
        push_item(status_bar, GINT_TO_POINTER(context_id), "Upload error");
        create_quick_message("Upload error!", window, 1);
    }
}
