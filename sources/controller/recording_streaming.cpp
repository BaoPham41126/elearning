/*
 * File: recording_streaming.c
 *
 * Author: GVLab
 *
 * Created on: Aug 13, 2015
 */

#include "headers/controller/recording_streaming.h"
#include "iostream"
//GTK element for display buffer, input, output
DisplayBuffer *display_buffer[MAX_OUT];
DisplayInputStatus *input_status;

//socket to put sdl window
GtkWidget *sock;
GtkWidget *window;

//control stream data
ControlStream * control_stream;

//Main data of program
pthread_t in_thread[MAX_OUT];
pthread_t out_thread[MAX_OUT];
pthread_t v_out_thread[MAX_OUT];
pthread_t a_out_thread[MAX_OUT];
pthread_t reset_thread;
pthread_t clear_video_thread[MAX_OUT];
pthread_t clear_audio_thread[MAX_OUT];

StatusKind status_kind[MAX_OUT];
pthread_mutex_t out_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mem_mutex = PTHREAD_MUTEX_INITIALIZER;
Output** output;
OutputInfo** output_devices;
Input* input[MAX_OUT];
InputInfo** input_devices;
int running = NOT_RUNNING;
//int numIn;
//int numOut;
//struct timeval start_time;
log4c_category_t* catLog = NULL;
//char buf[5];
pthread_t sdl_thread;
int64_t out_data = 0;

//network
int64_t total_packet_size = 0;
int64_t total_size = 0;
int network_break_time = 0;
int network_down_time = 0;
int check_live = 0;
int check_connection = 2;

//init preview info, contain data to convert frame to SDL data
PreviewInfo preview_info[MAX_OUT];

//if preview == 1, SDL screen will show
int preview = PREVIEW;
int width = 0, height = 0;
int quit = RUNNING;
int fullscreen = 0;
int reduce = 0;
int clear = 0;

//id of input will show to screen
int preview_id = 0;
int current_out_id = 0;
int preview_mode = ALL_IN_ONE;

//data of SDL Screen
SDL_Surface *screen = NULL;

//preview sound
static float audio_level[MAX_OUT];

//preview buffer
static float video_buffer[MAX_OUT];
static float last_video_buffer[MAX_OUT];
static float audio_buffer[MAX_OUT];
/*
 * Function:  decode_video
 * --------------------
 * Call decode function to decode video frame.
 * --------------------
 *  Return: Decode video frame status.
 */
int decode_video(Input* in, AVPacket *packet, AVFrame *out_frame) {
    return in->decode_video(in, packet, out_frame);
}

/*
 * Function:  decode_audio
 * --------------------
 * Call decode function to decode audio frame.
 * --------------------
 *  Return: Decode audio frame status.
 */
int decode_audio(Input* in, AVPacket *packet, AVFrame *out_frame) {
    return in->decode_audio(in, packet, out_frame);
}

/*
 * Function:  clear_queue_buffer 
 * --------------------
 * Clear buffer of input queue.
 * 
 *  queue: The queue to be clear.
 * --------------------
 * Return: No return.
 */
void* clear_queue_buffer(void *data){
	AVFrame* i_frame;
	FrameQueue *queue = (FrameQueue *) data;

	int num_frame = queue->size;
	//get all frame and clear
	while(num_frame >0 && frame_queue_get(queue, &i_frame, &running) != -1){

		pthread_mutex_lock(&mem_mutex);
		
		//free memory
		if(i_frame->opaque){
			free(i_frame->opaque);
			i_frame->opaque = NULL;
		}

		if(i_frame->data[0]){
			av_freep(&i_frame->data[0]);
			// free(i_frame->data[0]);
			i_frame->data[0] = NULL;
		}
		if(i_frame){

			av_free(i_frame);
			i_frame = NULL;
		}
		pthread_mutex_unlock(&mem_mutex);
		num_frame--;
	}
}

// void *clear_live_stream(void *data){
	
// }

/*
 * Function:  clear_live_buffer 
 * --------------------
 * Clear buffer of all LIVE output.
 * --------------------
 * Return: No return.
 */
void clear_live_buffer(){
	int i;

	for (i = 0; i < numOut; ++i){
		if(output[i]->type == LIVE && !output[i]->disable){
			//clear video queue
			if(output[i]->video){
				// clear_queue_buffer(&output[i]->video->input_queue);
				pthread_create(&clear_video_thread[i], NULL, clear_queue_buffer, &output[i]->video->input_queue);
			}

			if(output[i]->audio){
				pthread_create(&clear_audio_thread[i], NULL, clear_queue_buffer, &output[i]->audio->input_queue);
			}
		}
	}

	usleep(1000);

	for (i = 0; i < numOut; ++i){
		if(output[i]->type == LIVE && !output[i]->disable){
			//clear video queue
			if(output[i]->video){
				// clear_queue_buffer(&output[i]->video->input_queue);
				pthread_join(clear_video_thread[i], NULL);
			}

			if(output[i]->audio){
				pthread_join(clear_audio_thread[i], NULL);
			}
		}
	}
}

/*
 * Function:  check_network 
 * --------------------
 * Check network is broken or network speed slow down.
 * --------------------
 * Return: No return.
 */
int check_network(){
	int i;
	//network error
    if(total_packet_size <= 0 && check_live > 0){
    	network_break_time++;
    	if(network_break_time >= 3 && status_code != WRITE_PACKET_ERROR){
    		status_code = NETWORK_BREAK;
    		check_connection = 1;
    	}

    	if(network_break_time >= network_break_times){
    		clear_live_buffer();
    		network_break_time = 0;
    	}
    	
    } else {
    	network_break_time = 0;
    	int check_down = 0;
    	for(i = 0; i < numOut; i++){
    		if(output[i]->type == LIVE && !output[i]->disable){
    			if(video_buffer[i] > 0.1){
    				check_down = 1;
    			} 
    		}
    	}
    	
    	if(check_down == 1){
    		network_down_time++;

			if(network_down_time >= 3){
	    		status_code = NETWORK_DOWN;
	    		check_connection = 2;
	    	} else {
	    		status_code = NO_ERROR;
	    	}

			if(network_down_time >= network_down_times){
	    		clear_live_buffer();
	    		network_down_time = 0;
	    		
	    	}
    	} else {
    		network_down_time--;
			if(network_down_time < 0){
				network_down_time = 0;
			}

			status_code = NO_ERROR;
    	}
    }
}

/*
 * Function:  reset_stream 
 * --------------------
 * Close stream with stream's index.
 * 
 *  output_index: The index of stream to be reset.
 * --------------------
 * Return: No return.
 */
void reset_stream(int output_index){
	Output *out = output[output_index];
	int i, j;
	if(!out->disable){

		//stop pushing frame to output
		out->status = STOP;

		//close output thread
		if (out->audio) {
			pthread_cond_signal(out->audio->input_queue.cond);
			pthread_join(a_out_thread[output_index], NULL);
		}
		if (out->video) {
			pthread_cond_signal(out->video->input_queue.cond);
			pthread_join(v_out_thread[output_index], NULL);
		}
		
		//check output thread closed
		while(1){
			if(out->audio){
				if(out->audio->stream_stop == STOP){
					if(out->video){
						if(out->video->stream_stop == STOP)
							break;
					} else {
						break;
					}
				}
			} else {
				if(out->video){
					if(out->video->stream_stop == STOP)
						break;
				} else {
					break;
				}
			}
		}

		out->pause_buffer = PAUSE;

		//clear queue buffer
		//clear video queue
		if(out->video){
			clear_queue_buffer(&out->video->input_queue);
		}

		//clear audio queue
		if(out->audio){
			clear_queue_buffer(&out->audio->input_queue);
		}

		//close output
		close_output(out);
		usleep(200);
		// //open output
		if(check_connection == 2)
			output_devices[output_index]->video_bit_rate /= 2; 
		do{
			out = open_output(output_devices[output_index]);
		} while(!out);
		output[output_index] = out;

		out->id = output_index;
		out->type = output_devices[output_index]->output_type;
		if(out->audio){
			out->audio->input_queue.output_id = output_index;
		}

		if(out->video){
			out->video->input_queue.output_id = output_index;
		}
		
		av_dump_format(out->oformat, output_index, out->oformat->filename, 1);

		for(i = 0; i< numIn; i++){
			//audio
			if(input[i]->a_stream){
				for(j = 0; j< input[i]->a_out_num; j++){
					if(input[i]->out_id[j] == output_index){
						input[i]->a_out_queue[j] = &out->audio->input_queue;
					}
				}
			} else if(input[i]->v_stream){
				for(j = 0; j< input[i]->v_out_num; j++){
					if(input[i]->out_id[j] == output_index){
						input[i]->v_out_queue[j] = &out->video->input_queue;
						input[i]->frame[j] = out->video->frame;
						input[i]->codec[j] = out->video->codec;
					}
				}
			}
		}

		//start stream
		out->pause_buffer = NORMAL;
		out->status = NORMAL;
		gettimeofday(&out->start_time, NULL);
		if (out->video) {
			pthread_create(&v_out_thread[output_index], NULL, out_video, out);
		}
		if (out->audio) {
			pthread_create(&a_out_thread[output_index], NULL, out_audio, out);
		}
	}
}

/*
 * Function:  reset_live_stream 
 * --------------------
 * Reset all live stream when connection is reset.
 * --------------------
 * Return: No return.
 */
void reset_live_stream(){
	int i;
	for(i = 0; i < numOut; i++){
		if(!output[i]->disable && output[i]->type == LIVE){
			printf("%d\n", i);
			reset_stream(i);

		}
	}
}

/*
 * Function:  get_current_pts 
 * --------------------
 * Get current packet time to syncing video.
 * 
 *  begin_time: Begin time of the recording.
 *	
 *  current_time: Current time of packet.
 * --------------------
 * Return: pts of the packet.
 */
int64_t get_current_pts(struct timeval *begin_time, struct timeval *current_time){
	int64_t usec;

	usec = (current_time->tv_sec - begin_time->tv_sec)*1000000;
	usec += (current_time->tv_usec - begin_time->tv_usec);

	return usec;
}

 /*
 * Function:  show_video_table 
 * --------------------
 * Get current Input device and output stream status. Show it to "In to out matrix".
 *
 */
void show_video_table(){
	int i, j;
	int in_id, out_id;
	char tmp[20];

	if(control_stream && running){

		//get in_id, out_id
		for(i = 0; i < control_stream->num; i++){
			//get input id, output_id
			in_id = control_stream->in_id[i];
			out_id = control_stream->out_id[i];
			//get key
			sprintf(tmp, "Ctrl+%s", control_stream->switch_key[i]);
			if(!output[out_id]->disable){
				if(!input_status->video[in_id][out_id]){
					//create new
                    input_status->video[in_id][out_id] = gtk_label_new(tmp);
					gtk_table_attach(GTK_TABLE(input_status->v_table), input_status->video[in_id][out_id], 
                        in_id+1, in_id+2, out_id+1, out_id+2, GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 3, 3);
					gtk_widget_show(input_status->video[in_id][out_id]);
				} else {
                    gtk_label_set_text((GtkLabel*)input_status->video[in_id][out_id], tmp);
					gtk_widget_show(input_status->video[in_id][out_id]);
				}
			}
		}

		//update current stream
		for(i = 0; i < numIn; i++){
			for(j = 0; j < input[i]->v_out_num; j++){
				in_id = i;
				out_id = input[i]->out_id[j];
				//set current text
				if(!output[out_id]->disable){
					if(input[i]->in_video_state[j] == 1 && input_status->video[in_id][out_id]){
                        gtk_label_set_text((GtkLabel*)input_status->video[in_id][out_id], "X");
					}
				}
			}
		}
	}
}

 /*
 * Function:  hide_video_table 
 * --------------------
 * Hide "In to out matrix" when stop recoring & streaming.
 *
 */
void hide_video_table(){
	int i, j;
	int in_id, out_id;

	if(control_stream && !running){

		//get in_id, out_id
		for(i = 0; i < control_stream->num; i++){
			//get input id, output_id
			in_id = control_stream->in_id[i];
			out_id = control_stream->out_id[i];
			// destroy
			if(!output[out_id]->disable){
				if(input_status->video[in_id][out_id]){
					gtk_widget_hide(input_status->video[in_id][out_id]);
				}
			}
		}
	}
}

/*
 * Function:  press_key 
 * --------------------
 * Calculate frame pts base on time duration and time_base.
 *
 *  duration: Time duration of output stream.
 *	
 *  time_base: Time base of output stream.
 * --------------------
 * Return: Frame pts.
 *
 */
void press_key( char *key){
	int i, j, k;
	int in_id = -1, out_id = -1;
	//check key
	if(control_stream && running){
		//get in_id, out_id
		for(i = 0; i < control_stream->num; i++){
			//find key
			if(strcmp(control_stream->switch_key[i], key) == 0){
				//get input id, output_id
				in_id = control_stream->in_id[i];
				out_id = control_stream->out_id[i];

				if(in_id >= 0 && out_id >= 0){
					//update video state, this will control which video_queue been sent from input video
					for(k = 0; k < numIn; k++){
						for(j = 0; j < input[k]->v_out_num; j++){
							if(input[k]->out_id[j] == out_id){
								
								if(in_id == k){
									//update in_video_state
									input[k]->in_video_state[j] = 1;

									//update preview id
									if(current_out_id == out_id)
										preview_id = k;
								} else
									input[k]->in_video_state[j] = 0;

								
							}
						}
						
					}
				}
			}
		}
		//update video table 
		show_video_table();
	}
}

/*
 * Function:  connect_in_out 
 * --------------------
 * Connect input and output:
 * Input will point to it output queue
 * Output will manage which queue input point to
 *
 *  inDevs: Information of input.
 *
 */
void connect_in_out(InputInfo** inDevs) {
	int i, a, v;
	int out_id;
	Stream* stream;
	int64_t o_samplesize;
	for (i = 0; i < numIn; i++) {
		//audio stream
		for (a = 0; a < inDevs[i]->a_out_num; a++) {
			out_id = inDevs[i]->a_out_id[a];
			stream = output[out_id]->audio;
			input[i]->out_id[a] = out_id;
			//get queue of the output
			input[i]->a_out_queue[a] = &stream->input_queue;
			o_samplesize = cal_sample_size(stream->stream);
			//fifo
			input[i]->fifo[a] = av_fifo_alloc(10 * o_samplesize);
            input[i]->o_samples[a] = (uint8_t*) malloc(o_samplesize);
			input[i]->stream[a] = output[out_id]->audio->stream;
			input[i]->o_samples_size[a] = o_samplesize;
		}

		//video stream
		input[i]->a_out_num = inDevs[i]->a_out_num;
		for (v = 0; v < inDevs[i]->v_out_num; v++) {
			out_id = inDevs[i]->v_out_id[v];
			stream = output[out_id]->video;
			input[i]->v_out_queue[v] = &stream->input_queue;
			
			//update video controller
			input[i]->in_video_state[v] = inDevs[i]->v_state[v];

			//update out_id
			input[i]->out_id[v] = out_id;

			//create convert context
			input[i]->imageConvert[v] = create_convert_context(input[i], output[out_id]);
			input[i]->frame[v] = output[out_id]->video->frame;
			input[i]->codec[v] = output[out_id]->video->codec;
		}
		input[i]->v_out_num = inDevs[i]->v_out_num;
	}
}


/*
 * Function:  cal_sample_level 
 * --------------------
 * Calculate audio level base on audio frame.
 *
 *  frame: Audio frame.
 *
 *  input_id: Id of input audio.
 *
 */
void cal_sample_level(AVFrame *frame, int input_id){
    float  window;
    int i;
    float sum = 0;

    //read audio sample
    for (i = 0; i < frame->nb_samples*2; ++i){
        window = (uint8_t)frame->data[0][i]/32768.0f;
        sum+= window*window;
    }

    //calculate audio level meter
    float rms = sqrt(sum / frame->nb_samples);
    float decibel = (-90 - 20 * log(rms))/23 ;
    audio_level[input_id] = decibel > 1.0 ? 1.0: (decibel < 0.0 ? 0.0: decibel);
}

/*
 * Function:  show_level 
 * --------------------
 * Show audio level to audio progressbar.
 *
 *  audio: Null value.
 *
 */
static gboolean show_level(gpointer audio) {
	int i;
	if(running){
		for(i = 0; i < numIn; i++){
			if(input_status->audio[i]){
				//show audio level
				gtk_progress_bar_set_fraction(GTK_PROGRESS_BAR(input_status->audio[i]), audio_level[i]);
			}
		}
	} else {
		return FALSE;
	}
}

/*
 * Function:  show_buffer 
 * --------------------
 * Show video buffer, audio buffer to UI. Buffer is memory used by output stream queue.
 *
 *  buff: Null value.
 *
 */
static gboolean show_buffer(gpointer buff) {
	int i;
	if(running){
		for(i = 0; i < numOut; i++){
			if(!output[i]->disable){
				//show video buffer
				if(display_buffer[i]->video_buffer){
					gtk_progress_bar_set_fraction(GTK_PROGRESS_BAR(display_buffer[i]->video_buffer), video_buffer[i]);
				}

				//show audio buffer
				if(display_buffer[i]->audio_buffer){
					gtk_progress_bar_set_fraction(GTK_PROGRESS_BAR(display_buffer[i]->audio_buffer), audio_buffer[i]);
				}
			}
		}
	} else {
		return FALSE;
	}
}

/*
 * Function:  calculate_band_width 
 * --------------------
 * Show current bandwidth.
 *
 *  buff: Null value.
 *
 */
static gboolean calculate_band_width(gpointer band_width){
	int i;
	if(running){
		if(status_code == WRITE_PACKET_ERROR){
			total_packet_size = 0;
		}

		// if(status_code == WRITE_PACKET_ERROR){

		// }

		pop_item(network_status_bar, GINT_TO_POINTER(network_context_id));
		float bandwidth = (float)total_packet_size * 8 / 1024 / network_refresh_time;

        sprintf(tmp_network, "%.1f kb/s", bandwidth);
        push_item(network_status_bar, GINT_TO_POINTER(network_context_id), tmp_network);

        check_network();

        //check if total network equal 0
		total_packet_size = 0;
	} else {
		return FALSE;
	}
}

/*
 * Function:  calculate_band_width 
 * --------------------
 * Show current bandwidth.
 *
 *  buff: Null value.
 *
 */
static gboolean show_status_bar(gpointer status){
	int i;
	if(running){
		switch(status_code){
			case NO_ERROR:
				sprintf(tmp_status, "Recording & Streaming");
				break;
			
			case WRITE_PACKET_ERROR:
				sprintf(tmp_status, "Error while wrtiting audio/video frame!");
				reset_live_stream();
				status_code = NO_ERROR;
				// pthread_create(&reset_thread, NULL, reset_live_stream, NULL);
				break;

			case NETWORK_BREAK:
				sprintf(tmp_status, "Network is broken!");
				break;

			case NETWORK_DOWN:
				sprintf(tmp_status, "Slow connection!");
				break;

			case DECODE_VIDEO_ERROR:
				sprintf(tmp_status, "Error while decoding video frame!");
				break;

			case DECODE_AUDIO_ERROR:
				sprintf(tmp_status, "Error while decoding audio frame!");
				break;
				
			case PACKET_INDEX_ERROR:
				sprintf(tmp_status, "Index of input's packet error!");
				break;
		}



		//update status bar
		pop_item(status_bar, GINT_TO_POINTER(context_id));
        push_item(status_bar, GINT_TO_POINTER(context_id), tmp_status);

	} else {
		return FALSE;
	}
}


/*
 * Function:  update_preview_id 
 * --------------------
 * Update preview id base on id user select.
 *
 *  out_id: Output id.
 *
 */
void update_preview_id(int out_id){
	int i, j;
	
	//check all input, get input point to out_id and have in_video_state = 1;
	for(i = 0; i < numIn; i++){
		for(j = 0; j < input[i]->v_out_num; j++){
			if(input[i]->out_id[j] == out_id && input[i]->in_video_state[j] == 1){
				preview_id = i;
			}
		}
	}

	//update current output id
	current_out_id = out_id;
}

/*
 * Function:  update_preview_mode 
 * --------------------
 * Update preview mode.
 *
 *  out_id: Output id.
 *
 */
void update_preview_mode(int mode){
	preview_mode = mode;
}

/*
 * Function:  out_video 
 * --------------------
 * Output video stream thread.
 * Get video frame from queue and write that frame to output video stream.
 *
 *  output_data: Output.
 *
 */
void* out_video(void* output_data) {
	Output *out;
	FrameQueue *queue;
	int nFrame, out_size;
	AVFrame* i_frame;
	AVFrame* o_frame;
	struct SwsContext* convert;
	int64_t pts;
	double *time;

	out = (Output *) output_data;
	queue = &out->video->input_queue;

	//Start getting video frame
    for (nFrame = 1; (running  || queue->size > 0); nFrame++) {
		if(out->status == STOP){
			break;
		}
		if(video_buffer[out->id] > out->max_buf_rate)
			out->buffer_status = PAUSE;
		else 
			out->buffer_status = NORMAL;

        if (frame_queue_get(queue, &i_frame, &running) != -1) {
			//calculate time pts
            time = (double*) i_frame->opaque;
			pts = *time / av_q2d(out->video->stream->time_base);
			// pts = av_frame_get_best_effort_timestamp(i_frame);
			// pts *= av_q2d(out->video->stream->time_base);
			// printf("%d\n", pts);
			pthread_mutex_lock(&out->mutex);
			//encode and write video frame
			out_size = encode_write_video(out->oformat, out->video->stream,
					i_frame, pts);
			if(out->type == LIVE){
				total_packet_size += out_size;
			}
			// printf("%d\n", out_size);
			pthread_mutex_unlock(&out->mutex);

			//calculate time duration
			pthread_mutex_lock(&mem_mutex);
			
			//free memory
			if(i_frame->opaque){
				free(i_frame->opaque);
				i_frame->opaque = NULL;
			}
			if(i_frame->data[0]){
				av_freep(&i_frame->data[0]);
				i_frame->data[0] = NULL;
			}
			if(i_frame){
				av_free(i_frame);
				i_frame = NULL;
			}
			pthread_mutex_unlock(&mem_mutex);
		}
	}

	if(out->status == STOP){
		out->video->stream_stop = STOP;
		printf("stop video\n");
	}
}

/*
 * Function:  out_audio 
 * --------------------
 * Output audio stream thread.
 * Get audio frame from queue and write that frame to output audio stream.
 *
 *  output_data: Output.
 *
 */
void* out_audio(void* output_data) {
	Output *out;
	FrameQueue *queue;
	int i, nFrame, out_size;
	AVFrame* i_frame;
	AVFrame* o_frame;
	int64_t duration, pts;
	AVStream* stream;
	double packet_time;
	int ret;

	//Start get frame
	out = (Output *) output_data;
	queue = &out->audio->input_queue;
	duration = 0;
	pts = 0;
	packet_time = 0;
    int j;
    for (nFrame = 1; (running  || queue->size > 0) ; nFrame++) {
		if(out->status == STOP){
			break;
        }
        if (frame_queue_get(queue, &i_frame, &running) != -1) {
			stream = out->audio->stream;
			//read audio sample from av_fifo

			o_frame = av_frame_alloc();
			o_frame->nb_samples = stream->codec->frame_size;

            // //fill audio sample
			avcodec_fill_audio_frame(o_frame, stream->codec->channels,
					stream->codec->sample_fmt, i_frame->data[0],
					i_frame->linesize[0], 0);

            //calculate audio frame pts
            pts = packet_time / av_q2d(out->audio->stream->time_base);

			//printf(">>>%d\n", pts);
			pthread_mutex_lock(&out->mutex);


            //write audio to output
			out_size = encode_write_audio(out->oformat, stream, o_frame,
                    pts);
			if(out->type == LIVE){
				total_packet_size += 0;
			}
			pthread_mutex_unlock(&out->mutex);

			packet_time = *(double*) i_frame->opaque;

			av_free(o_frame);

			//free data
			if(i_frame->data[0])
				free(i_frame->data[0]);
			if(i_frame->opaque)
				free(i_frame->opaque);
			if(i_frame)
                av_free(i_frame);
		}
	}

	if(out->status == STOP){
		printf("stop audio\n");
		out->audio->stream_stop = STOP;
    }

}

/*
 * Function:  output_stream 
 * --------------------
 * Create a thread for each video stream or audio stream.
 *
 *  output_data: Output.
 *
 */
void* output_stream(void* output_data) {
	int nFrame;
	FrameQueue *a_queue;
	Output *out;

	out = (Output *) output_data;

	//Create a thread each video or audio
	if (out->video) {
		pthread_create(&out->video->thread, NULL, out_video, output_data);
	}
	if (out->audio) {
		pthread_create(&out->audio->thread, NULL, out_audio, output_data);
	}

	//Wait for children thread finish
	if (out->video)
		pthread_join(out->video->thread, NULL);
	if (out->audio)
		pthread_join(out->audio->thread, NULL);
	return NULL;
}

/*
 * Function:  decode_send_audio 
 * --------------------
 * Decode audio packet and push it to output queue.
 *
 *  in: Input.
 *
 *	i_packet: input packet.
 *
 */
void decode_send_audio(Input* in, AVPacket* i_packet) {
	AVFrame* i_frame;
	AVFrame* o_frame;
	FrameQueue* queue;
	int i, j;
	double* time;

	uint8_t *i_samples = NULL;
	uint8_t *o_samples = NULL;
	int i_sample_size;
	int o_sample_size;
	AVStream *stream;
	AVFrame* o_tmp_frame;


	// AVFifoBuffer* fifo = NULL;

	struct timeval current_time;

    i_frame = av_frame_alloc();

    if (decode_audio(in, i_packet, i_frame)) {
		// pass all output map to current input
		in->a_stream->codec->frame_size = i_frame->nb_samples;
		for (i = 0; i < in->a_out_num; i++) {
			
			if(output[in->out_id[i]]->pause_buffer == NORMAL){
				queue = in->a_out_queue[i];
				// pthread_mutex_lock(&mem_mutex);
				
				//Copy input frame for each output queue
				o_frame = av_frame_alloc();
				*o_frame = *i_frame;

				//init audio frame
                o_frame->data[0] = (uint8_t*) malloc(sizeof(uint8_t) * i_frame->linesize[0]);

				//TODO: Filter audio here

				//fill audio data
				memcpy(o_frame->data[0], i_frame->data[0], i_frame->linesize[0]);

				//calculate audio level
				cal_sample_level(o_frame, in->input_id);

				// pthread_mutex_unlock(&mem_mutex);

				// here
				//write audio sample to av_fifo until av_fifo store enough samples
				i_samples = o_frame->data[0];
				i_sample_size = o_frame->linesize[0];

				av_fifo_generic_write(in->fifo[i], i_samples, i_sample_size, NULL);
                // }
                if(av_fifo_size(in->fifo[i]) >= in->o_samples_size[i]){
                    //set time
					time = (double*) malloc(sizeof(double));
					gettimeofday(&current_time, NULL);
					*time = (double) (get_current_pts(&output[queue->output_id]->start_time, &current_time)) / 1000000;

					stream = in->stream[i];
					
					av_fifo_generic_read(in->fifo[i], in->o_samples[i], in->o_samples_size[i], NULL);
					//fill audio sample
					o_tmp_frame = av_frame_alloc();
                    o_tmp_frame->data[0] = (uint8_t*) malloc(sizeof(uint8_t) * in->o_samples_size[i]);
					memcpy(o_tmp_frame->data[0], in->o_samples[i], in->o_samples_size[i]);
					// o_tmp_frame->linesize
					o_tmp_frame->linesize[0] = in->o_samples_size[i];
					o_tmp_frame->opaque = time;

                    // Put frame to outo_tmp_frameput queue
					while (frame_queue_put(queue, o_tmp_frame, &running) == -1 && running){

						usleep(10);
					}
				}

				//free data
				if(o_frame->data[0]){
					free(o_frame->data[0]);
					o_frame->data[0] = NULL;
				}
				if(o_frame->opaque){
					free(o_frame->opaque);
					o_frame->opaque = NULL;
				}
				if(o_frame){
					av_free(o_frame);
					o_frame = NULL;
				}
				
				//get audio buffer value
				audio_buffer[queue->output_id] = (float)(queue->size) / (float)queue->max_size;
			}
		}
		status_kind[in->input_id] = IS_NOT_DUPLICATE;
	} else {

		if(status_kind[in->input_id] == IS_NOT_DUPLICATE){
			//could not read audio frame
			status_code = DECODE_AUDIO_ERROR;
		}
	}

	//free audio frame
	if(i_frame){
		av_free(i_frame);
		i_frame = NULL;
	}
}

/*
 * Function:  copy_frame 
 * --------------------
 * Copy i_frame data to o_frame data (only for picture frame).
 *
 *  o_frame: Frame after copy.
 *
 *	i_frame: Input Frame.
 *
 *	codec: Input's codec context.
 *
 */
void copy_frame(AVFrame ** o_frame, AVFrame *i_frame, AVCodecContext* codec) {
	AVPicture* i_picture;
	AVPicture* o_picture;
	AVFrame *out;

	//alloc picture frame
	out = av_frame_alloc();
	*out = *i_frame;
	av_image_alloc(out->data, out->linesize, codec->width, codec->height,
			codec->pix_fmt, 1);

	//copy picture frame
	o_picture = (AVPicture *) out;
	i_picture = (AVPicture *) i_frame;
	av_picture_copy(o_picture, i_picture, codec->pix_fmt, codec->width,
			codec->height);
	*o_frame = out;
}

/*
 * Function:  scale_in_frame_to_out_frame 
 * --------------------
 * Scale frame from input context to output context.
 *
 *  o_frame: Frame after scale.
 *
 *	i_frame: Input Frame.
 *
 *	codec: Output's codec context.
 *
 * 	convert: Convert context for converting from input context to output context.
 *
 */
void scale_in_frame_to_out_frame(AVFrame ** o_frame, AVFrame *i_frame, 
	AVCodecContext* codec, struct SwsContext* convert)
{
	AVFrame *out;
	//alloc new frame
	out = av_frame_alloc();

	//alloc image
	av_image_alloc(out->data, out->linesize, codec->width, codec->height,
			codec->pix_fmt, 1);
	
	//set width, height
	out->width = codec->width;
	out->height = codec->height;
	out->format = i_frame->format;

	//scale input to output
	sws_scale(convert, i_frame->data, i_frame->linesize, 0,
		i_frame->height, out->data, out->linesize);

	*o_frame = out;
}

/*
 * Function:  preview_frame 
 * --------------------
 * Show video frame to SDL screen.
 *
 *  frame: Video frame.
 *
 *	pre_info: Preivew information.
 *
 */
void preview_frame(AVFrame *frame, PreviewInfo pre_info){
	//lock overlay
	SDL_LockYUVOverlay(pre_info.bmp[preview_mode]);

	//create picture and map to avframe data
	AVPicture pict;
    pict.data[0] = pre_info.bmp[preview_mode]->pixels[0];
    pict.data[1] = pre_info.bmp[preview_mode]->pixels[2];
    pict.data[2] = pre_info.bmp[preview_mode]->pixels[1];

    pict.linesize[0] = pre_info.bmp[preview_mode]->pitches[0];
    pict.linesize[1] = pre_info.bmp[preview_mode]->pitches[2];
    pict.linesize[2] = pre_info.bmp[preview_mode]->pitches[1];

    //scale data to SDL pix_fmt, width, height
    sws_scale(
        pre_info.convert, 
        (uint8_t const * const *)frame->data, 
        frame->linesize, 
        0,
        pre_info.codec->height,
        pict.data,
        pict.linesize
    );

    //unlock overlay
    SDL_UnlockYUVOverlay(pre_info.bmp[preview_mode]);
	
	//display to SDL screen
    SDL_DisplayYUVOverlay(pre_info.bmp[preview_mode], &pre_info.rect[preview_mode]);
}

/*
 * Function:  decode_send_video 
 * --------------------
 * Decode video packet and push it to output video queue.
 *
 *  in: Input.
 *
 *	i_packet: Input video packet.
 *
 */
void decode_send_video(Input* in, AVPacket *i_packet) {
	AVFrame* i_frame;
	AVFrame* o_frame;
	AVFrame* tmp_frame;
	struct SwsContext* convert;
	AVCodecContext* tmp_codec;
	FrameQueue* queue;
	int i, j;
	AVCodecContext *codec;
	double* time;
	
	codec = in->v_stream->codec;
	i_frame = av_frame_alloc();

	struct timeval current_time;

	//Decode video packet and get video frame
    if (decode_video(in, i_packet, i_frame)) {
		//push to live stream
		for (i = 0; i < in->v_out_num; i++) {
			if(in->in_video_state[i] == 1 && output[in->out_id[i]]->type == LIVE
				&& output[in->out_id[i]]->buffer_status == NORMAL && output[in->out_id[i]]->pause_buffer == NORMAL){

				convert = in->imageConvert[i];
				tmp_codec = in->codec[i];
				queue = in->v_out_queue[i];

				//get frame from input frame and scale to output context
				pthread_mutex_lock(&mem_mutex);
				if(convert){
					//scale
					scale_in_frame_to_out_frame(&o_frame, i_frame, tmp_codec, convert);
				} else {
					//not scale
					copy_frame(&o_frame, i_frame, codec);
				}
				
				pthread_mutex_unlock(&mem_mutex);

				//calculate and set time of frame
				time = (double*) malloc(sizeof(double));
				gettimeofday(&current_time, NULL);
				*time = (double) (get_current_pts(&output[queue->output_id]->start_time, &current_time)) / 1000000;
				
				o_frame->opaque = time;
				// printf(">>%f\n", *time);

				//push frame to output queue
				while (frame_queue_put(queue, o_frame, &running) == -1 && running && output[in->out_id[i]]->pause_buffer == NORMAL) {
					usleep(10);
				}
				//get video buffer value
				last_video_buffer[queue->output_id] = video_buffer[queue->output_id];
				video_buffer[queue->output_id] = (float)(queue->size) / (float)queue->max_size;
			}
		}

		//preview video frame
		switch(preview_mode){
			case ONE_BY_ONE:
			case FULL_SCREEN:
				if(in->input_id == preview_id && preview == PREVIEW){
					//get frame
					pthread_mutex_lock(&mem_mutex);
					copy_frame(&tmp_frame, i_frame, codec);
					pthread_mutex_unlock(&mem_mutex);

					//preview frame
					preview_frame(tmp_frame, preview_info[in->input_id]);
					//free frame
					if(tmp_frame->data[0]){
						av_freep(&tmp_frame->data[0]);
						tmp_frame->data[0] = NULL;
					}
					if(tmp_frame){
						av_free(tmp_frame);
						tmp_frame = NULL;
					}
				}
				break;
			case ALL_IN_ONE:
				if(preview == PREVIEW){
					//get frame
					pthread_mutex_lock(&mem_mutex);
					copy_frame(&tmp_frame, i_frame, codec);
					pthread_mutex_unlock(&mem_mutex);

					//preview frame
					preview_frame(tmp_frame, preview_info[in->input_id]);
					//free frame
					if(tmp_frame->data[0]){
						av_freep(&tmp_frame->data[0]);
						tmp_frame->data[0] = NULL;
					}
					if(tmp_frame){
						av_free(tmp_frame);
						tmp_frame = NULL;
					}
				}
				break;
			default:
				break;
		}

		// push video frame to hdd
		for (i = 0; i < in->v_out_num; i++) {
			if(in->in_video_state[i] == 1 && output[in->out_id[i]]->type == HDD 
				&& output[in->out_id[i]]->pause_buffer == NORMAL){

				convert = in->imageConvert[i];
				tmp_codec = in->codec[i];
				queue = in->v_out_queue[i];

				//get frame from input frame and scale to output context
				pthread_mutex_lock(&mem_mutex);
				if(convert){
					//scale
					scale_in_frame_to_out_frame(&o_frame, i_frame, tmp_codec, convert);
				} else {
					//not scale
					copy_frame(&o_frame, i_frame, codec);
				}
				pthread_mutex_unlock(&mem_mutex);
				time = (double*) malloc(sizeof(double));
				gettimeofday(&current_time, NULL);
				*time = (double) (get_current_pts(&output[queue->output_id]->start_time, &current_time)) / 1000000;
				//calculate and set time of frame
				o_frame->opaque = time;

				//push frame to output queue
				while (frame_queue_put(queue, o_frame, &running) == -1 && running && output[in->out_id[i]]->pause_buffer == NORMAL) {
					usleep(10);
				}
			}
		}
		status_kind[in->input_id] = IS_NOT_DUPLICATE;
	} else{
		if(status_kind[in->input_id] == IS_NOT_DUPLICATE){
			//could not read video frame
			status_code = DECODE_VIDEO_ERROR;
		}
	}

	//free memory
	if (in->kind == DECKLINK) {
		av_free(i_frame->data[0]);
		i_frame->data[0] = NULL;
	}
	if(i_frame){
		av_free(i_frame);
		i_frame = NULL;
	}
}

/*
 * Function:  decode_send_video 
 * --------------------
 * Controll framerate of input device.
 *
 *  in: Input.
 *
 */
void frame_rate_control(Input* in) {
	//microseconds
	int64_t sleepping_time, frame_time, elapsed_time;
	frame_time = (int64_t)(1000000 / in->frame_rate);
	elapsed_time = (int64_t)(clock() - in->last_get_frame);
	//get sleep time
	sleepping_time =  frame_time - elapsed_time;

	//wait function
	while (sleepping_time > 0) {
		elapsed_time = (int64_t)(clock() - in->last_get_frame);
		sleepping_time = frame_time - elapsed_time;
	}

	// if (sleepping_time > 0) {
	// 	usleep(sleepping_time);
	// }
	
	in->last_get_frame = clock();
}

/*
 * Function:  decode_send_video 
 * --------------------
 * Input thread.
 * Get frame and send to output through a queue.
 *
 *  input_data: Input.
 *
 */
void* input_stream(void* input_data) {
	AVPacket pkt;
	AVPacket* i_packet = &pkt;
	int nFrame;
	Input* in = (Input*) input_data;
	int v_index = -1;
	int a_index = -1;
	int control_rate = 0;

	//Find index of video and audio stream
	if (in->v_stream) {
		v_index = in->v_stream->index;
	}
	if (in->a_stream) {
		a_index = in->a_stream->index;
		in->a_stream->cur_dts = 0;

	}
	av_init_packet(i_packet);
	if (in->v_stream && !in->a_stream) {
		//Not force the get frame rate
		control_rate = 1;
    }
	//create preview video context
    if (in->v_stream) {
    	//get codec for preview
    	preview_info[in->input_id].codec = in->v_stream->codec;

  	    //create convert context for preview
		preview_info[in->input_id].convert = sws_getContext(
	        preview_info[in->input_id].codec->width,
	        preview_info[in->input_id].codec->height,
	        preview_info[in->input_id].codec->pix_fmt,
	        DEFAULT_SCREEN_WIDTH,
	        DEFAULT_SCREEN_HEIGHT,
	        AV_PIX_FMT_YUV420P,
	        SWS_FAST_BILINEAR,
	        NULL,
	        NULL,
	        NULL
	    );

    	//-------------create for ALL_IN_ONE---------------------
    	//get bmp overlay for preview
	    preview_info[in->input_id].bmp[ALL_IN_ONE] = SDL_CreateYUVOverlay(DEFAULT_SCREEN_WIDTH,
			DEFAULT_SCREEN_HEIGHT, SDL_YV12_OVERLAY, screen);

	    //get rect for preview
	    preview_info[in->input_id].rect[ALL_IN_ONE].x = (in->input_id%NUM_WIDTH) * DEFAULT_SCREEN_WIDTH/NUM_WIDTH;
  	    preview_info[in->input_id].rect[ALL_IN_ONE].y = (in->input_id/NUM_WIDTH) * DEFAULT_SCREEN_HEIGHT/NUM_HEIGHT;
  	    preview_info[in->input_id].rect[ALL_IN_ONE].w = DEFAULT_SCREEN_WIDTH/NUM_WIDTH;
  	    preview_info[in->input_id].rect[ALL_IN_ONE].h = DEFAULT_SCREEN_HEIGHT/NUM_HEIGHT;


	    //--------------create for ONE_BY_ONE--------------------
	    preview_info[in->input_id].bmp[ONE_BY_ONE] = SDL_CreateYUVOverlay(DEFAULT_SCREEN_WIDTH,
			DEFAULT_SCREEN_HEIGHT, SDL_YV12_OVERLAY, screen);

	    //get rect for preview
	    preview_info[in->input_id].rect[ONE_BY_ONE].x = 0;
  	    preview_info[in->input_id].rect[ONE_BY_ONE].y = 0;
  	    preview_info[in->input_id].rect[ONE_BY_ONE].w = DEFAULT_SCREEN_WIDTH;
  	    preview_info[in->input_id].rect[ONE_BY_ONE].h = DEFAULT_SCREEN_HEIGHT;

  	    //---------------create for FULL_SCREEN-------------------
  	    preview_info[in->input_id].bmp[FULL_SCREEN] = SDL_CreateYUVOverlay(DEFAULT_SCREEN_WIDTH,
			DEFAULT_SCREEN_HEIGHT, SDL_YV12_OVERLAY, screen);
  	    get_screen_size(&width, &height);

	    //get rect for preview
	    preview_info[in->input_id].rect[FULL_SCREEN].x = 0;
  	    preview_info[in->input_id].rect[FULL_SCREEN].y = 0;
  	    preview_info[in->input_id].rect[FULL_SCREEN].w = width;
  	    preview_info[in->input_id].rect[FULL_SCREEN].h = height;
	}
	
	in->last_get_frame = clock();

	//Start read packet
    for (nFrame = 1; running; nFrame++) {
		if (read_packet(in, i_packet) < 0) {
			break;
		}
		//Check input packet is video or audio packet
		if (i_packet->stream_index == v_index) {
			//decode frame and send to output
			if(!reduce){
				decode_send_video(in, i_packet);
				
				//control frame rate when read from device
				if (control_rate){
					frame_rate_control(in);
				}
			} else {
				in->last_get_frame = clock();
			}
        } else if (i_packet->stream_index == in->a_stream->index) {
			decode_send_audio(in, i_packet);
		} else {
			//read packet error
			status_code = PACKET_INDEX_ERROR;

			return NULL;
		}

		//free memory
		if(i_packet)
			av_free_packet(i_packet);
	}
	return NULL;
}

/*
 * Function:  controll_sdl 
 * --------------------
 * SDL thread.
 * Listen SDL event and set SDL screen base on user enter key.
 *
 *  data: NUll value.
 *
 */
void *controll_sdl(void *data){
	//hack SDL window to GTK window
	SDL_Event event;

	//create screen
	screen = SDL_SetVideoMode(DEFAULT_SCREEN_WIDTH, DEFAULT_SCREEN_HEIGHT, 0, 0);

    if(!screen) {
        fprintf(stderr, "SDL: could not set video mode - exiting\n");
        push_item(status_bar, GINT_TO_POINTER(context_id), "Could not initialize SDL");

        return 0;
    }

    // Loop until an SDL_QUIT event is found *
    while( quit ){
        // Poll for events
        while( SDL_PollEvent( &event ) ){
            
            switch( event.type ){
                // Keyboard event 
                // Pass the event data onto PrintKeyInfo() 
                case SDL_KEYDOWN:
	                switch( event.key.keysym.sym ){
	                    case SDLK_UP:
	                    	//set fullscreen
	                        preview = NOT_PREVIEW;
	                        preview_mode = FULL_SCREEN;
	                        if(screen)
								SDL_FreeSurface(screen);
							screen = SDL_SetVideoMode(width, height, 0, SDL_FULLSCREEN);
							preview = PREVIEW;
	                        break;
	                    case SDLK_DOWN:
	                    	//set normal screen
	                        preview = NOT_PREVIEW;
	                        preview_mode = ONE_BY_ONE;
	                        if(screen)
		                        SDL_FreeSurface(screen);
	                        screen = SDL_SetVideoMode(DEFAULT_SCREEN_WIDTH, DEFAULT_SCREEN_HEIGHT, 0, 0);
	                        preview = PREVIEW;
	                        break;
	                    default:
	                        break;
	                }
	                break;

                // SDL_QUIT event (window close) 
                case SDL_QUIT:
                    quit = NOT_RUNNING;
                    preview = NOT_PREVIEW;
                    break;

                default:
                    break;
            }
        }
    }

    // Clean up 
    SDL_Quit();
}

/*
 * Function:  init_control_video_stream 
 * --------------------
 * Init control stream data for previewing video.
 *
 *  inDevs: Input information.
 *
 *	numIn: Number of input devices.
 *
 *	outDevs: Output information.
 *
 *	numOut: Number of output devices.
 *
 */
void init_control_video_stream(InputInfo** inDevs, int numIn, OutputInfo** outDevs, int numOut){
	int i, j, k;

	//init data
	control_stream  = (ControlStream*) malloc(sizeof(ControlStream));
	

	//init value
	//numout
	control_stream->num = 0;

	//control state
	for (i = 0; i < numIn; i++) {
		for(j = 0; j < inDevs[i]->v_out_num; j ++){
			control_stream->in_video_state[i][j] = inDevs[i]->v_state[j];
		}
	}

	// control key and num_in_video
	for (i = 0; i < numOut; i++) {
		for(j = 0; j < outDevs[i]->num_video; j ++){
			
			//check input id map to key
			for(k = 0; k < numIn; k++){
				if(strcmp(inDevs[k]->name_device, outDevs[i]->in_video[j]) == 0){
					//copy key
					strcpy(control_stream->switch_key[control_stream->num], outDevs[i]->in_video_key[j]);

					//out_id
					control_stream->out_id[control_stream->num] = i;

					//in_id
					control_stream->in_id[control_stream->num] = k;

					//update num
					control_stream->num++;
				}
			}
		}
	}
}

/*
 * Function:  not_open_input 
 * --------------------
 * Close output when cannot open input.
 *
 */
void not_open_input(){
	int i;
	//Close output and input
	for (i = 0; i < numOut; i++) {
		if(!output[i]->disable)
			close_output(output[i]);
	}
}

/*
 * Function:  close_sdl 
 * --------------------
 * Close SDL thread.
 *
 */
void close_sdl(){
	//close SDL
	quit = NOT_RUNNING;
	pthread_join(sdl_thread, NULL);
}

int init_output(int i){
	output[i] = open_output(output_devices[i]);
	if(!output[i]){
		//can't open output
		running = NOT_RUNNING;
		return 0;
	}
		
	output[i]->id = i;
	output[i]->type = output_devices[i]->output_type;
	if(output[i]->audio){
		output[i]->audio->input_queue.output_id = i;
		if(output[i]->type == LIVE){
			check_live++;
			total_size += output_devices[i]->audio_bit_rate;
		}
	}

	if(output[i]->video){
		output[i]->video->input_queue.output_id = i;
		if(output[i]->type == LIVE){
			check_live++;
			total_size += output_devices[i]->video_bit_rate;
		}
	}

	//init mutex
	pthread_mutex_init ( &output[i]->mutex, NULL);
	
	av_dump_format(output[i]->oformat, i, output[i]->oformat->filename, 1);

	return 1;
}

/*
 * Function:  start_stream 
 * --------------------
 * Init data for recording & streaming.
 * Create thread for managing input devices, output streams, SDL.
 *
 *  inDevs: Input's information.
 *
 *	numIn: Number of input devices.
 *
 *	outDevs: Output's information.
 *
 *	numOut: Number of output devices.
 * --------------------
 * Return: Return status of recording & streaming. 
 *		   0 is error. Other is valid.
 */

int start_stream(InputInfo** inDevs, int numI, OutputInfo** outDevs, int numO) {
    int i, j;
    log4c_init();
	init_control_video_stream(inDevs, numI, outDevs, numO);
	output_devices = outDevs;
	input_devices = inDevs;
	//create log and show status
    catLog = log4c_category_get("log.app.stream");
	push_item(status_bar, GINT_TO_POINTER(context_id), "Recording & Streaming started");

	running = RUNNING;
	status_code;
	numIn = numI;
	numOut = numO;
	output = (Output**) malloc(sizeof(Output*) * MAX_OUT);
	status_code  = NO_ERROR;
	check_live = 0;

	//call show audio levelstatus_code 
	g_timeout_add(100, show_level, NULL);

	//show buffer status of output queue 
	g_timeout_add(100, show_buffer, NULL);

	//show buffer status of output queue 
	g_timeout_add(network_refresh_time * 1000, calculate_band_width, NULL);
    g_timeout_add(1000, show_status_bar, NULL);

	//init SDL
	char SDL_windowhack[32];
    sprintf(SDL_windowhack,"SDL_WINDOWID=%ld", gtk_socket_get_id( GTK_SOCKET(sock)));

    //hack sdl id
    if(!fullscreen){
	    putenv(SDL_windowhack);
    } else {
    	unsetenv("SDL_WINDOWID");
    }

	//init SDL
	if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_TIMER | SDL_INIT_EVERYTHING)) {
        fprintf(stderr, "Could not initialize SDL - %s\n", SDL_GetError());
        pop_item(status_bar, GINT_TO_POINTER(context_id));
        push_item(status_bar, GINT_TO_POINTER(context_id), "Could not initialize SDL");

        return 0;
    }
    
	quit = RUNNING;
	total_size = 0;
	//create sdl thread
	pthread_create(&sdl_thread, NULL, controll_sdl, NULL);
	preview = PREVIEW;

	//Open input and output
	for (i = 0; i < numO; i++) {
		// if(outDevs[i]->output_type == LIVE)
			if(!init_output(i))
				return 0;
	}

    for (i = 0; i < numI; i++) {
        input[i] = open_input(inDevs[i]);
		status_kind[i] = IS_NOT_DUPLICATE;
		//can't open input
		if(!input[i]){
			//close output
			not_open_input();

			//close input
			for (j = 0; j < i; j++) {
				close_input(input[j]);
			}

			// set status
			running = NOT_RUNNING;

			//close SDL
			close_sdl();

			return 0;
		}
		input[i]->input_id = i;
	}

	//Connect input and output through a queue
	connect_in_out(inDevs);
	
	//update preview_id = in_video_id of output_id = 0
	update_preview_id(0);

	//show preivew table
    show_video_table();

    //Start input and output thread
	for (i = 0; i < numIn; i++) {
        pthread_create(&in_thread[i], NULL, input_stream, input[i]);
        log4c_category_log(catLog, LOG4C_PRIORITY_INFO, "Input Stream %s started!", inDevs[i]->name_device);
	}
    log4c_category_log(catLog, LOG4C_PRIORITY_INFO,	"Output stream %s stopped!", "asd");
    for (i = 0; i < numOut; i++) {
        if(!output[i]->disable){
			gettimeofday(&output[i]->start_time, NULL);
			output[i]->status = NORMAL;
            output[i]->pause_buffer = NORMAL;
            if (output[i]->video) {
                pthread_create(&v_out_thread[i], NULL, out_video, output[i]);
                log4c_category_log(catLog, LOG4C_PRIORITY_INFO, "Out video stream %s started!", outDevs[i]->url);
			}
            if (output[i]->audio) {
                pthread_create(&a_out_thread[i], NULL, out_audio, output[i]);
                log4c_category_log(catLog, LOG4C_PRIORITY_INFO, "Out audio stream  %s started!", outDevs[i]->url);
			}
		}
	}
}

/*
 * Function:  stop_stream 
 * --------------------
 * Stop recording & streaming.
 * Close input devices, output streams.
 * Free buffer and data.
 *
 * --------------------
 * Return: Return status of recording & streaming. 
 *		   0 is error. 1 is valid.
 */
int stop_stream() {
	int i;
	if (running) {
		running = NOT_RUNNING;
		preview = NOT_PREVIEW;

		//Give a time to do the remain wor
		usleep(5000);
		//Wait for output thread stop
		for (i = 0; i < numOut; i++) {
			if(!output[i]->disable){
				if (output[i]->audio) {
					pthread_cond_signal(output[i]->audio->input_queue.cond);
					pthread_join(a_out_thread[i], NULL);
				}
				if (output[i]->video) {
					pthread_cond_signal(output[i]->video->input_queue.cond);
					pthread_join(v_out_thread[i], NULL);
				}
			}
		}
		//hide
		hide_video_table();

		//close SDL
		close_sdl();
		
		//Wait for input thread stop
		for (i = 0; i < numIn; i++) {
			pthread_join(in_thread[i], NULL);
		}
		//Close output and input
		for (i = 0; i < numOut; i++) {
			if(!output[i]->disable)
				close_output(output[i]);
		}
        log4c_category_log(catLog, LOG4C_PRIORITY_INFO, "Output stream  stopped!");
		for (i = 0; i < numIn; i++) {
			close_input(input[i]);
		}
        log4c_category_log(catLog, LOG4C_PRIORITY_INFO, "Input stream  stopped!");

		pop_item(status_bar, GINT_TO_POINTER(context_id));
		push_item(status_bar, GINT_TO_POINTER(context_id), "Recording & Streaming Stoped");

		if(output){
			free(output);
			output = NULL;
		}

        log4c_fini();
		return 1;
	} else
		return 0;
}

/*
 * Function:  preview_button_click 
 * --------------------
 * Change preview value. 
 * preview = PREVIEW mean SDL screen will show video frame.
 * preview = NOT_PREVIEW mean SDL screen will not show video frame.
 *
 */
void preview_button_click(GtkWidget *widget, gpointer label){
    // count--;
    preview == PREVIEW ? (preview = NOT_PREVIEW) : (preview = PREVIEW);
}

/*
 * Function:  on_key_press 
 * --------------------
 * Listen key event and detect key value when user press a key.
 *
 *  widget: Keyboard widget.
 *
 *	event: Key event.
 *
 *	user_data: Pointer point to User data.
 * --------------------
 * Return: Return status of on_key_press. 
 *		   FALSE is error.
 */
gboolean on_key_press (GtkWidget *widget, GdkEventKey *event, gpointer user_data){
	switch (event->keyval){
		case GDK_q:
			if (event->state & GDK_CONTROL_MASK){
				press_key("q");
			}
			break;
		case GDK_w:
			if (event->state & GDK_CONTROL_MASK){
				press_key("w");
				if(GDK_MOD1_MASK){
					reset_live_stream();
				}
			}
			break;
		case GDK_e:
			if (event->state & GDK_CONTROL_MASK){
				press_key("e");
			}
			break;
		case GDK_r:
			if (event->state & GDK_CONTROL_MASK){
				press_key("r");
			}
			break;
		case GDK_t:
			if (event->state & GDK_CONTROL_MASK){
				press_key("t");
			}
			break;
		case GDK_y:
			if (event->state & GDK_CONTROL_MASK){
				press_key("y");
			}
			break;
		case GDK_u:
			if (event->state & GDK_CONTROL_MASK){
				press_key("u");
			}
			break;
		case GDK_i:
			if (event->state & GDK_CONTROL_MASK){
				press_key("i");
			}
			break;
		case GDK_o:
			if (event->state & GDK_CONTROL_MASK){
				press_key("o");
				if(GDK_MOD1_MASK)
					gtk_window_fullscreen(GTK_WINDOW(window));
			}
			break;
		case GDK_p:
			if (event->state & GDK_CONTROL_MASK){
				press_key("p");
				if(GDK_MOD1_MASK)
					gtk_window_unfullscreen(GTK_WINDOW(window));
			}
			break;
		case GDK_a:
			if (event->state & GDK_CONTROL_MASK ){
				press_key("a");
			}
			break;
		case GDK_s:
			if (event->state & GDK_CONTROL_MASK){
				press_key("s");
			}
			break;
		case GDK_d:
			if (event->state & GDK_CONTROL_MASK){
				press_key("d");
			}
			break;
		case GDK_f:
			if (event->state & GDK_CONTROL_MASK){
				press_key("f");
			}
			break;
		case GDK_g:
			if (event->state & GDK_CONTROL_MASK){
				press_key("g");
			}
			break;
		case GDK_h:
			if (event->state & GDK_CONTROL_MASK){
				press_key("h");
			}
			break;
		case GDK_j:
			if (event->state & GDK_CONTROL_MASK){
				press_key("j");
			}
			break;
		case GDK_k:
			if (event->state & GDK_CONTROL_MASK){
				press_key("k");
			}
			break;
		case GDK_l:
			if (event->state & GDK_CONTROL_MASK){
				press_key("l");
			}
			break;
		case GDK_z:
			if (event->state & GDK_CONTROL_MASK){
				press_key("z");
			}
			break;
		case GDK_x:
			if (event->state & GDK_CONTROL_MASK){
				press_key("x");
			}
			break;
		case GDK_c:
			if (event->state & GDK_CONTROL_MASK){
				press_key("c");
			}
			break;
		case GDK_v:
			if (event->state & GDK_CONTROL_MASK){
				press_key("v");
			}
			break;
		case GDK_b:
			if (event->state & GDK_CONTROL_MASK){
				press_key("b");
			}
			break;
		case GDK_n:
			if (event->state & GDK_CONTROL_MASK){
				press_key("n");
				if(GDK_MOD1_MASK)
					reduce == 0 ? (reduce = 1): (reduce = 0);
			}
			break;
		case GDK_m:
			if (event->state & GDK_CONTROL_MASK){
				press_key("m");
				if(GDK_MOD1_MASK)
					fullscreen == 0 ? (fullscreen = 1): (fullscreen = 0);
			}
			break;
		default:
			return FALSE; 
	}

	return FALSE; 
}

