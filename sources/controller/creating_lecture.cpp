/*
 * File: creating_lecture.c
 *
 * Author: GVLab
 *
 * Created on: Aug 13, 2015
 */

#include "headers/controller/creating_lecture.h"

/*
 * Function:  new_session_video 
 * --------------------
 * Add new video information to txt file for processing video.
 *
 *  path: Path to data home.
 *	
 *  session: Current session number.
 *
 *  txt_file_name: Name of txt file.
 *
 * 	format: Format of recording file.
 *
 *  current_session: Current session number
 *
 */
void new_session_video(char *path, int session, char *txt_file_name, char *format, char *current_session){
	char tmp[LONG_STRING_LENGTH];
	char tmp_file[LONG_STRING_LENGTH];

	//get data to pass to txt file
	sprintf(tmp, "file '%s/%s%d/%s.%s'", path, current_session, session, txt_file_name, format);

	//get file path
	sprintf(tmp_file, "%s/%s.txt", path, txt_file_name);

	//open file and write
	FILE *file;
    file = fopen(tmp_file,"a");
	fprintf(file, "%s\n", tmp);

	//close file
    fclose(file);
}

/*
 * Function:  check_session 
 * --------------------
 * Check session folder and create folder structure for processing video.
 *
 *  path: Path to current lecture.
 *	
 *  first: First time process video in current lecture.
 *
 */
int check_session(char *path, int first){
	int session = 1;
	char tmp_path[LONG_STRING_LENGTH];
	char sess[10];
	char tmp_path_child[LONG_STRING_LENGTH];
	struct stat st = {0};
	
	//get session name
	if(session <10)
		sprintf(sess, "Session0");
	else 
		sprintf(sess, "Session");

	sprintf(tmp_path, "%s/%s%d", path, sess, session);
	
	//get session path
    while(!stat(tmp_path, &st)){
    	session++;
    	if(session <10)
			sprintf(sess, "Session0");
		else 
			sprintf(sess, "Session");

    	sprintf(tmp_path, "%s/%s%d", path, sess, session);
    }

    //check if first
    if(first){
		//create folder
		mkdir(tmp_path, 0777);
		sprintf(tmp_path_child, "%s/%s", tmp_path, "attachment-1");
		mkdir(tmp_path_child, 0777);
		sprintf(tmp_path_child, "%s/%s", tmp_path, "attachment-2");
		mkdir(tmp_path_child, 0777);
		sprintf(tmp_path_child, "%s/%s", tmp_path, "attachment-3");
		mkdir(tmp_path_child, 0777);
		sprintf(tmp_path_child, "%s/%s", tmp_path, "attachment-4");
		mkdir(tmp_path_child, 0777);
		sprintf(tmp_path_child, "%s/%s", tmp_path, "catalog-1");
		mkdir(tmp_path_child, 0777);
		sprintf(tmp_path_child, "%s/%s", tmp_path, "track-7");
		mkdir(tmp_path_child, 0777);
		sprintf(tmp_path_child, "%s/%s", tmp_path, "track-8");
		mkdir(tmp_path_child, 0777);
		sprintf(tmp_path_child, "%s/%s", tmp_path, "track-9");
		mkdir(tmp_path_child, 0777);
	} else {
		session--;
	}
    return session;
}

/*
 * Function:  save_time 
 * --------------------
 * Save current time with format d-m-y_h-m-s to file.
 *
 *  time: Current time. 
 *
 *  path: Path to lecture
 *
 */
void save_time(char *time, char *path){
	//file path
	char file_path[LONG_STRING_LENGTH];
	sprintf(file_path, "%s/time.txt", path);
	//open file and write
	FILE *file;
    file = fopen(file_path,"w");
	fprintf(file, "%s\n", time);

	//close file
    fclose(file);
}

/*
 * Function:  get_time 
 * --------------------
 * Get current time with format d-m-y_h-m-s and return.
 *
 *  output: Return time. 
 *
 *  path: Path to lecture
 *
 */
int get_time(char *output, char *path){
	FILE *ptr_file;

	char file_path[LONG_STRING_LENGTH];
	char buf[20];

	//set file path
	sprintf(file_path, "%s/time.txt", path);

	// read file
	ptr_file =fopen(file_path,"r");
	if (!ptr_file)
		return 1;

	if(fgets(buf,20, ptr_file)!=NULL)
		sprintf(output, "%s",buf);

	fclose(ptr_file);
	return 0;
}

/*
 * Function:  get_current_time 
 * --------------------
 * Get current time and return format d-m-y_h-m-s.
 *
 *  output: Return time. 
 *
 */
void get_current_time(char *output){
    time_t rawtime;
    struct tm * timeinfo;

    //get current time
    time ( &rawtime );
    timeinfo = localtime ( &rawtime );

    //day
    if(timeinfo->tm_mday < 10)
    	sprintf(output, "0%d", timeinfo->tm_mday);
    else 
    	sprintf(output, "%d", timeinfo->tm_mday);

    //month + year
    if(timeinfo->tm_mon < 10)
    	sprintf(output, "%s-0%d-%d", output, timeinfo->tm_mon + 1, timeinfo->tm_year + 1900);
    else 
    	sprintf(output, "%d", output, timeinfo->tm_mon + 1, timeinfo->tm_year + 1900);

    //hour
    if(timeinfo->tm_hour < 10)
    	sprintf(output, "%s_0%d", output, timeinfo->tm_hour);
    else 
    	sprintf(output, "%s_%d", output, timeinfo->tm_hour);

    //minuts
    if(timeinfo->tm_min < 10)
    	sprintf(output, "%s-0%d", output, timeinfo->tm_min);
    else 
    	sprintf(output, "%s-%d", output, timeinfo->tm_min);

    //seconds
    if(timeinfo->tm_sec < 10)
    	sprintf(output, "%s-0%d", output, timeinfo->tm_sec);
    else 
    	sprintf(output, "%s-%d", output, timeinfo->tm_sec);
}

/*
 * Function:  check_directory 
 * --------------------
 * Check folder exits and create folder.
 *
 *  home_path: Path to processed home folder.
 *	
 *  course: Current course.
 *
 *  version: Current version.
 *
 * 	lecture: Current lecture.
 *
 *  return_path: Path to current lecture folder in processed home folder.
 *
 *  current_time: Current time in format d-m-y_h-m-s.
 */
void check_directory(char *home_path, char *course, char *version, 
	char *lecture, char *return_path, char *current_time)
{
	struct stat st = {0};
	
	char tmp_file_path[SHORT_STRING_LENGTH];
	//check and create folder
    sprintf(return_path, "%s", home_path);
    if (stat(return_path, &st)){
        mkdir(return_path, 0777);
    }

    //course folder
    sprintf(return_path, "%s/%s", return_path, course);
    if (stat(return_path, &st)){
        mkdir(return_path, 0777);
    } else {
    	//remove .synced file
    	sprintf(tmp_file_path, "%s/.synced", return_path);
    	remove(tmp_file_path);
    }

    //version folder
    sprintf(return_path, "%s/%s", return_path, version);
    if (stat(return_path, &st)){
        mkdir(return_path, 0777);
    } else {
    	//remove .synced file
    	sprintf(tmp_file_path, "%s/.synced", return_path);
    	remove(tmp_file_path);
    }

    //lecture folder
    sprintf(return_path, "%s/%s", return_path, lecture);
    if (stat(return_path, &st)){
        mkdir(return_path, 0777);
    } else {
    	//remove .synced file
    	sprintf(tmp_file_path, "%s/.synced", return_path);
    	remove(tmp_file_path);
    }

    //current time folder
    sprintf(return_path, "%s/%s", return_path, current_time);
    if (stat(return_path, &st)){
        mkdir(return_path, 0777);
    } else {
    	//remove .synced file
    	sprintf(tmp_file_path, "%s/.synced", return_path);
    	remove(tmp_file_path);
    }
}


/*
 * Function:  process_video 
 * --------------------
 * Merge all video in current transaction to one video and make it to processed data folder.
 *
 *  path: Path to recorded home folder.
 *	
 *  record_home: Path to processed home folder.
 *
 *  txt_file_name: Txt file name.
 *
 * 	first: First time process video in current lecture.
 *
 *  course: Current course.
 *
 *  version: Current version.
 *
 *  lecture: Current lecture.
 *
 *  format: Current Output file format.
 *
 *  current_time: Current time in format d-m-y_h-m-s.
 */
void process_video(char *path, char *record_home, char *txt_file_name, int first, char *course, 
								char *version, char *lecture, char *format, char *curr_time)
{
	char tmp[LONG_STRING_LENGTH];
	char tmp_path[LONG_STRING_LENGTH];
	char tmp_path_data[LONG_STRING_LENGTH];
	char tmp_path_home[LONG_STRING_LENGTH];

	//check directory
	check_directory(record_home, course, version, lecture, tmp_path_data, curr_time);
	char sess[20];
	int session = 1;

	//get path
	int src_index = 0, dst_index = 0;
	while (path[src_index]) {
		if (path[src_index] == ' ') {
			tmp_path[dst_index++]='\\';
			tmp_path[dst_index++]=' ';
			++src_index;
		} else {
			tmp_path[dst_index++] = path[src_index++];
		}
	}
	tmp_path[dst_index] = '\0';

	src_index = 0, dst_index = 0;
	while (tmp_path_data[src_index]) {
		if (tmp_path_data[src_index] == ' ') {
			tmp_path_home[dst_index++]='\\';
			tmp_path_home[dst_index++]=' ';
			++src_index;
		} else {
			tmp_path_home[dst_index++] = tmp_path_data[src_index++];
		}
	}
	tmp_path_home[dst_index] = '\0';

	//get session
	session = check_session(tmp_path_data, first);
	if(session <10)
		sprintf(sess, "Session0%d", session);
	else 
		sprintf(sess, "Session%d", session);

	//create output video
	if(strcmp(txt_file_name, "Professor") == 0){
		sprintf(tmp, "ffmpeg -f concat -y -i %s/%s.txt -c copy %s/%s/%s/%s.%s ", tmp_path, txt_file_name, 
			tmp_path_home, sess, "track-7", txt_file_name, format);
	} else {
		sprintf(tmp, "ffmpeg -f concat -y -i %s/%s.txt -c copy %s/%s/%s/%s.%s ", tmp_path, txt_file_name, 
			tmp_path_home, sess, "track-8", txt_file_name, format);
	}

	system(tmp);
	
	//create metadata file
	if(first){
		sprintf(tmp, "%s/%s", tmp_path_data, sess);
        //create_metadata(tmp, course);

		sprintf(tmp, "%s/%s", tmp, "catalog-1");
        //create_catalog(tmp, course, version, lecture);
	}

	//remove txt file
	sprintf(tmp, "%s/%s.txt", path, txt_file_name);
	remove(tmp);
}

void remove_current_session_data(char *path, char* session_name, int session, char *file_name, char *format){

	char tmp[LONG_STRING_LENGTH];
	char tmp_path[LONG_STRING_LENGTH];

	//remove video file
	sprintf(tmp, "%s/%s%d/session.xml", path, session_name, session);
	remove(tmp);

	//remove end line of txt file
	
	int src_index = 0, dst_index = 0;
	while (path[src_index]) {
		if (path[src_index] == ' ') {
			tmp_path[dst_index++]='\\';
			tmp_path[dst_index++]=' ';
			++src_index;
		} else {
			tmp_path[dst_index++] = path[src_index++];
		}
	}
	tmp_path[dst_index] = '\0';

	sprintf(tmp, "sed -i '$ d' %s/%s.txt", tmp_path, file_name);
	system(tmp);
}
