/*
 * File: read_config.c
 *
 * Author: GVLab
 *
 * Created on: Aug 13, 2015
 */

#include "headers/model/read_config.h"

/*
 * Function:  get_audio_stream 
 * --------------------
 * Get audio data from config file.
 *
 *  stream: An audio stream element in config file.
 *
 *	out: Output information.
 */
void get_audio_stream(config_setting_t* stream, OutputInfo* out) {
	config_setting_t* audio = config_setting_get_member(stream, "audio");
	const char* string;

	//has audio element
	if (audio) {
		//read all audio data
		if (config_setting_lookup_string(audio, "in_name",
				&string) == CONFIG_TRUE)
			strcpy(out->in_audio, string);
		config_setting_lookup_int(audio, "num_channel", &out->num_channel);
		config_setting_lookup_int(audio, "bit_rate", &out->audio_bit_rate);
		config_setting_lookup_int(audio, "sample_rate", &out->sample_rate);
		config_setting_lookup_int(audio, "queue_size", &out->audio_queue_size);
        config_setting_lookup_int(audio, "codec", (int*) &out->audio_codec);
		out->audio_stream = USE_STREAM;
	} else {
		out->audio_stream = NOT_USE_STREAM;
	}
}

/*
 * Function:  get_video_stream 
 * --------------------
 * Get video data from config file.
 *
 *  stream: A video stream element in config file.
 *
 *	out: Output information.
 */
void get_video_stream(config_setting_t* stream, OutputInfo* out) {
	config_setting_t *list_video, *video_name, *tmp_name;
	int num_video = 0, i;

	//get video element
	config_setting_t* video = config_setting_get_member(stream, "video");
	const char* string = "";
	if (video) {
		//get all video data
		out->video_stream = USE_STREAM;
		//read video
		list_video = config_setting_get_member(video, "in_name");
		num_video = config_setting_length(list_video);
		out->num_video = num_video;
		for (i = 0; i < num_video; i++) {
			//read video name
			video_name = config_setting_get_elem(list_video, i);
			config_setting_lookup_string(video_name, "name", &string);
			strcpy(out->in_video[i], string);

			//read video key
			if (config_setting_lookup_string(video_name, "key", &string) == CONFIG_TRUE) {
				strcpy(out->in_video_key[i], string);
			}

			//read video type
			config_setting_lookup_int(video_name, "in_video_type", &out->in_video_type[i]);
		}
		out->num_video = num_video;

		if (config_setting_lookup_string(video, "codec", &string) == CONFIG_TRUE) {
			if (strcmp(string, "H264") == 0 || strcmp(string, "h264") == 0) {
				out->video_codec = AV_CODEC_ID_H264;
			} else if (strcmp(string, "MPEG") == 0
					|| strcmp(string, "mpeg") == 0) {
				out->video_codec = AV_CODEC_ID_FLV1;
			}
		}

		//read general information
		config_setting_lookup_int(video, "queue_size", &out->video_queue_size);
		config_setting_lookup_int(video, "frame_rate", &out->frame_rate);
		config_setting_lookup_int(video, "max_buf_rate", &out->max_buf_rate);
		config_setting_lookup_int(video, "bit_rate", &out->video_bit_rate);
		config_setting_lookup_int(video, "group_size", &out->gop_size);
		config_setting_lookup_int(video, "width", &out->width);
		config_setting_lookup_int(video, "height", &out->height);
		config_setting_lookup_int(video, "q_min", &out->q_min);
		config_setting_lookup_int(video, "q_max", &out->q_max);
		config_setting_lookup_int(video, "baseline", &out->baseline);
	} else {
		out->video_stream = NOT_USE_STREAM;
	}
}

/*
 * Function:  get_video_stream 
 * --------------------
 * Get stream info from config file.
 *
 *  stream: A stream element in config file.
 *
 *	OutDev: Output information.
 */
void get_stream_info(config_setting_t* stream, OutputInfo* OutDev) {
	const char* string;
	int i;
	get_audio_stream(stream, OutDev);
	get_video_stream(stream, OutDev);

	//get format
	if (config_setting_lookup_string(stream, "format", &string) == CONFIG_TRUE) {
		strcpy(OutDev->format, string);
	}

	//get url
	if (config_setting_lookup_string(stream, "url", &string) == CONFIG_TRUE) {
		strcpy(OutDev->url, string);
	}

	//get disable value
	if (config_setting_lookup_string(stream, "disable", &string) == CONFIG_TRUE) {
		if (strcmp(string, "true") == 0) {
			OutDev->disable = 1;
		} else {
			OutDev->disable = 0;
		}
	}

	//get stream name
	if (config_setting_lookup_string(stream, "name", &string) == CONFIG_TRUE) {
		strcpy(OutDev->name, string);
	}

	//get stream type
	if (config_setting_lookup_int(stream, "output_type", &i) == CONFIG_TRUE) {
		OutDev->output_type = i;
	}
}

/*
 * Function:  get_input_info 
 * --------------------
 * Get input information from config file.
 *
 *  input: An input element in config file.
 *
 *	inDev: Input information.
 */
int get_input_info(config_setting_t* input, InputInfo* inDev) {
	const char* string;
	int i;

	//get input name
	if (config_setting_lookup_string(input, "name", &string) == CONFIG_TRUE) {
		strcpy(inDev->name_device, string);
	}

	//get input driver
	if (config_setting_lookup_string(input, "driver", &string) == CONFIG_TRUE) {
		strcpy(inDev->driver, string);
	}

	//get input frame rate
	if (config_setting_lookup_int(input, "frame_rate", &i) == CONFIG_TRUE) {
		inDev->frame_rate = i;
	}

	//get input type
	if (config_setting_lookup_int(input, "input_type", &i) == CONFIG_TRUE) {
		inDev->input_type = i;
	}

	//get sample format, width, height
	config_setting_lookup_int(input, "sample_rate", &inDev->sample_rate);
	config_setting_lookup_int(input, "width", &inDev->width);
	config_setting_lookup_int(input, "height", &inDev->height);
}

/*
 * Function:  add_out_id 
 * --------------------
 * Add output id to input information.
 *
 *  outDev: Output information.
 *
 *	inDev: Input information.
 *
 * 	numIn: Number of input devices.
 */
int add_out_id(OutputInfo *outDev, InputInfo** inDevs, int numIn) {
	int i, j;

	//audio stream
	if (outDev->audio_stream) {
		//check all input name and map to output
		for (i = 0; i < numIn; i++) {
			if (strcmp(outDev->in_audio, inDevs[i]->name_device) == 0) {
				inDevs[i]->a_out_id[inDevs[i]->a_out_num] = outDev->id;
				outDev->sample_rate = inDevs[i]->sample_rate;
				inDevs[i]->a_out_num++;
				break;
			}
		}
		if (i >= numIn) {
			printf("No input audio %s for stream %s\n", outDev->in_audio,
					outDev->url);
			return 0;
		}
	}

	//video stream 
	for (j = 0; j < outDev->num_video; j++){
		if (strcmp(outDev->in_video[j], "") != 0) {
			for (i = 0; i < numIn; i++) {
				if (strcmp(outDev->in_video[j], inDevs[i]->name_device) == 0) {
					inDevs[i]->v_out_id[inDevs[i]->v_out_num] = outDev->id;
					
					//output stream frame rate = main input
					//update state of input in output
					if(outDev->in_video_type[j] == 0){
						// outDev->frame_rate = inDevs[i]->frame_rate;
						inDevs[i]->v_state[inDevs[i]->v_out_num] = 1;
					} else
						inDevs[i]->v_state[inDevs[i]->v_out_num] = 0;
					
					
					inDevs[i]->v_out_num++;

					break;
				}
			}
			if (i >= numIn) {
				printf("No input video %s for stream %s\n", outDev->in_video[0],
						outDev->url);
				return 0;
			}
		}
	}
	return 1;
}

/*
 * Function:  add_out2in 
 * --------------------
 * Map output to input base on config file.
 *
 *	inDevs: Input information.
 *
 * 	numIn: Number of input devices.
 *
 *	outDevs: Output information.
 *
 * 	numOut: Number of output streams.
 */
void add_out2in(InputInfo** inDevs, int numIn, OutputInfo **outDevs, int numOut) {
	int i;
	for (i = 0; i < numOut; i++) {
		if (!outDevs[i]->disable) {
			//add output id to input
			if (strcmp(outDevs[i]->in_audio, "") == 0
					&& strcmp(outDevs[i]->in_video[0], "") == 0) {
				printf("No input for stream %s\n", outDevs[i]->url);
			} else
				add_out_id(outDevs[i], inDevs, numIn);
		}
	}
}

/*
 * Function:  configure 
 * --------------------
 * Read input, output data from config file.
 *
 *	inDevs: Input information.
 *
 * 	numIn: Number of input devices.
 *
 *	outDevs: Output information.
 *
 * 	numOut: Number of output streams.
 */
void configure(InputInfo** InDevs, int* numIn, OutputInfo** OutDevs, int* numOut) {
	int numInput = 0;
	int numOutput = 0;
	config_t cfg;
	config_setting_t *listStream;
	config_setting_t * stream;
	config_setting_t *listInput;
	config_setting_t* input;
	int numOutPut;
	char* config_name = "config.cfg";
	char* stream_name;
	config_init(&cfg);
	
	//open file
	if (!config_read_file(&cfg, config_name)) {
		printf("can't read file config %s\n", config_name);
		printf("%s:%d  %s\n", config_error_file(&cfg), config_error_line(&cfg),
				config_error_text(&cfg));
		fflush(stdout);
		config_destroy(&cfg);
	}

	// read output stream information in config file
	listStream = config_lookup(&cfg, "outputs");
	numOutPut = config_setting_length(listStream);
	*numOut = numOutPut;
	int i;
	for (i = 0; i < numOutPut; i++) {
		//read stream information
		stream = config_setting_get_elem(listStream, i);
		get_stream_info(stream, OutDevs[i]);
		OutDevs[i]->id = i;
	}

	//read input devices information in config file
	listInput = config_lookup(&cfg, "inputs");
	numInput = config_setting_length(listInput);
	*numIn = numInput;
	for (i = 0; i < numInput; i++) {
		//read input information
		input = config_setting_get_elem(listInput, i);
		get_input_info(input, InDevs[i]);
		InDevs[i]->a_out_num = 0;
		InDevs[i]->v_out_num = 0;
	}

	//map input to output
	add_out2in(InDevs, numInput, OutDevs, numOutPut);

	config_destroy(&cfg);
}

/*
 * Function:  save_input_to_config_file 
 * --------------------
 * Save input information to config file.
 *
 *	inDevs: Input information.
 *
 * 	numIn: Number of input devices.
 */
void save_input_to_config_file(InputInfo** InDevs, int numIn){
	//init
	int i;
	int numInput = 0;
	config_t cfg;
	config_setting_t *listInput;
	config_setting_t* input;

	//tmp data
	config_setting_t* tmp_int;
	config_setting_t* tmp_string;

	int numOutPut;
	char* config_name = "config.cfg";

	config_init(&cfg);

	//open file
	if (!config_read_file(&cfg, config_name)) {
		printf("can't read file config %s\n", config_name);
		printf("%s:%d  %s\n", config_error_file(&cfg), config_error_line(&cfg),
				config_error_text(&cfg));
		fflush(stdout);
		config_destroy(&cfg);
	}

	//input
	listInput = config_lookup(&cfg, "inputs");
	numInput = config_setting_length(listInput);
	
	//save old input info
	for (i = 0; i < numIn; i++) {

		input = config_setting_get_elem(listInput, i);
		if(!input)
			input = config_setting_add(listInput, "input", CONFIG_TYPE_GROUP);

		//save name
		tmp_string = config_setting_get_member(input, "name");
		if(!tmp_string)
			tmp_string = config_setting_add(input, "name", CONFIG_TYPE_STRING);
		config_setting_set_string(tmp_string, InDevs[i]->name_device);

		//save driver
		tmp_string = config_setting_get_member(input, "driver");
		if(!tmp_string)
			tmp_string = config_setting_add(input, "driver", CONFIG_TYPE_STRING);
		config_setting_set_string(tmp_string, InDevs[i]->driver);

		//save input type
        tmp_int = config_setting_get_member(input, "input_type");
        if(!tmp_int)
			tmp_int = config_setting_add(input, "input_type", CONFIG_TYPE_INT);
        config_setting_set_int(tmp_int, InDevs[i]->input_type);

		//save input's information
	    switch(InDevs[i]->input_type){
	        case 0:
	        case 1:
	            //save width
	            tmp_int = config_setting_get_member(input, "width");
	            if(!tmp_int)
					tmp_int = config_setting_add(input, "width", CONFIG_TYPE_INT);
	            config_setting_set_int(tmp_int, InDevs[i]->width);
	            
	            //save height
	            tmp_int = config_setting_get_member(input, "height");
	            if(!tmp_int)
					tmp_int = config_setting_add(input, "height", CONFIG_TYPE_INT);
	            config_setting_set_int(tmp_int, InDevs[i]->height);

	            //save framrate
	            tmp_int = config_setting_get_member(input, "frame_rate");
	            if(!tmp_int)
					tmp_int = config_setting_add(input, "frame_rate", CONFIG_TYPE_INT);
	            config_setting_set_int(tmp_int, InDevs[i]->frame_rate);
	            break;
	        case 2:
	            //save sample rate
	            tmp_int = config_setting_get_member(input, "sample_rate");
	            if(!tmp_int)
					tmp_int = config_setting_add(input, "sample_rate", CONFIG_TYPE_INT);
	            config_setting_set_int(tmp_int, InDevs[i]->sample_rate);
	            break;

	        default:
	            break;
	    }
	}

	//remove input
	for (i = numIn-1; i >= 0; i--) {
		if(InDevs[i]->state == STATE_DELETED){
			config_setting_remove_elem(listInput, i);
		}
	}

	config_write_file(&cfg, config_name);

	config_destroy(&cfg);
}

/*
 * Function:  save_output_to_config_file 
 * --------------------
 * Save output information to config file.
 *
 *	outDevs: Output information.
 *
 * 	numOut: Number of output streams.
 */
void save_output_to_config_file(OutputInfo** OutDevs, int numOut){
	//init
	int i, j;
	int num_output = 0, num_video = 0;;
	config_t cfg;
	config_setting_t *list_output;
	config_setting_t *output;
	config_setting_t *audio;
	config_setting_t *video;
	config_setting_t *in_video;
	config_setting_t *tmp_video;

	//tmp data
	config_setting_t* tmp_int;
	config_setting_t* tmp_string;

	char *tmp;

	int numOutPut;
	char* config_name = "config.cfg";

	config_init(&cfg);

	//open file
	if (!config_read_file(&cfg, config_name)) {
		printf("can't read file config %s\n", config_name);
		printf("%s:%d  %s\n", config_error_file(&cfg), config_error_line(&cfg),
				config_error_text(&cfg));
		fflush(stdout);
		config_destroy(&cfg);
	}

	list_output = config_lookup(&cfg, "outputs");
	num_output = config_setting_length(list_output);

	for(i = 0; i < numOut; i++){
		output = config_setting_get_elem(list_output, i);
		if(!output)
			output = config_setting_add(list_output, "output", CONFIG_TYPE_GROUP);
		//---------------general-------------

		//update name
		tmp_string = config_setting_get_member(output, "name");
		if(!tmp_string)
			tmp_string = config_setting_add(output, "name", CONFIG_TYPE_STRING);
		config_setting_set_string(tmp_string, OutDevs[i]->name);

		//update output type
		tmp_int = config_setting_get_member(output, "output_type");
		if(!tmp_int)
			tmp_int = config_setting_add(output, "output_type", CONFIG_TYPE_INT);
        config_setting_set_int(tmp_int, OutDevs[i]->output_type);

        //update output url
        tmp_string = config_setting_get_member(output, "url");
        if(!tmp_string)
			tmp_string = config_setting_add(output, "url", CONFIG_TYPE_STRING);
		config_setting_set_string(tmp_string, OutDevs[i]->url);

		//update output format
        tmp_string = config_setting_get_member(output, "format");
        if(!tmp_string)
			tmp_string = config_setting_add(output, "format", CONFIG_TYPE_STRING);
		config_setting_set_string(tmp_string, OutDevs[i]->format);

		//update output disable
        tmp_string = config_setting_get_member(output, "disable");
        if(!tmp_string)
			tmp_string = config_setting_add(output, "disable", CONFIG_TYPE_STRING);
		if(!OutDevs[i]->disable)
			config_setting_set_string(tmp_string, "false");
		else 
			config_setting_set_string(tmp_string, "true");

		//---------------audio---------------
		//get audio
		if(strcmp(OutDevs[i]->in_audio, "")!= 0){
			audio = config_setting_get_member(output, "audio");

			if(audio){
				//update audio
				//update name
				tmp_string = config_setting_get_member(audio, "in_name");
				if(!tmp_string)
					tmp_string = config_setting_add(audio, "in_name", CONFIG_TYPE_STRING);
				config_setting_set_string(tmp_string, OutDevs[i]->in_audio);

				//update num channel
				tmp_int = config_setting_get_member(audio, "num_channel");
				if(!tmp_int)
					tmp_int = config_setting_add(audio, "num_channel", CONFIG_TYPE_INT);
				config_setting_set_int(tmp_int, OutDevs[i]->num_channel);

				//update bitrate
				tmp_int = config_setting_get_member(audio, "bit_rate");
				if(!tmp_int)
					tmp_int = config_setting_add(audio, "bit_rate", CONFIG_TYPE_INT);
				config_setting_set_int(tmp_int, OutDevs[i]->audio_bit_rate);

				//update sample rate
				tmp_int = config_setting_get_member(audio, "sample_rate");
				if(!tmp_int)
					tmp_int = config_setting_add(audio, "sample_rate", CONFIG_TYPE_INT);
				config_setting_set_int(tmp_int, OutDevs[i]->sample_rate);

				//update sample rate
				tmp_int = config_setting_get_member(audio, "queue_size");
				if(!tmp_int)
					tmp_int = config_setting_add(audio, "queue_size", CONFIG_TYPE_INT);
				config_setting_set_int(tmp_int, OutDevs[i]->audio_queue_size);

				//update codec
				tmp_int = config_setting_get_member(audio, "codec");
				if(!tmp_int)
					tmp_int = config_setting_add(audio, "codec", CONFIG_TYPE_INT);
				config_setting_set_int(tmp_int, OutDevs[i]->audio_codec);

			} else {
				//create new audio
				audio = config_setting_add(output, "audio", CONFIG_TYPE_GROUP);

				//save name
				tmp_string = config_setting_add(audio, "in_name", CONFIG_TYPE_STRING);
				config_setting_set_string(tmp_string, OutDevs[i]->in_audio);

				//save num channel
				tmp_int = config_setting_add(audio, "num_channel", CONFIG_TYPE_INT);
				config_setting_set_int(tmp_int, OutDevs[i]->num_channel);

				//save bitrate
				tmp_int = config_setting_add(audio, "bit_rate", CONFIG_TYPE_INT);
				config_setting_set_int(tmp_int, OutDevs[i]->audio_bit_rate);

				//save sample rate
				tmp_int = config_setting_add(audio, "sample_rate", CONFIG_TYPE_INT);
				config_setting_set_int(tmp_int, OutDevs[i]->sample_rate);

				//save sample rate
				tmp_int = config_setting_add(audio, "queue_size", CONFIG_TYPE_INT);
				config_setting_set_int(tmp_int, OutDevs[i]->audio_queue_size);

				//save audio codec
				tmp_int = config_setting_add(audio, "codec", CONFIG_TYPE_INT);
				config_setting_set_int(tmp_int, OutDevs[i]->audio_codec);
			}

		} else {
			audio = config_setting_get_member(output, "audio");
			if(audio){
				//destroy audio 
				config_setting_remove(output, "audio");
			}
		}

		if(OutDevs[i]->audio_stream == NOT_USE_STREAM){
			config_setting_remove(output, "audio");
		}

		//---------------video---------------
		if(OutDevs[i]->num_video > 0){
			video = config_setting_get_member(output, "video");

			if(video){
				
				//update name
				in_video  = config_setting_get_member(video, "in_name");
				if(!in_video)
					in_video = config_setting_add(audio, "in_name", CONFIG_TYPE_LIST);
				
				for(j = 0; j < OutDevs[i]->num_video; j++){
					tmp_video = config_setting_get_elem(in_video, j);
					if(!tmp_video)
						tmp_video = config_setting_add(in_video, "video", CONFIG_TYPE_GROUP);
					
					//video name
					tmp_string = config_setting_get_member(tmp_video, "name");
					if(!tmp_string)
						tmp_string = config_setting_add(tmp_video, "name", CONFIG_TYPE_STRING);
					config_setting_set_string(tmp_string, OutDevs[i]->in_video[j]);

					//video key
					tmp_string = config_setting_get_member(tmp_video, "key");
					if(!tmp_string)
						tmp_string = config_setting_add(tmp_video, "key", CONFIG_TYPE_STRING);
					config_setting_set_string(tmp_string, OutDevs[i]->in_video_key[j]);

					//video name
					tmp_string = config_setting_get_member(tmp_video, "name");
					if(!tmp_string)
						tmp_string = config_setting_add(tmp_video, "name", CONFIG_TYPE_STRING);
					config_setting_set_string(tmp_string, OutDevs[i]->in_video[j]);


					//video type
					tmp_int = config_setting_get_member(tmp_video, "in_video_type");
					if(!tmp_int)
						tmp_int = config_setting_add(tmp_video, "in_video_type", CONFIG_TYPE_INT);
					config_setting_set_int(tmp_int, OutDevs[i]->in_video_type[j]);
				}

				//remove video
				for(j = OutDevs[i]->num_video - 1; j >= 0; j--){
					if(OutDevs[i]->in_video_state[j] == 1){
						config_setting_remove_elem(in_video, j);
					}
				}

				//update bit_rate
				tmp_int = config_setting_get_member(video, "bit_rate");
				if(!tmp_int)
					tmp_int = config_setting_add(video, "bit_rate", CONFIG_TYPE_INT);
				config_setting_set_int(tmp_int, OutDevs[i]->video_bit_rate);
				
				//update group_size
				tmp_int = config_setting_get_member(video, "group_size");
				if(!tmp_int)
					tmp_int = config_setting_add(video, "group_size", CONFIG_TYPE_INT);
				config_setting_set_int(tmp_int, OutDevs[i]->gop_size);

				//update width 
				tmp_int = config_setting_get_member(video, "width");
				if(!tmp_int)
					tmp_int = config_setting_add(video, "width", CONFIG_TYPE_INT);
				config_setting_set_int(tmp_int, OutDevs[i]->width);

				// update height
				tmp_int = config_setting_get_member(video, "height");
				if(!tmp_int)
					tmp_int = config_setting_add(video, "height", CONFIG_TYPE_INT);
				config_setting_set_int(tmp_int, OutDevs[i]->height);

				// update pix_fmt
				tmp_int = config_setting_get_member(video, "pix_fmt");
				if(!tmp_int)
					tmp_int = config_setting_add(video, "pix_fmt", CONFIG_TYPE_INT);
				config_setting_set_int(tmp_int, OutDevs[i]->pix_fmt);
				
				//update codec
				tmp_string = config_setting_get_member(video, "codec");
				if(!tmp_string)
					tmp_string = config_setting_add(video, "codec", CONFIG_TYPE_STRING);
				if (OutDevs[i]->video_codec == AV_CODEC_ID_H264) {
					config_setting_set_string(tmp_string, "H264");
				} else if (OutDevs[i]->video_codec == AV_CODEC_ID_FLV1) {
					config_setting_set_string(tmp_string, "Mpeg");
				}

				//update queue_size
				tmp_int = config_setting_get_member(video, "queue_size");
				if(!tmp_int)
					tmp_int = config_setting_add(video, "queue_size", CONFIG_TYPE_INT);
				config_setting_set_int(tmp_int, OutDevs[i]->video_queue_size);

				//update max buffer rate
				tmp_int = config_setting_get_member(video, "max_buf_rate");
				if(!tmp_int)
					tmp_int = config_setting_add(video, "max_buf_rate", CONFIG_TYPE_INT);
				config_setting_set_int(tmp_int, OutDevs[i]->max_buf_rate);

				//update baseline
				tmp_int = config_setting_get_member(video, "baseline");
				if(!tmp_int)
					tmp_int = config_setting_add(video, "baseline", CONFIG_TYPE_INT);
				config_setting_set_int(tmp_int, OutDevs[i]->baseline);


				//update queue_size
				tmp_int = config_setting_get_member(video, "frame_rate");
				if(!tmp_int)
					tmp_int = config_setting_add(video, "frame_rate", CONFIG_TYPE_INT);
				config_setting_set_int(tmp_int, OutDevs[i]->frame_rate);


			} else {

				//create video
				video = config_setting_add(output, "video", CONFIG_TYPE_GROUP);

				//save name
				in_video  = config_setting_add(video, "in_name", CONFIG_TYPE_LIST);
				
				for(j = 0; j < OutDevs[i]->num_video; j++){
					tmp_video = config_setting_add(in_video, "video", CONFIG_TYPE_GROUP);
					
					//video name
					tmp_string = config_setting_add(tmp_video, "name", CONFIG_TYPE_STRING);
					config_setting_set_string(tmp_string, OutDevs[i]->in_video[j]);

					//video key
					tmp_string = config_setting_add(tmp_video, "key", CONFIG_TYPE_STRING);
					config_setting_set_string(tmp_string, OutDevs[i]->in_video_key[j]);


					//video type
					tmp_int = config_setting_add(tmp_video, "in_video_type", CONFIG_TYPE_INT);
					config_setting_set_int(tmp_int, OutDevs[i]->in_video_type[j]);
				}

				//remove video
				for(j = OutDevs[i]->num_video - 1; j >= 0; j--){
					if(OutDevs[i]->in_video_state[j] == 1){
						config_setting_remove_elem(in_video, j);
					}
				}

				//save bit_rate
				tmp_int = config_setting_add(video, "bit_rate", CONFIG_TYPE_INT);
				config_setting_set_int(tmp_int, OutDevs[i]->video_bit_rate);

				//save group_size
				tmp_int = config_setting_add(video, "group_size", CONFIG_TYPE_INT);
				config_setting_set_int(tmp_int, OutDevs[i]->gop_size);

				//save width 
				tmp_int = config_setting_add(video, "width", CONFIG_TYPE_INT);
				config_setting_set_int(tmp_int, OutDevs[i]->width);

				//save height
				tmp_int = config_setting_add(video, "height", CONFIG_TYPE_INT);
				config_setting_set_int(tmp_int, OutDevs[i]->height);

				//save codec
				tmp_string = config_setting_add(video, "codec", CONFIG_TYPE_STRING);
				if (OutDevs[i]->video_codec == AV_CODEC_ID_H264) {
					config_setting_set_string(tmp_string, "H264");
				} else if (OutDevs[i]->video_codec == AV_CODEC_ID_FLV1) {
					config_setting_set_string(tmp_string, "Mpeg");
				}

				//save queue_size
				tmp_int = config_setting_add(video, "queue_size", CONFIG_TYPE_INT);
				config_setting_set_int(tmp_int, OutDevs[i]->video_queue_size);

				//save max buffer rate
				tmp_int = config_setting_add(video, "max_buf_rate", CONFIG_TYPE_INT);
				config_setting_set_int(tmp_int, OutDevs[i]->max_buf_rate);

				//save baseline
				tmp_int = config_setting_add(video, "baseline", CONFIG_TYPE_INT);
				config_setting_set_int(tmp_int, OutDevs[i]->baseline);

				//save frame_rate
				tmp_int = config_setting_add(video, "frame_rate", CONFIG_TYPE_INT);
				config_setting_set_int(tmp_int, OutDevs[i]->frame_rate);
			}

		} else {
			video = config_setting_get_member(output, "video");
			if(video){
				//destroy video 
				config_setting_remove(output, "video");
			}
		}

		if(OutDevs[i]->video_stream == NOT_USE_STREAM){
			config_setting_remove(output, "video");
		}
	}

	//remove input
	for (i = numOut-1; i >= 0; i--) {
		if(OutDevs[i]->state == STATE_DELETED){
			config_setting_remove_elem(list_output, i);
		}
	}

	config_write_file(&cfg, config_name);

	config_destroy(&cfg);
}

/*
 * Function:  save_info_to_config_file 
 * --------------------
 * Save output disable info to config file.
 *
 *	outDevs: Output information.
 *
 * 	numOut: Number of output streams.
 */
void save_info_to_config_file(OutputInfo** OutDevs, int numOut){
	//init
	int i, j;
	int num_output = 0, num_video = 0;;
	config_t cfg;
	config_setting_t *list_output;
	config_setting_t *output;

	//tmp data
	config_setting_t* tmp_int;
	config_setting_t* tmp_string;

	char *tmp;

	int numOutPut;
	char* config_name = "config.cfg";

	config_init(&cfg);

	//open file
	if (!config_read_file(&cfg, config_name)) {
		printf("can't read file config %s\n", config_name);
		printf("%s:%d  %s\n", config_error_file(&cfg), config_error_line(&cfg),
				config_error_text(&cfg));
		fflush(stdout);
		config_destroy(&cfg);
	}
	int index = 0;
	list_output = config_lookup(&cfg, "outputs");
	num_output = config_setting_length(list_output);

	for(i = 0; i < numOut; i++){
		if(OutDevs[i]->state == 0){
			output = config_setting_get_elem(list_output, index);

			//update disable
			tmp_string = config_setting_get_member(output, "disable");
			if(!tmp_string)
				tmp_string = config_setting_add(output, "disable", CONFIG_TYPE_STRING);
			if(!OutDevs[i]->disable)
				config_setting_set_string(tmp_string, "false");
			else 
				config_setting_set_string(tmp_string, "true");

			//update index
			index++;
		}
	}

	config_write_file(&cfg, config_name);

	config_destroy(&cfg);
	return;
}

/*
 * Function:  save_output_url_to_config_file 
 * --------------------
 * Save output url to config file.
 *
 *	outDevs: Output information.
 *
 * 	numOut: Number of output streams.
 */
void save_output_url_to_config_file(OutputInfo** OutDevs, int numOut){
	//init
	int i, j;
	config_t cfg;
	config_setting_t *list_output;
	config_setting_t *output;

	//tmp data
	config_setting_t* tmp_string;

	char *tmp;

	int numOutPut;
	char* config_name = "config.cfg";

	config_init(&cfg);

	//open file
	if (!config_read_file(&cfg, config_name)) {
		printf("can't read file config %s\n", config_name);
		printf("%s:%d  %s\n", config_error_file(&cfg), config_error_line(&cfg),
				config_error_text(&cfg));
		fflush(stdout);
		config_destroy(&cfg);
	}

	list_output = config_lookup(&cfg, "outputs");

	for(i = 0; i < numOut; i++){
		if(OutDevs[i]->output_type == 0){
			output = config_setting_get_elem(list_output, i);
			if(!output)
				output = config_setting_add(list_output, "output", CONFIG_TYPE_GROUP);
			//---------------general-------------

	        //update output url
	        tmp_string = config_setting_get_member(output, "url");
	        if(!tmp_string)
				tmp_string = config_setting_add(output, "url", CONFIG_TYPE_STRING);
			config_setting_set_string(tmp_string, OutDevs[i]->url);
		}
	}

	config_write_file(&cfg, config_name);

	config_destroy(&cfg);
}

/*
 * Function:  read_course_data 
 * --------------------
 * Read course, version, lecture, processed home, recorded home from config file.
 *
 *	path: Recorded data home.
 *
 *	data_path: Processed data home.
 *
 * 	course: Current course.
 *
 *	version: Current version.
 *
 * 	lecture: Current lecture.
 *
 *	session: Session path prefix.
 */
void read_course_data(char *path, char *data_path, char *course, 
			char *version, char *lecture, char *session, int *refresh_time, int *break_time, int *down_time)
{
	config_t cfg;
	config_setting_t* tmp_string;
	config_setting_t* current_data;
	config_setting_t* home_path;
	config_setting_t* current_course;
	config_setting_t* current_version;
	config_setting_t* current_lecture;

	const char* string = "";

	int sub_index, ver_index, lec_index;
	char* config_name = "data_output_config.cfg";

	config_init(&cfg);

	// read config
	if (!config_read_file(&cfg, config_name)) {
		printf("can't read file config %s\n", config_name);
		printf("%s:%d  %s\n", config_error_file(&cfg), config_error_line(&cfg),
				config_error_text(&cfg));
		fflush(stdout);
		config_destroy(&cfg);
	}

	//read current data save
	//read current data
	current_data = config_lookup(&cfg, "current_data");

	//read home path
	if (config_setting_lookup_string(current_data, "home_path", &string) == CONFIG_TRUE)
		strcpy(path, string);

	//read home path
	if (config_setting_lookup_string(current_data, "data_path", &string) == CONFIG_TRUE)
		strcpy(data_path, string);

	//read current course
	if (config_setting_lookup_string(current_data, "current_course", &string) == CONFIG_TRUE)
		strcpy(course, string);

	//read current version
	if (config_setting_lookup_string(current_data, "current_version", &string) == CONFIG_TRUE)
		strcpy(version, string);
	//read current lecture
	if (config_setting_lookup_string(current_data, "current_lecture", &string) == CONFIG_TRUE)
		strcpy(lecture, string);

	if (config_setting_lookup_string(current_data, "current_session", &string) == CONFIG_TRUE)
		strcpy(session, string);

	//read network time
	config_setting_lookup_int(current_data, "network_refresh_time", refresh_time);
	config_setting_lookup_int(current_data, "network_break_times", break_time);
	config_setting_lookup_int(current_data, "network_down_times", down_time);


	//read complete
	config_destroy(&cfg);
	return;
}

/*
 * Function:  read_server_config 
 * --------------------
 * Read server data from config file.
 *
 *	path: Server path.
 *
 *	server: Server Ip.
 *
 * 	user: User name to login to server.
 */
void read_server_config(char *server, char *user, char *path){
	config_t cfg;
	config_setting_t* tmp_string;
	config_setting_t* upload_data;
	config_setting_t* home_path;
	config_setting_t* current_course;
	config_setting_t* current_version;
	config_setting_t* current_lecture;

	const char* string = "";
	int sub_index, ver_index, lec_index;
	char* config_name = "data_output_config.cfg";

	config_init(&cfg);

	// read config
	if (!config_read_file(&cfg, config_name)) {
		printf("can't read file config %s\n", config_name);
		printf("%s:%d  %s\n", config_error_file(&cfg), config_error_line(&cfg),
				config_error_text(&cfg));
		fflush(stdout);
		config_destroy(&cfg);
	}

	//read current data
	upload_data = config_lookup(&cfg, "upload_data");

	//read server
	if (config_setting_lookup_string(upload_data, "server", &string) == CONFIG_TRUE)
		strcpy(server, string);

	//read user
	if (config_setting_lookup_string(upload_data, "user", &string) == CONFIG_TRUE)
		strcpy(user, string);

	//read server path
	if (config_setting_lookup_string(upload_data, "server_path", &string) == CONFIG_TRUE)
		strcpy(path, string);

	//read complete
	config_destroy(&cfg);
	return;
}

/*
 * Function:  read_class_config 
 * --------------------
 * Read class data from config file.
 *
 *	class_id: Current class id.
 *
 *	term: Current term.
 */
void read_class_config(char *class_id, char *term){
	config_t cfg;
	config_setting_t* tmp_string;
	config_setting_t* upload_data;
	config_setting_t* home_path;

	const char* string = "";
	int sub_index, ver_index, lec_index;
	char* config_name = "data_output_config.cfg";

	config_init(&cfg);

	// read config
	if (!config_read_file(&cfg, config_name)) {
		printf("can't read file config %s\n", config_name);
		printf("%s:%d  %s\n", config_error_file(&cfg), config_error_line(&cfg),
				config_error_text(&cfg));
		fflush(stdout);
		config_destroy(&cfg);
	}

	//read current data
	upload_data = config_lookup(&cfg, "upload_data");

	if (config_setting_lookup_string(upload_data, "class_id", &string) == CONFIG_TRUE)
		strcpy(class_id, string);

	if (config_setting_lookup_string(upload_data, "term", &string) == CONFIG_TRUE)
		strcpy(term, string);

	//read complete
	config_destroy(&cfg);
	return;
}

/*
 * Function:  read_server_config 
 * --------------------
 * Save server data to config file.
 *
 *	path: Server path.
 *
 *	server: Server Ip.
 *
 * 	user: User name to login to server.
 */
void save_server_info_to_config(char *server, char *user, char *path){
	config_t cfg;
	config_setting_t* tmp_string;
	config_setting_t* upload_data;

	const char* string = "";
	int sub_index, ver_index, lec_index;
	char* config_name = "data_output_config.cfg";

	config_init(&cfg);

	// read config
	if (!config_read_file(&cfg, config_name)) {
		printf("can't read file config %s\n", config_name);
		printf("%s:%d  %s\n", config_error_file(&cfg), config_error_line(&cfg),
				config_error_text(&cfg));
		fflush(stdout);
		config_destroy(&cfg);
	}

	//save current data
	upload_data = config_lookup(&cfg, "upload_data");

	//save home path
	tmp_string = config_setting_get_member(upload_data, "server");
	config_setting_set_string(tmp_string, server);

	//save user
	tmp_string = config_setting_get_member(upload_data, "user");
	config_setting_set_string(tmp_string, user);

	//save current path
	tmp_string = config_setting_get_member(upload_data, "server_path");
	config_setting_set_string(tmp_string, path);

	//save complete
	config_write_file(&cfg, config_name);
	config_destroy(&cfg);
	return;
}

/*
 * Function:  save_class_info_to_config 
 * --------------------
 * Save class data to config file.
 *
 *	class_id: Current class id.
 *
 *	term: Current term.
 */
void save_class_info_to_config(char *class_id, char *term){
	config_t cfg;
	config_setting_t* tmp_string;
	config_setting_t* upload_data;

	const char* string = "";
	int sub_index, ver_index, lec_index;
	char* config_name = "data_output_config.cfg";

	config_init(&cfg);

	// read config
	if (!config_read_file(&cfg, config_name)) {
		printf("can't read file config %s\n", config_name);
		printf("%s:%d  %s\n", config_error_file(&cfg), config_error_line(&cfg),
				config_error_text(&cfg));
		fflush(stdout);
		config_destroy(&cfg);
	}

	//save current data
	upload_data = config_lookup(&cfg, "upload_data");
	//save class id
	tmp_string = config_setting_get_member(upload_data, "class_id");
	config_setting_set_string(tmp_string, class_id);

	//save term
	tmp_string = config_setting_get_member(upload_data, "term");
	config_setting_set_string(tmp_string, term);

	//save complete
	config_write_file(&cfg, config_name);
	config_destroy(&cfg);
	return;
}

/*
 * Function:  save_class_info_to_config 
 * --------------------
 * Save Processed data, recorded data to config file.
 *
 *	class_id: Current class id.
 *
 *	term: Current term.
 */
void save_location_setting(char *path, char *data_path, char *course, 
	char *version, char *lecture, char *session)
{
	config_t cfg;
	config_setting_t* tmp_string;

	config_setting_t* current_data;
	config_setting_t* home_path;
	config_setting_t* data;
	config_setting_t* current_course;
	config_setting_t* current_version;
	config_setting_t* current_lecture;
	config_setting_t* current_session;

	const char* string = "";
	int sub_index, ver_index, lec_index;
	char* config_name = "data_output_config.cfg";

	config_init(&cfg);

	// read config
	if (!config_read_file(&cfg, config_name)) {
		printf("can't read file config %s\n", config_name);
		printf("%s:%d  %s\n", config_error_file(&cfg), config_error_line(&cfg),
				config_error_text(&cfg));
		fflush(stdout);
		config_destroy(&cfg);
	}

	//read current data
	current_data = config_lookup(&cfg, "current_data");

	//save home path
	home_path = config_setting_get_member(current_data, "home_path");
	config_setting_set_string(home_path, path);

	data = config_setting_get_member(current_data, "data_path");
	config_setting_set_string(data, data_path);

	//save current course
	current_course = config_setting_get_member(current_data, "current_course");
	config_setting_set_string(current_course, course);

	//read current version
	current_version = config_setting_get_member(current_data, "current_version");
	config_setting_set_string(current_version, version);

	//read current lecture
	current_lecture = config_setting_get_member(current_data, "current_lecture");
	config_setting_set_string(current_lecture, lecture);

	current_session = config_setting_get_member(current_data, "current_session");
	config_setting_set_string(current_session, session);

	//read complete
	config_write_file(&cfg, config_name);
	config_destroy(&cfg);
	return;
}
