/*
 * File: frame_queue.c
 *
 * Author: GVLab
 *
 * Created on: Aug 13, 2015
 */

#include "headers/model/frame_queue.h"
#include "iostream"

 /*
 * Function:  frame_queue_init
 * --------------------
 * Init frame queue.
 *
 *  queue: Frame queue.
 *
 *	max_size: Max size of queue.
 */
void frame_queue_init(FrameQueue * queue, int max_size) {
    //malloc memory
    if (queue == NULL)
        queue = (FrameQueue*) malloc(sizeof(FrameQueue));

    //set defaule value
    queue->max_size = max_size;
    queue->frameq = (AVFrame**) malloc(sizeof(AVFrame*) * queue->max_size);
    queue->size = 0;
    queue->rindex = 0;
    queue->windex = 0;
    queue->output_id = 0;
    queue->mutex = (pthread_mutex_t*) malloc(sizeof(pthread_mutex_t));
    pthread_mutex_init(queue->mutex, NULL);
    queue->cond = (pthread_cond_t*) malloc(sizeof(pthread_cond_t));
    pthread_cond_init(queue->cond, NULL);

    //print log file
    queue->catLogQueue = log4c_category_get("log.app.frame_queue");
    log4c_category_log(queue->catLogQueue, LOG4C_PRIORITY_INFO,
            "Queue's initialed! Queue size: %d\n", queue->max_size);
}

 /*
 * Function:  frame_queue_deinit
 * --------------------
 * Destroy frame queue.
 *
 *  queue: Frame queue.
 */
void frame_queue_deinit(FrameQueue * queue) {
    int i;
    //remove remain frame in queue
    for (i = 0; i < queue->size; i++) {
        av_free(queue->frameq[queue->rindex]);
        queue->rindex = (queue->rindex + 1) % queue->max_size;
    }

    //free memory
    free(queue->frameq);
    queue->frameq = NULL;
    pthread_cond_destroy(queue->cond);
    free(queue->cond);
    pthread_mutex_destroy(queue->mutex);
    free(queue->mutex);
}

 /*
 * Function:  frame_queue_deinit
 * --------------------
 * Destroy frame queue.
 *
 *  queue: Frame queue.
 *
 *	frame: Frame to be put.
 * --------------------
 *  Return: Return 0 mean put frame to queue ok.
 *			Return 1 mean put frame to queue error.
 */
int frame_queue_put(FrameQueue *queue, AVFrame *frame, int *quit) {
    int result;

    //check size of queue if queue is full
    if (queue->size < queue->max_size) {
        //put frame to queue
        queue->frameq[queue->windex] = frame;
        pthread_mutex_lock(queue->mutex);
        queue->size++;
        pthread_cond_signal(queue->cond);
        pthread_mutex_unlock(queue->mutex);
        queue->windex = (queue->windex + 1) % queue->max_size;
        result = 0;
    } else {

        //put error
        result = -1;
        log4c_category_log(queue->catLogQueue, LOG4C_PRIORITY_WARN,
                "Queue is full!");
    }

    return result;
}

 /*
 * Function:  frame_queue_get
 * --------------------
 * Get frame from queue.
 *
 *  queue: Frame queue.
 *
 *	frame: Frame get from queue.
 *
 *	running: Running status of program.
 * --------------------
 *  Return: Return 0 mean get frame from queue ok.
 *			Return 1 mean get frame from queue error.
 */
int frame_queue_get(FrameQueue* queue, AVFrame **frame, int* running) {
    int result;
    pthread_mutex_lock(queue->mutex);

    //check queue size until queue not empty
    while (queue->size < 1 && *running) {
        pthread_cond_wait(queue->cond, queue->mutex);
    }
    if (queue->size > 0) {
        //get frame from queue
        *frame = queue->frameq[queue->rindex];
        queue->frameq[queue->rindex] = NULL;
        queue->size--;
        queue->rindex = (queue->rindex + 1) % queue->max_size;
        result = 0;
    } else
        result = -1;
    pthread_mutex_unlock(queue->mutex);
    return result;
}
