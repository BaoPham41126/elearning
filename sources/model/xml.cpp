/*
 * File: xml.c
 *
 * Author: GVLab
 *
 * Created on: Aug 13, 2015
 */

#include "headers/model/xml.h"
extern "C" {
#include <libxml2/libxml/parser.h>
#include <libxml2/libxml/tree.h>
#include <libxml2/libxml/xmlreader.h>
}

char subject_file[20] = "course.xml";
char version_file[20] = "version.xml";
char lecture_file[20] = "chapter.xml";
char session_file[20] = "session.xml";
char metadata_file[20] = "metadata.xml";
char catalog_file[20] = "episode.xml";
char att[10] = "name";
char id[10] = "id";

/*
 * Function:  get_course_names 
 * --------------------
 * Get course name from xml file.
 *
 *  root: Node root.
 *
 *  att: Attribute name of course.
 *
 *  dataInfo: Data information(Course, version, lecture).
 */
void get_course_names(xmlNode *root, char *att, DataInfo *dataInfo){
	xmlNode *cur_node = NULL;
	xmlChar *uri;
	dataInfo->num_subject = 0;

    for (cur_node = root->children; cur_node; cur_node = cur_node->next) {
        
        //get course id

        uri = (xmlChar*) xmlGetProp(cur_node, (xmlChar*) id);
        if(uri){
            strcpy(dataInfo->subject_list[dataInfo->num_subject].subject_id, (char*) uri);
        }

        //get name
        uri = xmlGetProp(cur_node, (xmlChar*) att);
        if(uri){
            strcpy(dataInfo->subject_list[dataInfo->num_subject].subject_name, (char*) uri);
        	dataInfo->num_subject++;
        }
    }
}

/*
 * Function:  get_version_names 
 * --------------------
 * Get version name from xml file.
 *
 *  root: Node root.
 *
 *  att: Attribute name of course.
 *
 *  dataInfo: Data information(Course, version, lecture).
 */
void get_version_names(xmlNode *root, char *att, SubjectInfo *subject){
	xmlNode *cur_node = NULL;
	xmlChar *uri;
	subject->num_version = 0;

    //get properties value
    for (cur_node = root->children; cur_node; cur_node = cur_node->next) {
        uri = xmlGetProp(cur_node, (xmlChar*) att);
        if(uri){
            strcpy(subject->version_list[subject->num_version].version_name, (char*) uri);
        	subject->num_version++;
        }
    }
}

/*
 * Function:  get_lecture_names 
 * --------------------
 * Get lecture name from xml file.
 *
 *  root: Node root.
 *
 *  att: Attribute name of course.
 *
 *  dataInfo: Data information(Course, version, lecture).
 */
void get_lecture_names(xmlNode *root, char *att, VersionInfo *version){
	xmlNode *cur_node = NULL;
	xmlChar *uri;
	version->num_lecture = 0;

    //get all lecture name
    for (cur_node = root->children; cur_node; cur_node = cur_node->next) {
        uri = xmlGetProp(cur_node, (xmlChar*) att);
        if(uri){
            // get lecture name
            strcpy(version->lecture_list[version->num_lecture].lecture_name, (char*) uri);
        	version->num_lecture++;
        }
    }
}

/*
 * Function:  get_lecture_description 
 * --------------------
 * Get lecture description from xml file.
 *
 *  root: Node root.
 *
 *  lecture: Current lecture.
 *
 *  description: Result description get from xml file.
 */
void get_lecture_description(xmlNode *root, char *lecture, char *description){
    xmlNode *cur_node = NULL;
    xmlChar *uri;

    //check all node
    for (cur_node = root->children; cur_node; cur_node = cur_node->next) {
        uri = xmlGetProp(cur_node, (xmlChar*) att);
        if(uri){
            //get lecture description
            if(strcmp(lecture, (char*) uri) == 0){
                uri = xmlGetProp(cur_node, (xmlChar*) "description");
                if(uri){
                    strcpy(description, (char*) uri);
                }
            }
        }
    }
}

/*
 * Function:  update_description 
 * --------------------
 * Update or add lecture description to xml file.
 *
 *  root: Node root.
 *
 *  lecture: Current lecture.
 *
 *  description: Sescription to be passed to xml file.
 */
void update_description(xmlNode *root, char *lecture, char *description){
    xmlNode *cur_node = NULL;
    xmlChar *uri;

    //check all node
    for (cur_node = root->children; cur_node; cur_node = cur_node->next) {
        uri = xmlGetProp(cur_node, (xmlChar*) att);
        if(uri){
            //update lecture description to xml file
            if(strcmp(lecture, (char*) uri) == 0){
                xmlSetProp(cur_node, (xmlChar*) "description", (xmlChar*) description);
            }
        }
    }
}

/*
 * Function:  open_xml_file 
 * --------------------
 * Open xml file.
 *
 *  file_path: Path of xml file.
 * --------------------
 *  Return: The root node of xml file.
 */
xmlNodePtr open_xml_file(char *file_path){
	xmlDoc *doc = NULL;

	//parse the file and get the DOM
    doc = xmlReadFile(file_path, NULL, 0);

    if (doc == NULL) {
        return NULL;
    }

    //Get the root element node
    return xmlDocGetRootElement(doc);
}

//read lecture file
void read_lecture_file(VersionInfo *version, char *home_path){
	char file_path[SHORT_STRING_LENGTH];
	char file_xml[SHORT_STRING_LENGTH];
	char tmp_path[SHORT_STRING_LENGTH];
    struct stat st = {0};
    xmlNode *root;

    sprintf(file_path, "%s/%s", home_path, version->version_name);
    sprintf(file_xml, "%s/%s", file_path, lecture_file);

    if (!stat(file_path, &st)){
    	//path exist
        if(!access(file_xml, F_OK)){
        	//file exist
        	root = open_xml_file(file_xml);

            //get lectures name in lecture file
        	if(root)
	        	get_lecture_names(root, att, version);
        }
    }
}

/*
 * Function:  read_version_file 
 * --------------------
 * Read version file and get all version names base on current course.
 * Call read lecture function to read lecture name for each version.
 *
 *  course: Current course.
 *
 *  lecture: Path to recorded data home.
 */
void read_version_file(SubjectInfo *course, char *home_path){
	char file_path[SHORT_STRING_LENGTH];
	char file_xml[SHORT_STRING_LENGTH];
	char tmp_path[SHORT_STRING_LENGTH];
    struct stat st = {0};
    xmlNode *root;
    int i;

    sprintf(file_path, "%s/%s", home_path, course->subject_name);
    sprintf(file_xml, "%s/%s", file_path, version_file);


    if (!stat(file_path, &st)){
    	//path exist
        if(!access(file_xml, F_OK)){
        	//file exist
        	root = open_xml_file(file_xml);

        	if(root){
                //get version names
	        	get_version_names(root, att, course);

                //read all lecture
	        	for(i = 0; i < course->num_version; i++){
			    	read_lecture_file(&course->version_list[i], file_path);
			    }
        	}
        }
    }
}

/*
 * Function:  read_course_file 
 * --------------------
 * Read course file and get all course names base on current recorded
 *  data home path.
 * Call read version function to read version name for each course.
 *
 *  dataInfo: Data information(Course, version, lecture).
 *
 *  home_path: Path to recorded data home.
 */
void read_course_file(DataInfo** dataInfo, char *home_path){
    xmlNode *root_element = NULL;

    int i;
    

    DataInfo* tmp_data = (DataInfo*) malloc(sizeof(DataInfo));
    *dataInfo = tmp_data;
    
    /*
     * this initialize the library and check potential ABI mismatches
     * between the version it was compiled for and the actual shared
     * library used.
     */
    char file_path[SHORT_STRING_LENGTH];
    sprintf(file_path, "%s/%s", home_path, subject_file);
    

    root_element = open_xml_file(file_path);

    /*
     *Free the global variables that may
     *have been allocated by the parser.
     */
    
    if(root_element){
        //get all course name
	    get_course_names(root_element, att, tmp_data);

        //read all version file
	    for(i = 0; i < tmp_data->num_subject; i++){
	    	read_version_file(&tmp_data->subject_list[i], home_path);
	    }
	}

    xmlCleanupParser();
};

/*
 * Function:  add_new_attribue 
 * --------------------
 * Add new attribute to NodeInfo.
 *
 *  node: Node information.
 *
 *  name: Name of attribute to be added.
 *
 *  value: Value of attribute to be added.
 */
void add_new_attribue(NodeInfo *node, char *name, char *value){
	strcpy(node->att_name[node->index], name);
	strcpy(node->att_value[node->index], value);
	node->index++;
}

/*
 * Function:  create_new_node 
 * --------------------
 * Create new nodeinfo base on NodeInfo.
 *
 *  node: Information of new node.
 * --------------------
 *  Return: The new node created.
 */
xmlNodePtr create_new_node(NodeInfo *node){
	//create new node
	xmlNodePtr nodePtr = NULL;
	int i;
    nodePtr = xmlNewNode(0, (xmlChar*)node->node_name);

    //set node value
    for(i = 0; i < node->index; i++){

        xmlNewProp(nodePtr, (xmlChar*) node->att_name[i], (xmlChar*) node->att_value[i]);
    }

	return nodePtr;
}

/*
 * Function:  create_new_element 
 * --------------------
 * Create new xml element base on given NodeInfo.
 *
 *  path: Path to xml file.
 *
 *  xml_file: Name of xml file.
 *
 *  node: Node information.
 */
void create_new_element(char *path, char *xml_file, NodeInfo node){
	char tmp_path[SHORT_STRING_LENGTH];
    struct stat st = {0};
    // update chapter.xml file
    sprintf(tmp_path, "%s/%s", path, xml_file);

    xmlDoc *doc = NULL;
    xmlNode *root = NULL;
    xmlNodePtr cur_node = NULL;
    xmlChar *uri;
    xmlNodePtr nodePtr= create_new_node(&node);

    LIBXML_TEST_VERSION;

	//open xml file in tree mode
    doc = xmlReadFile(tmp_path, NULL, 0);

    if (doc == NULL) {
        doc = xmlNewDoc((xmlChar*)"1.0");
	    root = xmlNewNode(0, (xmlChar*)"xml");
	    xmlDocSetRootElement(doc, root);

	    //add new child
	    xmlAddChild(root, nodePtr);
    } else {
	    //Get the root element node 
	    root = xmlDocGetRootElement(doc);

	    //check node exist
	    int check = 1;
	    for (cur_node = root->children; cur_node; cur_node = cur_node->next) {
            uri = xmlGetProp(cur_node, (xmlChar*) node.att_name[0]);
	        if(uri){
                if(strcmp(node.att_value[0], (char*) uri) == 0)
	        		check = 0;
	        }
	    }
	    if(check)
			xmlAddChild(root, nodePtr);
	}
    

    //Dumping document to stdio or file
     
	xmlSaveFormatFileEnc(tmp_path, doc, "UTF-8", 1);
    //free the document 
    xmlFreeDoc(doc);

    /*
     *Free the global variables that may
     *have been allocated by the parser.
     */
    xmlCleanupParser();

    // this is to debug memory for regression tests

    xmlMemoryDump();
}

/*
 * Function:  create_new_lecture 
 * --------------------
 * Create new lecture xml file base on current course, path, version, lecture, description.
 *
 *  home_path: Recorded data home path.
 *
 *  course_name: Current course.
 *
 *  version_name: Current version.
 *
 *  lecture_name: Current lecture.
 *
 *  description: Current description.
 */
void create_new_lecture(char *home_path, char *course_name, char *version_name, 
    char *lecture_name, char *description)
{
	
	char tmp_path[SHORT_STRING_LENGTH];
	NodeInfo node;
	
	node.index = 0;

    //set lecture file information
	strcpy(node.node_name, "chapter");

    //create node
	add_new_attribue(&node, "name", lecture_name);
    add_new_attribue(&node, "description", description);

    //create lecture file
	sprintf(tmp_path, "%s/%s/%s", home_path, course_name, version_name);
	create_new_element(tmp_path, lecture_file, node);
}

/*
 * Function:  create_new_version 
 * --------------------
 * Create new version xml file base on current course, path, version.
 *
 *  home_path: Recorded data home path.
 *
 *  course_name: Current course.
 *
 *  version_name: Current version.
 */
void create_new_version(char *home_path, char *course_name, char *version_name){
	char tmp_path[SHORT_STRING_LENGTH];
	NodeInfo node;
	
	node.index = 0;
    //set version first node name
	strcpy(node.node_name, "version");

    //create node
	add_new_attribue(&node, "name", version_name);

    //create version file
	sprintf(tmp_path, "%s/%s", home_path, course_name);
	create_new_element(tmp_path, version_file, node);
}

/*
 * Function:  create_new_course 
 * --------------------
 * Create new course xml file base on current course, path.
 *
 *  home_path: Recorded data home path.
 *
 *  course_name: Current course.
 *
 *  course_id: Current course id.
 */
void create_new_course(char *home_path, char *course_name, char *course_id){
	NodeInfo node;
	node.index = 0;

    //set course first node
	strcpy(node.node_name, "course");
    
    //create node
	add_new_attribue(&node, "name", course_name);
    add_new_attribue(&node, "id", course_id);
	create_new_element(home_path , subject_file, node);
}

/*
 * Function:  convert_output_data 
 * --------------------
 * Convert output stream information to Node information.
 * Create xml file contain all Node information.
 *
 *  outDevs: Output information.
 *
 *  path: Path to description file.
 *
 *  node_name: Node contain all output information.
 */
void convert_output_data(OutputInfo* outDevs, char *path, char *node_name){
	char tmp_att[40];
	NodeInfo node;
	node.index = 0;

    //create node for screen
	strcpy(node.node_name, "output");
	sprintf(tmp_att, "%s", node_name);
	add_new_attribue(&node, "name", tmp_att);
	sprintf(tmp_att, "%dx%d", outDevs->width, outDevs->height);
	add_new_attribue(&node, "video_size", tmp_att);
	sprintf(tmp_att, "%d", outDevs->frame_rate);
	add_new_attribue(&node, "frame_rate", tmp_att);

    //create description file
	create_new_element(path , session_file, node);
}

/*
 * Function:  create_video_description_file 
 * --------------------
 * Get path, output information to create description file.
 *
 *  home_path: Path to current lecture.
 *
 *  session: Current session.
 *
 *  outDevs: Output information.
 *
 *  numOut: Number of output streams.
 *
 *  current_session: Current path prefix name.
 */
void create_video_description_file(char *home_path, int session, OutputInfo** outDevs, 
    int numOut, char *current_session)
{
	//get path
	char tmp[SHORT_STRING_LENGTH];
    char tmp_file[STRING_LENGTH];
    char tmp_string[STRING_LENGTH];
	
    //set path
    sprintf(tmp, "%s/%s%d", home_path, current_session, session);
	
    //create video desciption xml file
    int i;
    for(i = 0; i < numOut; i++){
        if(outDevs[i]->output_type == 0 && outDevs[i]->state == 0){
            //get file name from url
            get_filename(outDevs[i]->url, tmp_file);

            //pass file name and infor to xml file
            sprintf(tmp_string, "%s.%s", tmp_file, outDevs[i]->format);
            convert_output_data(outDevs[i], tmp, tmp_string);
        }
    }
}

/*
 * Function:  create_metadata 
 * --------------------
 * Create metadata file base on course information.
 *
 *  path: Path to current session folder.
 *
 *  course_name: Current course.
 */
void create_metadata(char *path, char *course_name){
    char tmp_path[SHORT_STRING_LENGTH];
    struct stat st = {0};
    
    // update chapter.xml file
    sprintf(tmp_path, "%s/%s", path, metadata_file);
    xmlDoc *doc = NULL;
    xmlNode *root = NULL;
    xmlNodePtr cur_node = NULL;
    xmlChar *uri;

    LIBXML_TEST_VERSION;

    //parse the file and get the DOM
    doc = xmlReadFile(tmp_path, NULL, 0);

    //open file
    doc = xmlNewDoc((xmlChar*) "1.0");
    root = xmlNewNode(0, (xmlChar*)"session");
    xmlDocSetRootElement(doc, root);

    //add new child
    xmlNewChild(root, NULL, (xmlChar*) "courseName",
                (xmlChar*) course_name);
    xmlNewChild(root, NULL, (xmlChar*) "videoTitle",
                (xmlChar*) course_name);
    xmlNewChild(root, NULL, (xmlChar*) "sessionTitle",
                NULL);

    xmlSaveFormatFileEnc(tmp_path, doc, "UTF-8", 1);
    /*free the document */
    xmlFreeDoc(doc);

    xmlCleanupParser();

    xmlMemoryDump();
}

/*
 * Function:  create_catalog 
 * --------------------
 * Create catalog file base on course, version, lecture information.
 *
 *  path: Path to recorded data home.
 *
 *  course_name: Current course.
 *
 *  version: Current version.
 *
 *  lecture: Current lecture.
 */
void create_catalog(char *path, char *course_name, char *version, char *lecture){
    char tmp_path[SHORT_STRING_LENGTH];
    char tmp_value[STRING_LENGTH];

    time_t current_time;
    char* c_time_string;
    sprintf(tmp_path, "%s/%s", path, catalog_file);
    xmlDoc *doc = NULL;
    xmlNode *root = NULL;
    xmlNodePtr cur_node = NULL;
    xmlChar *uri;

    // Obtain current time.
    current_time = time(NULL);

    if (current_time == ((time_t)-1)){
        (void) fprintf(stderr, "Failure to obtain the current time.\n");
    }

    // Convert to local time format.
    c_time_string = ctime(&current_time);

    if (c_time_string == NULL){
        (void) fprintf(stderr, "Failure to convert the current time.\n");
    }

    LIBXML_TEST_VERSION;
    
    //parse the file and get the DOM
    doc = xmlReadFile(tmp_path, NULL, 0);

    //open xml file
    doc = xmlNewDoc((xmlChar*) "1.0");
    root = xmlNewNode(0, (xmlChar*)"dublincore");

    //create default node
    xmlDocSetRootElement(doc, root);
    xmlNewProp(root, (xmlChar*) "xmlns", (xmlChar*) "http://www.opencastproject.org/xsd/1.0/dublincore/");
    xmlNewProp(root, (xmlChar*) "xmlns:dcterms", (xmlChar*) "http://purl.org/dc/terms/");
    xmlNewProp(root, (xmlChar*) "xmlns:xsi", (xmlChar*) "http://www.w3.org/2001/XMLSchema-instance");

    //add new child
    cur_node = xmlNewChild(root, NULL, (xmlChar*) "dcterms:extent",
                (xmlChar*) "PT400212H18M29.546S");
    xmlNewProp(cur_node, (xmlChar*) "xsi:type", (xmlChar*) "dcterms:ISO8601");

    //add dcterms
    cur_node = xmlNewChild(root, NULL, (xmlChar*) "dcterms:created",
                (xmlChar*) c_time_string);
    xmlNewProp(cur_node, (xmlChar*) "xsi:type", (xmlChar*) "dcterms:W3CDTF");

    //add time
    sprintf(tmp_value, "%s-%s-%s-%s", version, course_name, lecture, c_time_string);
    xmlNewChild(root, NULL, (xmlChar*) "dcterms:identifier",
                (xmlChar*) tmp_value);
    xmlNewChild(root, NULL, (xmlChar*) "dcterms:title",
                (xmlChar*) tmp_value);

    //save data to file
    xmlSaveFormatFileEnc(tmp_path, doc, "UTF-8", 1);
    
    //free the document
    xmlFreeDoc(doc);

    xmlCleanupParser();

    xmlMemoryDump();
}

/*
 * Function:  read_lecture_description 
 * --------------------
 * Read current lecture description from xml file.
 *
 *  path: Path to Current version in recorded data home.
 *
 *  lecture: Current lecture.
 *
 *  description: The description of current lecture.
 */
void read_lecture_description(char *path, char *lecture, char *description){
    char file_xml[SHORT_STRING_LENGTH];
    struct stat st = {0};
    xmlNode *root;

    sprintf(file_xml, "%s/%s", path, lecture_file);

    if (!stat(path, &st)){
        //path exist
        if(!access(file_xml, F_OK)){
            //file exist
            root = open_xml_file(file_xml);

            //get lecture description
            if(root)
                get_lecture_description(root, lecture, description);
        }
    }
}

/*
 * Function:  update_lecture_description 
 * --------------------
 * Update current lecture description to xml file.
 *
 *  path: Path to Current version in recorded data home.
 *
 *  lecture: Current lecture.
 *
 *  description: The description of current lecture.
 */
void update_lecture_description(char *path, char *lecture, char *description){
    char file_xml[SHORT_STRING_LENGTH];
    struct stat st = {0};
    xmlNode *root;
    xmlDoc *doc = NULL;

    sprintf(file_xml, "%s/%s", path, lecture_file);

    LIBXML_TEST_VERSION;
    
    //parse the file and get the DOM
    doc = xmlReadFile(file_xml, NULL, 0);

    //open xml file in tree mode
    if (doc == NULL) {
        printf("%s\n", file_xml);
        doc = xmlNewDoc((xmlChar*) "1.0");
        root = xmlNewNode(0, (xmlChar*)"xml");
        xmlDocSetRootElement(doc, root);
    } else {
        /*Get the root element node */
        root = xmlDocGetRootElement(doc);

        if(root){
            update_description(root, lecture, description);
        }
    }
    

    // Dumping document to stdio or file
    xmlSaveFormatFileEnc(file_xml, doc, "UTF-8", 1);

    //free the document
    xmlFreeDoc(doc);

    /*
     *Free the global variables that may
     *have been allocated by the parser.
     */
    xmlCleanupParser();

    // this is to debug memory for regression tests
    xmlMemoryDump();
}
