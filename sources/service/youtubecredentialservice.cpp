#include <headers/service/youtubecredentialservice.h>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>
#include <QUrl>
#include <QUrlQuery>
#include <QDesktopServices>
#include <QJsonDocument>
#include <QJsonObject>
#include <QFile>
#include <QTextStream>
#include <iostream>

YoutubeCredentialDto* YoutubeCredentialService::getCurrentCredential(){
    return YoutubeCredentialService::credential;
}

void YoutubeCredentialService::saveCredentialToFile(YoutubeCredentialDto *credential){
    QFile file("credential.txt");
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
        return;
    QTextStream out(&file);
    out << credential->accessToken << "\n" << credential->refreshToken;
    file.close();
}

YoutubeCredentialDto* YoutubeCredentialService::getCredentialFromFile(){
    QFile file("credential.txt");
    if (!file.exists() || !file.open(QIODevice::ReadOnly | QIODevice::Text))
        return NULL;

    QTextStream in(&file);
    YoutubeCredentialDto* credential = new YoutubeCredentialDto();
    // You could use readAll() here, too.
    credential->accessToken = in.readLine();
    credential->refreshToken = in.readLine();

    file.close();
    YoutubeCredentialService::credential = credential;
    return credential;
}

void YoutubeCredentialService::grantPermission(){
    static QString authURL = "https://accounts.google.com/o/oauth2/v2/auth";
    static QString scope = "https://www.googleapis.com/auth/youtube";
    QUrl url(authURL);
    QUrlQuery query;
    query.addQueryItem("scope", scope);
    query.addQueryItem("redirect_uri", "http://localhost/code");
    query.addQueryItem("response_type", "code");
    query.addQueryItem("client_id", YoutubeCredentialService::clientId);
    url.setQuery(query);
    QDesktopServices::openUrl(url);
}

void YoutubeCredentialService::getCredentialByToken(QString code){
    static QString tokenURL = "https://www.googleapis.com/oauth2/v4/token";
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    QUrl url(tokenURL);
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/x-www-form-urlencoded");

    QUrlQuery query;
    query.addQueryItem("client_id", YoutubeCredentialService::clientId);
    query.addQueryItem("client_secret", YoutubeCredentialService::clientSecret);
    query.addQueryItem("code", code);
    query.addQueryItem("grant_type", "authorization_code");
    query.addQueryItem("redirect_uri", "http://localhost/code");

    QByteArray data;
    data.append(query.toString());

    QObject::connect(manager, &QNetworkAccessManager::finished, this, &YoutubeCredentialService::onTokenRequestFinished);
    manager->post(request, data);
}

void YoutubeCredentialService::refreshCredential(){
    static QString tokenURL = "https://www.googleapis.com/oauth2/v4/token";
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    QUrl url(tokenURL);
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/x-www-form-urlencoded");

    QUrlQuery query;
    query.addQueryItem("client_id", this->clientId);
    query.addQueryItem("client_secret", this->clientSecret);
    query.addQueryItem("refresh_token", YoutubeCredentialService::credential->refreshToken);
    query.addQueryItem("grant_type", "refresh_token");
    QByteArray data;
    data.append(query.toString());

    QObject::connect(manager, &QNetworkAccessManager::finished, this, &YoutubeCredentialService::onTokenRefreshFinished);
    manager->post(request, data);
}

void YoutubeCredentialService::onTokenRequestFinished(QNetworkReply *reply){
    if (reply->error() != QNetworkReply::NoError){
        std::cout << reply->errorString().toStdString() << std::endl;
        return;
    }
    QString data = (QString) reply->readAll();
    QJsonDocument jsonResponse = QJsonDocument::fromJson(data.toUtf8());
    QJsonObject json = jsonResponse.object();
    YoutubeCredentialDto *credential = new YoutubeCredentialDto(json["access_token"].toString(), json["refresh_token"].toString());
    this->saveCredentialToFile(credential);
    YoutubeCredentialService::credential = credential;
    Q_EMIT authenticateFinished(credential);
}

void YoutubeCredentialService::onTokenRefreshFinished(QNetworkReply *reply){
    if (reply->error() != QNetworkReply::NoError){
        std::cout << reply->errorString().toStdString() << std::endl;
        return;
    }
    QString data = (QString) reply->readAll();
    QJsonDocument jsonResponse = QJsonDocument::fromJson(data.toUtf8());
    QJsonObject json = jsonResponse.object();
    YoutubeCredentialService::credential->accessToken = json["access_token"].toString();
    this->saveCredentialToFile(YoutubeCredentialService::credential);
    Q_EMIT refreshFinished(YoutubeCredentialService::credential);
}
