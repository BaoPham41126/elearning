#include "headers/service/youtubebroadcastservice.h"
#include <QUrl>
#include <QUrlQuery>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

#include <iostream>
YoutubeBroadcastService::YoutubeBroadcastService()
{
    this->credential = YoutubeCredentialService::getCurrentCredential();
}

void YoutubeBroadcastService::createNewBroadcast(QString title, QDateTime startTime, QString privacyStatus){
    static QString URL = "https://www.googleapis.com/youtube/v3/liveBroadcasts";
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    QUrl url(URL);

    //prepare url
    QUrlQuery urlQuery;
    urlQuery.addQueryItem("part", "snippet,contentDetails,status");
    url.setQuery(urlQuery);

    //prepare request and header
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    QString authorizationData = "Bearer " + this->credential->accessToken;
    request.setRawHeader("Authorization", authorizationData.toLocal8Bit());

    //prepare data
    QJsonObject broadcast;
    broadcast["kind"] = QString("youtube#liveBroadcast");

    QJsonObject snippet;
    snippet["title"] = title;
    snippet["scheduledStartTime"] = startTime.toString("yyyy-MM-dd'T'HH:mm:ssZ");
    broadcast["snippet"] = snippet;

    QJsonObject status;
    status["privacyStatus"] = privacyStatus;
    broadcast["status"] = status;

    QJsonObject contentDetails;
    QJsonObject monitorStream;
    monitorStream["enableMonitorStream"] = false;
    contentDetails["monitorStream"] = monitorStream;
    broadcast["contentDetails"] = contentDetails;

    QObject::connect(manager, &QNetworkAccessManager::finished, this, &YoutubeBroadcastService::onCreateBroadcastComplete);
    manager->post(request, QJsonDocument(broadcast).toJson());
}

void YoutubeBroadcastService::loadBroadcast(){
    static QString streamURL = "https://www.googleapis.com/youtube/v3/liveBroadcasts";
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);

    QUrl url(streamURL);
    QUrlQuery query;
    query.addQueryItem("part", "snippet,contentDetails,status");
    query.addQueryItem("maxResults", "5");
    query.addQueryItem("mine", "true");
    url.setQuery(query);

    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    QString authorizationData = "Bearer " + this->credential->accessToken;
    request.setRawHeader("Authorization", authorizationData.toLocal8Bit());

    QObject::connect(manager, &QNetworkAccessManager::finished, this, &YoutubeBroadcastService::onGetBroadcastComplete);
    manager->get(request);
}

void YoutubeBroadcastService::updateBroadcast(YoutubeBroadcastDto broadcast){

}

void YoutubeBroadcastService::deleteBroadcast(QString broadcastId){
    static QString streamURL = "https://www.googleapis.com/youtube/v3/liveBroadcasts";
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);

    QUrl url(streamURL);
    QUrlQuery query;
    query.addQueryItem("id", broadcastId);
    url.setQuery(query);

    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    QString authorizationData = "Bearer " + this->credential->accessToken;
    request.setRawHeader("Authorization", authorizationData.toLocal8Bit());

    QObject::connect(manager, &QNetworkAccessManager::finished, this, &YoutubeBroadcastService::onDeleteBroadcastComplete);
    manager->get(request);
}

void YoutubeBroadcastService::bindStreamToBroadcast(QString streamId, QString broadcastId){
    static QString URL = "https://www.googleapis.com/youtube/v3/liveBroadcasts/bind";
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    QUrl url(URL);

    //prepare url
    QUrlQuery urlQuery;
    urlQuery.addQueryItem("part", "snippet,contentDetails,status");
    urlQuery.addQueryItem("id", broadcastId);
    urlQuery.addQueryItem("streamId", streamId);
    url.setQuery(urlQuery);

    //prepare request and header
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    QString authorizationData = "Bearer " + this->credential->accessToken;
    request.setRawHeader("Authorization", authorizationData.toLocal8Bit());

    //prepare data
    QJsonObject bindData;

    QObject::connect(manager, &QNetworkAccessManager::finished, this, &YoutubeBroadcastService::onBindCompleted);
    manager->post(request, QJsonDocument(bindData).toJson());
}

void YoutubeBroadcastService::changeBroadcastStatus(QString broadcastId, QString status){
    static QString URL = "https://www.googleapis.com/youtube/v3/liveBroadcasts/transition";
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    QUrl url(URL);

    //prepare url
    QUrlQuery urlQuery;
    urlQuery.addQueryItem("part", "snippet,contentDetails,status");
    urlQuery.addQueryItem("id", broadcastId);
    urlQuery.addQueryItem("broadcastStatus", status);
    url.setQuery(urlQuery);

    //prepare request and header
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    QString authorizationData = "Bearer " + this->credential->accessToken;
    request.setRawHeader("Authorization", authorizationData.toLocal8Bit());

    //prepare data
    QJsonObject transitionData;

    QObject::connect(manager, &QNetworkAccessManager::finished, this, &YoutubeBroadcastService::onTransitionCompleted);
    manager->post(request, QJsonDocument(transitionData).toJson());
}

void YoutubeBroadcastService::getNextBroadcastPage(QString pageToken){
    static QString streamURL = "https://www.googleapis.com/youtube/v3/liveBroadcasts";
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);

    QUrl url(streamURL);
    QUrlQuery query;
    query.addQueryItem("part", "snippet,contentDetails,status");
    query.addQueryItem("maxResults", "5");
    query.addQueryItem("pageToken", pageToken);
    query.addQueryItem("mine", "true");
    url.setQuery(query);

    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    QString authorizationData = "Bearer " + this->credential->accessToken;
    request.setRawHeader("Authorization", authorizationData.toLocal8Bit());

    QObject::connect(manager, &QNetworkAccessManager::finished, this, &YoutubeBroadcastService::onGetBroadcastComplete);
    manager->get(request);
}

YoutubeBroadcastDto* YoutubeBroadcastService::createBroadcastFromReply(QNetworkReply *reply){
    QString data = (QString) reply->readAll();
    QJsonDocument jsonResponse = QJsonDocument::fromJson(data.toUtf8());
    QJsonObject json = jsonResponse.object();
    YoutubeBroadcastDto *broadcast = new YoutubeBroadcastDto();
    broadcast->id = json["id"].toString();
    QJsonObject snippet = json["snippet"].toObject();
    broadcast->title = snippet["title"].toString();
    QJsonObject status = json["status"].toObject();
    broadcast->privacyStatus = status["privacyStatus"].toString();
    broadcast->lifeCycleStatus = status["lifeCycleStatus"].toString();
    QJsonObject contentDetails = json["contentDetails"].toObject();
    broadcast->boundStreamId = contentDetails["boundStreamId"].toString();
    return broadcast;
}

void YoutubeBroadcastService::onCreateBroadcastComplete(QNetworkReply *reply){
    if (reply->error() != QNetworkReply::NoError){
        std::cout << reply->errorString().toStdString() << std::endl;
        return;
    }
    YoutubeBroadcastDto *broadcast = this->createBroadcastFromReply(reply);
    Q_EMIT broadcastCreated(broadcast);
}

void YoutubeBroadcastService::onGetBroadcastComplete(QNetworkReply *reply){
    if (reply->error() != QNetworkReply::NoError){
        std::cout << reply->errorString().toStdString() << std::endl;
        return;
    }
    QString data = (QString) reply->readAll();
    QJsonDocument jsonResponse = QJsonDocument::fromJson(data.toUtf8());
    QJsonObject json = jsonResponse.object();
    QJsonArray broadcast_array = json["items"].toArray();
    if (broadcast_array.size() == 0) return;
    std::cout << broadcast_array.size() << std::endl;
    YoutubeBroadcastDto** output = new YoutubeBroadcastDto*[broadcast_array.size()];
    for (int i = 0; i < broadcast_array.size(); i++){
        QJsonObject json = broadcast_array.at(i).toObject();
        YoutubeBroadcastDto *broadcast = new YoutubeBroadcastDto();
        broadcast->id = json["id"].toString();
        QJsonObject snippet = json["snippet"].toObject();
        broadcast->title = snippet["title"].toString();
        QJsonObject status = json["status"].toObject();
        broadcast->privacyStatus = status["privacyStatus"].toString();
        broadcast->lifeCycleStatus = status["lifeCycleStatus"].toString();
        QJsonObject contentDetails = json["contentDetails"].toObject();
        broadcast->boundStreamId = contentDetails["boundStreamId"].toString();
        output[i] = broadcast;
    }
    BroadcastBoundStreamLoader *loader = new BroadcastBoundStreamLoader(output, broadcast_array.size(), this->credential);
    QObject::connect(loader, &BroadcastBoundStreamLoader::loadBroadcastComplete, this, &YoutubeBroadcastService::onLoadBroadcastCompleted);
    loader->load();
    if (json.contains("nextPageToken")){
        QString nextPageToken = json["nextPageToken"].toString();
        std::cout << nextPageToken.toStdString() << std::endl;
        this->getNextBroadcastPage(nextPageToken);
    }
}
void YoutubeBroadcastService::onLoadBroadcastCompleted(YoutubeBroadcastDto *broadcast){
    Q_EMIT loadBroadcastCompleted(broadcast);
}

void YoutubeBroadcastService::onUpdateBroadcastComplete(QNetworkReply *reply){

}

void YoutubeBroadcastService::onDeleteBroadcastComplete(QNetworkReply *reply){
    YoutubeBroadcastDto *broadcast = this->createBroadcastFromReply(reply);
    Q_EMIT broadcastDeleted(broadcast->id);
}

void YoutubeBroadcastService::onBindCompleted(QNetworkReply *reply){
    if (reply->error() != QNetworkReply::NoError){
        std::cout << reply->errorString().toStdString() << std::endl;
        return;
    }
    YoutubeBroadcastDto *broadcast = this->createBroadcastFromReply(reply);
    Q_EMIT bindCompleted(broadcast);
}

void YoutubeBroadcastService::onTransitionCompleted(QNetworkReply *reply){
    if (reply->error() != QNetworkReply::NoError){
        std::cout << reply->errorString().toStdString() << std::endl;
        return;
    }
    YoutubeBroadcastDto *broadcast = this->createBroadcastFromReply(reply);
    Q_EMIT transitionCompleted(broadcast);
}

BroadcastBoundStreamLoader::BroadcastBoundStreamLoader(YoutubeBroadcastDto **broadcasts, int total, YoutubeCredentialDto *credential){
    this->broadcasts = broadcasts;
    this->total = total;
    this->less = total;
    this->credential = credential;
}

void BroadcastBoundStreamLoader::load(){
    for(int i = 0; i < total; i++){
        YoutubeStreamService *streamService = new YoutubeStreamService();
        QObject::connect(streamService, &YoutubeStreamService::loadStreamCompleted, this, &BroadcastBoundStreamLoader::onStreamReturn);
        streamService->loadStream(broadcasts[i]->boundStreamId);
    }
}

void BroadcastBoundStreamLoader::onStreamReturn(YoutubeStreamDto *stream){
    for(int i = 0; i < total; i++){
        if (broadcasts[i]->boundStreamId.compare(stream->id) == 0){
            broadcasts[i]->boundStream = stream;
            Q_EMIT loadBroadcastComplete(broadcasts[i]);
            return;
        }
    }
}
