#include "headers/service/youtubestreamservice.h"
#include <QUrl>
#include <QUrlQuery>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

#include <iostream>
YoutubeCredentialDto* YoutubeCredentialService::credential = NULL;
YoutubeStreamService::YoutubeStreamService(){
    this->credential = YoutubeCredentialService::getCurrentCredential();
}

void YoutubeStreamService::createNewStream(QString title, QString cdnFormat){
    static QString URL = "https://www.googleapis.com/youtube/v3/liveStreams";
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    QUrl url(URL);

    //prepare url
    QUrlQuery urlQuery;
    urlQuery.addQueryItem("part", "snippet,contentDetails,status,cdn");
    url.setQuery(urlQuery);

    //prepare request and header
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    QString authorizationData = "Bearer " + this->credential->accessToken;
    request.setRawHeader("Authorization", authorizationData.toLocal8Bit());

    //prepare data
    QJsonObject stream;
    stream["kind"] = QString("youtube#liveStream");

    QJsonObject snippet;
    snippet["title"] = title;
    stream["snippet"] = snippet;

    QJsonObject cdn;
    cdn["format"] = cdnFormat;
    cdn["ingestionType"] = "rtmp";
    stream["cdn"] = cdn;

    QObject::connect(manager, &QNetworkAccessManager::finished, this, &YoutubeStreamService::onCreateStreamComplete);
    manager->post(request, QJsonDocument(stream).toJson());
}

void YoutubeStreamService::loadStream(QString streamId){
    static QString streamURL = "https://www.googleapis.com/youtube/v3/liveStreams";
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);

    QUrl url(streamURL);
    QUrlQuery query;
    query.addQueryItem("id", streamId);
    query.addQueryItem("part", "id,snippet,cdn,status");
    url.setQuery(query);

    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    QString authorizationData = "Bearer " + this->credential->accessToken;
    request.setRawHeader("Authorization", authorizationData.toLocal8Bit());

    QObject::connect(manager, &QNetworkAccessManager::finished, this, &YoutubeStreamService::onLoadStreamComplete);
    manager->get(request);
}

void YoutubeStreamService::updateStream(YoutubeStreamDto stream){

}

void YoutubeStreamService::deleteStream(QString streamId){

}

YoutubeStreamDto* YoutubeStreamService::createStreamFromReply(QNetworkReply *reply){
    QString data = (QString) reply->readAll();
    QJsonDocument jsonResponse = QJsonDocument::fromJson(data.toUtf8());
    QJsonObject json = jsonResponse.object();
    if (json.contains("items"))
        json = json["items"].toArray().at(0).toObject();
    YoutubeStreamDto *stream = new YoutubeStreamDto();
    stream->id = json["id"].toString();
    QJsonObject ingestionInfo = (json["cdn"].toObject())["ingestionInfo"].toObject();
    stream->address = ingestionInfo["ingestionAddress"].toString() + "/" + ingestionInfo["streamName"].toString();
    QJsonObject snippet = json["snippet"].toObject();
    stream->title = snippet["title"].toString();
    QJsonObject status = json["status"].toObject();
    stream->streamStatus = status["streamStatus"].toString();
    QJsonObject healthStatus = status["healthStatus"].toObject();
    stream->healthStatus = healthStatus["status"].toString();
    return stream;
}

void YoutubeStreamService::onCreateStreamComplete(QNetworkReply *reply){
    if (reply->error() != QNetworkReply::NoError){
        std::cout << reply->errorString().toStdString() << std::endl;
        return;
    }
    YoutubeStreamDto *stream = this->createStreamFromReply(reply);
    Q_EMIT streamCreated(stream);
}

void YoutubeStreamService::onLoadStreamComplete(QNetworkReply *reply){
    if (reply->error() != QNetworkReply::NoError){
        std::cout << reply->errorString().toStdString() << std::endl;
        return;
    }
    YoutubeStreamDto *stream = this->createStreamFromReply(reply);
    Q_EMIT loadStreamCompleted(stream);
}

void YoutubeStreamService::onUpdateStreamComplete(QNetworkReply *reply){

}

void YoutubeStreamService::onDeleteStreamComplete(QNetworkReply *reply){

}
