/*
 * File: uploading_view.c
 *
 * Author: GVLab
 *
 * Created on: Aug 13, 2015
 */

#include "headers/view/uploading_view.h"

//upload thread
pthread_t uploading_thread;

/*
 * Function:  data_upload_changed 
 * --------------------
 * Enable/disable selection course, version, lecture base on uploading's type.
 *  Type can be UPLOAD_LECTURE. UPLOAD_VERSION or UPLOAD_COURSE.
 *
 *  select: Pointer to GTK upload_type element.
 */
void type_upload_change(GtkWidget *select){
    int type;

    //set element sensitive base on upload type
    type = get_upload_type(display_upload->type);
    switch(type){
        case UPLOAD_LECTURE:
            gtk_widget_set_sensitive(GTK_WIDGET(display_upload->version), TRUE);
            gtk_widget_set_sensitive(GTK_WIDGET(display_upload->lecture), TRUE);
            break;

        case UPLOAD_VERSION:
            gtk_widget_set_sensitive(GTK_WIDGET(display_upload->version), TRUE);
            gtk_widget_set_sensitive(GTK_WIDGET(display_upload->lecture), FALSE);
            break;

        case UPLOAD_COURSE:
            gtk_widget_set_sensitive(GTK_WIDGET(display_upload->version), FALSE);
            gtk_widget_set_sensitive(GTK_WIDGET(display_upload->lecture), FALSE);
            break;
        default:
            break;
    }
}

/*
 * Function:  data_upload_changed 
 * --------------------
 * Show course, version, lecture base on change_type.
 *
 *  combo_box: Pointer to GTK processing element (course or version).
 *
 *  type: Type of changing. Can be COURSE_CHANGE or VERSION_CHANGE.
 */
void data_upload_changed(GtkWidget *combo_box, int type){
    int i, j;
    int course = 0, version = 0;
    switch(type){
        case COURSE_CHANGE:
            //clear
            clear_combo_box_data(display_upload->version);

            //curse index
            course = gtk_combo_box_get_active(GTK_COMBO_BOX(display_upload->course));

            //setvalue
            for (j = 0; j < course_data_info->subject_list[course].num_version; j++){
                gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT( display_upload->version), 
                    course_data_info->subject_list[course].version_list[j].version_name );
            }
            gtk_combo_box_set_active(GTK_COMBO_BOX(display_upload->version),0);

            //update course id
            gtk_entry_set_text(GTK_ENTRY(display_upload->course_entry), course_data_info->subject_list[course].subject_id);
            break;
        case VERSION_CHANGE:
            //clear
            clear_combo_box_data(display_upload->lecture);
            //index
            course = gtk_combo_box_get_active(GTK_COMBO_BOX(display_upload->course));
            version = gtk_combo_box_get_active(GTK_COMBO_BOX(display_upload->version));
            //set value
            for (j = 0; j < course_data_info->subject_list[course].version_list[version].num_lecture; j++){
                gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT( display_upload->lecture ), 
                    course_data_info->subject_list[course].version_list[version].lecture_list[j].lecture_name );
            }
            gtk_combo_box_set_active(GTK_COMBO_BOX(display_upload->lecture),0);
            break;
        default:
            break;
    }
}

/*
 * Function:  upload_data 
 * --------------------
 * Uploading thread.
 * Get all uploading's information from UI.
 * Prepare data for uploading video.
 * Upload video to server and show status.
 *
 *  main_window: Pointer to GTK main window.
 */
void *upload_data(void *main_window){
    GtkWidget *window = (GtkWidget *)main_window;
    int type = 0;
    char tmp_path[LONG_STRING_LENGTH];
    char server_path[LONG_STRING_LENGTH];

    type = gtk_combo_box_get_active(GTK_COMBO_BOX(display_upload->type));
    //set full path to local data
    switch(type){
        case UPLOAD_LECTURE:
            //local path
            sprintf(tmp_path, "%s/%s/%s/%s", data_path, display_upload->curr_course, 
                display_upload->curr_version, display_upload->curr_lecture);

            //server path
            sprintf(server_path, "%s/%s/%s/%s-%s/%s", display_upload->path, display_upload->class_id, 
                display_upload->term, display_upload->curr_course_id, display_upload->curr_course, display_upload->curr_lecture);

            //upload
            upload_to_server(display_upload->server, server_path, 
                display_upload->user, display_upload->pass, tmp_path, window);
            break;

        case UPLOAD_VERSION:
            sprintf(tmp_path, "%s/%s/%s", data_path, display_upload->curr_course, 
                display_upload->curr_version);

            //server path
            sprintf(server_path, "%s/%s/%s/%s-%s", display_upload->path, display_upload->class_id, 
                display_upload->term, display_upload->curr_course_id, display_upload->curr_course);

            //upload
            upload_to_server(display_upload->server, server_path, 
                display_upload->user, display_upload->pass, tmp_path, window);
            // printf("here\n");
            break;

        default:
            break;
    }
}

/*
 * Function:  upload_button_click 
 * --------------------
 * Create uploading thread to upload video to server.
 *
 *  widget: Pointer to GTK upload_video button.
 *
 *  window: Pointer to GTK main window.
 */
void upload_button_click(GtkWidget *widget, gpointer window){
    char tmp_data[STRING_LENGTH];
    GtkDialogFlags flags;

    //read course
    get_combo_box_text(GTK_COMBO_BOX(display_upload->course), tmp_data);
    strcpy(display_upload->curr_course, tmp_data);
    
    //read course id
    strcpy(display_upload->curr_course_id, gtk_entry_get_text(GTK_ENTRY(display_upload->course_entry)));

    //read version
    get_combo_box_text(GTK_COMBO_BOX(display_upload->version), tmp_data);
    strcpy(display_upload->curr_version, tmp_data);

    //read lecture
    get_combo_box_text(GTK_COMBO_BOX(display_upload->lecture), tmp_data);
    strcpy(display_upload->curr_lecture, tmp_data);

    //read password
    strcpy(display_upload->pass, gtk_entry_get_text(GTK_ENTRY(display_upload->password)));
    
    type = gtk_combo_box_get_active(GTK_COMBO_BOX(display_upload->type));

    push_item(status_bar, GINT_TO_POINTER(context_id), "Uploading...");
        
    //upload thread
    pthread_create(&uploading_thread, NULL, upload_data, window);
}

/*
 * Function:  upload_button_click 
 * --------------------
 * Get server config from UI and connect to server.
 *
 *  button: Pointer to GTK connect button.
 */
void connect_button_click(GtkWidget *button){
    //read server
    strcpy(display_upload->server, gtk_entry_get_text(GTK_ENTRY(display_upload->ftp)));

    //read user name
    strcpy(display_upload->user, gtk_entry_get_text(GTK_ENTRY(display_upload->user_name)));

    //read path
    strcpy(display_upload->path, gtk_entry_get_text(GTK_ENTRY(display_upload->server_path)));

    //read password
    strcpy(display_upload->pass, gtk_entry_get_text(GTK_ENTRY(display_upload->password)));

    //save to config file
    save_server_info_to_config(display_upload->server, display_upload->user, 
        display_upload->path);

    int i = connect_to_server(display_upload->server, display_upload->path, 
        display_upload->user, display_upload->pass);

    if(i == 1){
        gtk_label_set_text(GTK_LABEL(display_upload->status), "Connect error!");
        push_item(status_bar, GINT_TO_POINTER(context_id), "Connect error");
    } else {
        gtk_label_set_text(GTK_LABEL(display_upload->status), "Connect successful!");
        push_item(status_bar, GINT_TO_POINTER(context_id), "Connect successful");
    }
}

/*
 * Function:  save_class_button_click 
 * --------------------
 * Get class config from UI and save to config file.
 *
 *  button: Pointer to GTK save button.
 */
void save_class_button_click(GtkWidget *button){
    //read server
    strcpy(display_upload->class_id, gtk_entry_get_text(GTK_ENTRY(display_upload->class_entry)));

    //read user name
    strcpy(display_upload->term, gtk_entry_get_text(GTK_ENTRY(display_upload->term_entry)));

    //save to config file
    save_class_info_to_config(display_upload->class_id, display_upload->term);
}

/*
 * Function:  show_password 
 * --------------------
 * Show/hide password base on state of check_show_hide_pass button.
 *
 *  check: Pointer to GTK check_show_hide_pass button.
 *
 *  pass: GTK password entry.
 */
void show_password(GtkWidget *check, GtkWidget *pass){
    if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(check))){
        gtk_entry_set_visibility(GTK_ENTRY(display_upload->password), TRUE);
    } else {
        gtk_entry_set_visibility(GTK_ENTRY(display_upload->password), FALSE);
    }
}

/*
 * Function:  create_uploading_tab 
 * --------------------
 * Create Uploading tab: Create UI and display uploading data.
 *
 *  window: Main window of program.
 * --------------------
 *  Return: Pointer to GTK_VBOX (Contain all uploading's element).
 */
GtkWidget* create_uploading_tab(GtkWidget* window){
    GtkWidget *frame, *table, *label;
    GtkWidget *hbox, *vbox, *hseparator, *tmp_box;

    //create new hbox1
    vbox = gtk_vbox_new (FALSE, 5);

    //separator
    hseparator = gtk_hseparator_new();
    gtk_box_pack_start (GTK_BOX (vbox), hseparator, FALSE, FALSE, 5);

    //create new hbox
    hbox = gtk_vbox_new (FALSE, 5);
    gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 5);
    

    //-------------------------config server
    //home frame
    frame = gtk_frame_new("Config server");
    gtk_box_pack_start (GTK_BOX (hbox), frame, FALSE, FALSE, 5);

    //home table
    table = gtk_table_new(2, 4, FALSE);
    gtk_container_add(GTK_CONTAINER(frame), table);

    //label
    label = gtk_label_new("Server");
    gtk_table_attach(GTK_TABLE(table), label, 0, 1, 0, 1, 
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    //home entry
    display_upload->ftp = gtk_entry_new();
    gtk_entry_set_text(GTK_ENTRY(display_upload->ftp), display_upload->server);
    gtk_widget_set_size_request(GTK_WIDGET(display_upload->ftp), 450, 28);
    gtk_table_attach(GTK_TABLE(table), display_upload->ftp, 1, 2, 0, 1,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    //path
    label = gtk_label_new("Server Path");
    gtk_table_attach(GTK_TABLE(table), label, 0, 1, 1, 2,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    //server path entry
    display_upload->server_path = gtk_entry_new();
    gtk_entry_set_text(GTK_ENTRY(display_upload->server_path), display_upload->path);
    gtk_table_attach(GTK_TABLE(table), display_upload->server_path, 1, 2, 1, 2,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    //path
    label = gtk_label_new("User");
    gtk_table_attach(GTK_TABLE(table), label, 0, 1, 3, 4,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    //user entry
    display_upload->user_name = gtk_entry_new();
    gtk_entry_set_text(GTK_ENTRY(display_upload->user_name), display_upload->user);
    gtk_table_attach(GTK_TABLE(table), display_upload->user_name, 1, 2, 3, 4,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    //path
    label = gtk_label_new("Password");
    gtk_table_attach(GTK_TABLE(table), label, 0, 1, 4, 5,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    //password entry
    display_upload->password = gtk_entry_new();
    gtk_table_attach(GTK_TABLE(table), display_upload->password, 1, 2, 4, 5,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    gtk_entry_set_visibility(GTK_ENTRY(display_upload->password), FALSE);

    //show password
    display_upload->show = gtk_check_button_new_with_label("Show/Hide");
    gtk_table_attach(GTK_TABLE(table), display_upload->show, 2, 3, 4, 5,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    g_signal_connect(G_OBJECT(display_upload->show), "clicked", 
        G_CALLBACK(show_password), display_upload->password);

    //save button
    display_upload->save_button = gtk_button_new_with_label("Connect to server");
    gtk_table_attach(GTK_TABLE(table), display_upload->save_button, 0, 1, 5, 6,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    gtk_widget_set_size_request(GTK_WIDGET(display_upload->save_button), 140, 30);
    g_signal_connect(G_OBJECT(display_upload->save_button), "clicked", G_CALLBACK(connect_button_click), NULL);

    display_upload->status = gtk_label_new("Test connection");
    gtk_table_attach(GTK_TABLE(table), display_upload->status, 1, 2, 5, 6,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    //--------------------------config course-------------------
    read_class_config(display_upload->class_id, display_upload->term);
    //create new hbox
    tmp_box = gtk_hbox_new (FALSE, 5);
    gtk_box_pack_start (GTK_BOX (hbox), tmp_box, FALSE, FALSE, 5);
    //select course frame
    frame = gtk_frame_new("Class management");
    gtk_box_pack_start (GTK_BOX (tmp_box), frame, FALSE, FALSE, 5);

    //home table
    table = gtk_table_new(2, 4, FALSE);
    gtk_container_add(GTK_CONTAINER(frame), table);

    //class id
    label = gtk_label_new("Class ID");
    gtk_table_attach(GTK_TABLE(table), label, 0, 1, 0, 1,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    //server path entry
    display_upload->class_entry = gtk_entry_new();
    gtk_entry_set_text(GTK_ENTRY(display_upload->class_entry), display_upload->class_id);
    gtk_table_attach(GTK_TABLE(table), display_upload->class_entry, 1, 2, 0, 1,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    gtk_widget_set_size_request(GTK_WIDGET(display_upload->class_entry), 250, 28);

    //class id
    label = gtk_label_new("Term");
    gtk_table_attach(GTK_TABLE(table), label, 0, 1, 1, 2,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    //server path entry
    display_upload->term_entry = gtk_entry_new();
    gtk_entry_set_text(GTK_ENTRY(display_upload->term_entry), display_upload->term);
    gtk_table_attach(GTK_TABLE(table), display_upload->term_entry, 1, 2, 1, 2,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    //save button
    display_upload->save_button = gtk_button_new_with_label("Save");
    gtk_table_attach(GTK_TABLE(table), display_upload->save_button, 0, 1, 2, 3,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    gtk_widget_set_size_request(GTK_WIDGET(display_upload->save_button), 140, 30);
    g_signal_connect(G_OBJECT(display_upload->save_button), "clicked", G_CALLBACK(save_class_button_click), NULL);


    //--------------------------upload-------------------
    //select course frame
    frame = gtk_frame_new("Upload data");
    gtk_box_pack_start (GTK_BOX (tmp_box), frame, FALSE, FALSE, 5);

    //home table
    table = gtk_table_new(2, 4, FALSE);
    gtk_container_add(GTK_CONTAINER(frame), table);

    //label course
    label = gtk_label_new("Choose type");
    gtk_table_attach(GTK_TABLE(table), label, 0, 1, 1, 2,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    //label course
    display_upload->type = show_upload_type();
    gtk_table_attach(GTK_TABLE(table), display_upload->type , 1, 2, 1, 2,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    g_signal_connect( G_OBJECT( display_upload->type ), "changed",
        G_CALLBACK( type_upload_change ), NULL );

    //label course
    label = gtk_label_new("Course");
    gtk_table_attach(GTK_TABLE(table), label, 0, 1, 2, 3,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    //course
    display_upload->course = gtk_combo_box_text_new();
    show_course_management(display_upload->course, 1);
    gtk_widget_set_size_request(GTK_WIDGET(display_upload->course), 250, 28);
    gtk_table_attach(GTK_TABLE(table), display_upload->course, 1, 2, 2, 3,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    g_signal_connect( G_OBJECT( display_upload->course ), "changed",
        G_CALLBACK( data_upload_changed ), (gpointer) COURSE_CHANGE );

    //class id
    label = gtk_label_new("Course ID");
    gtk_table_attach(GTK_TABLE(table), label, 0, 1, 3, 4,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    //server course entry
    display_upload->course_entry = gtk_entry_new();
    gtk_entry_set_text(GTK_ENTRY(display_upload->course_entry), course_data_info->subject_list[course_index].subject_id);
    gtk_table_attach(GTK_TABLE(table), display_upload->course_entry, 1, 2, 3, 4,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    gtk_widget_set_sensitive(display_upload->course_entry, FALSE);


    //label version
    label = gtk_label_new("Version");
    gtk_table_attach(GTK_TABLE(table), label, 0, 1, 4, 5,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    //version
    display_upload->version = gtk_combo_box_text_new();
    show_course_management(display_upload->version, 2);
    gtk_table_attach(GTK_TABLE(table), display_upload->version, 1, 2, 4, 5,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    g_signal_connect( G_OBJECT( display_upload->version ), "changed",
        G_CALLBACK( data_upload_changed ), (gpointer) VERSION_CHANGE );

    //label lecture
    label = gtk_label_new("Lecture");
    gtk_table_attach(GTK_TABLE(table), label, 0, 1, 5, 6,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    //lecture
    display_upload->lecture = gtk_combo_box_text_new();
    show_course_management(display_upload->lecture, 3);
    gtk_table_attach(GTK_TABLE(table), display_upload->lecture, 1, 2, 5, 6,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    //upload button
    display_upload->button = gtk_button_new_with_label("Upload data");
    gtk_widget_set_size_request(GTK_WIDGET(display_upload->button), 140, 30);
    gtk_table_attach(GTK_TABLE(table), display_upload->button, 0, 1, 6, 7,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    g_signal_connect((gpointer) (display_upload->button), "clicked", G_CALLBACK(upload_button_click), window);

    return vbox;
}
