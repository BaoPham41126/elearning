/*
 * File: utils_view.c
 *
 * Author: GVLab
 *
 * Created on: Aug 13, 2015
 */

#include "headers/view/utils_view.h"


/*
 * Function:  show_output_video_type 
 * --------------------
 * Create element and show output's in_video_type.
 * --------------------
 *  Return: GTK output's in_video_type.
 */
GtkWidget *show_output_video_type(){
    GtkWidget *output_type = gtk_combo_box_text_new();
    gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT( output_type ), "Main");
    gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT( output_type ), "Switching");
    return output_type;
}

/*
 * Function:  show_video_codec_type 
 * --------------------
 * Create element and show output's video_codec.
 * --------------------
 *  Return: GTK output's video_codec.
 */
GtkWidget *show_video_codec_type(){
    GtkWidget *output_type = gtk_combo_box_text_new();
    gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT( output_type ), "h264");
    gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT( output_type ), "mpeg");

    return output_type;
}

/*
 * Function:  show_upload_type 
 * --------------------
 * Create element and show uploading's upload_type.
 * --------------------
 *  Return: GTK uploading's upload_type.
 */
GtkWidget *show_upload_type(){
    GtkWidget *upload = gtk_combo_box_text_new();

    //add value
    gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT( upload ), "Upload lecture");
    gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT( upload ), "Upload version");
    // gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT( upload ), "Upload course");

    //set active and return
    gtk_combo_box_set_active(GTK_COMBO_BOX(upload), 0);
    return upload;
}

/*
 * Function:  show_audio_codec_type 
 * --------------------
 * Create element and show output's audio_codec.
 * --------------------
 *  Return: GTK output's audio_codec.
 */
GtkWidget *show_audio_codec_type(enum AVCodecID audio_codec){
    GtkWidget *audio = gtk_combo_box_text_new();
    gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT( audio ), "aac");
    gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT( audio ), "mp3");

    if(audio_codec == AV_CODEC_ID_AAC)
    	gtk_combo_box_set_active(GTK_COMBO_BOX(audio), 0);
    else 
    	gtk_combo_box_set_active(GTK_COMBO_BOX(audio), 1);
    return audio;
}

/*
 * Function:  show_pix_fmt_type 
 * --------------------
 * Create element and show output's pix_format.
 * --------------------
 *  Return: GTK output's pix_format.
 */
GtkWidget *show_pix_fmt_type(){
    GtkWidget *output_type = gtk_combo_box_text_new();
    gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT( output_type ), "yuv420p");
    return output_type;
}

/*
 * Function:  show_input_type 
 * --------------------
 * Create element and show input's input_type.
 * --------------------
 *  Return: GTK input's input_type.
 */
GtkWidget *show_input_type(){
    GtkWidget *input_type = gtk_combo_box_text_new();
    gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT( input_type ), "monitor");
    gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT( input_type ), "video");
    gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT( input_type ), "audio");

    return input_type;
}

/*
 * Function:  show_output_type 
 * --------------------
 * Create element and show output's output_type.
 * --------------------
 *  Return: GTK output's output_type.
 */
GtkWidget *show_output_type(){
    GtkWidget *output_type = gtk_combo_box_text_new();
    gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT( output_type ), "HDD");
    gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT( output_type ), "Live");
    return output_type;
}

/*
 * Function:  show_format_type 
 * --------------------
 * Create element and show output's file_extension.
 * --------------------
 *  Return: GTK output's file_extension.
 */
GtkWidget *show_format_type(){
    GtkWidget *output_type = gtk_combo_box_text_new();
    gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT( output_type ), "mp4");
    gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT( output_type ), "flv");
    gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT( output_type ), "mp3");
    gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT( output_type ), "mkv");
    return output_type;
}

/*
 * Function:  set_format_combo_box 
 * --------------------
 * Change display format in GTK output's file_extension base on given format.
 *
 *  select: GTK Output's file_extension.
 *
 *  format: Current format.
 */
void set_format_combo_box(GtkWidget * select, char *format){
    gtk_combo_box_set_active(GTK_COMBO_BOX(select), 0);
    if(strcmp(format, "flv") == 0)
        gtk_combo_box_set_active(GTK_COMBO_BOX(select), 1);
    if(strcmp(format, "mp3") == 0)
        gtk_combo_box_set_active(GTK_COMBO_BOX(select), 2);
    if(strcmp(format, "mkv") == 0)
        gtk_combo_box_set_active(GTK_COMBO_BOX(select), 3);
}

/*
 * Function:  get_video_codec 
 * --------------------
 * Get data from output's video_codec and return the id of codec.
 * Codec id can be AV_CODEC_ID_H264 or AV_CODEC_ID_FLV1.
 *
 *  select: GTK Output's video_codec.
 * --------------------
 *  Return: Codec id.
 */
int get_video_codec(GtkWidget * select){
    gchar *tmp = gtk_combo_box_get_active_text(GTK_COMBO_BOX(select));

    if (strcmp(tmp, "H264") == 0 || strcmp(tmp, "h264") == 0) {
        return AV_CODEC_ID_H264;
    } else if (strcmp(tmp, "MPEG") == 0
            || strcmp(tmp, "mpeg") == 0) {
        return AV_CODEC_ID_FLV1;
    }
}

/*
 * Function:  get_upload_type 
 * --------------------
 * Get data from uploading's upload_type and return.
 *
 *  select: GTK Uploading's upload_type.
 * --------------------
 *  Return: Uploading's type.
 */
int get_upload_type(GtkWidget * select){
    return gtk_combo_box_get_active(GTK_COMBO_BOX(select));
}

/*
 * Function:  get_pix_fmt 
 * --------------------
 * Get data from output's pix_format and return.
 *
 *  select: GTK output's pix_format.
 * --------------------
 *  Return: The pixcel format of output.
 */
int get_pix_fmt(GtkWidget * select){
    gchar *tmp = gtk_combo_box_get_active_text(GTK_COMBO_BOX(select));

    if (strcmp(tmp, "yuv420p") == 0)
        return AV_PIX_FMT_YUV420P;
}

/*
 * Function:  get_combo_box_text 
 * --------------------
 * Get current text from combobox and return.
 * If combobox is unset, return "".
 *
 *  combobox: GTK combobox.
 *
 *  value: String value get from combobox.
 */
void get_combo_box_text(GtkComboBox *combobox, char *value){
	gint active;

	active = gtk_combo_box_get_active(GTK_COMBO_BOX(combobox));

	if(active == -1){
		strcpy(value, "");
	} else {
		strcpy(value, gtk_combo_box_get_active_text(GTK_COMBO_BOX(combobox)));
	}
}

/*
 * Function:  get_audio_codec_type 
 * --------------------
 * Get data from output's audio_codec and return.
 *
 *  select: GTK output's audio_codec.
 * --------------------
 *  Return: The codec of output audio.
 */
int get_audio_codec_type(GtkWidget *combobox){
	char tmp_codec[20];
    get_combo_box_text((GtkComboBox*) combobox, tmp_codec);

	if(strcmp(tmp_codec, "mp3") == 0)
		return AV_CODEC_ID_MP3;
	else
		return AV_CODEC_ID_AAC;
}

/*
 * Function:  create_remove_button 
 * --------------------
 * Create close button no border with image close from stock.
 * --------------------
 *  Return: GTK close button.
 */
GtkWidget *create_remove_button(){
    GtkWidget *img1, *tab_close;

    //get image
    img1= gtk_image_new_from_stock (GTK_STOCK_CLOSE, GTK_ICON_SIZE_MENU);

    //create button with image
    tab_close = gtk_button_new();
    gtk_container_add(GTK_CONTAINER(tab_close),img1); 

    //set noborder
    gtk_button_set_relief(GTK_BUTTON(tab_close), GTK_RELIEF_NONE);

    return tab_close;
}

/*
 * Function:  create_delete_button 
 * --------------------
 * Create delete button no border with image delete from stock.
 * --------------------
 *  Return: GTK delete button.
 */
GtkWidget *create_delete_button(){
    GtkWidget *img1, *tab_close;

    //get image
    img1= gtk_image_new_from_stock (GTK_STOCK_REMOVE, GTK_ICON_SIZE_MENU);

    //create button with image
    tab_close = gtk_button_new();
    gtk_container_add(GTK_CONTAINER(tab_close),img1); 

    //set noborder
    gtk_button_set_relief(GTK_BUTTON(tab_close), GTK_RELIEF_NONE);

    return tab_close;
}

/*
 * Function:  create_add_button 
 * --------------------
 * Create add button no border with image add from stock.
 * --------------------
 *  Return: GTK add button.
 */
GtkWidget *create_add_button(){
    GtkWidget *img1, *tab_close;

    //get image
    img1= gtk_image_new_from_stock (GTK_STOCK_ADD, GTK_ICON_SIZE_MENU);

    //create button with image
    tab_close = gtk_button_new();
    gtk_container_add(GTK_CONTAINER(tab_close),img1); 

    //set noborder
    gtk_button_set_relief(GTK_BUTTON(tab_close), GTK_RELIEF_NONE);

    return tab_close;
}

/*
 * Function:  create_directory_button 
 * --------------------
 * Create directory button no border with image directory from stock.
 * --------------------
 *  Return: GTK directory button.
 */
GtkWidget *create_directory_button(){
    GtkWidget *img1, *tab_close;

    //get image
    img1= gtk_image_new_from_stock (GTK_STOCK_DIRECTORY, GTK_ICON_SIZE_MENU);

    //create button with image
    tab_close = gtk_button_new();
    gtk_container_add(GTK_CONTAINER(tab_close),img1); 

    //set noborder
    gtk_button_set_relief(GTK_BUTTON(tab_close), GTK_RELIEF_NONE);

    return tab_close;
}

/*
 * Function:  create_apply_button 
 * --------------------
 * Create apply button no border with image apply from stock.
 * --------------------
 *  Return: GTK apply button.
 */
GtkWidget *create_apply_button(){
    GtkWidget *img1, *tab_close;

    //get image
    img1= gtk_image_new_from_stock (GTK_STOCK_APPLY, GTK_ICON_SIZE_MENU);

    //create button with image
    tab_close = gtk_button_new();
    gtk_container_add(GTK_CONTAINER(tab_close),img1); 

    //set noborder
    gtk_button_set_relief(GTK_BUTTON(tab_close), GTK_RELIEF_NONE);

    return tab_close;
}

/*
 * Function:  get_output_typpe 
 * --------------------
 * Map output's type from integer to string.
 * 0 is HDD, 1 is Live.
 *
 *  type: Type of output(integer).
 *
 *  value: Type of output(string).
 */
void get_output_typpe(int type, char *value){
	if(type == 0)
		strcpy(value, "HDD");
	else 
		strcpy(value, "Live");
}

/*
 * Function:  create_pixbuf 
 * --------------------
 * Create pixbuf from image.
 *
 *  filename: Path to image.
 * --------------------
 *  Return: GTK pixbuf of image.
 */
GdkPixbuf *create_pixbuf(const gchar * filename){
    GdkPixbuf *pixbuf;
    GError *error = NULL;
    pixbuf = gdk_pixbuf_new_from_file(filename, &error);
    if(!pixbuf) {
      fprintf(stderr, "%s\n", error->message);
       g_error_free(error);
    }

  return pixbuf;
}

/*
 * Function:  create_quick_message 
 * --------------------
 * Create alert popup to screen base on type and show message.
 *
 *  message: Message to be showed.
 *
 *  window: GTK main window.
 *
 *  type: Type of message. Can be GTK_MESSAGE_WARNING, GTK_MESSAGE_INFO 
 *   or GTK_MESSAGE_QUESTION
 * --------------------
 *  Return: Pointer to id of popup.
 */
GtkWidget *create_quick_message (gchar *message, gpointer window, int type){
    GtkWidget *dialog, *label, *content_area;
    /* Create the widgets */
    switch(type){
    	case 0:
            dialog = gtk_message_dialog_new ((GtkWindow*) window, GTK_DIALOG_DESTROY_WITH_PARENT,
		        GTK_MESSAGE_WARNING, GTK_BUTTONS_YES_NO, message);
		    break;
		case 1:
            dialog = gtk_message_dialog_new ((GtkWindow*) window, GTK_DIALOG_DESTROY_WITH_PARENT,
		        GTK_MESSAGE_INFO, GTK_BUTTONS_CLOSE, message);
			break;
		case 2:
            dialog = gtk_message_dialog_new ((GtkWindow*) window, GTK_DIALOG_DESTROY_WITH_PARENT,
		        GTK_MESSAGE_QUESTION, GTK_BUTTONS_YES_NO, message);
		    break;

		default:
			break;
    }

    if(type == 1){
    	gtk_dialog_run (GTK_DIALOG (dialog));
	    gtk_widget_destroy (dialog);
    }

    return dialog;
}

/*
 * Function:  clear_combo_box_data 
 * --------------------
 * Clear combobox data.
 *
 *  combobox: GTK combobox.
 */
void clear_combo_box_data(GtkWidget *combobox){
    int i;

    //clear text data
    i = gtk_combo_box_get_active(GTK_COMBO_BOX(combobox));

    //remove all text
    while(i >= 0){
        //remove text
        gtk_combo_box_text_remove(GTK_COMBO_BOX_TEXT(combobox), i);

        //get new active id
        gtk_combo_box_set_active(GTK_COMBO_BOX(combobox), 0);
        i = gtk_combo_box_get_active(GTK_COMBO_BOX(combobox));
    }
}

/*
 * Function:  push_item 
 * --------------------
 * Show message to gtk_status bar.
 *
 *  widget: GTK statusbar.
 *
 *  data: Buffer_id of status_bar.
 *
 *  text: Message to be showed.
 */
void push_item( GtkWidget *widget, gpointer data, char *text){
    gtk_statusbar_remove_all(GTK_STATUSBAR(widget), GPOINTER_TO_INT(data));
    gtk_statusbar_push( GTK_STATUSBAR(widget), GPOINTER_TO_INT(data), text);
}

/*
 * Function:  pop_item 
 * --------------------
 * Pop message from gtk_status bar.
 *
 *  widget: GTK statusbar.
 *
 *  data: Buffer_id of status_bar.
 */
void pop_item( GtkWidget *widget, gpointer data){
    gtk_statusbar_pop( GTK_STATUSBAR(widget), GPOINTER_TO_INT(data) );
}

/*
 * Function:  clear_status_bar 
 * --------------------
 * Clear all message from gtk_status bar.
 *
 *  widget: GTK statusbar.
 *
 *  data: Buffer_id of status_bar.
 */
void clear_status_bar(GtkWidget *widget, gpointer data){
    gtk_statusbar_remove_all( GTK_STATUSBAR(widget), GPOINTER_TO_INT(data) );
}

/*
 * Function:  show_input_video_name 
 * --------------------
 * Create element and show input's video_name base on v4l list.
 *
 *  name: Name of current device.
 * --------------------
 *  Return: GTK input's video_name.
 */
GtkWidget *show_input_video_name(char *name){
    GtkWidget *out = gtk_combo_box_text_new();
    int i, j;
    int index = 0;
    for (i = 0; i < numIn; ++i){
        if(inDevs[i]->state == STATE_NORMAL){
            if(inDevs[i]->input_type == MONITOR ){
                gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT( out ), "Screen");
                if(strcmp(name, ":0.0") == 0)
                    gtk_combo_box_set_active(GTK_COMBO_BOX(out), index);
                index++;
            }
            if(inDevs[i]->input_type == VIDEO ){
                for(j = 0; j < v4l_list->num_device; j++){
                    if(strcmp(inDevs[i]->name_device, v4l_list->name[j]) == 0)
                        gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT( out ), v4l_list->display_name[j]);
                    if(strcmp(name, inDevs[i]->name_device) == 0)
                        gtk_combo_box_set_active(GTK_COMBO_BOX(out), index);
                }
                index++;
            }
        }
        
    }
    return out;
}

/*
 * Function:  show_input_audio_name 
 * --------------------
 * Create element and show input's audio_name base on alsa list.
 *
 *  name: Name of current device.
 * --------------------
 *  Return: GTK input's audio_name.
 */
GtkWidget *show_input_audio_name(char *name){
    GtkWidget *out = gtk_combo_box_text_new();
    int i, j;
    int index = 0;
    for (i = 0; i < numIn; ++i){
        if(inDevs[i]->input_type == AUDIO && inDevs[i]->state == STATE_NORMAL){
            for(j = 0; j < alsa_list->num_device; j++){
                if(strcmp(inDevs[i]->name_device, alsa_list->name[j]) == 0){
                    gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT( out ), alsa_list->display_name[j]);
                }
                if(strcmp(name, inDevs[i]->name_device) == 0){
                    gtk_combo_box_set_active(GTK_COMBO_BOX(out), index);
                }
            }
            index++;
        }
    }
    return out;
}

/*
 * Function:  get_input_video_name 
 * --------------------
 * Get real name of input video device base on input's display name.
 *
 *  name: Name get from UI.
 *
 *  return_name: Real name of input device.
 */
void get_input_video_name(char *name, char *return_name){
    int i, j;
    for (i = 0; i < numIn; ++i){
        //device is screen
        if(inDevs[i]->state == STATE_NORMAL){
            if(inDevs[i]->input_type == MONITOR ){
                if(strcmp(name, "Screen") == 0)
                    strcpy(return_name, ":0.0");
            }

            //if device is v4l device
            if(inDevs[i]->input_type == VIDEO ){
                for(j = 0; j < v4l_list->num_device; j++){
                    if(strcmp(name, v4l_list->display_name[j]) == 0)
                        strcpy(return_name, v4l_list->name[j]);
                }
            }
        }
    }
}

/*
 * Function:  get_input_audio_name 
 * --------------------
 * Get real name of input audio device base on input's display name.
 *
 *  name: Name get from UI.
 *
 *  return_name: Real name of input device.
 */
void get_input_audio_name(char *name, char *return_name){
    int i, j;
    for (i = 0; i < numIn; ++i){
        //check device is alsa device
        if(inDevs[i]->input_type == AUDIO ){
            for(j = 0; j < alsa_list->num_device; j++){
                if(strcmp(name, alsa_list->display_name[j]) == 0)
                    strcpy(return_name, alsa_list->name[j]);
            }
        }
    }
}

/*
 * Function:  change_output_name 
 * --------------------
 * Change output's url base on output's file_name and output's format.
 */
void change_output_name(){
    char str[SHORT_STRING_LENGTH];
    char tmp[30];
    
    //save file url
    int i;
    for(i = 0; i < numOut; i++){
        if(outDevs[i]->output_type == 0){

            //get file name
            get_filename(outDevs[i]->url, tmp);
            sprintf(str, "%s/%s.%s", path, tmp, outDevs[i]->format);
            strcpy(outDevs[i]->url, str);
        }
    }
    //save file url
    save_output_url_to_config_file(outDevs, numOut);
}

/*
 * Function:  get_format_combo_box 
 * --------------------
 * Get format_id from output's file_extenxion and pass to string.
 *
 *  select: GTK output's file_extenxion.
 * --------------------
 *  Return: File extension.
 */
char *get_format_combo_box(GtkWidget * select){
    int tmp_int = gtk_combo_box_get_active(GTK_COMBO_BOX(select));
    char tmp[10];
    switch(tmp_int){
        case CONTAINER_MP4:
            strcpy(tmp, "mp4");
            break;
        case CONTAINER_FLV:
            strcpy(tmp, "flv");
            break;
        case CONTAINER_MP3:
            strcpy(tmp, "mp3");
            break;
        case CONTAINER_MKV:
            strcpy(tmp, "mkv");
            break;
        default:
            break;
    }
}

/*
 * Function:  get_format_combo_box 
 * --------------------
 * Show course, version, lecture base on type.
 *
 *  combo_box: GTK combobox (Course or version or lecture).
 *
 *  type: type of showing. Can be COURSE, VERSION or LECTURE.
 */
void show_course_management(GtkWidget *combo_box, int type){
    int i, j;
    //set value
    switch(type){
        case COURSE:
            for (i = 0; i < course_data_info->num_subject; i++){
                gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT( combo_box ), 
                    course_data_info->subject_list[i].subject_name );
                if(strcmp(course_data_info->subject_list[i].subject_name, current_course) == 0){
                    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_box),i);
                    course_index = i;
                }
            }
            break;
        case VERSION:
            for (j = 0; j < course_data_info->subject_list[course_index].num_version; j++){
                gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT( combo_box), 
                    course_data_info->subject_list[course_index].version_list[j].version_name );
                if(strcmp(course_data_info->subject_list[course_index].version_list[j].version_name,
                                        current_version) == 0){
                    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_box),j);
                    version_index = j;
                }
            }
            break;
        case LECTURE:
                for (j = 0; j < course_data_info->subject_list[course_index].version_list[version_index].num_lecture; j++){
                gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT( combo_box ), 
                    course_data_info->subject_list[course_index].version_list[version_index].lecture_list[j].lecture_name );
                if(strcmp(course_data_info->subject_list[course_index].version_list[version_index].lecture_list[j].lecture_name,
                                        current_lecture) == 0){
                    gtk_combo_box_set_active(GTK_COMBO_BOX(combo_box),j);
                    lecture_index = j;
                }
            }
            break;
        default:
            break;
    }
}

/*
 * Function:  get_format_combo_box 
 * --------------------
 * Create folder if not exits.
 * Check curent session index,
 * Change output's url base on current information.
 */
void change_file_path(){
    char tmp_path[LONG_STRING_LENGTH];
    char tmp_path_file[LONG_STRING_LENGTH];
    char tmp_path_file1[LONG_STRING_LENGTH];
    struct stat st = {0};

    //check and create folder
    sprintf(tmp_path, "%s", home_path);
    if (stat(tmp_path, &st))
        mkdir(tmp_path, 0777);

    sprintf(tmp_path, "%s/%s", tmp_path, current_course);
    if (stat(tmp_path, &st))
        mkdir(tmp_path, 0777);

    sprintf(tmp_path, "%s/%s", tmp_path, current_version);
    if (stat(tmp_path, &st))
        mkdir(tmp_path, 0777);

    if(type == NEW_LECTURE|| type == NORMAL){
        sprintf(tmp_path, "%s/%s", tmp_path, current_lecture);
    }

    //check and create folder
    sprintf(tmp_home_path, "%s", home_path);
    sprintf(tmp_home_path, "%s/%s", tmp_home_path, current_course);
    sprintf(tmp_home_path, "%s/%s", tmp_home_path, current_version);
    sprintf(tmp_home_path, "%s/%s", tmp_home_path, current_lecture);

    //update xml file
    SubjectInfo* tmp_subject;
    switch(type){
        case NEW_COURSE:
        {
            //add new course
            SubjectInfo* tmp_subject;
            strcpy(course_data_info->subject_list[course_data_info->num_subject].subject_name, current_course);
            tmp_subject = &course_data_info->subject_list[course_data_info->num_subject];
            course_data_info->num_subject++;
            create_new_course(home_path, current_course, current_course_id);

            //add new version
            strcpy(tmp_subject->version_list[0].version_name, current_version);
            VersionInfo* tmp_version = &tmp_subject->version_list[0];
            tmp_subject->num_version = 1;
            create_new_version(home_path, current_course, current_version);
        }
            break;
        case NEW_VERSION:
        {
            //get tmp_subject subject
            SubjectInfo* tmp_subject = &course_data_info->subject_list[course_index];

            //add new version
            strcpy(tmp_subject->version_list[tmp_subject->num_version].version_name, current_version);
            VersionInfo* tmp_version = &tmp_subject->version_list[tmp_subject->num_version];
            tmp_subject->num_version++;
            create_new_version(home_path, current_course, current_version);
        }

            break;
        case NEW_LECTURE:
        {
            //create folder
            if (stat(tmp_path, &st))
                mkdir(tmp_path, 0777);
            
            //get tmp_subject subject
            SubjectInfo* tmp_subject = &course_data_info->subject_list[course_index];

            //get tmp version
            VersionInfo* tmp_version = &tmp_subject->version_list[version_index];

            //add new lecture
            strcpy(tmp_version->lecture_list[tmp_version->num_lecture].lecture_name, current_lecture);
            tmp_version->num_lecture++;
            create_new_lecture(home_path, current_course, current_version, current_lecture, current_description);
        }
            break;
        default:
            break;
    }

    //check session valid
    int i;
    if(type == NEW_LECTURE || type == NORMAL){
        for(i = 0; i < numOut; i++){   
            //check all output streams type HDD
            if(outDevs[i]->output_type == 0&& outDevs[i]->disable == 0){

                //get full path with session
                sprintf(tmp_path_file, "%s/%s%d/%s%s", tmp_path, current_path_prefix, session, current_path_prefix,".xml");

                //check valid session
                while (!access(tmp_path_file, F_OK)||!access(tmp_path_file1, F_OK)){
                    session++;
                    sprintf(tmp_path_file, "%s/%s%d/%s%s", tmp_path, current_path_prefix, session, current_path_prefix, ".xml");
                } 
            }
        }
        
        //get tmp path    
        sprintf(tmp_path, "%s/%s%d", tmp_path, current_path_prefix, session);

        //create new session folder
        if (stat(tmp_path, &st))
            mkdir(tmp_path, 0777);

        strcpy(path, tmp_path);
        change_output_name();
    }
}
