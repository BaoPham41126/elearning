/*
 * File: course_management_view.c
 *
 * Author: GVLab
 *
 * Created on: Aug 13, 2015
 */

#include "headers/view/course_management_view.h"
/*
 * Function:  select_home_path 
 * --------------------
 * Use GTK file selection allow user selecting home path folder.
 *
 *  widget: Pointer to GTK selection element.
 */
void select_home_path(GtkWidget *widget){

    GtkWidget *file_selector;

    /* Create the selector */
    file_selector = gtk_file_selection_new ("Please select home path");

    g_signal_connect ((gpointer) (GTK_FILE_SELECTION (file_selector)->ok_button),
      "clicked",
      G_CALLBACK (store_filename),
      file_selector);

    /* Ensure that the dialog box is destroyed when the user clicks a button. */
    g_signal_connect_swapped ((gpointer) (GTK_FILE_SELECTION (file_selector)->ok_button),
      "clicked",
      G_CALLBACK (gtk_widget_destroy),
      file_selector);

    g_signal_connect_swapped ((gpointer) (GTK_FILE_SELECTION (file_selector)->cancel_button),
      "clicked",
      G_CALLBACK (gtk_widget_destroy),
      file_selector);

    /* Display that dialog */
    gtk_widget_show (file_selector);
}

/*
 * Function:  save_course_management_button_click 
 * --------------------
 * Get all course data from course_UI.
 * Save all course data to xml file, config file.
 *
 *  widget: Pointer pointing to GTK save button.
 */
void save_course_management_button_click(GtkWidget *widget){
    char tmp_data[SHORT_STRING_LENGTH];

    session = 0;
    ui_error = VALID;
    //read home entry
    strcpy(home_path, gtk_entry_get_text (GTK_ENTRY(display_course->home)));

    //read course
    if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(display_course->check_select_course))){
        get_combo_box_text(GTK_COMBO_BOX(display_course->course), tmp_data);
        strcpy(current_course, tmp_data);
    } else {
        strcpy(current_course, gtk_entry_get_text(GTK_ENTRY(display_course->course_new)));

        strcpy(current_course_id, gtk_entry_get_text(GTK_ENTRY(display_course->course_id)));
        type++;
    }

    if(strcmp(current_course, "") == 0){
        push_item(status_bar, GINT_TO_POINTER(context_id), "Error: Course is invalid");
        ui_error = COURSE_ERROR;
    }

    //read version
    if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(display_course->check_select_version))){
        get_combo_box_text(GTK_COMBO_BOX(display_course->version), tmp_data);
        strcpy(current_version, tmp_data);
    } else {
        strcpy(current_version, gtk_entry_get_text(GTK_ENTRY(display_course->version_new)));
        type++;
    }

    if(strcmp(current_version, "") == 0){
        push_item(status_bar, GINT_TO_POINTER(context_id), "Error: Version is invalid");
        ui_error = COURSE_ERROR;
    }

    if(ui_error == VALID){
        //save location
        change_file_path();

        type = 0;
        //clear description
        GtkTextBuffer *buffer;
        buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (display_lecture->description));
        gtk_text_buffer_set_text (buffer, "", -1);

        //get lecture
        get_combo_box_text(GTK_COMBO_BOX(display_lecture->lecture), tmp_data);
        strcpy(current_lecture, tmp_data);

        lecture_change(GTK_COMBO_BOX(display_lecture->lecture), NULL);

        //save data to file
        save_location_setting(home_path, data_path, current_course, current_version,
            current_lecture, current_path_prefix);
    
        //clear status bar
        clear_status_bar(status_bar, GINT_TO_POINTER(context_id));
    }
}

/*
 * Function:  check_new_version_button 
 * --------------------
 * If user can select a version, disable select a version, disable select lecture.
 * Allow user entering new version from UI.
 *
 * Else allow user selecting new version. Disable enter new version from UI.
 *
 *  widget: Pointer to GTK check_new_version button.
 *
 *  button: Pointer to GTK check_new_version button.
 */
void check_new_version_button(GtkWidget *widget, gpointer button){
    if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(button))){
        gtk_widget_set_sensitive(display_course->version, FALSE);
        gtk_widget_set_sensitive(display_course->version_new, TRUE);
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(display_lecture->check_new_lecture), TRUE);
    } else {
        gtk_widget_set_sensitive(display_course->version, TRUE);
        gtk_widget_set_sensitive(display_course->version_new, FALSE);
    }
}

/*
 * Function:  on_course_change 
 * --------------------
 * When current course change, version will be changed base on current course.
 *
 *  course: Pointer to GTK course selection element.
 */
void on_course_change( GtkComboBox *course){
    int j;
    //get course index
    course_index = gtk_combo_box_get_active( course );
    j = gtk_combo_box_get_active(GTK_COMBO_BOX(display_course->version));
    
    //clear all version combobox
    clear_combo_box_data(display_course->version);

    //set vale of version combobox
    show_course_management(display_course->version, 2);

    gtk_combo_box_set_active(GTK_COMBO_BOX(display_course->version),0);
    version_index = 0;
}

/*
 * Function:  on_version_change 
 * --------------------
 * When current version change, lecture will be changed base on current version.
 *
 *  version: Pointer to GTK version selection element.
 */
void on_version_change( GtkComboBox *version){
    int j;
    //get version index
    version_index = gtk_combo_box_get_active( version );
    
    //clear all version combobox
    clear_combo_box_data(display_lecture->lecture);

    //set vale of version combobox
    show_course_management(display_lecture->lecture, 3);

    gtk_combo_box_set_active(GTK_COMBO_BOX(display_lecture->lecture),0);
    lecture_index = 0;
}

/*
 * Function:  check_new_course_button 
 * --------------------
 * If user can select a course, disable select a course, disable select version, lecture.
 * Allow user entering new course from UI.
 *
 * Else allow user selecting new course. Disable enter new course from UI.
 *
 *  widget: Pointer to GTK check_new_course_button button.
 *
 *  button: Pointer to GTK check_new_course_button button.
 */
void check_new_course_button(GtkWidget *widget, gpointer button){
    if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(button))){
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(display_course->check_select_version), FALSE);
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(display_course->check_new_version), TRUE);

        gtk_widget_set_sensitive(display_course->course, FALSE);
        gtk_widget_set_sensitive(display_course->version, FALSE);
        gtk_widget_set_sensitive(display_course->check_select_version, FALSE);
        gtk_widget_set_sensitive(display_course->course_new, TRUE);
        gtk_widget_set_sensitive(display_course->course_id, TRUE);
    } else {
        gtk_widget_set_sensitive(display_course->check_select_version, TRUE);
        gtk_widget_set_sensitive(display_course->course, TRUE);
        gtk_widget_set_sensitive(display_course->course_new, FALSE);
        gtk_widget_set_sensitive(display_course->course_id, FALSE);
    }
}

/*
 * Function:  store_filename 
 * --------------------
 * Get file name when user select a path and display to UI(Recorded data home entry).
 *
 *  widget: Pointer to GTK file selection.
 *
 *  user_data: Pointer to GTK file selection.
 */
void store_filename(GtkWidget *widget, gpointer user_data) {
    GtkWidget *file_selector = GTK_WIDGET (user_data);
    const gchar *selected_filename;

    //get file name
    selected_filename = gtk_file_selection_get_filename (GTK_FILE_SELECTION (file_selector));
    strcpy(home_path, selected_filename);

    //display file name
    gtk_entry_set_text(GTK_ENTRY(display_course->home), home_path);
}

/*
 * Function:  create_course_management_tab 
 * --------------------
 * Create Course Management tab: Create UI and display current data.
 *
 *  window: Main window of program.
 * --------------------
 *  Return: Pointer to GTK_HBOX (Contain all course_management's element).
 */
GtkWidget* create_course_management_tab(GtkWidget* window){
    GtkWidget *hbox, *vbox, *table, *save_button;
    GtkWidget *frame, *course_frame, *version_frame;
    GtkWidget *version_table;
    GtkWidget *choose_home_path_button, *hseparator, *label;
    char str[SHORT_STRING_LENGTH];  
    int i, j;

    //create new hbox
    hbox = gtk_hbox_new (FALSE, 5);

    //create new vbox
    vbox = gtk_vbox_new (FALSE, 5);
    gtk_container_add(GTK_CONTAINER(hbox), vbox); 

    //separator
    hseparator = gtk_hseparator_new();
    gtk_box_pack_start (GTK_BOX (vbox), hseparator, FALSE, FALSE, 5);
    gtk_widget_show(hseparator);

    /*
     * DATA HOME FRAME
     */
    //home frame
    frame = gtk_frame_new("Recorded data home");
    gtk_box_pack_start (GTK_BOX (vbox), frame, FALSE, FALSE, 5);
    gtk_widget_set_size_request(GTK_WIDGET(frame), 550, 100);
    //home table
    table = gtk_table_new(2, 4, FALSE);
    gtk_container_add(GTK_CONTAINER(frame), table);

    //home entry
    display_course->home = gtk_entry_new();
    gtk_entry_set_text(GTK_ENTRY(display_course->home), home_path);
    gtk_widget_set_size_request(GTK_WIDGET(display_course->home), 415, 30);
    gtk_table_attach(GTK_TABLE(table), display_course->home, 0, 1, 0, 1, 
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    //home button choose file
    display_course->home_button = create_directory_button();
    gtk_table_attach(GTK_TABLE(table), display_course->home_button, 1, 2, 0, 1, 
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    g_signal_connect( G_OBJECT( display_course->home_button ), "clicked",
                     G_CALLBACK( select_home_path ), NULL);

    /*
     * COURSE DATA
     */
    frame = gtk_frame_new("Course");
    gtk_box_pack_start (GTK_BOX (vbox), frame, FALSE, FALSE, 5);

    //course table
    table = gtk_table_new(2, 4, FALSE);
    gtk_container_add(GTK_CONTAINER(frame), table);

    //choose exist course ratio button
    display_course->check_select_course = gtk_radio_button_new_with_label( NULL,"Select an existing course:");
    gtk_table_attach(GTK_TABLE(table), display_course->check_select_course, 0, 1, 0, 1, 
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    //course data
    display_course->course = gtk_combo_box_text_new();
    show_course_management(display_course->course, 1);
    gtk_table_attach(GTK_TABLE(table), display_course->course, 0, 1, 1, 2, 
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    gtk_widget_set_size_request(GTK_WIDGET(display_course->course), 415, 30);
    g_signal_connect( G_OBJECT( display_course->course ), "changed",
        G_CALLBACK( on_course_change ), NULL );

    //new course ratio button
    display_course->check_new_course = gtk_radio_button_new_with_label_from_widget (
        GTK_RADIO_BUTTON (display_course->check_select_course), "New course");
    gtk_table_attach(GTK_TABLE(table), display_course->check_new_course, 0, 1, 2, 3, 
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    g_signal_connect( G_OBJECT( display_course->check_new_course ), "clicked",
                     G_CALLBACK( check_new_course_button ), display_course->check_new_course);

    //new course entry
    display_course->course_new = gtk_entry_new();
    gtk_table_attach(GTK_TABLE(table), display_course->course_new, 0, 1, 3, 4, 
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    gtk_widget_set_sensitive(display_course->course_new, FALSE);

    //course id lable
    label = gtk_label_new("Course ID");
    gtk_table_attach(GTK_TABLE(table), label, 1, 2, 2, 3, GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    // course_id
    display_course->course_id = gtk_entry_new();
    gtk_table_attach(GTK_TABLE(table), display_course->course_id, 1, 2, 3, 4, 
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    gtk_widget_set_sensitive(display_course->course_id, FALSE);

    /*
     * VERSION DATA
     */

    //separator
    hseparator = gtk_hseparator_new();
    gtk_box_pack_start (GTK_BOX (vbox), hseparator, FALSE, FALSE, 5);
    gtk_widget_show(hseparator);

    frame = gtk_frame_new("Version");
    gtk_box_pack_start (GTK_BOX (vbox), frame, FALSE, FALSE, 5);

    //version table
    table = gtk_table_new(2, 4, FALSE);
    gtk_container_add(GTK_CONTAINER(frame), table);

    //choose exist version ratio button
    display_course->check_select_version = gtk_radio_button_new_with_label( NULL,"Select an existing version");
    gtk_table_attach(GTK_TABLE(table), display_course->check_select_version, 0, 1, 0, 1, 
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    //version data
    display_course->version = gtk_combo_box_text_new();
    show_course_management(display_course->version, 2);
    gtk_table_attach(GTK_TABLE(table), display_course->version, 0, 1, 1, 2, 
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    gtk_widget_set_size_request(GTK_WIDGET(display_course->version), 415, 30);

    g_signal_connect( G_OBJECT( display_course->version ), "changed",
        G_CALLBACK( on_version_change ), NULL );
    //new version ratio button
    display_course->check_new_version = gtk_radio_button_new_with_label_from_widget (
        GTK_RADIO_BUTTON (display_course->check_select_version), "New version");
    gtk_table_attach(GTK_TABLE(table), display_course->check_new_version, 0, 1, 2, 3, 
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    g_signal_connect( G_OBJECT( display_course->check_new_version ), "clicked",
                     G_CALLBACK( check_new_version_button ), display_course->check_new_version);


    //new version entry
    display_course->version_new = gtk_entry_new();
    gtk_table_attach(GTK_TABLE(table), display_course->version_new, 0, 1, 3, 4, 
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    gtk_widget_set_sensitive(display_course->version_new, FALSE);
    //--------------------------button--------------------------
    //create new vbox
    vbox = gtk_vbox_new (FALSE, 5);
    gtk_container_add(GTK_CONTAINER(hbox), vbox);

    //separator
    hseparator = gtk_hseparator_new();
    gtk_box_pack_start (GTK_BOX (vbox), hseparator, FALSE, FALSE, 5);
    gtk_widget_show(hseparator);

    //save button
    save_button = gtk_button_new_with_label("Save");
    gtk_widget_set_size_request(GTK_WIDGET(save_button), 100, 30);
    gtk_box_pack_end (GTK_BOX (vbox), save_button, FALSE, FALSE, 5);

    g_signal_connect(G_OBJECT(save_button), "clicked", G_CALLBACK(save_course_management_button_click), NULL);

    return hbox;
}
