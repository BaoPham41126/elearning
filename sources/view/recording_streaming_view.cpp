/*
 * File: recording_streaming_view.c
 *
 * Author: GVLab
 *
 * Created on: Aug 13, 2015
 */

#include "headers/view/recording_streaming_view.h"

/*
 * Function:  create_recording_streaming_tab 
 * --------------------
 * Create Recording & Streaming tab: Create UI and display recording & streaming data.
 *
 *  window: Main window of program.
 * --------------------
 *  Return: Pointer to GTK_VBOX (Contain all recording & streaming 's element).
 */
GtkWidget* create_recording_streaming_tab(GtkWidget* window, GtkWidget *sock){
    GtkWidget *frame, *preview_button, *button, *label, *apply_button;
    GtkWidget *hbox, *vbox, *hseparator, *vseparator, *tmp_vbox, *tmp_hbox, *tmp_box;
    GdkColor color;
    GtkWidget *logo;

    //create new hbox1
    vbox = gtk_vbox_new (FALSE, 5);

    //separator
    hseparator = gtk_hseparator_new();
    gtk_box_pack_start (GTK_BOX (vbox), hseparator, FALSE, FALSE, 5);

    //create new hbox
    hbox = gtk_hbox_new (FALSE, 5);
    gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 5);
    
    //----------------------------left---------------------
    //create vbox left
    tmp_vbox = gtk_vbox_new (FALSE, 5);
    gtk_box_pack_start (GTK_BOX (hbox), tmp_vbox, FALSE, FALSE, 5);


    //create frame contain sock
    frame = gtk_frame_new("Preview screen");
    gtk_box_pack_start (GTK_BOX (tmp_vbox), frame, FALSE, FALSE, 5);
    gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_OUT); 

    //gtksock contain SDL screen
    gtk_widget_set_size_request( sock, DEFAULT_SCREEN_WIDTH, DEFAULT_SCREEN_HEIGHT); 
    gtk_container_add(GTK_CONTAINER(frame), sock);
    // gtk_container_set_border_width(GTK_CONTAINER(sock), 10);
    gdk_color_parse ("black", &color);
    gtk_widget_modify_bg ( GTK_WIDGET(sock), GTK_STATE_NORMAL, &color);

    //select stream to preview
    //craete child hbox
    tmp_hbox = gtk_hbox_new (FALSE, 5);
    gtk_box_pack_start (GTK_BOX (tmp_vbox), tmp_hbox, FALSE, FALSE, 5);

    //create preview button
    preview_button = gtk_check_button_new();
    gtk_box_pack_start (GTK_BOX (tmp_hbox), preview_button, FALSE, FALSE, 5);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(preview_button), TRUE);

    //select
    combo_box = gtk_combo_box_text_new();
    gtk_box_pack_start (GTK_BOX (tmp_hbox), combo_box, FALSE, FALSE, 5);
    gtk_widget_set_size_request(GTK_WIDGET(combo_box), 390, 35);
    g_signal_connect(G_OBJECT(combo_box), "changed", G_CALLBACK(select_preview_stream_changed), NULL);

    //create start button
    start_button = gtk_button_new_with_label("Start");
    gtk_widget_set_size_request(GTK_WIDGET(start_button), 140, 35);
    gtk_box_pack_end (GTK_BOX (tmp_hbox), start_button, FALSE, FALSE, 5);

    // //create stop button
    stop_button = gtk_button_new_with_label("Stop");
    gtk_widget_set_size_request(GTK_WIDGET(stop_button), 140, 35);
    gtk_box_pack_end (GTK_BOX (tmp_hbox), stop_button, FALSE, FALSE, 5);

    //create frame contain status
    frame = gtk_frame_new("Lecture status");
    gtk_box_pack_start (GTK_BOX (tmp_vbox), frame, FALSE, FALSE, 5);
    gtk_widget_set_size_request(GTK_WIDGET(frame), 200, 60);
    gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_OUT); 

    lecture_box = gtk_hbox_new (FALSE, 5);
    gtk_container_add(GTK_CONTAINER(frame), lecture_box);

    //------------------------------right---------------------
    //separator
    vseparator = gtk_vseparator_new();
    gtk_box_pack_start (GTK_BOX (hbox), vseparator, FALSE, FALSE, 5);
    
    //create new vbox right 
    tmp_vbox = gtk_vbox_new (FALSE, 5);
    gtk_box_pack_start (GTK_BOX (hbox), tmp_vbox, FALSE, FALSE, 5);

    //create frame contain sock
    frame = gtk_frame_new("Input audio");
    gtk_box_pack_start (GTK_BOX (tmp_vbox), frame, FALSE, FALSE, 5);
    gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_OUT); 

    //attach audio box
    tmp_box = gtk_vbox_new (FALSE, 5);
    gtk_container_add(GTK_CONTAINER(frame), tmp_box);

    input_status->a_table = gtk_table_new(5, 3, FALSE);
    gtk_box_pack_start (GTK_BOX (tmp_box), input_status->a_table, FALSE, FALSE, 5);

    //create frame contain sock
    frame = gtk_frame_new("In to out matrix");
    gtk_box_pack_start (GTK_BOX (tmp_vbox), frame, FALSE, FALSE, 5);
    gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_OUT); 

    tmp_box = gtk_vbox_new (FALSE, 5);
    gtk_container_add(GTK_CONTAINER(frame), tmp_box);

    input_status->v_table = gtk_table_new(MAX_OUT, MAX_OUT, FALSE);
    gtk_box_pack_start (GTK_BOX (tmp_box), input_status->v_table, FALSE, FALSE, 5);
    
    //create frame contain sock
    frame = gtk_frame_new("Output buffer status");
    gtk_box_pack_start (GTK_BOX (tmp_vbox), frame, FALSE, FALSE, 5);
    gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_OUT); 

    //create status box
    buffer_status_box = gtk_vbox_new (FALSE, 2);
    gtk_container_add(GTK_CONTAINER(frame), buffer_status_box);

    //create table
    GtkWidget *table = gtk_table_new(2, 2, FALSE);
    gtk_box_pack_start (GTK_BOX (buffer_status_box), table, FALSE, FALSE, 2);

    //create image logo
    logo = gtk_image_new_from_file("resources/images/logo.png");
    gtk_box_pack_end (GTK_BOX (tmp_vbox), logo, FALSE, FALSE, 5);

    //connect function to button
    g_signal_connect(G_OBJECT(start_button), "clicked", 
    G_CALLBACK(start_button_click), NULL);

    g_signal_connect(G_OBJECT(stop_button), "clicked", 
    G_CALLBACK(stop_button_click), NULL);

    g_signal_connect(G_OBJECT(preview_button), "clicked", 
    G_CALLBACK(preview_button_click), NULL);

    return vbox;
}
