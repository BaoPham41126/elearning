/*
 * File: processing_view.c
 *
 * Author: GVLab
 *
 * Created on: Aug 13, 2015
 */

#include "headers/view/processing_view.h"

//processing thread
pthread_t processing_thread;

/*
 * Function:  data_process_changed 
 * --------------------
 * Show course, version, lecture base on change_type.
 *
 *  combo_box: Pointer to GTK processing element (course or version).
 *
 *  type: Type of changing. Can be COURSE_CHANGE or VERSION_CHANGE.
 */
void data_process_changed(GtkWidget *combo_box, int type){
    int i, j;
    int course = 0, version = 0;
    switch(type){
        case COURSE_CHANGE:
            //clear
            clear_combo_box_data(display_process->version);

            //curse index
            course = gtk_combo_box_get_active(GTK_COMBO_BOX(display_process->course));

            //setvalue
            for (j = 0; j < course_data_info->subject_list[course].num_version; j++){
                gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT( display_process->version), 
                    course_data_info->subject_list[course].version_list[j].version_name );
            }
            gtk_combo_box_set_active(GTK_COMBO_BOX(display_process->version),0);
            break;
        case VERSION_CHANGE:
            //clear
            clear_combo_box_data(display_process->lecture);
            //index
            course = gtk_combo_box_get_active(GTK_COMBO_BOX(display_process->course));
            version = gtk_combo_box_get_active(GTK_COMBO_BOX(display_process->version));

            //set value
            for (j = 0; j < course_data_info->subject_list[course].version_list[version].num_lecture; j++){
                gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT( display_process->lecture ), 
                    course_data_info->subject_list[course].version_list[version].lecture_list[j].lecture_name );
            }
            gtk_combo_box_set_active(GTK_COMBO_BOX(display_process->lecture),0);
            break;
        default:
            break;
    }
}

/*
 * Function:  store_data_name 
 * --------------------
 * Get file name when user select a path and display to UI(Processed data home).
 *
 *  widget: Pointer to GTK file selection.
 *
 *  user_data: Pointer to GTK file selection.
 */
void store_data_name(GtkWidget *widget, gpointer user_data){
    GtkWidget *file_selector = GTK_WIDGET (user_data);
    const gchar *selected_filename;

    selected_filename = gtk_file_selection_get_filename (GTK_FILE_SELECTION (file_selector));
    strcpy(data_path, selected_filename);
    gtk_entry_set_text(GTK_ENTRY(display_process->home), data_path);

    //save data to file
    save_location_setting(home_path, data_path, current_course, current_version,
        current_lecture, current_path_prefix);
}

/*
 * Function:  select_data_path 
 * --------------------
 * Use GTK file selection allow user selecting processed data home folder.
 *
 *  widget: Pointer to GTK selection element.
 */
void select_data_path(GtkWidget *widget){

    GtkWidget *file_selector;

    /* Create the selector */
    file_selector = gtk_file_selection_new ("Please select data path");

    g_signal_connect ((gpointer)(GTK_FILE_SELECTION (file_selector)->ok_button),
      "clicked",
      G_CALLBACK (store_data_name),
      file_selector);

    /* Ensure that the dialog box is destroyed when the user clicks a button. */
    g_signal_connect_swapped ((GTK_FILE_SELECTION (file_selector)->ok_button),
      "clicked",
      G_CALLBACK (gtk_widget_destroy),
      file_selector);

    g_signal_connect_swapped (GTK_FILE_SELECTION (file_selector)->cancel_button,
      "clicked",
      G_CALLBACK (gtk_widget_destroy),
      file_selector);

    /* Display that dialog */
    gtk_widget_show (file_selector);
}

/*
 * Function:  process_data 
 * --------------------
 * Process video thread.
 * Get all processing's information from UI.
 * Prepare data for processing video.
 * Process video and show status.
 *
 *  main_window: Pointer to GTK main window.
 */
void *process_data(void *main_window){
    GtkWidget *window = (GtkWidget *)main_window;

    char tmp_file_name[30];
    char tmp_data[STRING_LENGTH];
    char tmp_path[LONG_STRING_LENGTH];
    int i;
    int first = 1;

    //read course
    get_combo_box_text(GTK_COMBO_BOX(display_process->course), tmp_data);
    strcpy(display_process->curr_course, tmp_data);

    //read version
    get_combo_box_text(GTK_COMBO_BOX(display_process->version), tmp_data);
    strcpy(display_process->curr_version, tmp_data);

    //read lecture
    get_combo_box_text(GTK_COMBO_BOX(display_process->lecture), tmp_data);
    strcpy(display_process->curr_lecture, tmp_data);
    
    sprintf(tmp_path, "%s/%s/%s/%s", home_path, display_process->curr_course, 
        display_process->curr_version, display_process->curr_lecture);

    //get current time
    char current_time[30];
    // get_current_time(current_time);
    get_time(current_time, tmp_path);

    for(i = 0; i < numOut; i++){   
        //check all output streams type HDD
        if(outDevs[i]->output_type == 0 && outDevs[i]->disable == 0){
            get_filename(outDevs[i]->url, tmp_file_name);
            process_video(tmp_path, data_path, tmp_file_name, first, 
                display_process->curr_course, display_process->curr_version, 
                display_process->curr_lecture, outDevs[i]->format, current_time);
            first = 0;
        }
    }
    
    // Create the widgets
    push_item(status_bar, GINT_TO_POINTER(context_id), "Processed finished");
    create_quick_message("Process finished!", window, 1);
}

/*
 * Function:  process_button_click 
 * --------------------
 * Create processing video thread to process video after recorded.
 *
 *  widget: Pointer to GTK process_video button.
 *
 *  window: Pointer to GTK main window.
 */
void process_button_click(GtkWidget *widget, gpointer window){
    //processing thread
    pthread_create(&processing_thread, NULL, process_data, window);
}

 /*
 * Function:  create_processing_tab 
 * --------------------
 * Create Processing tab: Create UI and display processing data.
 *
 *  window: Main window of program.
 * --------------------
 *  Return: Pointer to GTK_VBOX (Contain all processing's element).
 */
GtkWidget* create_processing_tab(GtkWidget* window){
    GtkWidget *frame, *table, *label;
    GtkWidget *hbox, *vbox, *hseparator;

    //create new hbox1
    vbox = gtk_vbox_new (FALSE, 5);

    //separator
    hseparator = gtk_hseparator_new();
    gtk_box_pack_start (GTK_BOX (vbox), hseparator, FALSE, FALSE, 5);

    //create new hbox
    hbox = gtk_vbox_new (FALSE, 5);
    gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 5);
    
    //select data path

    //-------------------------data path
    //home frame
    frame = gtk_frame_new("Processed data home");
    gtk_box_pack_start (GTK_BOX (hbox), frame, FALSE, FALSE, 5);

    //home table
    table = gtk_table_new(2, 4, FALSE);
    gtk_container_add(GTK_CONTAINER(frame), table);

    //home entry
    display_process->home = gtk_entry_new();
    gtk_entry_set_text(GTK_ENTRY(display_process->home), data_path);
    gtk_widget_set_size_request(GTK_WIDGET(display_process->home), 550, 30);
    gtk_table_attach(GTK_TABLE(table), display_process->home, 0, 1, 0, 1, 
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    //home button choose file
    display_process->home_button = create_directory_button();
    gtk_table_attach(GTK_TABLE(table), display_process->home_button, 1, 2, 0, 1, 
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    g_signal_connect( G_OBJECT( display_process->home_button ), "clicked",
                     G_CALLBACK( select_data_path ), NULL);

    //----------------------------merge---------------------
    //home frame
    frame = gtk_frame_new("Process video");
    gtk_box_pack_start (GTK_BOX (hbox), frame, FALSE, FALSE, 5);

    //home table
    table = gtk_table_new(2, 4, FALSE);
    gtk_container_add(GTK_CONTAINER(frame), table);

    //label course
    label = gtk_label_new("Course");
    gtk_table_attach(GTK_TABLE(table), label, 0, 1, 0, 1, 
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    //course
    display_process->course = gtk_combo_box_text_new();
    show_course_management(display_process->course, 1);
    gtk_table_attach(GTK_TABLE(table), display_process->course, 1, 2, 0, 1, 
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    g_signal_connect( G_OBJECT( display_process->course ), "changed",
        G_CALLBACK( data_process_changed ), (gpointer) COURSE_CHANGE );
    gtk_widget_set_size_request(GTK_WIDGET(display_process->course), 250, 30);

    //label version
    label = gtk_label_new("Version");
    gtk_table_attach(GTK_TABLE(table), label, 0, 1, 1, 2,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    //version
    display_process->version = gtk_combo_box_text_new();
    show_course_management(display_process->version, 2);
    gtk_table_attach(GTK_TABLE(table), display_process->version, 1, 2, 1, 2,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    g_signal_connect( G_OBJECT( display_process->version ), "changed",
        G_CALLBACK( data_process_changed ), (gpointer) VERSION_CHANGE );

    //label lecture
    label = gtk_label_new("Lecture");
    gtk_table_attach(GTK_TABLE(table), label, 0, 1, 2, 3,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    //lecture
    display_process->lecture = gtk_combo_box_text_new();
    show_course_management(display_process->lecture, 3);
    gtk_table_attach(GTK_TABLE(table), display_process->lecture, 1, 2, 2, 3,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    //merge button
    display_process->merge = gtk_button_new_with_label("Process video");
    gtk_widget_set_size_request(GTK_WIDGET(display_process->merge), 120, 30);
    gtk_table_attach(GTK_TABLE(table), display_process->merge, 0, 1, 3, 4,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    g_signal_connect(G_OBJECT(display_process->merge), "clicked", G_CALLBACK(process_button_click), window);

    return vbox;
}
