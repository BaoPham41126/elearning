/*
 * File: main_view.c
 *
 * Author: GVLab
 *
 * Created on: Aug 13, 2015
 */

#include "headers/view/main_view.h"

OutputInfo* outDevs[MAX_OUT];
InputInfo* inDevs[MAX_OUT];
int numOut, numIn;

//video4linux devices, Asla devices
V4lDevices *v4l_list;
AlsaDevices *alsa_list;

//gtk element for program
GtkWidget *notebook;
GtkWidget *tab;
GtkWidget *main_box, *info_box, *location_box, *input_box ,*youtube_box;
GtkWidget *output_box, *process_box, *upload_box;
GtkWidget *lecture_box, *lecture_label;

//display input devices, output streams
DisplayInput *display_input[MAX_OUT];
DisplayOutput *display_output[MAX_OUT];
DisplayInfo *display_info[MAX_OUT];
DisplayCourseManage *display_course;
DisplayLectureManage *display_lecture;
DisplayProcess *display_process;
DisplayUploading *display_upload;

//output data location
DataInfo* course_data_info;
char path[SHORT_STRING_LENGTH];
char data_path[SHORT_STRING_LENGTH];

//buf
char buf[50];

//error type to display in UI
ErrorKind ui_error;
ErrorKind in_video_error;

//show output info
GtkWidget *output_info_table;

//show folder settings
char home_path[SHORT_STRING_LENGTH];
char current_course[SHORT_STRING_LENGTH];
char current_course_id[20];
char current_version[SHORT_STRING_LENGTH];
char current_lecture[SHORT_STRING_LENGTH];
char current_path_prefix[SHORT_STRING_LENGTH];
char current_description[1024];
int course_index, version_index, lecture_index;

//preview video
GtkWidget *combo_box;

//record
int session;
int type;
char tmp_home_path[LONG_STRING_LENGTH];
int starting;

//start button. stop button
GtkWidget *start_button, *stop_button;

//show status audio, video buffer
GtkWidget *label;

char tmp_space[STRING_LENGTH];
GtkWidget *right_status_bar;
gint right_context_id;

//status table contain all status of program
GtkWidget *buffer_status_box;

/*
 * Function:  check_disk_space 
 * --------------------
 * Check disk space in current recorded data home folder.
 * Auto check disk space after 10s and update status bar.
 * Display information in right statusbar.
 */
static void check_disk_space(gpointer data){
    char space[STRING_LENGTH]; 

    //get space
    get_space_info(home_path, space);

    // update space in status bar
    pop_item(right_status_bar, GINT_TO_POINTER(right_context_id));
    push_item(right_status_bar, GINT_TO_POINTER(right_context_id), space);
}

/*
 * Function:  show_current_stream 
 * --------------------
 * Show all used stream in select_box when recording_streaming started.
 * Show all course, version, lecture information when recording_streaming started.
 * Update preview mode is ALL_IN_ONE.
 */
void show_current_stream(){
    int i;
    char tmp_string[STRING_LENGTH];
    char filename[STRING_LENGTH];
    int num = 0;
    
    //remove all current text
    i = gtk_combo_box_get_active(GTK_COMBO_BOX(combo_box));

    if(i < 0){
        //set valid output stream have in_video
        for(i = 0; i < numOut; i++){
            if(!outDevs[i]->disable && outDevs[i]->state == STATE_NORMAL && outDevs[i]->num_video > 0){
                strcpy(tmp_string, "");
                if(outDevs[i]->output_type == 0){
                    get_filename(outDevs[i]->url, filename);
                    sprintf(tmp_string, "%s    -    hdd    -   %s.%s", outDevs[i]->name, filename, outDevs[i]->format);
                } else {
                    sprintf(tmp_string, "%s    -    live   -   %s", outDevs[i]->name, outDevs[i]->url);
                }
                gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(combo_box), tmp_string);
                num++;
            }
        }

        //show all
        gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(combo_box), "View all input video stream");

        //set active
        gtk_combo_box_set_active(GTK_COMBO_BOX(combo_box), num);
    }

    char tmp_status[SHORT_STRING_LENGTH];
    // update status
    sprintf(tmp_status, "Course :  %s   --   Version:  %s   --   Lecture:  %s", current_course, current_version, current_lecture);
    lecture_label = gtk_label_new(tmp_status);
    gtk_box_pack_start (GTK_BOX (lecture_box), lecture_label, FALSE, FALSE, 5);
    gtk_widget_show(lecture_label);
    update_preview_mode(ALL_IN_ONE);
}

/*
 * Function:  create_buffer_progressbar 
 * --------------------
 * Create and show all buffer progressbar for each used video stream, used audio stream.
 */
void create_buffer_progressbar(){
    int i;
    char tmp_string[15];

    //video progress bar
    for(i = 0; i < numOut; i++){
        //create label
        if(!outDevs[i]->disable){
            //video buffer      
            //set label
            display_buffer[i]->frame = gtk_frame_new(outDevs[i]->name);
            
            //display frame when have video stream or audio stream
            if(outDevs[i]->video_stream || outDevs[i]->audio_stream){
                gtk_box_pack_start (GTK_BOX (buffer_status_box), display_buffer[i]->frame, FALSE, FALSE, 3);
                gtk_widget_show(display_buffer[i]->frame);
            }
            //create table
            display_buffer[i]->table = gtk_table_new(2, 2, FALSE);
            gtk_container_add(GTK_CONTAINER(display_buffer[i]->frame), display_buffer[i]->table);

            //show elements
            gtk_widget_show(display_buffer[i]->table);
            
            //create label for audio and video
            display_buffer[i]->video_label = gtk_label_new("Video");
            display_buffer[i]->audio_label = gtk_label_new("Audio");

            //create progress bar for video and audio
            display_buffer[i]->video_buffer = gtk_progress_bar_new(); 
            gtk_widget_set_size_request (GTK_WIDGET (display_buffer[i]->video_buffer), 150, 10);
            display_buffer[i]->audio_buffer = gtk_progress_bar_new(); 
            gtk_widget_set_size_request (GTK_WIDGET (display_buffer[i]->audio_buffer), 150, 10);

            //check if queue exist and display video buffer progressbar
            if(outDevs[i]->video_stream){
                gtk_table_attach(GTK_TABLE(display_buffer[i]->table), display_buffer[i]->video_label, 0, 1, 0, 1, 
                  GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 3, 3);

                gtk_table_attach(GTK_TABLE(display_buffer[i]->table), display_buffer[i]->video_buffer, 1, 2, 0, 1, 
                  GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 3, 3);

                gtk_widget_show(display_buffer[i]->video_label);
                gtk_widget_show(display_buffer[i]->video_buffer);
            }

            //check if queue exist and display video buffer progressbar
            if(outDevs[i]->audio_stream){
                gtk_table_attach(GTK_TABLE(display_buffer[i]->table), display_buffer[i]->audio_label, 0, 1, 1, 2, 
                  GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 3, 3);

                gtk_table_attach(GTK_TABLE(display_buffer[i]->table), display_buffer[i]->audio_buffer, 1, 2, 1, 2, 
                  GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 3, 3);

                gtk_widget_show(display_buffer[i]->audio_label);
                gtk_widget_show(display_buffer[i]->audio_buffer);
            }
        }
    }
}

/*
 * Function:  hide_input_status 
 * --------------------
 * Hide all preview element when recording_streaming stoped.
 */
void hide_input_status(){
    int i;

    //hide input
    for(i = 0; i < numIn; i++){
        //audio
        if(inDevs[i]->input_type == AUDIO){
            //hide lable
            if(input_status->a_label[i])
                gtk_widget_destroy(input_status->a_label[i]);

            //hide progress bar
            if(input_status->audio[i])
                gtk_widget_destroy(input_status->audio[i]);
        } else {
            //monitor
            if(input_status->v_in_label[i])
                gtk_widget_destroy(input_status->v_in_label[i]);
        }   
    }

    //hide output
    for(i = 0; i < numOut; i++){
        if(!outDevs[i]->disable && outDevs[i]->state == STATE_NORMAL){
            if(input_status->v_out_label[i])
                gtk_widget_destroy(input_status->v_out_label[i]);
        }
    }
}

/*
 * Function:  hide_buffer_progressbar 
 * --------------------
 * Hide all buffer progressbar when recording_streaming stoped.
 */
void hide_buffer_progressbar(){
    int i;

    //video progress bar
    for(i = 0; i < numOut; i++){

        if(!outDevs[i]->disable){
            //destroy frame
            if(display_buffer[i]->audio_label)
                gtk_widget_destroy(display_buffer[i]->audio_label);
            if(display_buffer[i]->video_label)
                gtk_widget_destroy(display_buffer[i]->video_label);
            if(display_buffer[i]->audio_buffer)
                gtk_widget_destroy(display_buffer[i]->audio_buffer);
            if(display_buffer[i]->video_buffer)
                gtk_widget_destroy(display_buffer[i]->video_buffer);
            if(display_buffer[i]->table)
                gtk_widget_destroy(display_buffer[i]->table);
            if(display_buffer[i]->frame)
                gtk_widget_destroy(display_buffer[i]->frame);
        }
    }

    if(lecture_label)
        gtk_widget_destroy(lecture_label);
}

/*
 * Function:  select_preview_stream_changed 
 * --------------------
 * Update preview ID and update preview mode base on stream selected.
 *
 *  window: Pointer to GTK preview_stream_selection element.
 */
void select_preview_stream_changed(GtkWidget* window){
    int id, i;
    gchar *out_name;
    char tmp_out_name[50];
    //get current id of combobox
    if(starting){
        id = gtk_combo_box_get_active(GTK_COMBO_BOX(window));
        if(id < 0)
            id = 0;

        //get current output name
        out_name = gtk_combo_box_get_active_text(GTK_COMBO_BOX(window));
        get_stream_name(out_name, tmp_out_name);

        
        //update preview id when preview
        if(strcmp(tmp_out_name, "View") == 0)
            //update preview mode
            update_preview_mode(ALL_IN_ONE);
        else {
            //get output id
            for(i = 0; i < numOut; i++){
                if(strcmp(tmp_out_name, outDevs[i]->name) == 0){
                    id = i;
                }

            }
            //upaddte preview mode and preview id
            update_preview_id(id);
            update_preview_mode(ONE_BY_ONE);
        }
        
    }
}

/*
 * Function:  close_window_button_click 
 * --------------------
 * Confirm user want to close program.
 * If user want, stop recording_streaming and close program.
 *
 *  window: Pointer to GTK main window.
 */
void close_window_button_click(GtkWidget *window){
    gint result;
    GtkWidget *dialog;
    dialog = create_quick_message("Do you want to quit program?", window, 0);

    result = gtk_dialog_run (GTK_DIALOG (dialog));
    gtk_widget_destroy (dialog);

    //switch result dialog
    switch (result){
        default:
        case GTK_RESPONSE_DELETE_EVENT:
        case GTK_RESPONSE_NO:

            break;
        case GTK_RESPONSE_YES:
            //stop recording & streaming
            if(starting)
                stop_button_click(stop_button);

            //close program
            gtk_main_quit();
            break;
    }
}

/*
 * Function:  init_data 
 * --------------------
 * Init all display element and data.
 */
void init_data(){
    int i;
    //init data
    for (i = 0; i < MAX_OUT; i++) {
        outDevs[i] = get_default_out();
        inDevs[i] = get_default_in();
        display_input[i] = (DisplayInput*) malloc(sizeof(DisplayInput));
        display_output[i] = (DisplayOutput*) malloc(sizeof(DisplayOutput));
        display_info[i] = (DisplayInfo*) malloc(sizeof(DisplayInfo));
        display_buffer[i] = (DisplayBuffer*)malloc(sizeof(DisplayBuffer));
    }
    v4l_list = (V4lDevices*) malloc(sizeof(V4lDevices));
    alsa_list = (AlsaDevices*) malloc(sizeof(AlsaDevices));
    display_course = (DisplayCourseManage*)malloc(sizeof(DisplayCourseManage));
    display_lecture = (DisplayLectureManage*)malloc(sizeof(DisplayLectureManage));
    display_process = (DisplayProcess*)malloc(sizeof(DisplayProcess));
    display_upload = (DisplayUploading*)malloc(sizeof(DisplayUploading));
    input_status = (DisplayInputStatus*)malloc(sizeof(DisplayInputStatus));
}

/*
 * Function:  set_default_value 
 * --------------------
 * Set default value for program.
 */
void set_default_value(){
    starting = NOT_RUNNING;
    session = 0;
    type = 0;

    course_index = 0;
    version_index = 0;
    lecture_index = 0;
}

/*
 * Function:  update_program_UI 
 * --------------------
 * Update UI when user change data from config file.
 */
void update_program_UI(){
    // void update_UI()
    gtk_notebook_remove_page(GTK_NOTEBOOK(notebook), 1);
    gtk_notebook_remove_page(GTK_NOTEBOOK(notebook), 1);
    gtk_notebook_remove_page(GTK_NOTEBOOK(notebook), 1);

    //create config Input devices tab
    tab = gtk_label_new("Input Devices");
    input_box = create_input_devices_tab(window);
    gtk_notebook_insert_page(GTK_NOTEBOOK(notebook), input_box, tab, 1);

    //create config Input devices tab
    tab = gtk_label_new("Output Streams");
    output_box = create_output_streams_tab(window);
    gtk_notebook_insert_page(GTK_NOTEBOOK(notebook), output_box, tab, 2);

    //create main tab
    tab = gtk_label_new("Lecture Management");
    info_box = create_lecture_management_tab(window);
    gtk_notebook_insert_page(GTK_NOTEBOOK(notebook), info_box, tab, 3);

    //show elements
    gtk_widget_show_all(input_box);
    gtk_widget_show_all(output_box);
    gtk_widget_show_all(info_box);
}

/*
 * Function:  start_button_click 
 * --------------------
 * Re-read data from config file.
 * Show all preview element.
 * Change status and start Recording & Streaming.
 *
 *  widget: Pointer to GTK start_button.
 */
void start_button_click(GtkWidget *widget){
    
    char tmp_file_name[30];
    int i;
    char tmp_path[LONG_STRING_LENGTH];
    //read data from config file
    configure(inDevs, &numIn, outDevs, &numOut);
    //change HDD url base on session change
    change_file_path();

    //show current stream
    show_current_stream();
    starting = RUNNING;

    //show all buffer progressbar
    create_buffer_progressbar();

    //show all audio input
    show_input_status();

    //get path to lecture
    sprintf(tmp_path, "%s/%s/%s/%s", home_path, current_course, 
    current_version, current_lecture);

    //save time
    char current_time[19];

    //get current time
    get_current_time(current_time);
    save_time(current_time, tmp_path);

    //create description
    for(i = 0; i < numOut; i++){   
        //check all output streams type HDD and create session file xml
        if(outDevs[i]->output_type == 0&& outDevs[i]->disable == 0){
            get_filename(outDevs[i]->url, tmp_file_name);
            new_session_video(tmp_home_path, session, tmp_file_name, outDevs[i]->format, current_path_prefix);
        }
    }
    //create HDD description file
    create_video_description_file(tmp_home_path, session, outDevs, numOut, current_path_prefix);

    //hide, show button
    gtk_widget_hide(start_button);
    gtk_widget_show(stop_button);

    //start stream

    if(!start_stream(inDevs, numIn, outDevs, numOut)){

        //hide, show button
        gtk_widget_hide(stop_button);
        gtk_widget_show(start_button);

        //disable all buffer progress bar
        hide_buffer_progressbar();

        //disable all input progress bar
        hide_input_status();

        
        //remove file && session video
        for(i = 0; i < numOut; i++){   
            //check all output streams type HDD and create session file xml
            if(outDevs[i]->output_type == HDD&& outDevs[i]->disable == 0){
                get_filename(outDevs[i]->url, tmp_file_name);
                
                remove_current_session_data(tmp_path, current_path_prefix, session, tmp_file_name, outDevs[i]->format);
            }
        }
    }
}

/*
 * Function:  stop_button_click 
 * --------------------
 * Hide all preview element.
 * Change status and stop Recording & Streaming.
 *
 *  widget: Pointer to GTK start_button.
 */
void stop_button_click(GtkWidget *widget){
    // count--;
    //update session
    session++;
    starting = NOT_RUNNING;
    //stop stream
    stop_stream();

    //disable all buffer progress bar
    hide_buffer_progressbar();
    hide_input_status();

    //hide, show button
    gtk_widget_show(start_button);
    gtk_widget_hide(stop_button);

    clear_combo_box_data(combo_box);
}

/*
 * Function:  update_program_UI 
 * --------------------
 * Main view function.
 * Init data for UI. Init data for program.
 * Read all config data. Read all course, version, lecture, description.
 * Call function create UI and display data to UI.
 * Call function listen event from program.
 */
int create_UI(){
    int i, j;
    char buf[128];
    
    GtkWidget *program_vbox, *program_hbox;
    char space[STRING_LENGTH];

    //init data
    init_data();

    //set default value
    set_default_value();

    //get input, output, server, course, version, lecture data from config file
    configure(inDevs, &numIn, outDevs, &numOut);
    read_course_data(home_path, data_path, current_course, 
            current_version, current_lecture, current_path_prefix,
            &network_refresh_time, &network_break_times, &network_down_times);

    read_server_config(display_upload->server, display_upload->user, display_upload->path);

    //change output type url base on session
    change_file_path();

    //read data location from config file
    read_course_file(&course_data_info, home_path); 

    //get video4linux devices, and alsa devices
    get_devices(v4l_list, alsa_list);

    //create main window
    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    gtk_window_set_default_size(GTK_WINDOW(window), 500, 480);
    // gtk_container_set_border_width (GTK_CONTAINER (window), 5);
    gtk_window_set_icon(GTK_WINDOW(window), create_pixbuf("resources/images/icon.png"));
    // gtk_window_set_resizable (GTK_WINDOW(window), FALSE);
    gtk_window_set_title(GTK_WINDOW(window), "Lecture Recording & Streaming");

    //create box
    program_vbox = gtk_vbox_new(FALSE, 5);
    gtk_container_add(GTK_CONTAINER(window), program_vbox);

    //create notebook
    notebook = gtk_notebook_new();
    gtk_notebook_set_tab_pos(GTK_NOTEBOOK(notebook), GTK_POS_TOP);
    gtk_box_pack_start(GTK_BOX (program_vbox), notebook, FALSE, FALSE, 0);

    //hbox for status
    program_hbox = gtk_hbox_new(FALSE, 0);
    gtk_box_pack_start (GTK_BOX (program_vbox), program_hbox, FALSE, FALSE, 0);

    //status bar
    status_bar = gtk_statusbar_new();      
    gtk_box_pack_start (GTK_BOX (program_hbox), status_bar, FALSE, FALSE, 0);
    gtk_widget_set_size_request (GTK_WIDGET (status_bar), 650, 25);
    context_id = gtk_statusbar_get_context_id(GTK_STATUSBAR(status_bar), "Statusbar index");
    
    //set first status bar text
    push_item(status_bar, GINT_TO_POINTER(context_id), "Lecture Recording & Streaming");

    //right status bar
    right_status_bar = gtk_statusbar_new();
    gtk_box_pack_end (GTK_BOX (program_hbox), right_status_bar, FALSE, FALSE, 0);
    gtk_widget_set_size_request (GTK_WIDGET (right_status_bar), 220, 25);
    right_context_id = gtk_statusbar_get_context_id(GTK_STATUSBAR(right_status_bar), "Right Statusbar index");

    //right status bar
    network_status_bar = gtk_statusbar_new();
    gtk_box_pack_end (GTK_BOX (program_hbox), network_status_bar, FALSE, FALSE, 0);
    gtk_widget_set_size_request (GTK_WIDGET (network_status_bar), 100, 25);
    network_context_id = gtk_statusbar_get_context_id(GTK_STATUSBAR(network_status_bar), "Network Statusbar index");
    push_item(network_status_bar, GINT_TO_POINTER(network_context_id), "0kB/s");
    
    //set first status bar text
    check_disk_space(NULL);
    g_timeout_add(60000, (GSourceFunc) check_disk_space, NULL);

    //create socket to put sdl window
    sock = gtk_socket_new(); 

    //create setting output tab
    tab = gtk_label_new("Course Management");
    location_box = create_course_management_tab(window);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), location_box, tab);

    //create config Input devices tab
    tab = gtk_label_new("Input Devices");

    input_box = create_input_devices_tab(window);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), input_box, tab);

    //create config Input devices tab
    tab = gtk_label_new("Output Streams");
    output_box = create_output_streams_tab(window);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), output_box, tab);

    //create main tab
    tab = gtk_label_new("Lecture Management");
    info_box = create_lecture_management_tab(window);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), info_box, tab);

    //create main tab
    tab = gtk_label_new("Recording & Streaming");
    main_box = create_recording_streaming_tab(window, sock);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), main_box, tab);

    //create processing tab
    tab = gtk_label_new("Processing");
    process_box = create_processing_tab(window);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), process_box, tab);

    //create uploading tab
    tab = gtk_label_new("Uploading");
    upload_box = create_uploading_tab(window);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), upload_box, tab);

    tab = gtk_label_new("Youtube");
    youtube_box = create_youtube_tab(window);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), youtube_box, tab);

    //show all elements, hide stop button
    gtk_widget_show_all(window);
    gtk_widget_hide(stop_button);

    //connect check function
    for (i = 0; i < numOut; i++){
        //check audio, video
        check_audio_video(display_output[i], outDevs[i]);

        //connect check audio, video function
        g_signal_connect(G_OBJECT(display_output[i]->audio_check), "clicked", G_CALLBACK(check_audio_click), (gpointer) i);
        g_signal_connect(G_OBJECT(display_output[i]->video_check), "clicked", G_CALLBACK(check_video_click), (gpointer) i);

        //connect check 
        g_signal_connect(G_OBJECT(display_info[i]->check), "clicked", G_CALLBACK(check_output_info), (gpointer) i);
    }

    //set current page
    gtk_notebook_set_current_page(GTK_NOTEBOOK(notebook), 4);

    //destroy window when exit
    g_signal_connect(G_OBJECT(window), "delete_event", G_CALLBACK (close_window_button_click), NULL);

    //onkey press function
    g_signal_connect (G_OBJECT (window), "key_press_event", G_CALLBACK (on_key_press), NULL);

    gtk_main();

    return 0;
}
