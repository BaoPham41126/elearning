/*
 * File: input_view.c
 *
 * Author: GVLab
 *
 * Created on: Aug 13, 2015
 */

#include "headers/view/input_view.h"

//in notebook
GtkWidget *in_notebook;

//Show input devices
GtkWidget *input_type;
GtkWidget *input_hbox;

/*
 * Function:  create_input_devices_tab 
 * --------------------
 * Create Input devices tab: Create UI and display input device's data.
 *
 *  window: Main window of program.
 * --------------------
 *  Return: Pointer to GTK_VBOX (Contain all input_devices's element).
 */
GtkWidget* create_input_devices_tab(GtkWidget* window){
    GtkWidget *new_window, *label, *hbox, *box, *table;
    GtkWidget *save_button, *new_button, *hbox1, *refresh_button;
    GtkWidget *hseparator, *frame, *tab, *tab_box, *input_hbox, *tab_container, *tab_close;

    char str[SHORT_STRING_LENGTH];  
    int i;

    //create box contain element
    box = gtk_vbox_new (FALSE, 5);

    //--------------------------Input-------------------------------
    //separator
    hseparator = gtk_hseparator_new();
    gtk_box_pack_start (GTK_BOX (box), hseparator, FALSE, FALSE, 5);

    hbox = gtk_hbox_new (FALSE, 5);
    gtk_box_pack_start (GTK_BOX (box), hbox, FALSE, FALSE, 5);
    
    //create note book
    in_notebook = gtk_notebook_new();
    gtk_notebook_set_tab_pos(GTK_NOTEBOOK(in_notebook), GTK_POS_LEFT);
    gtk_container_add(GTK_CONTAINER(hbox), in_notebook);
    //----------------------
    
    //show input information
    for(i = 0; i < numIn; i++){
        //create tab container
        tab_container = gtk_hbox_new (FALSE, 0);
        
        //create tab close
        tab_close = create_remove_button();
        gtk_box_pack_start (GTK_BOX (tab_container), tab_close, TRUE, TRUE, 0);
        g_signal_connect( G_OBJECT( tab_close ), "clicked", G_CALLBACK( close_input_tab_click), (gpointer) i);
        
        //create tab label
        sprintf(buf, "Input%d", i+1);
        tab = gtk_label_new(buf);
        gtk_box_pack_start (GTK_BOX (tab_container), tab, FALSE, FALSE, 5);
        
        //create tab_box
        tab_box = gtk_hbox_new (FALSE, 5);
        gtk_notebook_append_page(GTK_NOTEBOOK(in_notebook), tab_box, tab_container);
        gtk_widget_show_all(tab_container);

        //hbox1
        input_hbox = gtk_vbox_new(FALSE, 5);
        gtk_box_pack_start (GTK_BOX (tab_box), input_hbox, FALSE, FALSE, 5);

        //inittable
        display_input[i]->table = gtk_table_new(5, 2, FALSE);

        //create frame
        frame = gtk_frame_new("");
        gtk_box_pack_start (GTK_BOX (input_hbox), frame, FALSE, FALSE, 5);

        gtk_container_add(GTK_CONTAINER(frame), display_input[i]->table);

        //show input element
        show_input(display_input[i]->table, display_input[i], inDevs[i], 0);
    }

    //create new hbox
    input_hbox = gtk_vbox_new(FALSE, 5);
    gtk_box_pack_end (GTK_BOX (hbox), input_hbox, FALSE, FALSE, 5);


    //save button
    save_button = gtk_button_new_with_label("Save");
    gtk_widget_set_size_request(GTK_WIDGET(save_button), 100, 30);
    gtk_box_pack_end (GTK_BOX (input_hbox), save_button, FALSE, FALSE, 5);
    g_signal_connect((gpointer) save_button, "clicked", G_CALLBACK(save_input_button_click), NULL);

    //New button
    new_button = gtk_button_new_with_label("New input");
    gtk_widget_set_size_request(GTK_WIDGET(new_button), 100, 30);
    gtk_box_pack_end (GTK_BOX (input_hbox), new_button, FALSE, FALSE, 5);
    g_signal_connect((gpointer) new_button, "clicked", G_CALLBACK(new_input_button_click), window);

    //New button
    refresh_button = gtk_button_new_with_label("Refresh input");
    gtk_widget_set_size_request(GTK_WIDGET(refresh_button), 100, 30);
    gtk_box_pack_end (GTK_BOX (input_hbox), refresh_button, FALSE, FALSE, 5);
    g_signal_connect((gpointer) refresh_button, "clicked", G_CALLBACK(refresh_input_button_click), NULL);
    
    return box;
}

/*
 * Function:  close_input_tab_click 
 * --------------------
 * Confirm user want to remove current input.
 * If user want to remove, call remove function to remove this input.
 *
 *  button: Pointer to GTK close_input_tab button.
 *
 *  page_index: The index of current input's tab.
 */
void close_input_tab_click( GtkButton *button, int page_index){ 
    //init
    GtkWidget *new_window, *box, *table, *save_button, *label;
    
    //create new window
    new_window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_default_size(GTK_WINDOW(new_window), 100, 50);
    gtk_window_set_position(GTK_WINDOW(new_window), GTK_WIN_POS_CENTER);
    gtk_window_set_title(GTK_WINDOW(new_window), "Remove input");

    //create box container
    box = gtk_vbox_new (FALSE, 0);
    gtk_container_add(GTK_CONTAINER(new_window), box); 
    gtk_widget_show(box);

    //create table container
    table = gtk_table_new(3, 3, FALSE);
    gtk_box_pack_start (GTK_BOX (box), table, FALSE, FALSE, 5);
    gtk_widget_show(table);

    label = gtk_label_new("Remove this input?");
    gtk_table_attach(GTK_TABLE(table), label, 0, 1, 0, 1, 
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    //save button
    save_button = create_apply_button();
    gtk_table_attach(GTK_TABLE(table), save_button, 1, 2, 0, 1, 
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    //create data pass to close
    ClosePage *close_data = (ClosePage*) malloc(sizeof(ClosePage));
    close_data->window = new_window;
    close_data->page_index = page_index;
    g_signal_connect((gpointer) save_button, "clicked", G_CALLBACK(ok_remove_input), close_data);

    //show all window
    gtk_widget_show_all(new_window);
}

/*
 * Function:  ok_remove_input 
 * --------------------
 * Remove given input and hide input information from UI (Hide from UI and change state).
 *
 *  widget: Pointer to GTK close_input_tab button.
 *
 *  close_data: Contain the index of current input's tab and pointer to main window.
 */
void ok_remove_input(GtkWidget *widget, ClosePage *close_data){
    GtkWidget * page;
    inDevs[close_data->page_index]->state = STATE_DELETED;
    //close new window
    page = gtk_notebook_get_nth_page((GtkNotebook*) in_notebook, close_data->page_index);
    gtk_widget_hide(page);

    gtk_widget_destroy(GTK_WIDGET(close_data->window));
}

/*
 * Function:  refresh_input_button_click 
 * --------------------
 * Get name of all devices in system and display to UI (input's name) base on input's type.
 *
 *  button: Pointer to GTK refresh_input button.
 */
void refresh_input_button_click(GtkWidget *button){
    int i, j;
    //get devices
    get_devices(v4l_list, alsa_list);
    
    //clear input and show new input
    for(j = 0; j < numIn; j++){

        // set input name
        switch(inDevs[j]->input_type){
            case MONITOR:
                break;
            case VIDEO:
                //clear
                clear_combo_box_data(display_input[j]->name);

                //add value
                for(i = 0; i < v4l_list->num_device; i++){
                    gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT( display_input[j]->name ), 
                        v4l_list->display_name[i]);
                }

                //set new value
                gtk_combo_box_set_active(GTK_COMBO_BOX(display_input[j]->name),0);
                for (i = 0; i < v4l_list->num_device; i++){
                    if(strcmp(inDevs[j]->name_device, v4l_list->name[i]) == 0)
                        gtk_combo_box_set_active(GTK_COMBO_BOX(display_input[j]->name),i);
                }
                break;

            case AUDIO:
                // clear
                clear_combo_box_data(display_input[j]->name);

                //add value
                for(i = 0; i < alsa_list->num_device; i++){
                    gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(display_input[j]->name), 
                        alsa_list->display_name[i]);
                }

                //add new value
                gtk_combo_box_set_active(GTK_COMBO_BOX(display_input[j]->name),0);
                for (i = 0; i < alsa_list->num_device; i++){
                    if(strcmp(inDevs[j]->name_device, alsa_list->name[i]) == 0)
                        gtk_combo_box_set_active(GTK_COMBO_BOX(display_input[j]->name),i);
                }
                break;

            default:
                break;
        }
    }
}

/*
 * Function:  ok_new_input_button_click 
 * --------------------
 * Create new input and show this input to UI.
 *
 *  widget: Pointer to GTK add_input button.
 *
 *  window: Pointer to GTK main window.
 */
void ok_new_input_button_click(GtkWidget *widget, gpointer window){
    int type = 0;
    GtkWidget *frame, *tab_box, *tab, *tab_container, *tab_close;
    //get new input type
    type = gtk_combo_box_get_active(GTK_COMBO_BOX(input_type));
        
    //init new InputInfo
    inDevs[numIn] = get_default_in();

    //set new input info value
    inDevs[numIn]->input_type = type;
    switch(type){
        case MONITOR:
            strcpy(inDevs[numIn]->driver, "x11grab");
            get_screen_size(&inDevs[numIn]->width, &inDevs[numIn]->height);
            break;
        case VIDEO:
            strcpy(inDevs[numIn]->driver, "video4linux");
            //set default size
            break;

        case AUDIO:
            strcpy(inDevs[numIn]->driver, "alsa");
            break;

        default:
            break;
    }  

    //create tab container
    tab_container = gtk_hbox_new (FALSE, 0);
    
    //create tab close
    tab_close = create_remove_button();
    gtk_box_pack_start (GTK_BOX (tab_container), tab_close, TRUE, TRUE, 0);
    g_signal_connect( G_OBJECT( tab_close ), "clicked", G_CALLBACK( close_input_tab_click), (gpointer) numIn);
    
    //create tab label
    sprintf(buf, "Input%d", numIn+1);
    tab = gtk_label_new(buf);
    gtk_box_pack_start (GTK_BOX (tab_container), tab, FALSE, FALSE, 5);

    //create new tab_box
    tab_box = gtk_hbox_new (FALSE, 5);
    gtk_notebook_append_page(GTK_NOTEBOOK(in_notebook), tab_box, tab_container);
    gtk_widget_show_all(tab_container);
    //hbox
    input_hbox = gtk_vbox_new(FALSE, 5);
    gtk_box_pack_start (GTK_BOX (tab_box), input_hbox, FALSE, FALSE, 5);

    //show new_input
    //inittable
    display_input[numIn]->table = gtk_table_new(5, 2, FALSE);

    //create frame
    frame = gtk_frame_new(buf);
    gtk_box_pack_start (GTK_BOX (input_hbox), frame, FALSE, FALSE, 5);
    gtk_container_add(GTK_CONTAINER(frame), display_input[numIn]->table);

    //show input element
    show_input(display_input[numIn]->table, display_input[numIn], inDevs[numIn], 1);

    numIn++;
    gtk_widget_show_all(tab_box);

    //close new window
    gtk_widget_destroy(GTK_WIDGET(window));
}

/*
 * Function:  new_input_button_click 
 * --------------------
 * Confirm user want to create new input.
 * If user want to create, call create function to create this input.
 *
 *  widget: Pointer to GTK add_input button.
 *
 *  window: Pointer to GTK main window.
 */
void new_input_button_click(GtkWidget *widget, gpointer window){
    //init
    GtkWidget *new_window, *box, *table, *save_button, *label;
    
    //create new window
    new_window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_default_size(GTK_WINDOW(new_window), 200, 50);
    gtk_window_set_position(GTK_WINDOW(new_window), GTK_WIN_POS_CENTER);
    gtk_window_set_title(GTK_WINDOW(new_window), "Create new input");

    //create box container
    box = gtk_vbox_new (FALSE, 0);
    gtk_container_add(GTK_CONTAINER(new_window), box); 
    gtk_widget_show(box);

    //create table container
    table = gtk_table_new(3, 3, FALSE);
    gtk_box_pack_start (GTK_BOX (box), table, FALSE, FALSE, 5);
    gtk_widget_show(table);

    label = gtk_label_new("Choose input type you want to create:");
    gtk_table_attach(GTK_TABLE(table), label, 0, 2, 0, 1, 
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    input_type = show_input_type();
    gtk_table_attach(GTK_TABLE(table), input_type, 0, 1, 1, 2, 
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    gtk_combo_box_set_active(GTK_COMBO_BOX(input_type), 0);

    //save button
    save_button = create_apply_button();
    gtk_table_attach(GTK_TABLE(table), save_button, 1, 2, 1, 2, 
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    g_signal_connect((gpointer) save_button, "clicked", G_CALLBACK(ok_new_input_button_click), new_window);

    //show all window
    gtk_widget_show_all(new_window);
}

/*
 * Function:  show_input 
 * --------------------
 * Create GTK element and show input information base on InputInfo.
 *
 *  table: Pointer to GTK display_input.
 *
 *  display_input: Input's GTK element.
 *
 *  input: Input information.
 *
 *  new_input: Input is new or old.
 */
void show_input(GtkWidget *table, DisplayInput* display_input, InputInfo *input, int new_input){
    int i;
    int type = -1;
    
    //set type
    // if(!new_input)
    type = input->input_type;

    //display input name
    display_input->label = gtk_label_new("Name");
    gtk_table_attach(GTK_TABLE(table), display_input->label, 0, 1, 0, 1, 
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    display_input->name = show_input_name(input->input_type);
    gtk_table_attach(GTK_TABLE(table), display_input->name, 1, 2, 0, 1, 
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    //set input name
    switch(type){
        case MONITOR:
            gtk_combo_box_set_active(GTK_COMBO_BOX(display_input->name),0);
            break;
        case VIDEO:
            gtk_combo_box_set_active(GTK_COMBO_BOX(display_input->name),0);
            for (i = 0; i < v4l_list->num_device; i++){
                if(strcmp(input->name_device, v4l_list->name[i]) == 0)
                    gtk_combo_box_set_active(GTK_COMBO_BOX(display_input->name),i);
            }
            break;

        case AUDIO:
            gtk_combo_box_set_active(GTK_COMBO_BOX(display_input->name),0);
            for (i = 0; i < alsa_list->num_device; i++){
                if(strcmp(input->name_device, alsa_list->name[i]) == 0)
                    gtk_combo_box_set_active(GTK_COMBO_BOX(display_input->name),i);
            }
            break;

        default:
            break;
    }
    
    //display input name
    display_input->label = gtk_label_new("Input type");
    gtk_table_attach(GTK_TABLE(table), display_input->label, 0, 1, 1, 2,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    display_input->input_type = show_input_type();
    
    gtk_combo_box_set_active(GTK_COMBO_BOX(display_input->input_type),input->input_type);
    gtk_table_attach(GTK_TABLE(table), display_input->input_type, 1, 2, 1, 2,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    //set sensitive is false for input_type
    gtk_widget_set_sensitive(display_input->input_type, FALSE);

    //display for video
    if(type == 0 || type == 1){
        //display video width
        display_input->label = gtk_label_new("Width");
        gtk_table_attach(GTK_TABLE(table), display_input->label, 0, 1, 2, 3, 
          GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
         sprintf(buf, "%d", input->width);
        display_input->width = gtk_entry_new();
        gtk_entry_set_text (GTK_ENTRY(display_input->width), buf);
        gtk_table_attach(GTK_TABLE(table), display_input->width, 1, 2, 2, 3, 
          GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

        //display video height
        display_input->label = gtk_label_new("Height");
        gtk_table_attach(GTK_TABLE(table), display_input->label, 0, 1, 3, 4, 
          GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
        sprintf(buf, "%d", input->height);
        display_input->height = gtk_entry_new();
        
        gtk_entry_set_text(GTK_ENTRY(display_input->height), buf);
        gtk_table_attach(GTK_TABLE(table), display_input->height, 1, 2, 3, 4, 
          GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

        //display video frame rate
        display_input->label = gtk_label_new("Frame rate");
        gtk_table_attach(GTK_TABLE(table), display_input->label, 0, 1, 4, 5, 
          GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
        sprintf(buf, "%d", input->frame_rate);
        display_input->frame_rate = gtk_entry_new();
        gtk_entry_set_text(GTK_ENTRY(display_input->frame_rate), buf);
        gtk_table_attach(GTK_TABLE(table), display_input->frame_rate, 1, 2, 4, 5, 
          GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    } else {
        //display video frame rate
        display_input->label = gtk_label_new("Sample rate");
        gtk_table_attach(GTK_TABLE(table), display_input->label, 0, 1, 4, 5, 
          GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
        sprintf(buf, "%d", input->sample_rate);
        display_input->sample_rate = gtk_entry_new();
        gtk_entry_set_text(GTK_ENTRY(display_input->sample_rate), buf);
        gtk_table_attach(GTK_TABLE(table), display_input->sample_rate, 1, 2, 4, 5, 
          GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    }

    //test button
    display_input->test_button = gtk_button_new_with_label("Preview");
    gtk_table_attach(GTK_TABLE(table), display_input->test_button, 0, 1, 5, 6, 
          GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    g_signal_connect((gpointer) display_input->test_button, "clicked", G_CALLBACK(preview_input_button_click), display_input);
}

/*
 * Function:  preview_input_device 
 * --------------------
 * Preview thread.
 * Call ffmpeg function to display input.
 *
 *  data: System command.
 */
void *preview_input_device(void *data){
    //get data
    char *call = (char *)data;

    //display
    system((char*) data);
}

/*
 * Function:  preview_input_button_click 
 * --------------------
 * Get input data from UI.
 * Create system command to display input.
 *
 *  button: GTK preview_input button.
 *
 *  input: GTK input's element.
 */
void preview_input_button_click(GtkWidget *button, gpointer input){
    DisplayInput *display_input = (DisplayInput*) input;
    char *call;
    call = (char *)malloc(sizeof(char) * SHORT_STRING_LENGTH);
    char name[20];
    char driver[20];
    int width = 0, height = 0, frame_rate = 0;
    int type = 0;
    type = gtk_combo_box_get_active(GTK_COMBO_BOX(display_input->input_type));
    pthread_t test_thread;

    gchar *tmp_string;
    int tmp_int;

    //read input's information
    switch(type){
        case MONITOR:

            //read width
            width = atoi(gtk_entry_get_text(GTK_ENTRY(display_input->width)));

            //read height
            height = atoi(gtk_entry_get_text(GTK_ENTRY(display_input->height)));

            //read frame_rate
            frame_rate = atoi(gtk_entry_get_text(GTK_ENTRY(display_input->frame_rate)));

            sprintf(call, "ffmpeg -f x11grab -r %d -s %dx%d -i :0.0 -vcodec rawvideo -pix_fmt yuv420p -filter:v scale=640:-1 -window_size 480x360 -f sdl \"Screen\"", 
                frame_rate, width, height);

            pthread_create(&test_thread, NULL, preview_input_device, call);
            break;
        case VIDEO:

            tmp_int = gtk_combo_box_get_active(GTK_COMBO_BOX(display_input->name));
            if(tmp_int >= 0){            
                //read width
                width = atoi(gtk_entry_get_text(GTK_ENTRY(display_input->width)));

                //read height
                height = atoi(gtk_entry_get_text(GTK_ENTRY(display_input->height)));

                //read frame_rate
                frame_rate = atoi(gtk_entry_get_text(GTK_ENTRY(display_input->frame_rate)));

                //display
                sprintf(call, "ffmpeg -f v4l2 -r %d -s %dx%d -i %s -vcodec rawvideo -pix_fmt yuv420p -filter:v scale=480:-1 -window_size 480x360 -f sdl \"%s\"", 
                    frame_rate, width, height, v4l_list->name[tmp_int], v4l_list->display_name[tmp_int]);
                pthread_create(&test_thread, NULL, preview_input_device, call);
            }

            break;
        case AUDIO:
            //read name of input
            tmp_int = gtk_combo_box_get_active(GTK_COMBO_BOX(display_input->name));
            if(tmp_int >= 0){
                //display
                sprintf(call, "ffmpeg -f alsa -t 10 -y -i %s ../test.mp3", alsa_list->name[tmp_int]);

                pthread_create(&test_thread, NULL, preview_input_device, call);
            }
            break;

        default:
            break;
    }
}

/*
 * Function:  show_input_name 
 * --------------------
 * Show input's name base on input's type.
 *
 *  type: Input's type.
 * --------------------
 *  Return: GTK input's name select_box.
 */
GtkWidget *show_input_name(int type){
    GtkWidget *input_name = gtk_combo_box_text_new();
    int i;
    switch(type){
        case MONITOR:
            //add monitor
            gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT( input_name ), "Screen");
            break;
        case VIDEO:
            //add video4linux devices
            for(i = 0; i < v4l_list->num_device; i++){
                gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT( input_name ), v4l_list->display_name[i]);
            }
            break;
        case AUDIO:
            //add audio devices
            for(i = 0; i < alsa_list->num_device; i++){
                gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT( input_name ), 
                    alsa_list->display_name[i]);
            }
            // gtk_combo_box_set_active(GTK_COMBO_BOX(input_name),0);
            break;
        default:
            break;
    }
    return input_name;
}

/*
 * Function:  show_input_status 
 * --------------------
 * Show input status and buffer progressbar when start recording & streaming.
 */
void show_input_status(){
    int i;
    char tmp_name[20];

    //input
    for(i = 0; i < numIn; i++){
        //audio
        if(inDevs[i]->input_type == AUDIO){
            //create lable
            input_status->a_label[i] = gtk_label_new(inDevs[i]->name_device);
            gtk_table_attach(GTK_TABLE(input_status->a_table), input_status->a_label[i], 0, 1, i, i+1, 
                  GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 3, 3);
            gtk_widget_show(input_status->a_label[i]);

            //show progress bar
            input_status->audio[i] = gtk_progress_bar_new();
            gtk_widget_set_size_request (GTK_WIDGET (input_status->audio[i]), 100, 10);
            gtk_table_attach(GTK_TABLE(input_status->a_table), input_status->audio[i], 1, 2, i, i+1, 
                  GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 3, 3);
            gtk_widget_show(input_status->audio[i]);
        } else if(inDevs[i]->input_type == VIDEO){
            //show input video
            get_dev_name(inDevs[i]->name_device, tmp_name);
            input_status->v_in_label[i] = gtk_label_new(tmp_name);
            gtk_table_attach(GTK_TABLE(input_status->v_table), input_status->v_in_label[i], 1+i, 2+i, 0, 1, 
                  GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 3, 3);
            gtk_widget_show(input_status->v_in_label[i]);
        } else {
            //screen
            input_status->v_in_label[i] = gtk_label_new("screen");
            gtk_table_attach(GTK_TABLE(input_status->v_table), input_status->v_in_label[i], 1+i, 2+i, 0, 1, 
                  GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 3, 3);
            gtk_widget_show(input_status->v_in_label[i]);
        }
    }

    //output
    for(i = 0; i < numOut; i++){
        if(!outDevs[i]->disable && outDevs[i]->state == STATE_NORMAL){
            input_status->v_out_label[i] = gtk_label_new(outDevs[i]->name);
            gtk_table_attach(GTK_TABLE(input_status->v_table), input_status->v_out_label[i], 0, 1, i+1, i+2, 
                  GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 3, 3);
            gtk_widget_show(input_status->v_out_label[i]);
        }
    }
}

/*
 * Function:  read_UI_input 
 * --------------------
 * Read all input's data from UI.
 *
 *  input: Input's information.
 *
 *  display_input: GTK input's element.
 */
void read_UI_input(DisplayInput* display_input, InputInfo *input){ 
    int tmp_int;

    //save input's information
    switch(input->input_type){
        case MONITOR:
        case VIDEO:
            //save name of input
            if(input->input_type){
                tmp_int = gtk_combo_box_get_active(GTK_COMBO_BOX(display_input->name));
                strcpy(input->name_device, v4l_list->name[tmp_int]);
            } else {
                strcpy(input->name_device, ":0.0");
            }
            //save width
            input->width = atoi(gtk_entry_get_text(GTK_ENTRY(display_input->width)));
            if(input->width <= 0){
                sprintf(tmp_status, "Error: Input %s's width is invalid", input->name_device);
                push_item(status_bar, GINT_TO_POINTER(context_id), tmp_status);
                ui_error = INPUT_ERROR;
            }

            //save height
            input->height = atoi(gtk_entry_get_text(GTK_ENTRY(display_input->height)));
            if(input->height <= 0){
                sprintf(tmp_status, "Error: Input %s's height is invalid", input->name_device);
                push_item(status_bar, GINT_TO_POINTER(context_id), tmp_status);
                ui_error = INPUT_ERROR;
            }

            //save framrate
            input->frame_rate = atoi(gtk_entry_get_text(GTK_ENTRY(display_input->frame_rate)));
            if(input->frame_rate <= 0){
                sprintf(tmp_status, "Error: Input %s's framerate is invalid", input->name_device);
                push_item(status_bar, GINT_TO_POINTER(context_id), tmp_status);
                ui_error = INPUT_ERROR;
            }
            break;
        case AUDIO:
            //save name of input
            tmp_int = gtk_combo_box_get_active(GTK_COMBO_BOX(display_input->name));
            strcpy(input->name_device, alsa_list->name[tmp_int]);

            //save sample rate
            input->sample_rate = atoi(gtk_entry_get_text(GTK_ENTRY(display_input->sample_rate)));
            if(input->sample_rate <= 0){
                sprintf(tmp_status, "Error: Input %s's samplerate is invalid", input->name_device);
                push_item(status_bar, GINT_TO_POINTER(context_id), tmp_status);
                ui_error = INPUT_ERROR;
            }
            break;

        default:
            break;
    }
}

/*
 * Function:  save_input_button_click 
 * --------------------
 * Get input data from UI.
 * Check if user enter data valid.
 * Save input data to config file.
 *
 *  widget: GTK save_input button.
 */
void save_input_button_click(GtkWidget *widget){
    int i, j;  
    //read config input devices from UI
    ui_error = VALID;
    for(i = 0; i < numIn; i++){
        if(inDevs[i]->state == STATE_NORMAL){
            read_UI_input(display_input[i], inDevs[i]);
        }
    }
    if(ui_error == VALID)
        clear_status_bar(status_bar, GINT_TO_POINTER(context_id));

    //save input to file config
    save_input_to_config_file(inDevs, numIn);

    //update output
    for (i = 0; i < numOut; i++){
        //update output audio
        if(outDevs[i]->state == STATE_NORMAL){

            //--------update audio--
            //destroy
            if(display_output[i]->audio_name)
                gtk_widget_destroy(display_output[i]->audio_name);
            
            //update
            display_output[i]->audio_name = show_input_audio_name(outDevs[i]->in_audio);
            gtk_widget_set_size_request(GTK_WIDGET(display_output[i]->audio_name), 10, 30);

            //add to table
            gtk_table_attach(GTK_TABLE(display_output[i]->audio_table), display_output[i]->audio_name, 1, 2, 0, 1, 
                GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

            //show
            gtk_widget_show(display_output[i]->audio_name);

            //--------update video-----
            for(j = 0; j < outDevs[i]->num_video; j++){
                if(outDevs[i]->in_video_state[j] == 0){
                    //destroy
                    if(display_output[i]->video_name[j])
                        gtk_widget_destroy(display_output[i]->video_name[j]);
                    
                    //update
                    display_output[i]->video_name[j] = show_input_video_name(outDevs[i]->in_video[j]);
                    gtk_widget_set_size_request(GTK_WIDGET(display_output[i]->video_name[j]), 10, 30);

                    //add to table
                    gtk_table_attach(GTK_TABLE(display_output[i]->video_table), display_output[i]->video_name[j], 1, 2, j+11, j+12, 
                        GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
                    //show
                    gtk_widget_show(display_output[i]->video_name[j]);
                }
            }

        }
    }
}
