/*
 * File: output_view.c
 *
 * Author: GVLab
 *
 * Created on: Aug 13, 2015
 */

#include "headers/view/output_view.h"

//Show output streams
GtkWidget *out_notebook;

/*
 * Function:  check_audio_video 
 * --------------------
 * Check video stream, audio stream of output.
 * Hide/Show video's information, audio's information base on video_stream, audio_stream state.
 *
 *  output: Output's information.
 *
 *  display_output: GTK output's element.
 */
void check_audio_video(DisplayOutput* display_output, OutputInfo *output){
    // check audio
    if(output->audio_stream){
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(display_output->audio_check), TRUE);
        gtk_widget_show(display_output->audio_table);
    } else {
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(display_output->audio_check), FALSE);
        gtk_widget_hide(display_output->audio_table);
    }

    //check video
    if(output->video_stream){
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(display_output->video_check), TRUE);
        gtk_widget_show(display_output->video_table);
    } else {
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(display_output->video_check), FALSE);
        gtk_widget_hide(display_output->video_table);
    }
}

/*
 * Function:  check_audio_click 
 * --------------------
 * Change audio_stream state between USE_STREAM and NOT_USE_STREAM.
 *
 *  check: GTK output's check_audio element.
 *
 *  i: Id of output.
 */
void check_audio_click(GtkWidget *check, int i){
    //update audio stream value
    (outDevs[i]->audio_stream == USE_STREAM) ? (outDevs[i]->audio_stream = NOT_USE_STREAM) : (outDevs[i]->audio_stream = USE_STREAM);

    //update UI
    check_audio_video(display_output[i], outDevs[i]);
}

/*
 * Function:  check_video_click 
 * --------------------
 * Change video_stream state between USE_STREAM and NOT_USE_STREAM.
 *
 *  check: GTK output's check_video element.
 *
 *  i: Id of output.
 */
void check_video_click(GtkWidget *check, int i){

    //update audio stream value
    (outDevs[i]->video_stream == USE_STREAM) ? (outDevs[i]->video_stream = NOT_USE_STREAM) : (outDevs[i]->video_stream = USE_STREAM);

    //update UI
    check_audio_video(display_output[i], outDevs[i]);
}

/*
 * Function:  on_output_type_change 
 * --------------------
 * Hide/Show output's element base on output type.
 * Output's type is HDD: Show file name and file extension.
 * Output's type is LIVE: Show Streaming Url.
 *
 *  select: GTK output's type element.
 *
 *  display_output: GTK output element.
 */
void on_output_type_change(GtkWidget *select, DisplayOutput* display_output){
    if(gtk_combo_box_get_active(GTK_COMBO_BOX(select)) == 0){
        if(display_output->out_url)
            gtk_widget_destroy(display_output->out_url);
        if(display_output->out_url_lbl)
            gtk_widget_destroy(display_output->out_url_lbl);

        //file name
        display_output->file_lbl = gtk_label_new("File");
        gtk_table_attach(GTK_TABLE(display_output->table), display_output->file_lbl, 0, 1, 3, 4, 
          GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
        display_output->file = gtk_entry_new();
        gtk_entry_set_text (GTK_ENTRY(display_output->file), "file");
        gtk_table_attach(GTK_TABLE(display_output->table), display_output->file, 1, 2, 3, 4, 
          GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

        //display output file format
        display_output->format_lbl = gtk_label_new("File Extension");
        gtk_table_attach(GTK_TABLE(display_output->table), display_output->format_lbl, 0, 1, 4, 5,
          GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
        display_output->format = show_format_type();
        gtk_table_attach(GTK_TABLE(display_output->table), display_output->format, 1, 2, 4, 5,
          GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
        gtk_combo_box_set_active(GTK_COMBO_BOX(display_output->format), 0);
    
    } else {
        //destroy file name
        if(display_output->file)
            gtk_widget_destroy(display_output->file);
        if(display_output->format)
            gtk_widget_destroy(display_output->format);
        if(display_output->file_lbl)
            gtk_widget_destroy(display_output->file_lbl);
        if(display_output->format_lbl)
            gtk_widget_destroy(display_output->format_lbl);

        //live url
        display_output->out_url_lbl = gtk_label_new("Live url");
        gtk_table_attach(GTK_TABLE(display_output->table), display_output->out_url_lbl, 0, 1, 2, 3, 
          GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
        display_output->out_url = gtk_entry_new();
        gtk_entry_set_text (GTK_ENTRY(display_output->out_url), "rtmp://serverIp/AppId/streamId");
        gtk_table_attach(GTK_TABLE(display_output->table), display_output->out_url, 1, 2, 2, 3, 
          GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    }
    gtk_widget_show_all(display_output->table);
    int i;
}

/*
 * Function:  show_output_general 
 * --------------------
 * Create GTK element and show output general's information base on OutputInfo.
 *
 *  table: Pointer to GTK display_output's general_table.
 *
 *  display_output: Output's GTK element.
 *
 *  output: Output information.
 *
 *  new_output: Output is new or old.
 */
void show_output_general(GtkWidget *table, DisplayOutput* display_output, OutputInfo *output, int new_output){
    int i;
    int type = output->output_type;
    char tmp_char = '/';
    char tmp_file[30];

    //display output name
    display_output->label = gtk_label_new("Name");
    gtk_table_attach(GTK_TABLE(table), display_output->label, 0, 1, 0, 1, 
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    display_output->name = gtk_entry_new();
    if(!new_output)
        gtk_entry_set_text (GTK_ENTRY(display_output->name), output->name);
    gtk_table_attach(GTK_TABLE(table), display_output->name, 1, 2, 0, 1, 
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    gtk_widget_set_size_request(GTK_WIDGET(display_output->name), 200, 30);

    //display output type
    display_output->label = gtk_label_new("Output type");
    gtk_table_attach(GTK_TABLE(table), display_output->label, 0, 1, 1, 2, 
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    display_output->out_type = show_output_type();
    gtk_table_attach(GTK_TABLE(table), display_output->out_type, 1, 2, 1, 2, 
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    gtk_combo_box_set_active(GTK_COMBO_BOX(display_output->out_type), type);
    g_signal_connect( G_OBJECT( display_output->out_type ), "changed",
        G_CALLBACK( on_output_type_change ), display_output );

    //display url/location, file name
    if(type == 0){  
        //get location, file name
        if(!new_output)
            get_filename(output->url, tmp_file);
        else 
            strcpy(tmp_file, "");

        //file name
        display_output->file_lbl = gtk_label_new("File");
        gtk_table_attach(GTK_TABLE(table), display_output->file_lbl, 0, 1, 3, 4, 
          GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
        display_output->file = gtk_entry_new();
        gtk_entry_set_text (GTK_ENTRY(display_output->file), tmp_file);
        gtk_table_attach(GTK_TABLE(table), display_output->file, 1, 2, 3, 4, 
          GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

        //display output file format
        display_output->format_lbl = gtk_label_new("File Extension");
        gtk_table_attach(GTK_TABLE(table), display_output->format_lbl, 0, 1, 4, 5,
          GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
        display_output->format = show_format_type();
        gtk_table_attach(GTK_TABLE(table), display_output->format, 1, 2, 4, 5,
          GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
        set_format_combo_box(display_output->format, output->format);
    } else {
        //live url
        display_output->out_url_lbl = gtk_label_new("Live url");
        gtk_table_attach(GTK_TABLE(table), display_output->out_url_lbl, 0, 1, 2, 3, 
          GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
        display_output->out_url = gtk_entry_new();
        gtk_entry_set_text (GTK_ENTRY(display_output->out_url), output->url);
        gtk_table_attach(GTK_TABLE(table), display_output->out_url, 1, 2, 2, 3, 
          GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    }
}

/*
 * Function:  remove_in_video 
 * --------------------
 * Remove in_video from output stream (Hide from UI and change state).
 *
 *  button: GTK output's remove_in_video button.
 *
 *  index: Index of in_video in output stream.
 */
void remove_in_video(GtkWidget *button, int index){
    //init
    gint out;
    out = gtk_notebook_get_current_page(GTK_NOTEBOOK(out_notebook));
    
    //set state
    outDevs[out]->in_video_state[index] = 1;

    //hide
    gtk_widget_hide(display_output[out]->video_name[index]);
    gtk_widget_hide(display_output[out]->video_type[index]);
    gtk_widget_hide(display_output[out]->video_key[index]);
    gtk_widget_hide(display_output[out]->ctrl_label[index]);
    gtk_widget_hide(display_output[out]->remove[index]);
}

/*
 * Function:  add_in_video 
 * --------------------
 * Add in_video to output stream (Create and show to UI).
 *
 *  button: GTK output's add_in_video button.
 */
void add_in_video(GtkWidget *button){
    //init
    gint out;
    int i;
    out = gtk_notebook_get_current_page(GTK_NOTEBOOK(out_notebook));
    //get num video
    i = outDevs[out]->num_video;
    outDevs[out]->in_video_state[i] = 0;
    //show name
    display_output[out]->video_name[i] = show_input_video_name(outDevs[out]->in_video[i]);
    gtk_widget_set_size_request(GTK_WIDGET(display_output[out]->video_name[i]), 10, 30);
    gtk_table_attach(GTK_TABLE(display_output[out]->video_table), display_output[out]->video_name[i], 1, 2, i+11, i+12,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    //show type
    display_output[out]->video_type[i] = show_output_video_type();
    gtk_combo_box_set_active(GTK_COMBO_BOX(display_output[out]->video_type[i]),outDevs[out]->in_video_type[i]);
    gtk_table_attach(GTK_TABLE(display_output[out]->video_table), display_output[out]->video_type[i], 0, 1, i+11, i+12,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    //label
    //key
    display_output[out]->ctrl_label[i] = gtk_label_new("Ctrl +");
    gtk_table_attach(GTK_TABLE(display_output[out]->video_table), display_output[out]->ctrl_label[i], 2, 3, i+11, i+12,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    //key
    display_output[out]->video_key[i] = gtk_entry_new();
    gtk_entry_set_max_length((GTK_ENTRY(display_output[out]->video_key[i])), 1);
    gtk_table_attach(GTK_TABLE(display_output[out]->video_table), display_output[out]->video_key[i], 3, 4, i+11, i+12,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    gtk_widget_set_size_request(GTK_WIDGET(display_output[out]->video_key[i]), 20, 30);

    //remove button
    display_output[out]->remove[i] = create_delete_button();
    gtk_table_attach(GTK_TABLE(display_output[out]->video_table), display_output[out]->remove[i], 4, 5, i+11, i+12,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    
    //connect function remove'
    g_signal_connect(G_OBJECT(display_output[out]->remove[i]), "clicked", G_CALLBACK(remove_in_video), (gpointer) i);

    //show all
    gtk_widget_show(display_output[out]->video_name[i]);
    gtk_widget_show(display_output[out]->video_type[i]);
    gtk_widget_show(display_output[out]->video_key[i]);
    gtk_widget_show(display_output[out]->ctrl_label[i]);
    gtk_widget_show_all(display_output[out]->remove[i]);
    //update numvideo
    outDevs[out]->num_video++;
}

/*
 * Function:  show_output_video 
 * --------------------
 * Create GTK element and show output video's information base on OutputInfo.
 *
 *  table: Pointer to GTK display_output's video_table.
 *
 *  display_output: Output's GTK element.
 *
 *  output: Output information.
 *
 *  new_output: Output is new or old.
 */
void show_output_video(GtkWidget *table, DisplayOutput* display_output, 
    OutputInfo *output, int new_output)
{
    int i;
    //display output video name, type  and key
    display_output->label = gtk_label_new("Input name");
    gtk_table_attach(GTK_TABLE(table), display_output->label, 1, 2, 10, 11,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    display_output->label = gtk_label_new("Type");
    gtk_table_attach(GTK_TABLE(table), display_output->label, 0, 1, 10, 11,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    display_output->label = gtk_label_new("Key");
    gtk_table_attach(GTK_TABLE(table), display_output->label, 2, 4, 10, 11,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    for(i = 0; i < output->num_video; i++){
        //show name
        display_output->video_name[i] = show_input_video_name(output->in_video[i]);
        gtk_widget_set_size_request(GTK_WIDGET(display_output->video_name[i]), 10, 30);
        gtk_table_attach(GTK_TABLE(table), display_output->video_name[i], 1, 2, i+11, i+12,
          GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

        //show type
        display_output->video_type[i] = show_output_video_type();
        gtk_combo_box_set_active(GTK_COMBO_BOX(display_output->video_type[i]),output->in_video_type[i]);
        gtk_table_attach(GTK_TABLE(table), display_output->video_type[i], 0, 1, i+11, i+12,
          GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

        display_output->ctrl_label[i] = gtk_label_new("Ctrl +");
        gtk_table_attach(GTK_TABLE(table), display_output->ctrl_label[i], 2, 3, i+11, i+12,
          GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

        //key
        display_output->video_key[i] = gtk_entry_new();
        gtk_entry_set_text(GTK_ENTRY(display_output->video_key[i]), output->in_video_key[i]);
        gtk_entry_set_max_length((GTK_ENTRY(display_output->video_key[i])), 1);
        gtk_table_attach(GTK_TABLE(table), display_output->video_key[i], 3, 4, i+11, i+12,
          GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
        gtk_widget_set_size_request(GTK_WIDGET(display_output->video_key[i]), 20, 30);

        //remove button
        display_output->remove[i] = create_delete_button();
        gtk_table_attach(GTK_TABLE(table), display_output->remove[i], 4, 5, i+11, i+12,
          GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
        
        //connect function remove
        g_signal_connect(G_OBJECT(display_output->remove[i]), "clicked", G_CALLBACK(remove_in_video), (gpointer) i);
    }

    //add new video button
    display_output->add = create_add_button();
    gtk_table_attach(GTK_TABLE(table), display_output->add, 4, 5, 14, 15, 
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    //connect function
    g_signal_connect(G_OBJECT(display_output->add), "clicked", G_CALLBACK(add_in_video), NULL);

    //show width
    display_output->label = gtk_label_new("Width");
    gtk_table_attach(GTK_TABLE(table), display_output->label, 0, 1, 0, 1,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
     sprintf(buf, "%d", output->width);
    display_output->width = gtk_entry_new();
    gtk_entry_set_text (GTK_ENTRY(display_output->width), buf);
    gtk_table_attach(GTK_TABLE(table), display_output->width, 1, 2, 0, 1,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    //show height
    display_output->label = gtk_label_new("Height");
    gtk_table_attach(GTK_TABLE(table), display_output->label, 0, 1, 1, 2,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
     sprintf(buf, "%d", output->height);
    display_output->height = gtk_entry_new();
    gtk_entry_set_text (GTK_ENTRY(display_output->height), buf);
    gtk_table_attach(GTK_TABLE(table), display_output->height, 1, 2, 1, 2,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    //show bitrate
    display_output->label = gtk_label_new("Bit rate");
    gtk_table_attach(GTK_TABLE(table), display_output->label, 0, 1, 2, 3, 
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
     sprintf(buf, "%d", output->video_bit_rate);
    display_output->video_bit_rate = gtk_entry_new();
    gtk_entry_set_text (GTK_ENTRY(display_output->video_bit_rate), buf);
    gtk_table_attach(GTK_TABLE(table), display_output->video_bit_rate, 1, 2, 2, 3,  
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    //show group size
    display_output->label = gtk_label_new("Group Size");
    gtk_table_attach(GTK_TABLE(table), display_output->label, 0, 1, 3, 4, 
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
     sprintf(buf, "%d", output->gop_size);
    display_output->gop_size = gtk_entry_new();
    gtk_entry_set_text (GTK_ENTRY(display_output->gop_size), buf);
    gtk_table_attach(GTK_TABLE(table), display_output->gop_size, 1, 2, 3, 4, 
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    //show video_codec
    display_output->label = gtk_label_new("Video codec");
    gtk_table_attach(GTK_TABLE(table), display_output->label, 0, 1, 4, 5, 
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    display_output->video_codec = show_video_codec_type();

    if(output->video_codec == AV_CODEC_ID_FLV1)
        gtk_combo_box_set_active(GTK_COMBO_BOX(display_output->video_codec),1);
    else
        gtk_combo_box_set_active(GTK_COMBO_BOX(display_output->video_codec),0);
    gtk_table_attach(GTK_TABLE(table), display_output->video_codec, 1, 2, 4, 5, 
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    //show baseline
    display_output->label = gtk_label_new("Baseline");
    gtk_table_attach(GTK_TABLE(table), display_output->label, 0, 1, 5, 6,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    display_output->baseline = gtk_check_button_new();
    if(output->baseline)
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(display_output->baseline), TRUE);
    else 
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(display_output->baseline), FALSE);
    gtk_table_attach(GTK_TABLE(table), display_output->baseline, 1, 2, 5, 6,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    //show buffsize
    display_output->label = gtk_label_new("Queue Size");
    gtk_table_attach(GTK_TABLE(table), display_output->label, 0, 1, 6, 7,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
     sprintf(buf, "%d", output->video_queue_size);
    display_output->video_queue_size = gtk_entry_new();
    gtk_entry_set_text (GTK_ENTRY(display_output->video_queue_size), buf);
    gtk_table_attach(GTK_TABLE(table), display_output->video_queue_size, 1, 2, 6, 7,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    //show frame rate
    display_output->label = gtk_label_new("Frame rate");
    gtk_table_attach(GTK_TABLE(table), display_output->label, 0, 1, 7, 8,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
     sprintf(buf, "%d", output->frame_rate);
    display_output->frame_rate = gtk_entry_new();
    gtk_entry_set_text (GTK_ENTRY(display_output->frame_rate), buf);
    gtk_table_attach(GTK_TABLE(table), display_output->frame_rate, 1, 2, 7, 8,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    //show maximum buffer rate
    display_output->label = gtk_label_new("Max buffer rate");
    gtk_table_attach(GTK_TABLE(table), display_output->label, 0, 1, 8, 9,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
     sprintf(buf, "%d", output->max_buf_rate);
    display_output->max_buf_rate = gtk_entry_new();
    gtk_entry_set_text (GTK_ENTRY(display_output->max_buf_rate), buf);
    gtk_table_attach(GTK_TABLE(table), display_output->max_buf_rate, 1, 2, 8, 9,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    display_output->label = gtk_label_new("%");
    gtk_table_attach(GTK_TABLE(table), display_output->label, 2, 3, 8, 9,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
     sprintf(buf, "%d", output->max_buf_rate);

    //show pix_fmt
    display_output->label = gtk_label_new("Pixcel format");
    gtk_table_attach(GTK_TABLE(table), display_output->label, 0, 1, 9, 10,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    display_output->pix_fmt = show_pix_fmt_type();
    if(output->pix_fmt == AV_PIX_FMT_YUV420P)
        gtk_combo_box_set_active(GTK_COMBO_BOX(display_output->pix_fmt),0);

    gtk_table_attach(GTK_TABLE(table), display_output->pix_fmt, 1, 2, 9, 10,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);  
}

/*
 * Function:  show_output_audio 
 * --------------------
 * Create GTK element and show output audio's information base on OutputInfo.
 *
 *  table: Pointer to GTK display_output's audio_table.
 *
 *  display_output: Output's GTK element.
 *
 *  output: Output information.
 *
 *  new_output: Output is new or old.
 */
void show_output_audio(GtkWidget *table, DisplayOutput* display_output, 
    OutputInfo *output, int new_output)
{
    int i;
    //display output name
    display_output->label = gtk_label_new("Input name");
    gtk_table_attach(GTK_TABLE(table), display_output->label, 0, 1, 0, 1, 
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    display_output->audio_name = show_input_audio_name(output->in_audio);
    gtk_widget_set_size_request(GTK_WIDGET(display_output->audio_name), 10, 30);
    if(new_output)
        gtk_combo_box_set_active(GTK_COMBO_BOX(display_output->audio_name), 0);
    gtk_table_attach(GTK_TABLE(table), display_output->audio_name, 1, 2, 0, 1, 
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    gtk_widget_set_size_request(GTK_WIDGET(display_output->audio_name), 200, 30);

    //show Num Channels
    display_output->label = gtk_label_new("Num channel");
    gtk_table_attach(GTK_TABLE(table), display_output->label, 0, 1, 1, 2,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
     sprintf(buf, "%d", output->num_channel);
    display_output->num_channel = gtk_entry_new();
    gtk_entry_set_text (GTK_ENTRY(display_output->num_channel), buf);
    gtk_table_attach(GTK_TABLE(table), display_output->num_channel, 1, 2, 1, 2,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    //show Bitrate
    display_output->label = gtk_label_new("Bit rate");
    gtk_table_attach(GTK_TABLE(table), display_output->label, 0, 1, 2, 3,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
     sprintf(buf, "%d", output->audio_bit_rate);
    display_output->audio_bit_rate = gtk_entry_new();
    gtk_entry_set_text (GTK_ENTRY(display_output->audio_bit_rate), buf);
    gtk_table_attach(GTK_TABLE(table), display_output->audio_bit_rate, 1, 2, 2, 3,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    //show Audio bitrate
    display_output->label = gtk_label_new("Sample rate");
    gtk_table_attach(GTK_TABLE(table), display_output->label, 0, 1, 3, 4,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
     sprintf(buf, "%d", output->sample_rate);
    display_output->sample_rate = gtk_entry_new();
    gtk_entry_set_text (GTK_ENTRY(display_output->sample_rate), buf);
    gtk_table_attach(GTK_TABLE(table), display_output->sample_rate, 1, 2, 3, 4,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    //show Audio codec
    display_output->label = gtk_label_new("Audio codec");
    gtk_table_attach(GTK_TABLE(table), display_output->label, 0, 1, 4, 5,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    display_output->audio_codec = show_audio_codec_type(output->audio_codec);
    gtk_table_attach(GTK_TABLE(table), display_output->audio_codec, 1, 2, 4, 5,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    //show Audio bitrate
    display_output->label = gtk_label_new("Queue size");
    gtk_table_attach(GTK_TABLE(table), display_output->label, 0, 1, 5, 6,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
     sprintf(buf, "%d", output->audio_queue_size);
    display_output->audio_queue_size = gtk_entry_new();
    gtk_entry_set_text (GTK_ENTRY(display_output->audio_queue_size), buf);
    gtk_table_attach(GTK_TABLE(table), display_output->audio_queue_size, 1, 2, 5, 6,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
}

//show output stream base on output_id
/*
 * Function:  show_output_audio 
 * --------------------
 * Show output stream base on output_id.
 *
 *  i: Output_id.
 *
 *  new_output: Output is new or old.
 */
GtkWidget* show_output_stream(int i, int new_output){
    GtkWidget *tab_box, *frame_box, *frame, *output_hbox;

    //create output tab
    tab_box = gtk_hbox_new (FALSE, 5);
    

    //--------------------------------general information-----------
    output_hbox = gtk_vbox_new(FALSE, 5);
    gtk_box_pack_start (GTK_BOX (tab_box), output_hbox, FALSE, FALSE, 5);

    // create frame for general
    display_output[i]->table = gtk_table_new(20, 2, FALSE);
    frame = gtk_frame_new("General information");
    gtk_container_add(GTK_CONTAINER(frame), display_output[i]->table);
    gtk_box_pack_start (GTK_BOX (output_hbox), frame, FALSE, FALSE, 5);

    //show output element
    show_output_general(display_output[i]->table, display_output[i], outDevs[i], new_output);
    //-------------------------display audio----------------
    //create frame
    frame = gtk_frame_new("Audio");
    frame_box = gtk_vbox_new (FALSE, 5);
    gtk_container_add(GTK_CONTAINER(frame), frame_box);
    gtk_box_pack_start (GTK_BOX (output_hbox), frame, FALSE, FALSE, 5);

    //check box
    display_output[i]->audio_check = gtk_check_button_new_with_label("Use audio stream");
    gtk_box_pack_start (GTK_BOX (frame_box), display_output[i]->audio_check, FALSE, FALSE, 5);

    //audio table container audio data
    display_output[i]->audio_table = gtk_table_new(5, 2, FALSE);
    gtk_box_pack_start (GTK_BOX (frame_box), display_output[i]->audio_table, FALSE, FALSE, 5);

    //show audio information
    show_output_audio(display_output[i]->audio_table, display_output[i], outDevs[i], new_output);

    //-------------------------
    //new hbox
    output_hbox = gtk_vbox_new(FALSE, 5);
    gtk_box_pack_start (GTK_BOX (tab_box), output_hbox, FALSE, FALSE, 5);

    //-------------------------display Video-----------------
    //create new frame
    frame = gtk_frame_new("Video");
    frame_box = gtk_vbox_new (FALSE, 5);
    gtk_container_add(GTK_CONTAINER(frame), frame_box);
    gtk_box_pack_start (GTK_BOX (output_hbox), frame, FALSE, FALSE, 5);
    
    //check box
    display_output[i]->video_check = gtk_check_button_new_with_label("Use video stream");
    gtk_box_pack_start (GTK_BOX (frame_box), display_output[i]->video_check, FALSE, FALSE, 5);


    display_output[i]->video_table = gtk_table_new(5, 2, FALSE);
    frame = gtk_frame_new("Video");
    gtk_box_pack_start (GTK_BOX (frame_box), display_output[i]->video_table, FALSE, FALSE, 5);

    //show video information
    show_output_video(display_output[i]->video_table, display_output[i], outDevs[i], new_output);

    return tab_box;
}

/*
 * Function:  read_UI_output 
 * --------------------
 * Read all output's data from UI.
 *
 *  output: Output's information.
 *
 *  display_output: GTK output's element.
 */
void read_UI_output(DisplayOutput* display_output, OutputInfo *output){ 
    int i;
    int type;
    char tmp[LONG_STRING_LENGTH];
    char tmp_location[LONG_STRING_LENGTH];
    char *tmp_format;
    char tmp_name[30];
    char tmp_combo[STRING_LENGTH];

    //-----------------general------------
    //save name
    strcpy(output->name, gtk_entry_get_text(GTK_ENTRY(display_output->name)));

    //save output type
    output->output_type = gtk_combo_box_get_active(GTK_COMBO_BOX(display_output->out_type));

    //save output url
    if(output->output_type == 0){  
        //save url
        sprintf(tmp_location, "%s/%s/%s/%s", home_path, current_course, current_version, current_lecture);
        tmp_format = get_format_combo_box(display_output->format);
        //save format
        strcpy(output->format, tmp_format);
        
        //save url
        sprintf(tmp, "%s/%s%d/%s.%s", tmp_location, current_path_prefix, session, 
            gtk_entry_get_text(GTK_ENTRY(display_output->file)), output->format);
        strcpy(output->url, tmp);

    } else if(output->output_type == 1) {
        //save url
        strcpy(output->url, gtk_entry_get_text(GTK_ENTRY(display_output->out_url)));

        //set format flash
        strcpy(output->format, "flv");
    }

    //----------------audio-----------------
    //save audio name
    strcpy(tmp_name, "");
    get_combo_box_text(GTK_COMBO_BOX(display_output->audio_name), tmp_combo);
    get_input_audio_name(tmp_combo, tmp_name);
    strcpy(output->in_audio, tmp_name);
    if(strcmp(tmp_name, "") == 0 && output->audio_stream){
        sprintf(tmp_status, "Error: Output %s's input audio is invalid", output->name);
        push_item(status_bar, GINT_TO_POINTER(context_id), tmp_status);
        ui_error = OUTPUT_ERROR;
    }

    //save num chanel
    output->num_channel = atoi(gtk_entry_get_text(GTK_ENTRY(display_output->num_channel)));
    if(output->num_channel <= 0){
        sprintf(tmp_status, "Error: Output %s's num_channel is invalid", output->name);
        push_item(status_bar, GINT_TO_POINTER(context_id), tmp_status);
        ui_error = OUTPUT_ERROR;
    }

    //save bit rate
    output->audio_bit_rate = atoi(gtk_entry_get_text(GTK_ENTRY(display_output->audio_bit_rate)));
    if(output->audio_bit_rate <= 0){
        sprintf(tmp_status, "Error: Output %s's audio bit_rate is invalid", output->name);
        push_item(status_bar, GINT_TO_POINTER(context_id), tmp_status);
        ui_error = OUTPUT_ERROR;
    }

    //save sample rate
    output->sample_rate = atoi(gtk_entry_get_text(GTK_ENTRY(display_output->sample_rate)));
    if(output->sample_rate <= 0){
        sprintf(tmp_status, "Error: Output %s's audio sample_rate is invalid", output->name);
        push_item(status_bar, GINT_TO_POINTER(context_id), tmp_status);
        ui_error = OUTPUT_ERROR;
    }
    
    //save queue size
    output->audio_queue_size = atoi(gtk_entry_get_text(GTK_ENTRY(display_output->audio_queue_size)));
    if(output->audio_queue_size <= 100){
        sprintf(tmp_status, "Error: Output %s's audio queue size is too small", output->name);
        push_item(status_bar, GINT_TO_POINTER(context_id), tmp_status);
        ui_error = OUTPUT_ERROR;
    }

    //save audio codec
    output->audio_codec = (AVCodecID) get_audio_codec_type(display_output->audio_codec);

    //----------------video-----------------
    //save video bitrate
    output->video_bit_rate = atoi(gtk_entry_get_text(GTK_ENTRY(display_output->video_bit_rate)));
    if(output->video_bit_rate <= 0){
        sprintf(tmp_status, "Error: Output %s's video bit_rate is invalid", output->name);
        push_item(status_bar, GINT_TO_POINTER(context_id), tmp_status);
        ui_error = OUTPUT_ERROR;
    }

    //save video gop_size
    output->gop_size = atoi(gtk_entry_get_text(GTK_ENTRY(display_output->gop_size)));
    if(output->gop_size <= 0){
        sprintf(tmp_status, "Error: Output %s's video group of picture is invalid", output->name);
        push_item(status_bar, GINT_TO_POINTER(context_id), tmp_status);
        ui_error = OUTPUT_ERROR;
    }

    //save video width
    output->width = atoi(gtk_entry_get_text(GTK_ENTRY(display_output->width)));
    if(output->width <= 0){
        sprintf(tmp_status, "Error: Output %s's video width is invalid", output->name);
        push_item(status_bar, GINT_TO_POINTER(context_id), tmp_status);
        ui_error = OUTPUT_ERROR;
    }

    if(output->width%2 == 1){
        sprintf(tmp_status, "Error: Output %s's video width must be devicible by 2", output->name);
        push_item(status_bar, GINT_TO_POINTER(context_id), tmp_status);
        ui_error = OUTPUT_ERROR;
    }

    //save video height
    output->height = atoi(gtk_entry_get_text(GTK_ENTRY(display_output->height)));
    if(output->height <= 0){
        sprintf(tmp_status, "Error: Output %s's video height is invalid", output->name);
        push_item(status_bar, GINT_TO_POINTER(context_id), tmp_status);
        ui_error = OUTPUT_ERROR;
    }

    if(output->height%2 == 1){
        sprintf(tmp_status, "Error: Output %s's video height must be devicible by 2", output->name);
        push_item(status_bar, GINT_TO_POINTER(context_id), tmp_status);
        ui_error = OUTPUT_ERROR;
    }

    //save video codec
    output->video_codec = (AVCodecID) get_video_codec(display_output->video_codec);
    if(strcmp(output->format, "mp4") == 0 && output->video_codec == AV_CODEC_ID_FLV1){
        sprintf(tmp_status, "Error: Output %s's video codec mpeg can't be use with format mp4", output->name);
        push_item(status_bar, GINT_TO_POINTER(context_id), tmp_status);
        ui_error = OUTPUT_ERROR;
    }

    //save pix format
    output->pix_fmt = (AVPixelFormat) get_pix_fmt(display_output->pix_fmt);

    //save queue size
    output->video_queue_size = atoi(gtk_entry_get_text(GTK_ENTRY(display_output->video_queue_size)));
    if(output->video_queue_size <= 50){
        sprintf(tmp_status, "Error: Output %s's video queue size is too small", output->name);
        push_item(status_bar, GINT_TO_POINTER(context_id), tmp_status);
        ui_error = OUTPUT_ERROR;
    }

    //save max buffer rate size
    output->max_buf_rate = atoi(gtk_entry_get_text(GTK_ENTRY(display_output->max_buf_rate)));

    //save baseline
    output->baseline = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(display_output->baseline));

    //save frame rate
    output->frame_rate = atoi(gtk_entry_get_text(GTK_ENTRY(display_output->frame_rate)));
    if(output->video_queue_size <= 0){
        sprintf(tmp_status, "Error: Output %s's video framerate is invalid", output->name);
        push_item(status_bar, GINT_TO_POINTER(context_id), tmp_status);
        ui_error = OUTPUT_ERROR;
    }

    in_video_error = IN_VIDEO_ERROR;
    //save video name, input video type
    for(i = 0; i < output->num_video; i++){
        strcpy(tmp_name, "");
        get_combo_box_text(GTK_COMBO_BOX(display_output->video_name[i]), tmp_combo);
        get_input_video_name(tmp_combo, tmp_name);
        strcpy(output->in_video[i], tmp_name);
        if(strcmp(tmp_name, "") != 0 && output->in_video_state[i] == 0){
            in_video_error = VALID;
        }

        if(strcmp(tmp_name, "") == 0 && output->in_video_state[i] == 0){
            sprintf(tmp_status, "Error: Output %s's input video is invalid", output->name);
            push_item(status_bar, GINT_TO_POINTER(context_id), tmp_status);
            ui_error = OUTPUT_ERROR;
        }

        //get key
        strcpy(output->in_video_key[i], gtk_entry_get_text(GTK_ENTRY(display_output->video_key[i])));

        //get video type
        output->in_video_type[i] = gtk_combo_box_get_active(GTK_COMBO_BOX(display_output->video_type[i]));        
    }
    if(in_video_error == IN_VIDEO_ERROR && output->num_video > 0){
        sprintf(tmp_status, "Error: Output %s has no input video", output->name);
        push_item(status_bar, GINT_TO_POINTER(context_id), tmp_status);
        ui_error = OUTPUT_ERROR;
    }
}

/*
 * Function:  save_output_button_click 
 * --------------------
 * Get output data from UI.
 * Check if user enter data valid.
 * Save output data to config file.
 *
 *  widget: GTK save_output button.
 */
void save_output_button_click(GtkWidget *widget){
    int i;  

    //read config output streams from UI
    ui_error = VALID;
    
    for(i = 0; i < numOut; i++){
        if(outDevs[i]->state != STATE_DELETED)
            read_UI_output(display_output[i], outDevs[i]);
    }
    //output error
    if(ui_error == VALID){
        clear_status_bar(status_bar, GINT_TO_POINTER(context_id));
    }

    //save output to file config
    save_output_to_config_file(outDevs, numOut);

    //update output info
    update_output_info();
}

/*
 * Function:  ok_remove_output 
 * --------------------
 * Remove given output and hide output information from UI.
 *
 *  widget: Pointer to GTK close_input_tab button.
 *
 *  close_data: Contain the index of current output's tab and pointer to main window.
 */
void ok_remove_output(GtkWidget *widget, ClosePage *close_data){
    GtkWidget * page;
    
    outDevs[close_data->page_index]->state = STATE_DELETED;
    outDevs[close_data->page_index]->disable = 1;

    //close new window
    page = gtk_notebook_get_nth_page(GTK_NOTEBOOK(out_notebook), close_data->page_index);
    gtk_widget_hide(page);
    hide_output_info(display_info[close_data->page_index]);

    gtk_widget_destroy(GTK_WIDGET(close_data->window));
}

/*
 * Function:  close_output_tab_click 
 * --------------------
 * Confirm user want to remove current output.
 * If user want to remove, call remove function to remove this output.
 *
 *  button: Pointer to GTK close_output_tab button.
 *
 *  page_index: The index of current output's tab.
 */
void close_output_tab_click( GtkButton *button, int page_index){ 
    //init
    GtkWidget *new_window, *box, *table, *save_button, *label;
    
    //create new window
    new_window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_default_size(GTK_WINDOW(new_window), 100, 50);
    gtk_window_set_position(GTK_WINDOW(new_window), GTK_WIN_POS_CENTER);
    gtk_window_set_title(GTK_WINDOW(new_window), "Remove Output");

    //create box container
    box = gtk_vbox_new (FALSE, 0);
    gtk_container_add(GTK_CONTAINER(new_window), box); 
    gtk_widget_show(box);

    //create table container
    table = gtk_table_new(3, 3, FALSE);
    gtk_box_pack_start (GTK_BOX (box), table, FALSE, FALSE, 5);
    gtk_widget_show(table);

    label = gtk_label_new("Remove this output?");
    gtk_table_attach(GTK_TABLE(table), label, 0, 1, 0, 1, 
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    //save button
    save_button = create_apply_button();
    gtk_table_attach(GTK_TABLE(table), save_button, 1, 2, 0, 1, 
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    //create data pass to close
    ClosePage *close_data = (ClosePage*) malloc(sizeof(ClosePage));
    close_data->window = new_window;
    close_data->page_index = page_index;

    g_signal_connect(G_OBJECT(save_button), "clicked", G_CALLBACK(ok_remove_output), close_data);

    //show all window
    gtk_widget_show_all(new_window);
}

/*
 * Function:  ok_new_output_button_click 
 * --------------------
 * Create new output and show this output to UI.
 *
 *  widget: Pointer to GTK add_output button.
 *
 *  window: Pointer to GTK main window.
 */
void ok_new_output_button_click(GtkWidget *widget, gpointer window){
    int type = 0;
    GtkWidget *frame, *tab_box, *tab, *tab_close, *tab_container;
    GtkWidget *output_hbox;
        
    //init new OutputInfo
    outDevs[numOut] = get_default_out();

    //create tab container
    tab_container = gtk_hbox_new (FALSE, 0);
    
    //create tab close
    tab_close = create_remove_button();
    gtk_box_pack_start (GTK_BOX (tab_container), tab_close, TRUE, TRUE, 0);
    g_signal_connect( G_OBJECT( tab_close ), "clicked", G_CALLBACK( close_output_tab_click), (gpointer) numOut);

    //create tab label
    sprintf(buf, "Output%d", numOut+1);
    tab = gtk_label_new(buf);
    gtk_box_pack_start (GTK_BOX (tab_container), tab, FALSE, FALSE, 5);

    //add new tab
    tab_box = show_output_stream (numOut, 1);
    gtk_notebook_append_page(GTK_NOTEBOOK(out_notebook), tab_box, tab_container);   
    
    //show all element
    gtk_widget_show_all(tab_container);
    gtk_widget_show_all(tab_box);
    
    //check audio video
    check_audio_video(display_output[numOut], outDevs[numOut]);

    //connect function
    g_signal_connect(G_OBJECT(display_output[numOut]->audio_check), "clicked", G_CALLBACK(check_audio_click), (gpointer) numOut);
    g_signal_connect(G_OBJECT(display_output[numOut]->video_check), "clicked", G_CALLBACK(check_video_click), (gpointer) numOut);

    //update output info
    show_output_info(output_info_table, display_info[numOut], outDevs[numOut], numOut, 0);
    
    //update numOut
    numOut++;

    //close new window
    gtk_widget_destroy(GTK_WIDGET(window));
}

/*
 * Function:  new_output_button_click 
 * --------------------
 * Confirm user want to create new output.
 * If user want to create, call create function to create this output.
 *
 *  widget: Pointer to GTK add_input button.
 *
 *  window: Pointer to GTK main window.
 */
void new_output_button_click(GtkWidget *widget, gpointer window){
    //init
    GtkWidget *new_window, *box, *table, *save_button, *label;
    
    //create new window
    new_window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_default_size(GTK_WINDOW(new_window), 100, 50);
    gtk_window_set_position(GTK_WINDOW(new_window), GTK_WIN_POS_CENTER);
    gtk_window_set_title(GTK_WINDOW(new_window), "Create new input?");

    //create box container
    box = gtk_vbox_new (FALSE, 0);
    gtk_container_add(GTK_CONTAINER(new_window), box); 
    gtk_widget_show(box);

    //create table container
    table = gtk_table_new(3, 3, FALSE);
    gtk_box_pack_start (GTK_BOX (box), table, FALSE, FALSE, 5);
    gtk_widget_show(table);

    label = gtk_label_new("Create new output");
    gtk_table_attach(GTK_TABLE(table), label, 0, 1, 0, 1, 
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    //save button
    save_button = create_apply_button();
    gtk_table_attach(GTK_TABLE(table), save_button, 1, 2, 0, 1, 
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    g_signal_connect(G_OBJECT(save_button), "clicked", G_CALLBACK(ok_new_output_button_click), new_window);

    //show all window
    gtk_widget_show_all(new_window);
}

 /*
 * Function:  create_output_streams_tab 
 * --------------------
 * Create Output Streams tab: Create UI and display output data.
 *
 *  window: Main window of program.
 * --------------------
 *  Return: Pointer to GTK_VBOX (Contain all output_streams's element).
 */
GtkWidget* create_output_streams_tab(GtkWidget* window){
    GtkWidget *new_window, *label, *hbox, *box, *table, *save_button, *new_button;
    GtkWidget *hseparator, *frame, *tab, *tab_box, *tab_container, *tab_close;
    GtkWidget *output_hbox, *frame_box;
    char str[SHORT_STRING_LENGTH];  
    int i;

    //create box contain element
    box = gtk_vbox_new (FALSE, 5);

    //separator
    hseparator = gtk_hseparator_new();
    gtk_box_pack_start (GTK_BOX (box), hseparator, FALSE, FALSE, 5);    

    hbox = gtk_hbox_new (FALSE, 5);
    gtk_box_pack_start (GTK_BOX (box), hbox, FALSE, FALSE, 5);

    //--------------------------output-------------------------------
    //create note book
    out_notebook = gtk_notebook_new();
    gtk_notebook_set_tab_pos(GTK_NOTEBOOK(out_notebook), GTK_POS_LEFT);
    gtk_container_add(GTK_CONTAINER(hbox), out_notebook);

    //show output information
    for(i = 0; i < numOut; i++){
        //create tab container
        tab_container = gtk_hbox_new (FALSE, 0);
        
        //create tab close
        tab_close = create_remove_button();
        gtk_box_pack_start (GTK_BOX (tab_container), tab_close, TRUE, TRUE, 0);
        g_signal_connect( G_OBJECT( tab_close ), "clicked", G_CALLBACK( close_output_tab_click), (gpointer) i);

        
        //create tab label
        sprintf(buf, "Output%d", i+1);
        tab = gtk_label_new(buf);
        gtk_box_pack_start (GTK_BOX (tab_container), tab, FALSE, FALSE, 5);

        //show output
        tab_box = show_output_stream(i, 0);
        gtk_notebook_append_page(GTK_NOTEBOOK(out_notebook), tab_box, tab_container);
        gtk_widget_show_all(tab_container);
    }

    //new hbox
    output_hbox = gtk_vbox_new(FALSE, 5);
    gtk_box_pack_start (GTK_BOX (hbox), output_hbox, FALSE, FALSE, 5);

    //save button
    save_button = gtk_button_new_with_label("Save");
    gtk_widget_set_size_request(GTK_WIDGET(save_button), 100, 30);
    gtk_box_pack_end (GTK_BOX (output_hbox), save_button, FALSE, FALSE, 5);
    g_signal_connect(G_OBJECT(save_button), "clicked", G_CALLBACK(save_output_button_click), NULL);

    //New button
    new_button = gtk_button_new_with_label("New Output");
    gtk_widget_set_size_request(GTK_WIDGET(new_button), 100, 30);
    gtk_box_pack_end (GTK_BOX (output_hbox), new_button, FALSE, FALSE, 5);
    g_signal_connect(G_OBJECT(new_button), "clicked", G_CALLBACK(new_output_button_click), window);

    return box;
}
