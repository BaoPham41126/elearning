/*
 * File: lecture_management_view.c
 *
 * Author: GVLab
 *
 * Created on: Aug 13, 2015
 */

#include "headers/view/lecture_management_view.h"

/*
 * Function:  save_lecture_management_button_click 
 * --------------------
 * Get all lecture_management's data from UI.
 * Check all data is valid and save all data to config file.
 *
 *  widget: Pointer to GTK save_button.
 */
void save_lecture_management_button_click(GtkWidget *widget){
    int i;  
    ui_error = VALID;
    //read config input devices from UI
    read_UI_lecture_management();
    if(ui_error == VALID){
        clear_status_bar(status_bar, GINT_TO_POINTER(context_id));
    
        type =3;
        //save location
        change_file_path();
        type = 0;

        //save data to file
        save_location_setting(home_path, data_path, current_course, current_version,
            current_lecture, current_path_prefix);

        //save input to file config
        save_info_to_config_file(outDevs, numOut);

        //read description
        gchar *text;
        GtkTextBuffer *buffer;
        GtkTextIter start, end;

        //get text
        buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (display_lecture->description));
        gtk_text_buffer_get_start_iter (buffer, &start);
        gtk_text_buffer_get_end_iter (buffer, &end);
        text = gtk_text_buffer_get_text (buffer, &start, &end, FALSE);  

        //set text
        strcpy(current_description, text);

        //get version folder
        char tmp_path[SHORT_STRING_LENGTH];
        sprintf(tmp_path, "%s/%s/%s", home_path, current_course, current_version);

        //update value
        update_lecture_description(tmp_path, current_lecture, current_description);
    }
}

/*
 * Function:  lecture_change 
 * --------------------
 * Change lecture's description base on current lecture, 
 *  current version, current course.
 *
 *  lecture: Pointer to GTK lecture_selection element.
 */
void lecture_change(GtkComboBox *lecture, gpointer data){
    //set path to version folder
    char tmp_path[SHORT_STRING_LENGTH];
    gchar *lecture_name;
    GtkTextBuffer *buffer;
    sprintf(tmp_path, "%s/%s/%s", home_path, current_course, current_version);

    //get lecture
    int i = gtk_combo_box_get_active(GTK_COMBO_BOX(lecture));
    if(i >= 0){
        lecture_name = gtk_combo_box_get_active_text(GTK_COMBO_BOX(lecture));

        //get description
        read_lecture_description(tmp_path, lecture_name, current_description);
        
        //save description to UI
        buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (display_lecture->description));
        gtk_text_buffer_set_text (buffer, current_description, -1);
    }
}

/*
 * Function:  check_new_lecture_button 
 * --------------------
 * If user can select a lecture, disable select a lecture.
 * Allow user entering new lecture from UI.
 *
 * Else allow user selecting new lecture. Disable enter new lecture from UI.
 *
 *  widget: Pointer to GTK check_new_lecture button.
 *
 *  button: Pointer to GTK check_new_lecture button.
 */
void check_new_lecture_button(GtkWidget *widget, gpointer button){
    if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(button))){
        gtk_widget_set_sensitive(display_lecture->lecture, FALSE);
        gtk_widget_set_sensitive(display_lecture->lecture_new, TRUE);
    } else {
        gtk_widget_set_sensitive(display_lecture->lecture, TRUE);
        gtk_widget_set_sensitive(display_lecture->lecture_new, FALSE);
    }
}

/*
 * Function:  read_UI_lecture_management 
 * --------------------
 * Read DisplayInfo data from UI and save it to OutputInfo.
 */
void read_UI_lecture_management(){

    gchar *tmp;
    char tmp_data[STRING_LENGTH];

    session = 0;
    //read session
    strcpy(current_path_prefix, gtk_entry_get_text(GTK_ENTRY(display_lecture->path_prefix)));
    if(strcmp(current_path_prefix, "") == 0){
        push_item(status_bar, GINT_TO_POINTER(context_id), "Error: Path prefix is invalid");
        ui_error = LECTURE_ERROR;
    }  
    
    //read lecture
    if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(display_lecture->check_select_lecture))){
        get_combo_box_text(GTK_COMBO_BOX(display_lecture->lecture), tmp_data);
        strcpy(current_lecture, tmp_data);
    } else {
        strcpy(current_lecture, gtk_entry_get_text(GTK_ENTRY(display_lecture->lecture_new)));
        
    }      
    if(strcmp(current_lecture, "") == 0){
        push_item(status_bar, GINT_TO_POINTER(context_id), "Error: Lecture is invalid");
        ui_error = LECTURE_ERROR;
    }  

    //read description
    gchar *text;
    GtkTextBuffer *buffer;
    GtkTextIter start, end;

    buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (display_lecture->description));
    gtk_text_buffer_get_start_iter (buffer, &start);
    gtk_text_buffer_get_end_iter (buffer, &end);
    text = gtk_text_buffer_get_text (buffer, &start, &end, FALSE);  

    strcpy(current_description, text);
}

/*
 * Function:  show_output_info 
 * --------------------
 * Create lecture_management's element and show all current data.
 *
 *  table: Pointer to GTK output_info_table.
 *
 *  info: Lecture_management's GTK element.
 *
 *  output: Lecture_management's data.
 *
 *  index: Lecture_management index.
 *
 *  new_info: Lecture_management is new or old.
 */
void show_output_info(GtkWidget *table, DisplayInfo *info, OutputInfo *output, int index, int new_info){
    char tmp_string[20];
    int i, j;
    
    //create checkbox
    info->check = gtk_check_button_new();
    gtk_table_attach(GTK_TABLE(table), info->check, 0, 1, index + 1, index + 2, 
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    if(output->disable == 0)
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(info->check), TRUE);
    gtk_widget_show(info->check);

    //create name entry
    info->name = gtk_entry_new();
    gtk_table_attach(GTK_TABLE(table), info->name, 1, 2, index + 1, index + 2, 
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    gtk_entry_set_text(GTK_ENTRY(info->name), output->name);
    gtk_widget_set_size_request(GTK_WIDGET(info->name), 100, 35);
    gtk_widget_show(info->name);
    gtk_widget_set_sensitive(GTK_WIDGET(info->name), FALSE);

    //create type entry
    info->type = gtk_entry_new();
    gtk_widget_set_size_request(GTK_WIDGET(info->type), 100, 35);
    gtk_table_attach(GTK_TABLE(table), info->type, 2, 3, index + 1, index + 2, 
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    get_output_typpe(output->output_type, tmp_string);
    gtk_entry_set_text(GTK_ENTRY(info->type), tmp_string);
    gtk_widget_show(info->type);
    gtk_widget_set_sensitive(GTK_WIDGET(info->type), FALSE);

    //create audio entry
    info->audio = gtk_entry_new();
    gtk_widget_set_size_request(GTK_WIDGET(info->audio), 100, 35);
    gtk_table_attach(GTK_TABLE(table), info->audio, 3, 4, index + 1, index + 2, 
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    if(output->audio_stream)
        gtk_entry_set_text(GTK_ENTRY(info->audio), "1");
    else 
        gtk_entry_set_text(GTK_ENTRY(info->audio), "0");
    // gtk_entry_set_max_length(GTK_ENTRY(info->audio),1);
    gtk_widget_show(info->audio);
    gtk_widget_set_sensitive(GTK_WIDGET(info->audio), FALSE);

    //create video entry
    info->video = gtk_entry_new();
    gtk_widget_set_size_request(GTK_WIDGET(info->video), 100, 35);
    gtk_table_attach(GTK_TABLE(table), info->video, 4, 5, index + 1, index + 2, 
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    strcpy(tmp_string, "");
    get_num_in_video(output ,tmp_string);
    if(output->video_stream){
        gtk_entry_set_text(GTK_ENTRY(info->video), tmp_string);
    } else 
        gtk_entry_set_text(GTK_ENTRY(info->video), "0");
    gtk_widget_show(info->video);
    gtk_widget_set_sensitive(GTK_WIDGET(info->video), FALSE);
    
    //create switch key entry
    info->label = gtk_label_new("Ctrl +");
    gtk_table_attach(GTK_TABLE(table), info->label, 5, 6, index + 1, index + 2, 
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    gtk_widget_show(info->label);

    //get key
    strcpy(tmp_string, "");
    for(j = 0; j < output->num_video; j++){
        if(output->in_video_state[j] == 0){
            if(strcmp(tmp_string, "") != 0)
                sprintf(tmp_string, "%s/ %s", tmp_string, output->in_video_key[j]);
            else
                strcpy(tmp_string, output->in_video_key[j]);
        }
    }

    //show key
    info->switch_key = gtk_entry_new();
    gtk_widget_set_size_request(GTK_WIDGET(info->switch_key), 100, 35);
    gtk_table_attach(GTK_TABLE(table), info->switch_key, 6, 7, index + 1, index + 2, 
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    gtk_entry_set_text(GTK_ENTRY(info->switch_key), tmp_string);
    gtk_entry_set_max_length(GTK_ENTRY(info->switch_key),10);
    gtk_widget_show(info->switch_key);
    gtk_widget_set_sensitive(GTK_WIDGET(info->switch_key), FALSE);
}

/*
 * Function:  hide_output_info 
 * --------------------
 * Hide given lecture_management's GTK element.
 *
 *  info: Lecture_management's GTK element.
 */
void hide_output_info(DisplayInfo *info){
    gtk_widget_hide(info->check);
    gtk_widget_hide(info->label);
    gtk_widget_hide(info->name);
    gtk_widget_hide(info->type);
    gtk_widget_hide(info->audio);
    gtk_widget_hide(info->video);
    gtk_widget_hide(info->switch_key);
}

/*
 * Function:  update_output_info 
 * --------------------
 * When output stream data changed in Output Streams tab, 
 *  Change info display in "Lecture style" table.
 */
void update_output_info(){
    char tmp_string[20];
    int i, j;
    //check all output
    for(i = 0; i < numOut; i++){
        //name
        gtk_entry_set_text(GTK_ENTRY(display_info[i]->name), outDevs[i]->name);

        //update type entry
        get_output_typpe(outDevs[i]->output_type, tmp_string);
        gtk_entry_set_text(GTK_ENTRY(display_info[i]->type), tmp_string);

        //update audio entry
        if(outDevs[i]->audio_stream)
            gtk_entry_set_text(GTK_ENTRY(display_info[i]->audio), "1");
        else 
            gtk_entry_set_text(GTK_ENTRY(display_info[i]->audio), "0");
        strcpy(tmp_string, "");
        get_num_in_video(outDevs[i], tmp_string);

        //update video entry
        if(outDevs[i]->video_stream){
            gtk_entry_set_text(GTK_ENTRY(display_info[i]->video), tmp_string);
        } else 
            gtk_entry_set_text(GTK_ENTRY(display_info[i]->video), "0");

        //get key
        strcpy(tmp_string, "");
        for(j = 0; j < outDevs[i]->num_video; j++){
            if(outDevs[i]->in_video_state[j] == 0){
                if(strcmp(tmp_string, "") != 0)
                    sprintf(tmp_string, "%s, %s", tmp_string, outDevs[i]->in_video_key[j]);
                else
                    strcpy(tmp_string, outDevs[i]->in_video_key[j]);
            }
        }

        //update key
        gtk_entry_set_text(GTK_ENTRY(display_info[i]->switch_key), tmp_string);
    }
}

/*
 * Function:  check_output_info 
 * --------------------
 * Change the using of output stream.
 *
 *  check: Pointer to GTK check_use button.
 *
 *  i: Index of output stream.
 */
void check_output_info(GtkWidget *check, int i){
    //update audio stream value
    (outDevs[i]->disable  == 1) ? (outDevs[i]->disable = 0) : (outDevs[i]->disable = 1);
}

 /*
 * Function:  create_lecture_management_tab 
 * --------------------
 * Create Lecture Management tab: Create UI and display current data.
 *
 *  window: Main window of program.
 * --------------------
 *  Return: Pointer to GTK_HBOX (Contain all lecture_management's element).
 */
GtkWidget* create_lecture_management_tab(GtkWidget* window){
    GtkWidget *hbox, *vbox, *hseparator, *frame, *table;
    GtkWidget *label, *tmp_box, *info_hbox, *save_button, *tmp_vbox;
    int i;
    char tmp_path[SHORT_STRING_LENGTH];
    GtkTextBuffer *buffer;

    //create new hbox
    hbox = gtk_hbox_new (FALSE, 5);

    //create new vbox
    vbox = gtk_vbox_new (FALSE, 5);
    gtk_container_add(GTK_CONTAINER(hbox), vbox); 

    //separator
    hseparator = gtk_hseparator_new();
    gtk_box_pack_start (GTK_BOX (vbox), hseparator, FALSE, FALSE, 5);


    //----------------------lecture management----------------
    //create new data
    frame = gtk_frame_new("Lecture management");
    gtk_box_pack_start (GTK_BOX (vbox), frame, FALSE, FALSE, 5);

    //create tmp hbox
    tmp_box = gtk_hbox_new(FALSE, 5);
    gtk_container_add(GTK_CONTAINER(frame), tmp_box); 
    //-------------------------lecture-------------------
    tmp_vbox = gtk_vbox_new(FALSE, 5);
    gtk_container_add(GTK_CONTAINER(tmp_box), tmp_vbox); 

    //show lecture
    frame = gtk_frame_new("Lecture");
    gtk_box_pack_start (GTK_BOX (tmp_vbox), frame, FALSE, FALSE, 5);

    //lecture table
    table = gtk_table_new(2, 4, FALSE);
    gtk_container_add(GTK_CONTAINER(frame), table);

    //choose exist lecture ratio button
    display_lecture->check_select_lecture = gtk_radio_button_new_with_label( NULL,"Select an existing lecture:");
    gtk_table_attach(GTK_TABLE(table), display_lecture->check_select_lecture, 0, 1, 0, 1, 
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    //lecture data
    display_lecture->lecture = gtk_combo_box_text_new();
    show_course_management(display_lecture->lecture, 3);
    gtk_table_attach(GTK_TABLE(table), display_lecture->lecture, 0, 1, 1, 2, 
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    gtk_widget_set_size_request( display_lecture->lecture, 150, 30); 
    g_signal_connect( G_OBJECT( display_lecture->lecture ), "changed",
                     G_CALLBACK( lecture_change ), display_lecture->description);

    //new lecture ratio button
    display_lecture->check_new_lecture = gtk_radio_button_new_with_label_from_widget (
        GTK_RADIO_BUTTON (display_lecture->check_select_lecture), "New lecture:");
    gtk_table_attach(GTK_TABLE(table), display_lecture->check_new_lecture, 0, 1, 2, 3, 
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    g_signal_connect( G_OBJECT( display_lecture->check_new_lecture ), "clicked",
                     G_CALLBACK( check_new_lecture_button ), display_lecture->check_new_lecture);


    //new lecture entry
    display_lecture->lecture_new = gtk_entry_new();
    gtk_table_attach(GTK_TABLE(table), display_lecture->lecture_new, 0, 1, 3, 4, 
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    gtk_widget_set_size_request( display_lecture->lecture_new, 150, 30); 
    gtk_widget_set_sensitive(display_lecture->lecture_new, FALSE);

    //path prefix

    //----------------------path prefix------------------
    //show path prefix
    frame = gtk_frame_new("Path prefix");
    gtk_box_pack_start (GTK_BOX (tmp_vbox), frame, TRUE, TRUE, 5);

    //path prefix data
    display_lecture->path_prefix = gtk_entry_new();
    gtk_entry_set_text(GTK_ENTRY(display_lecture->path_prefix), current_path_prefix);
    gtk_container_add(GTK_CONTAINER(frame), display_lecture->path_prefix);
    gtk_widget_set_size_request( display_lecture->path_prefix, 150, 30); 

    //----------------------Description------------------
    // show Description
    frame = gtk_frame_new("Description");
    gtk_box_pack_start (GTK_BOX (tmp_box), frame, FALSE, FALSE, 5);

    display_lecture->description = gtk_text_view_new();
    gtk_container_add(GTK_CONTAINER(frame), display_lecture->description);
    gtk_widget_set_size_request(GTK_WIDGET(display_lecture->description), 500, 100);
    
    //get version folder
    sprintf(tmp_path, "%s/%s/%s", home_path, current_course, current_version);

    //get description
    read_lecture_description(tmp_path, current_lecture, current_description);

    //save description
    buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (display_lecture->description));
    gtk_text_buffer_set_text (buffer, current_description, -1);

    //----------------------lecture style----------------
    //create frame container
    frame = gtk_frame_new("Lecture style");
    gtk_box_pack_start (GTK_BOX (vbox), frame, FALSE, FALSE, 5);

    //create table container
    output_info_table = gtk_table_new(MAX_OUT, MAX_OUT, FALSE);
    gtk_container_add(GTK_CONTAINER(frame), output_info_table);
    
    //display header
    label = gtk_label_new("Use");
    gtk_table_attach(GTK_TABLE(output_info_table), label, 0, 1, 0, 1, 
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    label = gtk_label_new("Output Name");
    gtk_table_attach(GTK_TABLE(output_info_table), label, 1, 2, 0, 1, 
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    label = gtk_label_new("Output type");
    gtk_table_attach(GTK_TABLE(output_info_table), label, 2, 3, 0, 1, 
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    label = gtk_label_new("Audio Stream");
    gtk_table_attach(GTK_TABLE(output_info_table), label, 3, 4, 0, 1, 
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    label = gtk_label_new("Video Stream");
    gtk_table_attach(GTK_TABLE(output_info_table), label, 4, 5, 0, 1, 
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    label = gtk_label_new("Switch video key");
    gtk_table_attach(GTK_TABLE(output_info_table), label, 5, 7, 0, 1, 
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    //display information
    for(i = 0; i < numOut; i++){
        //show output info
        show_output_info(output_info_table, display_info[i], outDevs[i], i, 0);
    }

    //create new hbox
    info_hbox = gtk_vbox_new(FALSE, 5);
    gtk_box_pack_end (GTK_BOX (hbox), info_hbox, FALSE, FALSE, 5);

    //separater
    hseparator = gtk_hseparator_new();
    gtk_box_pack_start (GTK_BOX (info_hbox), hseparator, FALSE, FALSE, 5);

    //save button
    save_button = gtk_button_new_with_label("Save");
    gtk_widget_set_size_request(GTK_WIDGET(save_button), 100, 30);
    gtk_box_pack_end (GTK_BOX (info_hbox), save_button, FALSE, FALSE, 5);
    g_signal_connect(G_OBJECT(save_button), "clicked", G_CALLBACK(save_lecture_management_button_click), NULL);

    return hbox;
}
