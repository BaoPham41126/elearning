/*
 * File: uploading_view.c
 *
 * Author: GVLab
 *
 * Created on: Aug 13, 2015
 */

#include <QFormLayout>
#include <QComboBox>
#include <QLabel>
#include <QDateTimeEdit>
#include <QDialogButtonBox>

#include "headers/youtube/youtubehandler.h"
#include "headers/youtube/youtubeapi.h"
#include "headers/youtube/broadcastbuilder.h"
#include "headers/view/youtube_view.h"
#include "headers/view/main_view.h"
#include "QString"

struct ChannelData {
 GtkWidget*broadcast_link;
 GtkWidget*name;
 GtkWidget*link;
 GtkWidget*status;
 GtkWidget* deleteButton;
 GtkWidget* goLiveButton;
 QString broadcastId;
 QString streamId;
 int row;
} ;
QList<ChannelData*> listChannel;

DisplayYoutube *display_youtube = new DisplayYoutube();
AuthenticationHandler *auth_handler = new AuthenticationHandler(display_youtube);
YoutubeHandler *youtube_handler = new YoutubeHandler(display_youtube);


void check_youtube_account(){
    Credential *credential = new Credential();
    Authentication* auth = new Authentication();
    if (credential->readFromFile()){
        display_youtube->credential = credential;
        std::cout << credential->accessToken.toStdString() << std::endl;
        gtk_widget_set_sensitive (display_youtube->grant_permission_button, FALSE);
        gtk_label_set_text ((GtkLabel*) display_youtube->account_connect_status, "Connected");
        auth->refreshToken(credential->refreshToken);
        QObject::connect(auth, &Authentication::refreshFinished, auth_handler, &AuthenticationHandler::onTokenRefreshFinished);
    }
    else {
        gtk_label_set_text ((GtkLabel*) display_youtube->account_connect_status, "Please connect your account");
    }
}

void grant_permission_button_clicked(){
    gtk_label_set_text ((GtkLabel*) display_youtube->account_connect_status, "Connecting");
    Authentication* auth = new Authentication();
    auth->grantPermission();
    bool ok;
    QString code = QInputDialog::getText(NULL, "Nhập code", "Code:", QLineEdit::Normal, "", &ok);
    if (ok && !code.isEmpty()){
        QObject::connect(auth, &Authentication::authenticateFinished, auth_handler, &AuthenticationHandler::onAuthenticateFinished);
        auth->getToken(code);
    }
}

QString privacyStatusList[3] = {"private", "public", "unlisted"};
QString cdnFormatList[9] = {"1440p_hfr", "1440p", "1080p_hfr", "1080p", "720p_hfr", "720p", "480p", "360p", "240p"};

void create_new_broadcast_button_clicked(){
    QDialog dialog(NULL);
    QFormLayout form(&dialog);
    form.addRow(new QLabel("Title"));
    QLineEdit *titleInput = new QLineEdit(&dialog);
    form.addRow(titleInput);
    form.addRow(new QLabel("Start time"));
    QDateTimeEdit *startDateInput = new QDateTimeEdit(&dialog);
    startDateInput->setDateTime(QDateTime::currentDateTime());
    form.addRow(startDateInput);
    form.addRow(new QLabel("Privacy status"));
    QComboBox *privacyStatusinput = new QComboBox(&dialog);
    for (int i = 0; i < 3; i++){
        privacyStatusinput->addItem(privacyStatusList[i]);
    }
    form.addRow(privacyStatusinput);
    form.addRow(new QLabel("CDN format"));
    QComboBox *cdnFormatInput = new QComboBox(&dialog);
    for (int i = 0; i < 9; i++){
        cdnFormatInput->addItem(cdnFormatList[i]);
    }
    form.addRow(cdnFormatInput);
    QDialogButtonBox buttonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel,
                               Qt::Horizontal, &dialog);
    form.addRow(&buttonBox);
    QObject::connect(&buttonBox, &QDialogButtonBox::accepted, &dialog, &QDialog::accept);
    QObject::connect(&buttonBox, &QDialogButtonBox::rejected, &dialog, &QDialog::reject);
    if (dialog.exec() == QDialog::Accepted) {
        BroadcastBuilder *builder = new BroadcastBuilder(display_youtube->credential);
        QObject::connect(builder, &BroadcastBuilder::created, youtube_handler, &YoutubeHandler::onBroadcastCreated);
        builder->createLiveStream(titleInput->text(), startDateInput->dateTime(), privacyStatusinput->currentText(), cdnFormatInput->currentText());
    }
}

void get_all_upcoming_broadcast(){
    YoutubeAPI* youtubeAPI = new YoutubeAPI(display_youtube->credential);
    QObject::connect(youtubeAPI, &YoutubeAPI::allBroadcastReturn, youtube_handler, &YoutubeHandler::onBroadcastReturn);
    youtubeAPI->getAllBroadcast();
}
void deleteTable (GtkWidget* input)
{
    GtkButton * deleteButton = (GtkButton*) input;
    ChannelData * dataToDelete = NULL;
    for(int i =0 ;i <listChannel.length();i++){
       if((GtkButton*)listChannel.at(i)->deleteButton == (GtkButton*)deleteButton){
           dataToDelete = listChannel.at(i);
           listChannel.removeAt(i);
       }
    }
    if(dataToDelete==NULL){
        return;
    }
    char* broadcast_link = (char*) gtk_entry_get_text(GTK_ENTRY(dataToDelete->broadcast_link));
    QString link = QString(broadcast_link);
    link = link.replace("https://www.youtube.com/watch?v=","");
    YoutubeAPI* youtubeAPI = new YoutubeAPI(display_youtube->credential);
    QObject::connect(youtubeAPI, &YoutubeAPI::broadcastDeleted, youtube_handler, &YoutubeHandler::onBroadcastDeleted);
    youtubeAPI->deleteBroadcast(link);
    gtk_widget_hide(dataToDelete->broadcast_link);
    gtk_widget_hide(dataToDelete->link);
    gtk_widget_hide(dataToDelete->status);
    gtk_widget_hide(dataToDelete->deleteButton);
    gtk_widget_hide(dataToDelete->goLiveButton);
    gtk_widget_hide(dataToDelete->name);


}

void goLive (GtkWidget* input)
{
    GtkButton * goLiveButton = (GtkButton*) input;
    ChannelData * data = NULL;
    for(int i =0 ;i <listChannel.length();i++){
       if((GtkButton*)listChannel.at(i)->goLiveButton == (GtkButton*)goLiveButton){
           data = listChannel.at(i);
       }
    }
    if(data==NULL){
        return;
    }
    YoutubeAPI* youtubeAPI = new YoutubeAPI(display_youtube->credential);
    youtubeAPI->changeBroadcastStatus(data->broadcastId, "live");


}
/*
 * Function:  create_youtube_tab
 * --------------------
 * Create Uploading tab: Create UI and display youtube data.
 *
 *  window: Main window of program.
 * --------------------
 *  Return: Pointer to GTK_VBOX (Contain all yotube's element).
 */
GtkWidget* insertTable (GtkWidget*tableChannel , char *broadcast_link , char * name,char * rtmpLink, char * status, char* broadcastId, char* streamId)
{
//NHO THEM gtk_widget_show()
    ChannelData *channelData = new ChannelData;
    GtkWidget * entry;
    if(tableChannel==NULL)
        return NULL;
    guint *row,*col ;
    row = new guint; col = new guint ;
    gtk_table_get_size ((GtkTable *)tableChannel,row,col);
    gtk_table_resize((GtkTable *)tableChannel,*row+1,*col);
    channelData->row=*row;
    //Date entry
    channelData->broadcast_link=gtk_entry_new();
    entry =  channelData->broadcast_link;
    gtk_entry_set_text((GtkEntry*)entry, broadcast_link);
    gtk_table_attach(GTK_TABLE(tableChannel), entry,  2, 3,*row, *row+1,
       GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    gtk_widget_show(channelData->broadcast_link);
    //Name entry
    channelData->name=gtk_entry_new();
    entry =  channelData->name;
    gtk_entry_set_text((GtkEntry*)entry, name);
    gtk_table_attach(GTK_TABLE(tableChannel), entry,  0, 2,*row, *row+1,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    gtk_widget_show(channelData->name);
    //Status entry
    channelData->status=gtk_entry_new();
    entry =  channelData->status;
    gtk_entry_set_text((GtkEntry*)entry, status);
    gtk_table_attach(GTK_TABLE(tableChannel), entry,  3, 4,*row, *row+1,
       GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    gtk_widget_show(channelData->status);
    //Link entry
    channelData->link=gtk_entry_new();
    entry =  channelData->link;
    gtk_entry_set_text((GtkEntry*)entry, rtmpLink);
    gtk_widget_show(channelData->link);
    gtk_table_attach(GTK_TABLE(tableChannel), entry, 4, 6,*row, *row+1,
    GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    channelData->deleteButton = gtk_button_new_with_label("Delete");
    GtkWidget *delete_button = channelData->deleteButton;
    gtk_table_attach(GTK_TABLE(tableChannel), delete_button, 6, 7, *row, *row +1,
    GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    gtk_widget_show(delete_button);
    g_signal_connect(G_OBJECT(delete_button), "clicked", G_CALLBACK(deleteTable), delete_button);
    channelData->broadcastId = QString(broadcastId);
    channelData->streamId = QString(streamId);
    channelData->goLiveButton = gtk_button_new_with_label("Go Live");
    GtkWidget *goLiveButton = channelData->goLiveButton;
    gtk_table_attach(GTK_TABLE(tableChannel), goLiveButton, 7, 8, *row, *row +1,
    GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    gtk_widget_show(goLiveButton);
    g_signal_connect(G_OBJECT(goLiveButton), "clicked", G_CALLBACK(goLive), goLiveButton);
    listChannel.append(channelData);
    return tableChannel;

}
GtkWidget* updateTable (GtkWidget*tableChannel , char * broadcast_link , char * name,char * rtmpLink,char * status , int row)
{
   ChannelData* channelData = NULL;
   GtkWidget * entry;
    for(int i =0 ; i<listChannel.size();i++)
        if(listChannel.at(i)->row==row)
            channelData=listChannel.at(i);
    if(tableChannel==NULL||channelData==NULL)
        return NULL;
    //Date entry
    if(broadcast_link!=NULL)
    {
        entry = channelData->broadcast_link;
        gtk_entry_set_text((GtkEntry*)entry, broadcast_link);
        gtk_table_attach(GTK_TABLE(tableChannel), entry,  2, 3,row, row+1,
           GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
        }
    //Name entry
    if(name!=NULL)
    {
        entry = channelData->name;
        gtk_entry_set_text((GtkEntry*)entry, name);
        gtk_table_attach(GTK_TABLE(tableChannel), entry,  0, 2,row, row+1,
          GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    }
    if(status!=NULL)
    {
        //Status entry
        entry = channelData->status;

        gtk_entry_set_text((GtkEntry*)entry, status);
        gtk_table_attach(GTK_TABLE(tableChannel), entry,  3, 4,row, row+1,
           GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
      }
    //Link entry
    if(rtmpLink!=NULL)
    {
        entry = channelData->link;
        gtk_entry_set_text((GtkEntry*)entry, rtmpLink);
        gtk_table_attach(GTK_TABLE(tableChannel), entry, 4, 6,row, row+1,
          GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
      }
    return tableChannel;

}

GtkWidget* create_youtube_tab(GtkWidget* window){
    GtkWidget *frame, *table, *label , *channelFrame ,deleteButton ;
    GtkWidget *hbox, *vbox, *hseparator;

    //create new hbox1
    vbox = gtk_vbox_new (FALSE, 5);

    //separator
    hseparator = gtk_hseparator_new();
    gtk_box_pack_start (GTK_BOX (vbox), hseparator, FALSE, FALSE, 5);

    //create new hbox
    hbox = gtk_vbox_new (FALSE, 5);
    gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 5);


    //-------------------------config server
    //home frame
    frame = gtk_frame_new("Config youtube account");
    gtk_box_pack_start (GTK_BOX (hbox), frame, FALSE, FALSE, 5);

    //home table
    table = gtk_table_new(2, 4, FALSE);
    gtk_container_add(GTK_CONTAINER(frame), table);
    //-------------------------display youtube channel
    //channel frame
    channelFrame = gtk_frame_new("Created Channel");
    gtk_frame_set_shadow_type ((GtkFrame *)frame,
                               (GtkShadowType)GTK_SHADOW_IN);
    gtk_box_pack_start (GTK_BOX (hbox), channelFrame, FALSE, FALSE, 5);

    //channel table
    display_youtube->tableChannel = gtk_table_new(1, 7, FALSE);
    gtk_container_add(GTK_CONTAINER(channelFrame), display_youtube->tableChannel);

   //label for channel table

    GdkColor color;
    gdk_color_parse ("#FF0000", &color);


    label = gtk_label_new("Name");
    gtk_widget_modify_fg (label, GTK_STATE_NORMAL, &color);
    gtk_label_set_line_wrap((GtkLabel*)label,TRUE);
    gtk_label_set_line_wrap_mode((GtkLabel*)label,(PangoWrapMode)PANGO_WRAP_WORD);
    gtk_table_attach(GTK_TABLE(display_youtube->tableChannel), label, 0, 1, 0, 1,
      GTK_EXPAND , GTK_EXPAND , 5, 5);



    label = gtk_label_new("Broadcast address");
    gtk_table_attach(GTK_TABLE(display_youtube->tableChannel), label, 1, 3, 0, 1,
      GTK_EXPAND, GTK_EXPAND, 5, 5);


    label = gtk_label_new("Status");
    gtk_table_attach(GTK_TABLE(display_youtube->tableChannel), label, 3, 4, 0, 1,
      GTK_FILL ,GTK_EXPAND, 5, 5);


    label = gtk_label_new("Stream address");
    gtk_table_attach(GTK_TABLE(display_youtube->tableChannel), label, 4, 6, 0, 1,
      GTK_EXPAND, GTK_EXPAND, 5, 5);
    //insertTable(display_youtube->tableChannel,"10/02/2017","test stream djoasdjioasjdioasjdioaOPPEDsjdiojasiodjasi" ,"http//:kenh14.vnjdasdasdasdasdasd","Activated");
    //insertTable(display_youtube->tableChannel,"10/02/2017","test stream djoasdjioasjdioasjdioaOPPEDsjdiojasiodjasi" ,"http//:kenh14.vnjdasdasdasdasdasd","Activated");
    //updateTable(display_youtube->tableChannel,"ABV",NULL ,NULL,"STOPPED",1);
    //save button
    display_youtube->grant_permission_button = gtk_button_new_with_label("Connect to server");
    gtk_table_attach(GTK_TABLE(table), display_youtube->grant_permission_button, 0, 1, 0, 1,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    gtk_widget_set_size_request(GTK_WIDGET(display_youtube->grant_permission_button), 140, 30);
    g_signal_connect(G_OBJECT(display_youtube->grant_permission_button), "clicked", G_CALLBACK(grant_permission_button_clicked), NULL);

    display_youtube->account_connect_status = gtk_label_new("Connecting");
    gtk_table_attach(GTK_TABLE(table), display_youtube->account_connect_status, 1, 2, 0, 1,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

    GtkWidget *create_button = gtk_button_new_with_label("Create broadcast");
    gtk_table_attach(GTK_TABLE(table), create_button, 2, 3, 0, 1,
      GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
    gtk_widget_set_size_request(GTK_WIDGET(create_button), 140, 30);
    g_signal_connect(G_OBJECT(create_button), "clicked", G_CALLBACK(create_new_broadcast_button_clicked), NULL);

    check_youtube_account();
    return vbox;
}
