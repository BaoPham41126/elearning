/*
 * File: broadcast.c
 *
 * Author: GVLab
 *
 * Created on: Aug 13, 2015
 */
#include <QApplication>
#include "headers/view/main_view.h"

/*
 * Function: init_stream
 * ----------------------------
 *   Init ffmpeg context.
 */
int init_stream() {
	avdevice_register_all();
	av_register_all();
	avformat_network_init();
}

/*
 * Function: main
 * ----------------------------
 * Init data for program and call main function.
 *
 * argc : Number of arguments are passed to program. 0 is default.
 *
 * argv : Arguments are passed to program.
 * ----------------------------
 *   returns: 0, exit program.
 */
int main(int argc, char** argv) {
    QApplication app(argc, argv);
    //init data function
    XInitThreads();
    init_stream();
    //init gtk
    gtk_init(&argc, &argv);
    
    create_UI();

    return 0;
}

// #include <string.h>
// #include <stdio.h>
// #include <fcntl.h>
// #include <linux/videodev2.h>

// int main()
// {
//     int fd = v4l2_open("/dev/video1", O_RDWR);
//     if (fd != -1)
//     {
//         struct v4l2_format format;
//         // struct v4l2_fmtdesc fmtdesc;
//         memset(&format,0,sizeof(format));
//         format.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
//         if (ioctl(fd,VIDIOC_S_FMT,&format) == 0)
//         {    
//             printf(">>>>>>>>>>>>>%d\n", format.fmt.pix.width);
//             printf(">>>>>>>>>>>>>>>>>>>>>>>>>>>>%d\n", format.fmt.pix.height);
//             // fmtdesc.index++;
//         }
//         v4l2_close(fd);
//     }
// }
